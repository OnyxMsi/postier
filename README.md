# Postier

## Introduction

The goal of this project is to provide a cross-platform, standard and tested way to fetch content from various packaging interfaces at the same time.
It was first written in Python, but Rust has proved itself far more efficient.

The supported interfaces (called `sources` in the project) try to follow as closely as possible their official documentation.

## Supported sources

* Debian
* Python

mod cli;
use cli::constants::ReturnValue;
use cli::line::{configure_logging, parse_args};

fn main() -> Result<(), ReturnValue> {
    let args = parse_args()?;
    configure_logging(&args);
    let mut app = cli::app::Application::new(args);
    app.run()
}

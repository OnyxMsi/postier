use super::constants::ReturnValue;
pub(super) mod event;
mod skel;
mod source;
mod tui;
mod utils;
use super::{configuration, line};

const SLEEP_DURATION_MS: u64 = 20;

pub struct Application {
    arguments: super::line::Arguments,
}

/* What to run */
type WorkOnSourceFct = fn(&source::Source) -> Result<(), ReturnValue>;

/// Helper
fn source_name_filter<'a>(
    configuration: &'a configuration::Configuration,
    filters: &std::vec::Vec<String>,
) -> std::vec::Vec<&'a str> {
    let ns = configuration.sources.names();
    if filters.len() == 0 {
        ns
    } else {
        ns.into_iter()
            .filter(|n| filters.iter().map(|s| s.as_str()).any(|s| &s == n))
            .collect()
    }
}

impl Application {
    pub fn new(arguments: super::line::Arguments) -> Self {
        Application { arguments }
    }
    fn get_archive_configuration_path(&self) -> std::path::PathBuf {
        self.arguments.repository.join("configuration.yaml")
    }
    /// Load configuration according to arguments
    fn get_configuration(&self) -> Result<configuration::Configuration, ReturnValue> {
        match &self.arguments.command {
            line::Commands::Init(iargs) => {
                let p = std::path::Path::new(&iargs.configuration);
                configuration::load_configuration_from_file(p)
            }
            _ => {
                let p = self.get_archive_configuration_path();
                configuration::load_configuration_from_file(&p)
            }
        }
    }
    fn filter_source_names<'c>(
        &self,
        configuration: &'c configuration::Configuration,
    ) -> std::vec::Vec<&'c str> {
        match &self.arguments.command {
            line::Commands::Fetch(iargs) => source_name_filter(configuration, &iargs.sources),
            line::Commands::Update(iargs) => source_name_filter(configuration, &iargs.sources),
            _ => configuration.sources.names(),
        }
    }
    fn tui_factory(&self) -> tui::Tui {
        if self.arguments.raw {
            log::debug!("Rich interface is disabled");
            tui::Tui::Raw(tui::Raw::new())
        } else {
            tui::Tui::Rich(tui::Rich::new())
        }
    }
    /// Just a wrapper on various run
    fn run_wrapper(
        &self,
        source_name: &str,
        conf: &configuration::Configuration,
        handler: &dyn event::EventHandler,
        fct: WorkOnSourceFct,
    ) -> Result<(), ReturnValue> {
        let parameters = source::Parameters::new(source_name, &conf, &self.arguments, handler);
        fct(&source::Source::new(&parameters)?)
    }
    /// Single thread job
    fn work_on_source_single(
        &mut self,
        mut tui: tui::Tui,
        fct: WorkOnSourceFct,
    ) -> Result<tui::Tui, ReturnValue> {
        let conf = self.get_configuration()?;
        let skel = std::cell::RefCell::new(skel::Skeleton::new());
        let _tui = std::cell::RefCell::new(&mut tui);
        /* Use this function to refresh the TUI at every event */
        let refresh = |e| {
            (*skel.borrow_mut()).refresh();
            (*skel.borrow_mut()).on_process(e);
            let _ = (*_tui.borrow_mut()).draw(&skel.borrow());
        };
        let names: std::vec::Vec<&str> = self.filter_source_names(&conf);
        names
            .iter()
            .map(|source_name| self.run_wrapper(source_name, &conf, &refresh, fct))
            .collect::<Result<(), ReturnValue>>()?;
        Ok(tui)
    }
    /// Multi threads job
    /// Create a thread pool with one process that update the TUI and the others that works on the
    /// sources
    fn work_on_source_multi(
        &mut self,
        mut tui: tui::Tui,
        fct: WorkOnSourceFct,
    ) -> Result<tui::Tui, ReturnValue> {
        log::debug!("Run using {} jobs", self.arguments.jobs);
        /* How to share event between sources processes and tui process */
        let (process_sender, process_receiver) = flume::unbounded::<event::Process>();
        let pool = rayon::ThreadPoolBuilder::new()
            .num_threads(self.arguments.jobs)
            .build()
            .unwrap();
        /* Will be true as long sources processes are running */
        let mut run_completed = std::sync::Arc::new(std::sync::atomic::AtomicBool::new(false));
        let run_completed_u = std::sync::Arc::clone(&mut run_completed);
        let run_completed_e = std::sync::Arc::clone(&run_completed);
        let run = || {
            use rayon::prelude::*;
            let conf = self.get_configuration()?;
            let names: std::vec::Vec<&str> = self.filter_source_names(&conf);
            let r = names
                .into_par_iter()
                .map(|source_name| {
                    let sender = process_sender.clone();
                    self.run_wrapper(source_name, &conf, &sender, fct)
                })
                .collect::<Result<(), ReturnValue>>();
            run_completed_u.swap(true, std::sync::atomic::Ordering::Relaxed);
            r
        };
        let sleep_duration = std::time::Duration::from_millis(SLEEP_DURATION_MS);
        let refresh = || {
            let mut skel = skel::Skeleton::new();
            while !run_completed_e.load(std::sync::atomic::Ordering::Relaxed) {
                skel.refresh();
                for ev in process_receiver.drain() {
                    skel.on_process(ev);
                }
                let _ = tui.draw(&skel);
                std::thread::sleep(sleep_duration);
            }
            /* One last time to flush what remain */
            skel.refresh();
            for ev in process_receiver.drain() {
                skel.on_process(ev);
            }
            let _ = tui.draw(&skel);
        };
        /* Ignore TUI process result */
        let (ru, _) = pool.join(run, refresh);
        ru?;
        Ok(tui)
    }
    fn work_on_source(
        &mut self,
        tui: tui::Tui,
        fct: WorkOnSourceFct,
    ) -> Result<tui::Tui, ReturnValue> {
        if self.arguments.jobs <= 1 {
            self.work_on_source_single(tui, fct)
        } else {
            self.work_on_source_multi(tui, fct)
        }
    }
    /// Init command
    fn init(&mut self, tui: tui::Tui) -> Result<tui::Tui, ReturnValue> {
        let conf = self.get_configuration()?;
        /* Create working directory */
        log::debug!("Create directory {}", self.arguments.repository.display());
        std::fs::create_dir(&self.arguments.repository).map_err(|e| {
            log::error!(
                "Can't create directory {}: {}",
                self.arguments.repository.display(),
                e
            );
            ReturnValue::CommandLineError
        })?;
        /* Init every source */
        let tui = self.work_on_source(tui, |s| {
            log::debug!("Init source {}", s.get_name());
            use postier::lib::Source;
            s.init().inspect_err(|_e| {
                log::error!("Init failed for source {}", s.get_name());
            })
        })?;
        /* Copy configuration */
        let conf_copy_path = self.get_archive_configuration_path();
        crate::cli::configuration::save_configuration_in_file(&conf, &conf_copy_path)?;
        log::info!("Initialization completed");
        Ok(tui)
    }
    /// Fetch items
    fn fetch(&mut self, tui: tui::Tui) -> Result<tui::Tui, ReturnValue> {
        self.work_on_source(tui, |s| {
            use postier::lib::Source;
            s.fetch().inspect_err(|_e| {
                log::error!("Fetch failed for source {}", s.get_name());
            })
        })
    }
    /// Update database
    fn update(&mut self, tui: tui::Tui) -> Result<tui::Tui, ReturnValue> {
        self.work_on_source(tui, |s| {
            use postier::lib::Source;
            s.update().inspect_err(|_e| {
                log::error!("Update failed for source {}", s.get_name());
            })
        })
    }
    pub fn run(&mut self) -> Result<(), ReturnValue> {
        let mut tui = self.tui_factory();
        tui.init()?;
        let tui = match self.arguments.command {
            line::Commands::Init(_) => self.init(tui),
            line::Commands::Fetch(_) => self.fetch(tui),
            line::Commands::Update(_) => self.update(tui),
            line::Commands::Purge => self.init(tui),
        }?;
        tui.term();
        Ok(())
    }
}

use crate::cli::constants::ReturnValue;
use clap::{Parser, Subcommand};
use std::vec::Vec;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
#[command(propagate_version = true)]
pub struct Arguments {
    /// Level of verbosity
    #[arg(short, long, action = clap::ArgAction::Count)]
    pub verbose: u8,
    /// How many parrallel jobs can be run
    #[arg(short, long, default_value_t = get_cpu_count())]
    pub jobs: usize,
    /// Disable terminal user interface
    #[arg(short, long)]
    pub raw: bool,
    /// Where to store
    pub repository: std::path::PathBuf,
    #[command(subcommand)]
    pub command: Commands,
}

#[derive(Parser, Debug)]
pub struct InitArguments {
    /// Where to find the configuration
    pub configuration: String,
}

#[derive(Parser, Debug)]
pub struct FetchArguments {
    /// Filter sources
    pub sources: Vec<String>,
}

pub type UpdateArguments = FetchArguments;

#[derive(Subcommand, Debug)]
pub enum Commands {
    /// Initialize a new archive
    Init(InitArguments),
    /// Update knowledge from remote sources
    Update(FetchArguments),
    /// Fetch requirements
    Fetch(UpdateArguments),
    /// Purge archive content
    Purge,
}

pub fn parse_args() -> Result<Arguments, ReturnValue> {
    Arguments::try_parse().map_err(|e| {
        eprintln!("{}", e);
        ReturnValue::CommandLineError
    })
}

pub fn configure_logging(arguments: &Arguments) {
    let v: usize = arguments.verbose.try_into().unwrap();
    if arguments.raw {
        stderrlog::new().quiet(false).verbosity(v).init().unwrap();
    }
}

pub fn get_cpu_count() -> usize {
    std::thread::available_parallelism().unwrap().get()
}

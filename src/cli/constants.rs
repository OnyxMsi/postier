#[derive(Debug)]
pub enum ReturnValue {
    Success = 0,
    CommandLineError,
    ConfigurationError,
    ApplicationError,
    SourceError,
    TUIError,
}

use crate::cli::constants::ReturnValue;
use serde::{de::Error, Deserialize, Deserializer, Serialize};

fn from_file_or_raw_or_none<'de, D>(deserializer: D) -> Result<Option<String>, D::Error>
where
    D: Deserializer<'de>,
{
    /* Why not a &str ? Because the value might be edited when deserialized in order
     * to process YAML encodings*/
    let r: Option<String> = Deserialize::deserialize(deserializer)?;
    Ok(match r.as_ref() {
        Some(c) => {
            if std::path::Path::new(c).is_file() {
                log::debug!("Read content from {}", c);
                Some(std::fs::read_to_string(c).map_err(|e| {
                    log::error!("Can't read from {}: {}", c, e);
                    Error::custom(e)
                })?)
            } else {
                Some(c.to_string())
            }
        }
        None => None,
    })
}

#[derive(Deserialize, Serialize, PartialEq, Debug)]
pub struct DebianRepositoryParameters {
    pub url: String,
    pub distribution: String,
    pub components: Vec<String>,
    #[serde(default, deserialize_with = "from_file_or_raw_or_none")]
    pub signed_by: Option<String>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct DebianSource {
    pub name: String,
    pub repositories: Vec<DebianRepositoryParameters>,
    pub packages: Vec<String>,
    pub architectures: Vec<String>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct PythonEnvironmentMarkers {
    pub python_version: String,
    pub python_full_version: Option<String>,
    pub os_name: Option<String>,
    pub sys_platform: Option<String>,
    pub platform_release: Option<String>,
    pub platform_system: Option<String>,
    pub platform_version: Option<String>,
    pub platform_machine: Option<String>,
    pub platform_python_implementation: Option<String>,
    pub implementation_name: Option<String>,
    pub implementation_version: Option<String>,
    pub abi_tags: std::vec::Vec<String>,
    pub platform_tags: std::vec::Vec<String>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct PythonSource {
    pub name: String,
    pub indexes: Vec<String>,
    pub requirements: Vec<String>,
    pub environment_markers: std::vec::Vec<PythonEnvironmentMarkers>,
    pub local_interpreter_path: Option<String>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct NpmSource {
    pub name: String,
    pub indexes: Vec<String>,
    pub packages: Vec<String>,
}
#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct ContainerPlatform {
    pub os: String,
    pub architecture: String,
}
#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct ContainerSource {
    pub name: String,
    pub registries: Vec<String>,
    pub images: Vec<String>,
    pub platforms: Vec<ContainerPlatform>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct Sources {
    pub debian: Option<Vec<DebianSource>>,
    pub python: Option<Vec<PythonSource>>,
    pub npm: Option<Vec<NpmSource>>,
    pub container: Option<Vec<ContainerSource>>,
}
impl Sources {
    /* I suppose there is a way to do it without allocating a vec ... but i can't make it work */
    pub fn names(&self) -> std::vec::Vec<&str> {
        let mut res = std::vec::Vec::new();
        macro_rules! extend {
            ($sources:expr) => {
                if let Some(ref conf) = $sources {
                    for c in conf {
                        res.push(c.name.as_str());
                    }
                }
            };
        }
        extend!(self.debian);
        extend!(self.python);
        extend!(self.container);
        res
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct Configuration {
    pub version: u32,
    pub sources: Sources,
}

pub fn load_configuration_from_file(path: &std::path::Path) -> Result<Configuration, ReturnValue> {
    log::debug!("Load configuration from {}", path.display());
    let data = std::fs::read_to_string(path).map_err(|e| {
        log::error!("Error while reading {}: {}", path.display(), e);
        ReturnValue::ConfigurationError
    })?;
    log::debug!("Parse configuration");
    let conf: Configuration = serde_yml::from_str(&data).map_err(|e| {
        log::error!("Invalid configuration {}: {}", path.display(), e);
        ReturnValue::ConfigurationError
    })?;
    Ok(conf)
}
pub fn save_configuration_in_file(
    conf: &Configuration,
    path: &std::path::Path,
) -> Result<(), ReturnValue> {
    log::debug!("Save configuration in {}", path.display());
    let out_s = serde_yml::to_string(conf).map_err(|e| {
        log::error!("Can't write configuration as YAML: {}", e);
        ReturnValue::ConfigurationError
    })?;
    let mut dest = std::fs::File::create(path).map_err(|e| {
        log::error!("Can't open {}: {}", path.display(), e);
        ReturnValue::ConfigurationError
    })?;
    use std::io::prelude::*;
    dest.write_all(out_s.as_bytes()).map_err(|e| {
        log::error!("Can't write configuration in {}: {}", path.display(), e);
        ReturnValue::ConfigurationError
    })?;
    Ok(())
}
#[cfg(test)]
mod test {
    #[test]
    fn serialize() {
        let path = std::path::Path::new(file!())
            .parent()
            .take()
            .unwrap()
            .join("..")
            .join("..")
            .join("examples")
            .join("configuration.yaml");
        let conf = super::load_configuration_from_file(&path).unwrap();
        //assert_eq!(conf.sources.debian.len(), 3);
        assert_eq!(conf.sources.python.unwrap().len(), 1);
    }
    use serde::{Deserialize, Serialize};
    #[derive(Serialize, Deserialize)]
    struct Stupid {
        #[serde(deserialize_with = "super::from_file_or_raw_or_none")]
        pub field: Option<String>,
    }
    #[test]
    fn from_file_or_raw_or_none() {
        let p: Stupid = serde_yml::from_str("field:").unwrap();
        assert_eq!(p.field, None);
        let p: Stupid = serde_yml::from_str("field: 'not a path'").unwrap();
        assert_eq!(p.field.as_deref(), Some("not a path"));
        let t = std::env::temp_dir().join("postier_configuration_from_file_or_raw_or_none");
        if t.exists() {
            std::fs::remove_file(&t).unwrap();
        }
        let p: Stupid = serde_yml::from_str(format!("field: {}", &t.display()).as_str()).unwrap();
        assert_eq!(p.field.as_deref(), Some(t.as_os_str().to_str().unwrap()));
        let c = "Rien";
        let mut f = std::fs::File::create(&t).unwrap();
        use std::io::prelude::*;
        f.write_all(c.as_bytes()).unwrap();
        let p: Stupid = serde_yml::from_str(format!("field: {}", &t.display()).as_str()).unwrap();
        assert_eq!(p.field.as_deref(), Some(c));
    }
    #[test]
    fn save_configuration_in_file() {
        let c = r#"version: 12
sources:
  debian: null
  python: null
  npm: null
  container: null
"#;
        let conf = super::Configuration {
            version: 12,
            sources: super::Sources {
                debian: None,
                python: None,
                container: None,
                npm: None,
            },
        };
        let t = std::env::temp_dir().join("postier_configuration_save_configuration_in_file");
        if !t.exists() {
            std::fs::create_dir(&t).unwrap();
        }
        let d = t.join("configuration.yaml");
        super::save_configuration_in_file(&conf, &d).unwrap();
        assert!(d.exists());
        assert_eq!(std::fs::read_to_string(&d).unwrap(), c);
        /* Try to reload it */
        super::load_configuration_from_file(&d).unwrap();
    }
}

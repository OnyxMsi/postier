/* Application current status */
pub enum Event {
    /// Commands
    DebianInitStart,
    PythonInitStart,
    ContainerInitStart,
    DebianUpdateStart,
    PythonUpdateStart,
    ContainerUpdateStart,
    DebianFetchStart,
    PythonFetchStart,
    ContainerFetchStart,
    /// Repository URL, distribution
    DebianUpdateRepositoryStart(String),
    /// Repository URL, distribution and success
    DebianUpdateRepositoryEnd((String, bool)),
    /// Repository URL, distribution, component, package identifier
    DebianUpdateRepositoryPackage((String, String, String)),
    /// Dependency to solve
    DebianFetchSolveStart(String),
    /// Dependency to solve and success
    DebianFetchSolveEnd(bool),
    /// New dependency to solve later
    DebianFetchPushDependency,
    /// Dependency to solve
    PythonFetchSolveStart(String),
    /// Dependency to solve and success
    PythonFetchSolveEnd(bool),
    /// New dependency to solve later
    PythonFetchPushDependency(String),
    RunEnd,
}

pub struct Process {
    pub source_name: String,
    pub event: Event,
}

pub trait EventHandler {
    fn send(&self, process: Process);
}
/// For multithreads runs
impl EventHandler for flume::Sender<Process> {
    fn send(&self, process: Process) {
        self.send(process);
        ()
    }
}
/// For single process runs
impl<F> EventHandler for F
where
    F: Fn(Process),
{
    fn send(&self, process: Process) {
        (self)(process)
    }
}

use super::{event, utils};
use ratatui::{buffer, layout, widgets};

pub struct Skeleton {
    sources: std::collections::BTreeMap<String, Source>,
}

impl widgets::Widget for &Skeleton {
    fn render(self, area: layout::Rect, buf: &mut buffer::Buffer) {
        let constraints = self.sources.iter().map(|_| layout::Constraint::Min(2));
        let layouts = layout::Layout::vertical(constraints).split(area);
        for ((name, source), layout) in std::iter::zip(&self.sources, &layouts[..]) {
            let b = widgets::Block::bordered().title(name.as_str());
            let b_area = b.inner(*layout);
            b.render(*layout, buf);
            source.render(b_area, buf);
        }
    }
}

struct DebianUpdateRepository {
    loader: utils::Loader,
    done: Option<bool>,
    components: std::collections::BTreeMap<String, String>,
}
impl widgets::Widget for &DebianUpdateRepository {
    fn render(self, area: layout::Rect, buf: &mut buffer::Buffer) {
        let l = self
            .components
            .iter()
            .map(|(component, package)| format!("|_ {}: {}", component, package));
        widgets::List::new(l).render(area, buf)
    }
}
impl DebianUpdateRepository {
    fn refresh(&mut self) {
        self.loader.update();
    }
}
struct DebianUpdate {
    pub repositories: std::collections::BTreeMap<String, DebianUpdateRepository>,
    pub packages: usize,
}
impl widgets::Widget for &DebianUpdate {
    fn render(self, area: layout::Rect, buf: &mut buffer::Buffer) {
        let constraints = self.repositories.iter().map(|_| layout::Constraint::Min(2));
        let layouts = layout::Layout::vertical(constraints).split(area);
        for ((name, repository), layout) in std::iter::zip(&self.repositories, &layouts[..]) {
            let title = match repository.done {
                Some(res) => {
                    if res {
                        format!("{} found {} OK", name, self.packages)
                    } else {
                        format!("{} found {} KO", name, self.packages)
                    }
                }
                None => format!(
                    "{} found {} {}",
                    name,
                    self.packages,
                    repository.loader.as_str()
                ),
            };
            let b = widgets::Block::new().title(title);
            let b_area = b.inner(*layout);
            b.render(*layout, buf);
            repository.render(b_area, buf);
        }
    }
}
impl DebianUpdate {
    fn refresh(&mut self) {
        for repo in self.repositories.values_mut() {
            repo.refresh();
        }
    }
}

struct DebianFetch {
    relationships_solved: usize,
    relationships_count: usize,
    current_relationship: Option<String>,
}
impl widgets::Widget for &DebianFetch {
    fn render(self, area: layout::Rect, buf: &mut buffer::Buffer) {
        match &self.current_relationship {
            Some(rel) => {
                format!(
                    "{}/{} {}",
                    self.relationships_solved, self.relationships_count, rel
                )
                .render(area, buf);
            }
            None => {}
        }
    }
}
struct PythonFetch {
    dependency_solved: usize,
    dependency_count: usize,
    current_dependency: Option<String>,
}
impl widgets::Widget for &PythonFetch {
    fn render(self, area: layout::Rect, buf: &mut buffer::Buffer) {
        match &self.current_dependency {
            Some(rel) => {
                format!(
                    "{}/{} {}",
                    self.dependency_solved, self.dependency_count, rel
                )
                .render(area, buf);
            }
            None => {}
        }
    }
}

enum Source {
    DebianUpdate(DebianUpdate),
    DebianFetch(DebianFetch),
    PythonFetch(PythonFetch),
    ContainerFetch,
    None,
}

impl widgets::Widget for &Source {
    fn render(self, area: layout::Rect, buf: &mut buffer::Buffer) {
        match self {
            Source::DebianUpdate(u) => u.render(area, buf),
            Source::DebianFetch(f) => f.render(area, buf),
            Source::PythonFetch(f) => f.render(area, buf),
            _ => {}
        }
    }
}

impl Source {
    fn refresh(&mut self) {
        match self {
            Source::DebianUpdate(d) => d.refresh(),
            _ => {}
        }
    }
}

impl Skeleton {
    pub fn new() -> Self {
        Skeleton {
            sources: std::collections::BTreeMap::new(),
        }
    }
    /// Refresh the current skeleton.
    /// Nothing has to be updated
    pub fn refresh(&mut self) {
        for source in self.sources.values_mut() {
            source.refresh();
        }
    }
    /// An event was caught
    pub fn on_process(&mut self, process: event::Process) {
        match process.event {
            event::Event::DebianUpdateStart => {
                self.sources.insert(
                    process.source_name,
                    Source::DebianUpdate(DebianUpdate {
                        repositories: std::collections::BTreeMap::new(),
                        packages: 0,
                    }),
                );
            }
            event::Event::DebianFetchStart => {
                self.sources.insert(
                    process.source_name,
                    Source::DebianFetch(DebianFetch {
                        relationships_solved: 0,
                        relationships_count: 0,
                        current_relationship: None,
                    }),
                );
            }
            event::Event::PythonFetchStart => {
                self.sources.insert(
                    process.source_name,
                    Source::PythonFetch(PythonFetch {
                        dependency_count: 0,
                        dependency_solved: 0,
                        current_dependency: None,
                    }),
                );
            }
            event::Event::ContainerFetchStart => {
                self.sources
                    .insert(process.source_name, Source::ContainerFetch);
            }
            event::Event::RunEnd => {
                self.sources.remove(&process.source_name);
            }
            event::Event::DebianUpdateRepositoryStart(repository) => {
                if let Some(source) = self.sources.get_mut(&process.source_name) {
                    if let Source::DebianUpdate(d) = source {
                        d.repositories.insert(
                            repository,
                            DebianUpdateRepository {
                                loader: utils::Loader::new(),
                                done: None,
                                components: std::collections::BTreeMap::new(),
                            },
                        );
                    }
                }
            }
            event::Event::DebianUpdateRepositoryEnd((repository, success)) => {
                if let Some(source) = self.sources.get_mut(&process.source_name) {
                    if let Source::DebianUpdate(d) = source {
                        if let Some(repo) = d.repositories.get_mut(&repository) {
                            repo.done = Some(success);
                        }
                    }
                }
            }
            event::Event::DebianUpdateRepositoryPackage((repository, component, package)) => {
                if let Some(source) = self.sources.get_mut(&process.source_name) {
                    if let Source::DebianUpdate(d) = source {
                        if let Some(repo) = d.repositories.get_mut(&repository) {
                            repo.components.insert(component, package);
                        }
                        d.packages += 1;
                    }
                }
            }
            event::Event::DebianFetchSolveStart(relationship) => {
                if let Some(source) = self.sources.get_mut(&process.source_name) {
                    if let Source::DebianFetch(d) = source {
                        d.current_relationship = Some(relationship);
                    }
                }
            }
            event::Event::DebianFetchPushDependency => {
                if let Some(source) = self.sources.get_mut(&process.source_name) {
                    if let Source::DebianFetch(d) = source {
                        d.relationships_count += 1;
                    }
                }
            }
            event::Event::DebianFetchSolveEnd(_) => {
                if let Some(source) = self.sources.get_mut(&process.source_name) {
                    if let Source::DebianFetch(d) = source {
                        d.relationships_solved += 1;
                    }
                }
            }
            event::Event::PythonFetchSolveStart(dependency) => {
                if let Some(source) = self.sources.get_mut(&process.source_name) {
                    if let Source::PythonFetch(p) = source {
                        p.current_dependency = Some(dependency);
                    }
                }
            }
            event::Event::PythonFetchPushDependency(_) => {
                if let Some(source) = self.sources.get_mut(&process.source_name) {
                    if let Source::PythonFetch(p) = source {
                        p.dependency_count += 1;
                    }
                }
            }
            event::Event::PythonFetchSolveEnd(_) => {
                if let Some(source) = self.sources.get_mut(&process.source_name) {
                    if let Source::PythonFetch(p) = source {
                        p.dependency_solved += 1;
                    }
                }
            }
            _ => {}
        };
    }
}

use super::event;
use crate::cli::{configuration, constants, line};
use postier::lib::{container, debian, python, Control};

/// Generic [postier::lib::Source] that will be built using a [configuration::Configuration] and [line::Arguments]

/// Parameters for [Source] runtime
pub struct Parameters<'p> {
    name: &'p str,
    configuration: &'p configuration::Configuration,
    args: &'p line::Arguments,
    handler: &'p dyn event::EventHandler,
    repository: std::path::PathBuf,
}

impl<'p> Parameters<'p> {
    pub fn new(
        name: &'p str,
        configuration: &'p configuration::Configuration,
        args: &'p line::Arguments,
        handler: &'p dyn event::EventHandler,
    ) -> Self {
        Self {
            name,
            configuration,
            args,
            handler,
            repository: args.repository.join(name),
        }
    }
    fn event(&self, event: event::Event) {
        self.handler.send(event::Process {
            source_name: self.name.to_string(),
            event,
        });
    }
}

fn debian_repository_identifier(
    repository: &postier::lib::debian::repository::Repository,
) -> String {
    format!("{} {}", repository.url, repository.distribution)
}
fn debian_dependency_identifier(
    relationship: &postier::lib::debian::format::relationship::Relationship,
    architecture: &postier::lib::debian::format::architecture::Architecture,
) -> String {
    format!("{} {}", relationship, architecture)
}

impl<'p> debian::Parameters for Parameters<'p> {
    fn get_directory(&self) -> &std::path::Path {
        self.repository.as_path()
    }
    fn solve_dependency_start(
        &self,
        relationship: &debian::format::relationship::Relationship,
        architecture: &debian::format::architecture::Architecture,
        dependencies_stack_size: usize,
    ) -> Control {
        self.event(event::Event::DebianFetchSolveStart(
            debian_dependency_identifier(relationship, architecture),
        ));
        Control::Continue
    }
    fn solve_dependency_failed(
        &self,
        relationship: &debian::format::relationship::Relationship,
        architecture: &debian::format::architecture::Architecture,
        dependencies_stack_size: usize,
    ) -> Control {
        self.event(event::Event::DebianFetchSolveEnd(false));
        Control::Continue
    }
    fn push_dependency(&self, relationship: &str, architecture: &str) -> postier::lib::Control {
        self.event(event::Event::DebianFetchPushDependency);
        Control::Continue
    }
    fn dependency_result_found(
        &self,
        relationship: &debian::format::relationship::Relationship,
        architecture: &debian::format::architecture::Architecture,
        url: &str,
        destination: &std::path::Path,
        dependencies_stack_size: usize,
    ) -> postier::lib::Control {
        self.event(event::Event::DebianFetchSolveEnd(true));
        Control::Continue
    }
    fn update_repository_start(
        &self,
        repository: &debian::repository::Repository,
        architectures: &[&str],
    ) -> postier::lib::Control {
        self.event(event::Event::DebianUpdateRepositoryStart(
            debian_repository_identifier(repository),
        ));
        Control::Continue
    }
    fn update_repository_failed(
        &self,
        repository: &debian::repository::Repository,
        error: debian::Error,
    ) -> postier::lib::Control {
        self.event(event::Event::DebianUpdateRepositoryEnd((
            debian_repository_identifier(repository),
            false,
        )));
        Control::Continue
    }
    fn update_repository_done(
        &self,
        repository: &debian::repository::Repository,
        architectures: &[&str],
    ) -> postier::lib::Control {
        self.event(event::Event::DebianUpdateRepositoryEnd((
            debian_repository_identifier(repository),
            true,
        )));
        Control::Continue
    }
    fn update_repository_package_found(
        &self,
        package: &str,
        version: &str,
        architecture: &str,
        repository: &debian::repository::Repository,
        component: &str,
    ) -> postier::lib::Control {
        self.event(event::Event::DebianUpdateRepositoryPackage((
            debian_repository_identifier(repository),
            component.to_string(),
            format!("{} {}", package, version),
        )));
        Control::Continue
    }
}
impl<'p> python::Parameters for Parameters<'p> {
    fn get_directory(&self) -> &std::path::Path {
        self.repository.as_path()
    }
    fn push_dependency(
        &self,
        dependency: &str,
        environment_marker: &python::EnvironmentMarker,
    ) -> postier::lib::Control {
        self.event(event::Event::PythonFetchPushDependency(
            dependency.to_string(),
        ));
        Control::Continue
    }
    fn solve_dependency_start(
        &self,
        dependency: &python::syntax::dependency::Dependency,
        environment_marker: &python::EnvironmentMarker,
    ) -> postier::lib::Control {
        self.event(event::Event::PythonFetchSolveStart(dependency.to_string()));
        Control::Continue
    }
    fn dependency_solved(
        &self,
        dependency: &python::syntax::dependency::Dependency,
        environment_marker: &python::EnvironmentMarker,
    ) -> postier::lib::Control {
        self.event(event::Event::PythonFetchSolveEnd(true));
        Control::Continue
    }
    fn solve_dependency_failed(
        &self,
        dependency: &python::syntax::dependency::Dependency,
        environment_marker: &python::EnvironmentMarker,
        error: &python::Error,
    ) -> postier::lib::Control {
        self.event(event::Event::PythonFetchSolveEnd(false));
        Control::Continue
    }
}
impl<'p> container::Parameters for Parameters<'p> {
    fn get_directory(&self) -> &std::path::Path {
        self.repository.as_path()
    }
}

pub enum SourceType<'s> {
    Debian(debian::Source<'s>),
    Python(python::Source<'s>),
    Container(container::Source<'s>),
}
pub struct Source<'s> {
    parameters: &'s Parameters<'s>,
    _type: SourceType<'s>,
}

/// Factory for [debian::Source] source
pub fn debian_source_factory<'s>(
    configuration: &configuration::DebianSource,
    parameters: &'s Parameters<'s>,
) -> Result<SourceType<'s>, debian::Error> {
    let mut source = debian::Source::new(parameters)?;
    for repo_conf in &configuration.repositories {
        let url = repo_conf.url.as_str();
        let distribution = repo_conf.distribution.as_str();
        let components: std::vec::Vec<&str> =
            repo_conf.components.iter().map(|c| c.as_str()).collect();
        let signed_by = repo_conf.signed_by.as_deref();
        source.add_debian_repository(url, distribution, &components, signed_by)?;
    }
    for arch in &configuration.architectures {
        source.add_architecture(arch.as_str());
    }
    for package in &configuration.packages {
        source.add_debian_package(package.as_str())?;
    }
    Ok(SourceType::Debian(source))
}
/// Factory for [python::Source] source
pub fn python_source_factory<'s>(
    configuration: &configuration::PythonSource,
    parameters: &'s Parameters<'s>,
) -> Result<SourceType<'s>, python::Error> {
    let mut source = python::Source::new(parameters)?;
    for index in &configuration.indexes {
        source.add_index(index)?;
    }
    for req in &configuration.requirements {
        source.add_requirement(req);
    }
    macro_rules! v {
        ($s:expr) => {
            python::syntax::version::Version::parse(&$s)?
        };
    }
    macro_rules! v_or {
        ($s:expr, $o:expr) => {
            v!($s.as_ref().unwrap_or(&$o))
        };
    }
    macro_rules! s_or {
        ($s:expr) => {
            match $s.as_ref() {
                Some(s) => s.clone(),
                None => "".to_string(),
            }
        };
    }
    for env in &configuration.environment_markers {
        let abi_tags: std::vec::Vec<&str> = env.abi_tags.iter().map(|s| s.as_str()).collect();
        let platform_tags: std::vec::Vec<&str> =
            env.platform_tags.iter().map(|s| s.as_str()).collect();
        source.add_environment(python::EnvironmentMarker {
            python_version: v!(env.python_version),
            python_full_version: v_or!(env.python_full_version, env.python_version),
            os_name: s_or!(env.os_name),
            sys_platform: s_or!(env.sys_platform),
            platform_release: s_or!(env.platform_release),
            platform_system: s_or!(env.platform_system),
            platform_version: s_or!(env.platform_version),
            platform_machine: s_or!(env.platform_machine),
            platform_python_implementation: s_or!(env.platform_python_implementation),
            implementation_name: s_or!(env.implementation_name),
            implementation_version: v_or!(env.implementation_version, env.python_version),
            abi_tags: abi_tags.iter().map(|s| s.to_string()).collect(),
            platform_tags: platform_tags.iter().map(|s| s.to_string()).collect(),
            extra: None,
        })
    }
    Ok(SourceType::Python(source))
}
/// Factory for [container::Source] source
pub fn container_source_factory<'s>(
    configuration: &configuration::ContainerSource,
    parameters: &'s Parameters<'s>,
) -> Result<SourceType<'s>, container::Error> {
    let mut source = container::Source::new(parameters)?;
    for registry in &configuration.registries {
        let u = url::Url::parse(registry.as_str()).map_err(|e| {
            log::error!("Can't parse registry url {}: {}", registry.as_str(), e);
            container::Error::LibError(postier::lib::Error::InvalidParametersError)
        })?;
        source.add_registry(u);
    }
    for platform in &configuration.platforms {
        source.add_platform(container::Platform {
            os: platform.os.clone(),
            architecture: platform.architecture.clone(),
        })
    }
    for image in &configuration.images {
        let (manifest, tag) = match image.rfind(":") {
            None => {
                log::error!(
                    "Invalid container image {}: must be <manifest>:<tag>",
                    image
                );
                return Err(container::Error::LibError(
                    postier::lib::Error::InvalidParametersError,
                ));
            }
            Some(c_idx) => {
                if c_idx == image.len() - 1 {
                    log::error!(
                        "Invalid container image {}: must be <manifest>:<tag>",
                        image
                    );
                    return Err(container::Error::LibError(
                        postier::lib::Error::InvalidParametersError,
                    ));
                }
                (&image[..c_idx], &image[(c_idx + 1)..])
            }
        };
        source.add_image(manifest.to_string(), tag.to_string())
    }
    Ok(SourceType::Container(source))
}

impl<'s> Source<'s> {
    pub fn new(parameters: &'s Parameters<'s>) -> Result<Self, constants::ReturnValue> {
        macro_rules! factory {
            ($source_type:path, $sources:expr, $source_factory:expr) => {
                if let Some(ref sources) = $sources.as_ref() {
                    let sources2: &std::vec::Vec<$source_type> = sources;
                    if let Some(ref conf) =
                        sources2.iter().filter(|s| s.name == parameters.name).next()
                    {
                        return Ok(Source {
                            parameters,
                            _type: $source_factory(conf, &parameters)
                                .map_err(|_| constants::ReturnValue::SourceError)?,
                        });
                    }
                }
            };
        }
        factory!(
            configuration::DebianSource,
            parameters.configuration.sources.debian,
            debian_source_factory
        );
        factory!(
            configuration::PythonSource,
            parameters.configuration.sources.python,
            python_source_factory
        );
        factory!(
            configuration::ContainerSource,
            parameters.configuration.sources.container,
            container_source_factory
        );
        log::error!("{}: no such source", parameters.name);
        Err(constants::ReturnValue::CommandLineError)
    }
    pub fn send(&self, event: event::Event) {
        self.parameters.handler.send(event::Process {
            source_name: self.parameters.name.to_string(),
            event,
        });
    }
    pub fn get_name(&self) -> &str {
        self.parameters.name
    }
}
impl postier::lib::Source<constants::ReturnValue> for Source<'_> {
    fn init(&self) -> Result<(), constants::ReturnValue> {
        self.send(match &self._type {
            SourceType::Debian(_) => event::Event::DebianInitStart,
            SourceType::Python(_) => event::Event::PythonInitStart,
            SourceType::Container(_) => event::Event::ContainerInitStart,
        });
        let r = match &self._type {
            SourceType::Debian(s) => s.init().map_err(|_| constants::ReturnValue::SourceError),
            SourceType::Python(s) => s.init().map_err(|_| constants::ReturnValue::SourceError),
            SourceType::Container(s) => s.init().map_err(|_| constants::ReturnValue::SourceError),
        };
        self.send(event::Event::RunEnd);
        r
    }
    fn update(&self) -> Result<(), constants::ReturnValue> {
        self.send(match &self._type {
            SourceType::Debian(_) => event::Event::DebianUpdateStart,
            SourceType::Python(_) => event::Event::PythonUpdateStart,
            SourceType::Container(_) => event::Event::ContainerUpdateStart,
        });
        let r = match &self._type {
            SourceType::Debian(s) => s.update().map_err(|_| constants::ReturnValue::SourceError),
            SourceType::Python(s) => s.update().map_err(|_| constants::ReturnValue::SourceError),
            SourceType::Container(s) => s.update().map_err(|_| constants::ReturnValue::SourceError),
        };
        self.send(event::Event::RunEnd);
        r
    }
    /// Fetch stuff from source
    fn fetch(&self) -> Result<(), constants::ReturnValue> {
        self.send(match &self._type {
            SourceType::Debian(_) => event::Event::DebianFetchStart,
            SourceType::Python(_) => event::Event::PythonFetchStart,
            SourceType::Container(_) => event::Event::ContainerFetchStart,
        });
        let r = match &self._type {
            SourceType::Debian(s) => s.fetch().map_err(|_| constants::ReturnValue::SourceError),
            SourceType::Python(s) => s.fetch().map_err(|_| constants::ReturnValue::SourceError),
            SourceType::Container(s) => s.fetch().map_err(|_| constants::ReturnValue::SourceError),
        };
        self.send(event::Event::RunEnd);
        r
    }
}

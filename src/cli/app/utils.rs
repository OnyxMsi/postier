/// A stupid way to do a loader
pub struct Loader {
    state: u8,
}
impl Loader {
    pub fn new() -> Self {
        Self { state: 0 }
    }
    pub fn update(&mut self) {
        self.state = (self.state + 1) % 4;
    }
    pub fn as_str(&self) -> &'static str {
        match self.state {
            0 => "/",
            1 => "-",
            2 => "\\",
            3 => "|",
            _ => unreachable!(),
        }
    }
}

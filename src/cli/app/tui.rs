use super::skel;
use crate::cli::constants;

pub struct Raw {}
impl Raw {
    pub fn new() -> Self {
        Raw {}
    }
    fn draw(&mut self, skel: &skel::Skeleton) -> Result<(), constants::ReturnValue> {
        Ok(())
    }
}

pub struct Rich {
    terminal: ratatui::DefaultTerminal,
}

impl Rich {
    pub fn new() -> Self {
        Rich {
            terminal: ratatui::init(),
        }
    }
    fn init(&mut self) -> Result<(), constants::ReturnValue> {
        self.terminal.clear().map_err(|e| {
            log::error!("Fail to initialize terminal: {}", e);
            constants::ReturnValue::TUIError
        })?;
        Ok(())
    }
    fn draw(&mut self, skel: &skel::Skeleton) -> Result<(), constants::ReturnValue> {
        self.terminal
            .draw(|frame| {
                frame.render_widget(skel, frame.area());
            })
            .map_err(|e| {
                log::error!("Can't draw interface: {}", e);
                constants::ReturnValue::TUIError
            })?;
        Ok(())
    }
    fn term(&self) {
        ratatui::restore();
        ()
    }
}

pub enum Tui {
    Rich(Rich),
    Raw(Raw),
}
impl Tui {
    pub fn init(&mut self) -> Result<(), constants::ReturnValue> {
        match self {
            Tui::Rich(r) => r.init(),
            Tui::Raw(_) => Ok(()),
        }
    }
    pub fn draw(&mut self, skel: &skel::Skeleton) -> Result<(), constants::ReturnValue> {
        match self {
            Tui::Rich(r) => r.draw(skel),
            Tui::Raw(_) => Ok(()),
        }
    }
    pub fn term(&self) {
        match self {
            Tui::Rich(r) => r.term(),
            Tui::Raw(_) => {}
        }
    }
}

use crate::lib::debian::format::{architecture, relationship};
use crate::lib::{database, debian};

/* This defines an interface to fetch debian package from a database
 */

#[derive(Hash)]
struct FetchItem {
    relationship: String,
    architecture: String,
    flags: u8,
}

pub struct Fetcher {
    repository: std::path::PathBuf,
    stack: std::vec::Vec<FetchItem>,
    cache: std::collections::HashSet<u64>,
    fetch_cache: crate::lib::cache::FetchCache,
}

struct FetchEntry<'f> {
    package: &'f debian::storage::package::Entry,
    repository: &'f std::path::Path,
}

impl crate::lib::cache::FetchItem for FetchEntry<'_> {
    fn request_factory(
        &self,
        client: &reqwest::blocking::Client,
    ) -> reqwest::blocking::RequestBuilder {
        client.get(self.package.url.as_str())
    }
    fn get_sha256(&self) -> std::vec::Vec<u8> {
        self.package.sha256.as_bytes().into()
    }
    fn path_factory(&self) -> std::path::PathBuf {
        let u = std::path::Path::new(self.package.url.as_str());
        self.repository.join(u.file_name().unwrap())
    }
}

impl Fetcher {
    pub fn new(repository: std::path::PathBuf) -> Self {
        Fetcher {
            repository,
            stack: std::vec::Vec::new(),
            cache: std::collections::HashSet::new(),
            fetch_cache: crate::lib::cache::FetchCache::new(),
        }
    }
    fn fetch_package(
        &mut self,
        parameters: &dyn debian::Parameters,
        package: &debian::storage::package::Entry,
        architecture: &architecture::Architecture,
        relationship: &relationship::Relationship,
    ) -> Result<(), crate::lib::Error> {
        let fetch_entry = FetchEntry {
            package,
            repository: self.repository.as_path(),
        };
        if self.fetch_cache.has(&fetch_entry) {
            let p = self.fetch_cache.get(&fetch_entry)?;
            log::debug!("{} already exists in pool", package.url);
            parameters.dependency_result_already_found(
                relationship,
                architecture,
                &package.url,
                p.as_path(),
                self.stack.len(),
            );
        } else {
            let p = self.fetch_cache.get(&fetch_entry)?;
            log::debug!("{} saved in {}", package.url, p.display());
            parameters.dependency_result_found(
                relationship,
                architecture,
                &package.url,
                p.as_path(),
                self.stack.len(),
            );
            /* Add new relationships */
            if package.depends.is_some() {
                self.add_item(
                    parameters,
                    &package.depends.as_ref().unwrap(),
                    &package.architecture,
                );
            }
        }
        Ok(())
    }
    fn solve(
        &mut self,
        db: &mut database::Database,
        parameters: &dyn debian::Parameters,
        top: &FetchItem,
    ) -> Result<(), debian::Error> {
        let architecture = architecture::Architecture::parse(&top.architecture)?;
        for relationship in relationship::Relationship::parse(&top.relationship)? {
            parameters.solve_dependency_start(&relationship, &architecture, self.stack.len());
            log::debug!(
                "Solve \"{}\" for architecture {}",
                relationship,
                architecture
            );
            let mut res = relationship.solve(db, &architecture);
            match res.take() {
                Some(p) => {
                    self.fetch_package(parameters, &p, &architecture, &relationship)
                        .map_err(debian::Error::LibError)?;
                }
                None => {
                    log::info!("Can't solve \"{}\" [{}]", relationship, architecture);
                    parameters.solve_dependency_failed(
                        &relationship,
                        &architecture,
                        self.stack.len(),
                    );
                }
            }
        }
        Ok(())
    }
    pub fn add_item(
        &mut self,
        parameters: &dyn debian::Parameters,
        relationships: &str,
        architecture: &str,
    ) -> () {
        /* relationship is to be splitted to multiple entries so we can cache them
         * Simplest way to be to parse it, but it must be saved as String so it will
         * survive until the end of the run
         * Instead split it using comma separator
         */
        for relationship in relationships.split(",").map(|s| s.trim()) {
            let item = FetchItem {
                relationship: relationship.to_string(),
                architecture: architecture.to_string(),
                flags: 0,
            };
            let thash = crate::lib::utils::calculate_hash(&item);
            if self.cache.contains(&thash) {
                log::debug!("{} ({}) was already solved", relationship, architecture);
            } else {
                log::debug!("{} ({}) needs to be solved", relationship, architecture);
                self.cache.insert(thash);
                self.stack.push(item);
                parameters.push_dependency(relationship, architecture);
            }
        }
    }
    pub fn run(
        &mut self,
        db: &mut database::Database,
        parameters: &dyn debian::Parameters,
    ) -> Result<(), debian::Error> {
        while self.stack.len() > 0 {
            let top = self.stack.pop().unwrap();
            self.solve(db, parameters, &top)?;
        }
        Ok(())
    }
}

use crate::lib;
use crate::lib::{debian, utils};
use std::io::Read;

pub struct Repository {
    pub url: url::Url,
    pub distribution: String,
    pub components: Vec<String>,
    pub signed_by: Option<(pgp::SignedPublicKey, pgp::armor::Headers)>,
}
impl Repository {
    pub fn new(
        url: &str,
        distribution: &str,
        components: &[&str],
        signed_by: Option<&str>,
    ) -> Result<Self, debian::Error> {
        use pgp::composed::Deserializable;
        let signed_by = match signed_by {
            Some(s) => Some(pgp::SignedPublicKey::from_string(s).map_err(|e| {
                log::error!("Can't load PGP key: {}", e);
                debian::Error::InvalidSignatureError
            })?),
            None => None,
        };
        Ok(Repository {
            url: utils::url_with_trailling_slash(url).map_err(debian::Error::LibError)?,
            distribution: distribution.to_string(),
            signed_by,
            components: components.iter().map(|s| s.to_string()).collect(),
        })
    }
}

pub mod iurl;

/* See https://wiki.debian.org/DebianRepository/Format */
#[derive(Debug)]
pub enum Compression {
    None,
    Xz,
    Gzip,
    Bzip2,
    //Lzma
}

impl Compression {
    fn get_extension(&self) -> &str {
        match self {
            Compression::None => "",
            Compression::Xz => ".xz",
            Compression::Gzip => ".gz",
            Compression::Bzip2 => ".bz",
            //Compression::Lzma => ".lzma"
        }
    }
    pub fn deflate(&self, data: &[u8]) -> Result<String, debian::Error> {
        match self {
            Compression::None => Ok(String::from(String::from_utf8_lossy(data))),
            Compression::Gzip => {
                log::debug!("Deflate {} bytes as gzip content", data.len());
                let mut decoder = flate2::read::GzDecoder::new(data);
                let mut s = String::new();
                decoder.read_to_string(&mut s).map_err(|e| {
                    log::error!("Gzip decompression error {}", e);
                    debian::Error::LibError(lib::Error::IOError(e))
                })?;
                Ok(s)
            }
            Compression::Xz => {
                log::debug!("Deflate {} bytes as Xz content", data.len());
                let mut r = std::io::BufReader::new(data);
                let mut v: Vec<u8> = Vec::new();
                lzma_rs::xz_decompress(&mut r, &mut v).map_err(|e| {
                    log::error!("Xz decompression error {}", e);
                    let io = std::io::Error::from(std::io::ErrorKind::InvalidInput);
                    debian::Error::LibError(lib::Error::IOError(io))
                })?;
                log::debug!("Convert {} deflated bytes to unicode string", v.len());
                String::from_utf8(v).map_err(|e| {
                    log::error!(
                        "Can't convert Xz {} bytes to unicode string {}",
                        data.len(),
                        e
                    );
                    let io = std::io::Error::from(std::io::ErrorKind::InvalidInput);
                    debian::Error::LibError(lib::Error::IOError(io))
                })
            }
            Compression::Bzip2 => {
                log::debug!("Deflate {} bytes as Bzip2 content", data.len());
                let mut r = std::io::BufReader::new(data);
                let mut s = String::new();
                bzip2::read::BzDecoder::new(&mut r)
                    .read_to_string(&mut s)
                    .map_err(|e| {
                        log::error!("Bzip compression error {}", e);
                        let io = std::io::Error::from(std::io::ErrorKind::InvalidInput);
                        debian::Error::LibError(lib::Error::IOError(io))
                    })?;
                Ok(s)
            }
        }
    }
}

#[cfg(test)]
mod test {
    use crate::lib::debian::repository::Compression;
    #[test]
    fn compression_deflate() {
        let none_data = "The CROU, it's just another way of thinking";
        let none_res = Compression::None.deflate(none_data.as_bytes()).unwrap();
        assert_eq!(none_res, none_data);
        use base64ct::{Base64, Encoding};
        let gzip_data_b64 = "H4sIAnw76GUA/6HtBUDBCYBQCF3l3bq0RQMEUQN4sLQPCvki2l52UyzbesxwToX7LUIiafrgkx95guYxPK4GQzFTgysAAAA=";
        let gzip_data = Base64::decode_vec(gzip_data_b64).unwrap();
        let gzip_res = Compression::Gzip.deflate(&gzip_data).unwrap();
        assert_eq!(gzip_res, none_res);
        let xz_data_b64 = "/Td6WFoAAATm1rRGAgAhARYAAAB0L+WjAQAqVGhlIENST1UsIGl0J3MganVzdCBhbm90aGVyIHdheSBvZiB0aGlua2luZwAABC0oOS4/z2gAAUMrrVBuVx+2830BAAAAAARZWg==";
        let xz_data = Base64::decode_vec(xz_data_b64).unwrap();
        let xz_res = Compression::Xz.deflate(&xz_data).unwrap();
        assert_eq!(xz_res, none_res);
        let bz_data_b64 = "QlpoOTFBWSZTWb+EK0YAAAYXgECECACWACP5nqAgACIkxqeppmpkPaIUGjRoMgNLNVoUGvWL5Wsek7/XJzQjBClGEBMJHFti7kinChIX8IVowA==";
        let bz_data = Base64::decode_vec(bz_data_b64).unwrap();
        let bz_res = Compression::Bzip2.deflate(&bz_data).unwrap();
        assert_eq!(bz_res, none_res);
    }
    #[test]
    fn repository_new() {
        let a = super::Repository::new("http://some.where", "some", &vec!["a", "b"], None).unwrap();
        assert_eq!(a.url.as_str(), "http://some.where/");
        let a = super::Repository::new(
            "http://some.where",
            "some",
            &vec!["a", "b"],
            Some("NOT A VALID KEY"),
        );
        assert!(a.is_err());
        let valid = r#"-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEY865UxYJKwYBBAHaRw8BAQdAd7Z0srwuhlB6JKFkcf4HU4SSS/xcRfwEQWzr
crf6AEq0SURlYmlhbiBTdGFibGUgUmVsZWFzZSBLZXkgKDEyL2Jvb2t3b3JtKSA8
ZGViaWFuLXJlbGVhc2VAbGlzdHMuZGViaWFuLm9yZz6IlgQTFggAPhYhBE1k/sEZ
wgKQZ9bnkfjSWFuHg9SBBQJjzrlTAhsDBQkPCZwABQsJCAcCBhUKCQgLAgQWAgMB
Ah4BAheAAAoJEPjSWFuHg9SBSgwBAP9qpeO5z1s5m4D4z3TcqDo1wez6DNya27QW
WoG/4oBsAQCEN8Z00DXagPHbwrvsY2t9BCsT+PgnSn9biobwX7bDDg==
=5NZE
-----END PGP PUBLIC KEY BLOCK-----"#;
        super::Repository::new("http://some.where", "some", &vec!["a", "b"], Some(valid)).unwrap();
    }
}

/* This is about URLs required in the rest of this module */
use crate::lib::debian;

macro_rules! ujoin {
    ($u:expr, $s:expr) => {
        crate::lib::utils::ujoin($u, $s).map_err(debian::Error::LibError)
    };
}

pub fn distribution_release(
    base_url: &url::Url,
    distribution: &str,
    compression: &debian::repository::Compression,
) -> Result<url::Url, debian::Error> {
    ujoin!(
        base_url,
        &format!(
            "dists/{}/Release{}",
            distribution,
            compression.get_extension()
        )
    )
}
pub fn distribution_in_release(
    base_url: &url::Url,
    distribution: &str,
    compression: &debian::repository::Compression,
) -> Result<url::Url, debian::Error> {
    ujoin!(
        base_url,
        &format!(
            "dists/{}/InRelease{}",
            distribution,
            compression.get_extension()
        )
    )
}
pub fn distribution_component_architecture_packages(
    base_url: &url::Url,
    distribution: &str,
    component: &str,
    architecture: &str,
    compression: &debian::repository::Compression,
) -> Result<url::Url, debian::Error> {
    ujoin!(
        base_url,
        &format!(
            "dists/{}/{}/binary-{}/Packages{}",
            distribution,
            component,
            architecture,
            compression.get_extension()
        )
    )
}
pub fn distribution_package(
    base_url: &url::Url,
    filename: &str,
) -> Result<url::Url, debian::Error> {
    ujoin!(base_url, filename)
}
#[cfg(test)]
mod test {
    use crate::lib::debian::repository::iurl;
    use crate::lib::debian::repository::Compression;
    use url::Url;
    #[test]
    fn release() {
        let h = Url::parse("http://www.perdu.com/debian/").unwrap();
        assert_eq!(
            iurl::distribution_release(&h, "my", &Compression::None)
                .unwrap()
                .as_str(),
            "http://www.perdu.com/debian/dists/my/Release"
        );
        assert_eq!(
            iurl::distribution_release(&h, "my", &Compression::Xz)
                .unwrap()
                .as_str(),
            "http://www.perdu.com/debian/dists/my/Release.xz"
        );
    }
    #[test]
    fn in_release() {
        let h = Url::parse("http://www.perdu.com/debian/").unwrap();
        assert_eq!(
            iurl::distribution_in_release(&h, "my", &Compression::Gzip)
                .unwrap()
                .as_str(),
            "http://www.perdu.com/debian/dists/my/InRelease.gz"
        );
        assert_eq!(
            iurl::distribution_in_release(&h, "my", &Compression::Xz)
                .unwrap()
                .as_str(),
            "http://www.perdu.com/debian/dists/my/InRelease.xz"
        );
    }
    #[test]
    fn packages() {
        let h = Url::parse("http://www.perdu.com/debian/").unwrap();
        assert_eq!(
            iurl::distribution_component_architecture_packages(
                &h,
                "my",
                "awesome",
                "arch",
                &Compression::Xz
            )
            .unwrap()
            .as_str(),
            "http://www.perdu.com/debian/dists/my/awesome/binary-arch/Packages.xz"
        );
        assert_eq!(
            iurl::distribution_component_architecture_packages(
                &h,
                "my",
                "awesome",
                "arch",
                &Compression::None
            )
            .unwrap()
            .as_str(),
            "http://www.perdu.com/debian/dists/my/awesome/binary-arch/Packages"
        );
    }
    #[test]
    fn package() {
        let h = Url::parse("http://www.perdu.com/debian/").unwrap();
        assert_eq!(
            iurl::distribution_package(&h, "pool/main/l/lua-nvim/lua-nvim_0.2.2-1-1_amd64.deb")
                .unwrap()
                .as_str(),
            "http://www.perdu.com/debian/pool/main/l/lua-nvim/lua-nvim_0.2.2-1-1_amd64.deb"
        );
    }
}

use crate::lib::database;
use crate::lib::debian;
use serde::{Deserialize, Serialize};

const DATABASE_SEGMENT: &str = "package_definition";
struct Interface<'s> {
    pub package: &'s str,
}
/* Clone is used in relationship::Relationship::solve */
#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Entry {
    pub version: String,
    pub url: String,
    pub architecture: String,
    pub depends: Option<String>,
    pub sha256: String,
}
impl Entry {
    pub fn save(
        db: &mut database::Database,
        package: &str,
        version: &str,
        architecture: &str,
        url: &str,
        depends: &Option<&String>,
        sha256: &str,
    ) -> Result<(), debian::Error> {
        let interface = Interface { package };
        let e = Entry {
            version: version.to_string(),
            url: url.to_string(),
            architecture: architecture.to_string(),
            depends: depends.map(|s| s.to_string()),
            sha256: sha256.to_string(),
        };
        /* Retreive previous stuff */
        let mut package_items: std::vec::Vec<Entry> = match db.get(&interface).unwrap_or(None) {
            Some(p) => p,
            None => std::vec::Vec::new(),
        };
        /* Look for this version in database */
        if package_items.iter().any(|p| p.version.eq(&version)) {
            log::debug!(
                "Package {} version {} is already in database",
                package,
                version
            );
        } else {
            log::debug!(
                "Package {} version {} is not in database yet",
                package,
                version
            );
            package_items.push(e);
            db.save(&interface, &package_items, false)
                .map_err(debian::Error::DatabaseError)?;
        }
        Ok(())
    }
    pub fn load(
        db: &mut database::Database,
        package: &str,
    ) -> Result<std::vec::Vec<Self>, debian::Error> {
        let interface = Interface { package };
        Ok(db
            .get(&interface)
            .map_err(debian::Error::DatabaseError)?
            .unwrap_or_else(std::vec::Vec::new))
    }
}
impl database::Interface<std::vec::Vec<Entry>> for Interface<'_> {
    fn get_segment_and_key(&self) -> (&str, &str) {
        /* Note that this will panic on definition with no package name */
        (DATABASE_SEGMENT, self.package)
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn save() {
        fn count(db: &mut crate::lib::database::Database, package: &str) -> usize {
            db.get(&super::Interface { package })
                .unwrap()
                .map_or(0, |v| v.len())
        }
        fn s(db: &mut crate::lib::database::Database, package: &str, version: &str) -> () {
            super::Entry::save(
                db,
                package,
                version,
                "amd64",
                "http://www.perdu.com",
                &None,
                "aabbccdd",
            )
            .unwrap();
        }
        let mut db = crate::lib::database::test::get_db("test_debian_storage_package_save");
        s(&mut db, "p1", "1.0");
        assert_eq!(count(&mut db, "p1"), 1);
        assert_eq!(count(&mut db, "p2"), 0);
        s(&mut db, "p1", "2.0");
        assert_eq!(count(&mut db, "p1"), 2);
        s(&mut db, "p2", "2.0");
        assert_eq!(count(&mut db, "p1"), 2);
        assert_eq!(count(&mut db, "p2"), 1);
        /* Saving the same version twice will do nothing */
        s(&mut db, "p1", "2.0");
        assert_eq!(count(&mut db, "p1"), 2);
    }
    #[test]
    fn load() {
        fn save(db: &mut crate::lib::database::Database, package: &str, version: &str) -> () {
            let interface = super::Interface { package };
            let mut prev: std::vec::Vec<super::Entry> =
                db.get(&interface).unwrap().unwrap_or(std::vec::Vec::new());
            prev.push(super::Entry {
                version: version.to_string(),
                architecture: "any".to_string(),
                depends: None,
                url: "http://www.perdu.com".to_string(),
                sha256: "aabbccddee".to_string(),
            });
            db.save(&interface, &prev, true).unwrap();
        }
        let mut db = crate::lib::database::test::get_db("test_debian_storage_package_load");
        save(&mut db, "p1", "v1.0");
        save(&mut db, "p1", "v2.0");
        save(&mut db, "p2", "v1.0");
        let p = super::Entry::load(&mut db, "p1").unwrap();
        assert_eq!(p.len(), 2);
        assert_eq!(p[0].version, "v1.0");
        assert_eq!(p[1].version, "v2.0");
        let p = super::Entry::load(&mut db, "p2").unwrap();
        assert_eq!(p.len(), 1);
        let p = super::Entry::load(&mut db, "p3").unwrap();
        assert_eq!(p.len(), 0);
    }
}

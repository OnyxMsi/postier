use crate::lib::{database, debian};
use serde::{Deserialize, Serialize};

const DATABASE_SEGMENT: &str = "index_definition";
struct Interface<'s> {
    pub filename: &'s str,
}
/* For indexes the only thing to store is the checksum in order to make sure
 * we don't download something that was already loaded
*/
#[derive(Debug, Deserialize, Serialize)]
pub struct Entry {
    pub checksum: String,
}
impl Entry {
    pub fn save(
        db: &mut database::Database,
        filename: &str,
        checksum: &str,
    ) -> Result<(), debian::Error> {
        let interface = Interface { filename };
        let e = Entry {
            checksum: checksum.to_string(),
        };
        /* Retreive previous stuff */
        let mut index_items: std::vec::Vec<Entry> = match db.get(&interface).unwrap_or(None) {
            Some(p) => p,
            None => std::vec::Vec::new(),
        };
        /* Look for this version in database */
        if index_items.iter().any(|p| p.checksum.eq(&checksum)) {
            log::debug!(
                "Index {} checksum {} is already in database",
                filename,
                checksum
            );
        } else {
            log::debug!(
                "Index {} version {} is not in database yet",
                filename,
                checksum
            );
            index_items.push(e);
            db.save(&interface, &index_items, false)
                .map_err(debian::Error::DatabaseError)?;
        }
        Ok(())
    }
    pub fn load(
        db: &mut database::Database,
        filename: &str,
    ) -> Result<std::vec::Vec<Self>, debian::Error> {
        let interface = Interface { filename };
        Ok(db
            .get(&interface)
            .map_err(debian::Error::DatabaseError)?
            .unwrap_or_else(std::vec::Vec::new))
    }
    pub fn has(db: &mut database::Database, filename: &str, checksum: &str) -> bool {
        let interface = Interface { filename };
        let prev = db
            .get(&interface)
            .unwrap_or_else(|_r| Some(std::vec::Vec::new()))
            .unwrap_or_else(std::vec::Vec::new);
        prev.iter().any(|e| e.checksum == checksum)
    }
}
impl database::Interface<std::vec::Vec<Entry>> for Interface<'_> {
    fn get_segment_and_key(&self) -> (&str, &str) {
        /* Note that this will panic on definition with no package name */
        (DATABASE_SEGMENT, self.filename)
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn save() {
        fn count(db: &mut crate::lib::database::Database, filename: &str) -> usize {
            db.get(&super::Interface { filename })
                .unwrap()
                .map_or(0, |v| v.len())
        }
        let mut db = crate::lib::database::test::get_db("test_debian_storage_index_save");
        super::Entry::save(&mut db, "i1", "aa").unwrap();
        assert_eq!(count(&mut db, "i1"), 1);
        assert_eq!(count(&mut db, "i2"), 0);
        super::Entry::save(&mut db, "i1", "ab").unwrap();
        assert_eq!(count(&mut db, "i1"), 2);
        super::Entry::save(&mut db, "i2", "aa").unwrap();
        assert_eq!(count(&mut db, "i1"), 2);
        assert_eq!(count(&mut db, "i2"), 1);
        /* Saving the same version twice will do nothing */
        super::Entry::save(&mut db, "i1", "ab").unwrap();
        assert_eq!(count(&mut db, "i1"), 2);
    }
    fn s(db: &mut crate::lib::database::Database, filename: &str, checksum: &str) -> () {
        let interface = super::Interface { filename };
        let mut prev: std::vec::Vec<super::Entry> =
            db.get(&interface).unwrap().unwrap_or(std::vec::Vec::new());
        prev.push(super::Entry {
            checksum: checksum.to_string(),
        });
        db.save(&interface, &prev, true).unwrap();
    }
    #[test]
    fn load() {
        let mut db = crate::lib::database::test::get_db("test_debian_storage_index_load");
        s(&mut db, "i1", "aa");
        s(&mut db, "i1", "ab");
        s(&mut db, "i2", "aa");
        let p = super::Entry::load(&mut db, "i1").unwrap();
        assert_eq!(p.len(), 2);
        assert_eq!(p[0].checksum, "aa");
        assert_eq!(p[1].checksum, "ab");
        let p = super::Entry::load(&mut db, "i2").unwrap();
        assert_eq!(p.len(), 1);
        let p = super::Entry::load(&mut db, "i3").unwrap();
        assert_eq!(p.len(), 0);
    }
    fn has() {
        let mut db = crate::lib::database::test::get_db("test_debian_storage_index_has");
        s(&mut db, "i1", "aa");
        s(&mut db, "i1", "ab");
        assert!(super::Entry::has(&mut db, "i1", "aa"));
        assert!(super::Entry::has(&mut db, "i1", "ab"));
        assert!(!super::Entry::has(&mut db, "i1", "ac"));
        assert!(!super::Entry::has(&mut db, "i2", "aa"));
    }
}

/* This is about formats and grammars
 * Both Release and Packages uses the so called "indice" format
 * Architecture, Version & Relationship uses the so called "policy" format
 */
pub mod architecture;
pub(super) mod indice;
pub(super) mod packages;
pub(super) mod policy;
pub mod relationship;
pub(super) mod release;
pub mod version;

use crate::lib::debian::format::{packages, release};
use crate::lib::debian::repository::iurl;
use crate::lib::{database, debian};

/* This defines an interface to update a database from package repositories
 */

macro_rules! repository_log {
    ($level:expr, $repo:expr, $msg:expr $(, $params:expr )* ) => {
        log::log!($level, concat!("[Debian repository {} {}] ", $msg), $repo.url, $repo.distribution, $($params),*);
    }
}
macro_rules! repository_debug {
    ($repo:expr, $msg:expr $(, $params:expr )* ) => {
        repository_log!(log::Level::Debug, $repo, $msg $(, $params)*);
    }
}
macro_rules! repository_error {
    ($repo:expr, $msg:expr $(, $params:expr )* ) => {
        repository_log!(log::Level::Error, $repo, $msg $(, $params)*);
    }
}
macro_rules! repository_info {
    ($repo:expr, $msg:expr $(, $params:expr )* ) => {
        repository_log!(log::Level::Info, $repo, $msg $(, $params)*);
    }
}
macro_rules! repository_warning {
    ($repo:expr, $msg:expr $(, $params:expr )* ) => {
        repository_log!(log::Level::Warn, $repo, $msg $(, $params)*);
    }
}

/* Note that the order matters. Try the quickest first */
static COMPRESSIONS: [debian::repository::Compression; 4] = [
    debian::repository::Compression::Xz,
    debian::repository::Compression::Gzip,
    debian::repository::Compression::Bzip2,
    debian::repository::Compression::None,
];

/* Debian repository supports multiple compression formats
 * Each remote file can use one of these formats, or not
 * Try them one by one until one matches
 */
fn find_first_compressed_file<R>(
    parameters: &dyn debian::Parameters,
    build_url: impl Fn(&debian::repository::Compression) -> Result<url::Url, debian::Error>,
    mut build_result: impl FnMut(&str) -> Result<R, debian::Error>,
) -> Result<R, debian::Error> {
    for comp in &COMPRESSIONS {
        let url = build_url(comp)?;
        log::debug!("Try URL {}", url);
        let mut res = reqwest::blocking::get(url.as_str()).map_err(|e| {
            log::error!("Can't get {}: {}", url, e);
            debian::Error::LibError(crate::lib::Error::ReqwestError(e))
        })?;
        if res.status().is_success() {
            log::debug!("Found file at {}", url);
            let mut v = Vec::new();
            use std::io::Read;
            res.read_to_end(&mut v).expect("Unable to read data from");
            /* https://www.reddit.com/r/rust/comments/5k5mez */
            let bytes: &[u8] = &v;
            match comp.deflate(bytes) {
                Ok(deflated) => match build_result(&deflated) {
                    Ok(r) => {
                        return Ok(r);
                    }
                    Err(_e) => {
                        log::debug!("Invalid file at {}, continue to next compression", url);
                    }
                },
                Err(_e) => {
                    log::debug!("Compression error on {}, continue to next compression", url);
                }
            }
        } else {
            log::debug!(
                "Error on HTTP Get {} ({}): can't read content",
                url,
                res.status()
            );
        }
    }
    log::error!("Can't find any file");
    Err(debian::Error::InvalidContentError)
}

fn build_release_document(
    parameters: &dyn debian::Parameters,
    body: &str,
) -> Result<release::DocumentDefinition, debian::Error> {
    /* Deflate body */
    /* Only one definition is expected */
    let mut parser = debian::format::indice::IndiceDocumentParser::new(body)?;
    let def = parser.next().ok_or_else(|| {
        log::error!("Invalid InRelease/Release file, can't find any definition");
        debian::Error::InvalidContentError
    })?;
    release::DocumentDefinition::from_definition(&def)
}
fn build_in_release_document(
    parameters: &dyn debian::Parameters,
    body: &str,
    signed_by: &Option<(pgp::SignedPublicKey, pgp::armor::Headers)>,
) -> Result<release::DocumentDefinition, debian::Error> {
    /* If there is a PGP public key, check signature before parsing the document */
    match signed_by {
        Some(signed_by) => {
            let (pkey, _h) = signed_by;
            let content = debian::sig::check_pgp_signature_and_get_content(body, &pkey)?;
            build_release_document(parameters, &content)
        }
        None => build_release_document(parameters, body),
    }
}
fn check_package_definition(package_definition: &packages::DocumentDefinition, idx: usize) -> bool {
    /* Skip definitions:
     * - without package
     * - without version
     */
    if package_definition.package.is_none() {
        log::warn!("Skip definition {}: no package", idx);
        return false;
    }
    if package_definition.version.is_none() {
        log::warn!("Skip definition {}: no version", idx);
        return false;
    }
    true
}
fn build_packages_indice(
    parameters: &dyn debian::Parameters,
    db: &mut database::Database,
    body: &str,
    repository: &debian::repository::Repository,
    architecture: &str,
    component: &str,
) -> Result<(), debian::Error> {
    let parser = debian::format::indice::IndiceDocumentParser::new(body)?;
    for (idx, definition) in parser.enumerate() {
        log::debug!("Convert definition {} to package indice", idx);
        let package_definition = match packages::DocumentDefinition::from_definition(&definition) {
            Ok(p) => p,
            Err(_e) => {
                log::debug!("Definition {} is not a proper package indice", idx);
                continue;
            }
        };
        if !check_package_definition(&package_definition, idx) {
            continue;
        }
        let package = package_definition.package.as_ref().take().unwrap();
        let version = package_definition.version.as_ref().take().unwrap();
        /* Get URL for this package */
        let url = iurl::distribution_package(&repository.url, &package_definition.filename)?;
        parameters.update_repository_package_found(
            package,
            version,
            architecture,
            repository,
            component,
        );
        debian::storage::package::Entry::save(
            db,
            package,
            version,
            architecture,
            url.as_str(),
            &package_definition.depends.as_ref(),
            &package_definition.sha256,
        )?;
        /* Handles virtual package
         * https://www.debian.org/doc/debian-policy/ch-relationships.html#virtual-packages-provides
         */
        if let Some(provide) = &package_definition.provides {
            log::debug!(
                "Package {} {} version {} provide virtual package {}",
                idx,
                package,
                version,
                provide
            );
            parameters.update_repository_package_found(
                provide.as_str(),
                version,
                architecture,
                repository,
                component,
            );
            debian::storage::package::Entry::save(
                db,
                &provide,
                version,
                architecture,
                url.as_str(),
                &package_definition.depends.as_ref(),
                &package_definition.sha256,
            )?;
        }
    }
    Ok(())
}

fn fetch_in_release(
    parameters: &dyn debian::Parameters,
    repository: &debian::repository::Repository,
) -> Result<release::DocumentDefinition, debian::Error> {
    repository_debug!(repository, "Fetch InRelease file");
    find_first_compressed_file(
        parameters,
        |c| iurl::distribution_in_release(&repository.url, &repository.distribution, c),
        |s| build_in_release_document(parameters, s, &repository.signed_by),
    )
}
fn fetch_release(
    parameters: &dyn debian::Parameters,
    repository: &debian::repository::Repository,
) -> Result<release::DocumentDefinition, debian::Error> {
    repository_debug!(repository, "Fetch Release file");
    log::debug!(
        "[{} {}] Fetch Release file",
        repository.url,
        repository.distribution
    );
    find_first_compressed_file(
        parameters,
        |c| iurl::distribution_release(&repository.url, &repository.distribution, c),
        |s| build_release_document(parameters, s),
    )
}
fn fetch_packages_indices(
    parameters: &dyn debian::Parameters,
    db: &mut database::Database,
    repository: &debian::repository::Repository,
    _release: &release::DocumentDefinition,
    component: &str,
    architecture: &str,
) -> Result<(), debian::Error> {
    repository_debug!(
        repository,
        "Fetch Packages file for component {} on architecture {}",
        component,
        architecture
    );
    find_first_compressed_file(
        parameters,
        |c| {
            iurl::distribution_component_architecture_packages(
                &repository.url,
                &repository.distribution,
                component,
                architecture,
                c,
            )
        },
        |s| build_packages_indice(parameters, db, s, repository, architecture, component),
    )
}

fn update_repository_release_component_architecture(
    parameters: &dyn debian::Parameters,
    db: &mut database::Database,
    repository: &debian::repository::Repository,
    release: &release::DocumentDefinition,
    component: &str,
    architecture: &str,
) -> Result<(), debian::Error> {
    repository_debug!(
        repository,
        "Update component component {} on architecture {}",
        component,
        architecture
    );
    /* Identify indexes that are supposed to exists for this component and architecture
     * This is for database cache
     * For each compression, look if there is such index in this release
     * unwrap() before collect() is safe because filter_map will only return Some()
     */
    let indexes: std::vec::Vec<&release::IndexElementDefinition> = COMPRESSIONS
        .iter()
        .filter_map(|comp| {
            iurl::distribution_component_architecture_packages(
                &repository.url,
                &repository.distribution,
                component,
                architecture,
                comp,
            )
            .map(|u| release.get_index_by_url(&u))
            .ok()
        })
        .filter_map(|i| i)
        .collect();
    /* If this indice is already in database and has not changed, no need to continue
     * If indice is in database, compare both hash, if they differ, then this indice is to
     * be fetched
     */
    if indexes
        .iter()
        .all(|i| debian::storage::index::Entry::has(db, &i.filename, &i.checksum))
    {
        repository_debug!(
            repository,
            "No change for component {} on architecture {}",
            component,
            architecture
        );
    } else {
        /* Update this component / architecture
         * First thing to do is to clear any previous indice in database
         */
        fetch_packages_indices(parameters, db, repository, release, component, architecture)?;
        for i in &indexes {
            debian::storage::index::Entry::save(db, &i.filename, &i.checksum)?;
        }
    }
    db.commit().map_err(debian::Error::DatabaseError)?;
    Ok(())
}

fn update_repository_architectures(
    parameters: &dyn debian::Parameters,
    db: &mut database::Database,
    repository: &debian::repository::Repository,
    architectures: &std::vec::Vec<&str>,
) -> Result<(), debian::Error> {
    repository_debug!(repository, "Update debian repository");
    /* Look first for InRelease files, then for Release */
    let release = fetch_in_release(parameters, repository)
        .or_else(|_| fetch_release(parameters, repository))?;
    /* Now retreive indexes for architectures and components */
    let it = itertools::iproduct!(repository.components.iter(), architectures.iter());
    let mut res = Ok(());
    for (component, architecture) in it {
        /* If component is not part of this repository, ignore it */
        if release.components.iter().all(|c| c != component) {
            repository_warning!(
                repository,
                "{}: no such component in InRelease or Release",
                component
            );
            continue;
        }
        parameters.update_repository_release_start(&repository, architecture, component);
        match update_repository_release_component_architecture(
            parameters,
            db,
            repository,
            &release,
            component,
            architecture,
        ) {
            Ok(_) => {
                parameters.update_repository_release_done(&repository, component, architecture);
            }
            Err(e) => {
                log::warn!(
                    "Failed to update repository {} component {} on architecture {}: {:?}",
                    repository.url,
                    component,
                    architecture,
                    e
                );
                parameters.update_repository_release_failed(&repository, &e);
                res = Err(e);
            }
        };
    }
    res
}

pub struct Updater<'s> {
    /* Only borrow repositories and architecture, they are stored in the debian Source */
    architectures: std::vec::Vec<&'s str>,
    repositories: std::vec::Vec<&'s debian::repository::Repository>,
}

impl<'s> Updater<'s> {
    pub fn new() -> Self {
        Updater {
            architectures: std::vec::Vec::new(),
            repositories: std::vec::Vec::new(),
        }
    }
    pub fn add_architecture(&mut self, architecture: &'s str) -> () {
        if !self.architectures.contains(&architecture) {
            self.architectures.push(architecture);
        }
    }
    pub fn add_repository(&mut self, repository: &'s debian::repository::Repository) -> () {
        self.repositories.push(repository);
        ()
    }
    pub fn run(
        &self,
        db: &mut database::Database,
        parameters: &dyn debian::Parameters,
    ) -> Result<(), debian::Error> {
        for repository in &self.repositories {
            parameters.update_repository_start(&repository, &self.architectures);
            match update_repository_architectures(parameters, db, &repository, &self.architectures)
            {
                Ok(_) => parameters.update_repository_done(&repository, &self.architectures),
                Err(e) => {
                    log::warn!("Failed to update repository {}: {:?}", repository.url, e);
                    parameters.update_repository_failed(&repository, e)
                }
            };
        }
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::lib::debian::repository::Compression;
    use url::Url;

    impl crate::lib::debian::Parameters for std::path::PathBuf {
        fn get_directory(&self) -> &std::path::Path {
            self.as_path()
        }
    }

    #[test]
    fn test_find_first_compressed_file() {
        let d = crate::lib::utils::test::get_temp_dir();
        let server = httpmock::MockServer::start();
        let none_url = server.url("/no_compression");
        let none_body = "YOLOLO";
        let gzip_url = server.url("/gzip_compression");
        /* YALALA */
        let gzip_body_b64 = "H4sIApdC6GUA/16EBcDBEAAAAAJBtv0nkL/Izc0EGPhdAgYAAAA=";
        use base64ct::{Base64, Encoding};
        let gzip_body = Base64::decode_vec(gzip_body_b64).unwrap();
        let unknown_url = server.url("/does/not/exists");
        let mock_none = server.mock(|when, then| {
            when.method("GET").path("/no_compression");
            then.status(200).body(none_body);
        });
        let mock_gzip = server.mock(|when, then| {
            when.method("GET").path("/gzip_compression");
            then.status(200).body(gzip_body);
        });
        let mock_unknown = server.mock(|when, then| {
            when.path("/does/not/exists");
            then.status(404);
        });
        /* gzip has a match, none should not be used */
        let res = super::find_first_compressed_file(
            &d,
            |c| match c {
                Compression::None => Ok(Url::parse(none_url.as_str()).unwrap()),
                Compression::Gzip => Ok(Url::parse(gzip_url.as_str()).unwrap()),
                _ => Ok(Url::parse(unknown_url.as_str()).unwrap()),
            },
            |b| Ok(String::from(b)),
        );
        mock_gzip.assert();
        mock_none.assert_hits(0);
        match res {
            Ok(s) => assert_eq!(s.as_str(), "YALALA"),
            Err(_) => assert!(false),
        }
        /* This time gzip does not exists*/
        let res = super::find_first_compressed_file(
            &d,
            |c| match c {
                Compression::None => Ok(Url::parse(none_url.as_str()).unwrap()),
                _ => Ok(Url::parse(unknown_url.as_str()).unwrap()),
            },
            |b| Ok(String::from(b)),
        );
        assert!(mock_unknown.hits() > 0);
        /* Because it was called before */
        mock_none.assert();
        match res {
            Ok(s) => assert_eq!(s.as_str(), none_body),
            Err(_) => assert!(false),
        }
        /* No result this time */
        let res = super::find_first_compressed_file(
            &d,
            |_c| Ok(Url::parse(unknown_url.as_str()).unwrap()),
            |b| Ok(String::from(b)),
        );
        assert!(mock_unknown.hits() > 2);
        match res {
            Ok(_) => assert!(false),
            Err(_) => assert!(true),
        }
    }
    #[test]
    fn build_release_document() {
        let d = crate::lib::utils::test::get_temp_dir();
        let data = r#"
Date: Sat, 10 Jun 2023 08:53:33 UTC
Architectures: amd64 arm64
Components: main contrib non-free
SHA256:
 d7272158df85ef6cc2e7edee79ef9246348eed0152970738b5e0683daa91e534  1356807 contrib/Contents-amd64
 0e21127b32280b67d6ea7561259f703b244a9169e779347ed4bb7cfa6268d009   102603 contrib/Contents-amd64.gz
 f388b3541940cada4df5489fbc1016440fa9290a23b998674ba41e4aacb20ccd  1075476 contrib/Contents-arm64
 aaaa209c6f255165a7b9431fe92d02809c25fca3684f7e601887268cdab4a1a5    84016 contrib/Contents-arm64.gz
 10b740b16a0d26a88646becbcb8ef8ff06fbe89809cbaac7b6f307c4d8e8881e  1072443 contrib/Contents-armel
 6b7a5352ebf79480492b1a8f97eed09ca5ee9ceb420166f08a7b4c134e23511d    83766 contrib/Contents-armel.gz
"#;
        let res = super::build_release_document(&d, data).unwrap();
        assert_eq!(res.indexes.len(), 6);
        assert_eq!(res.architectures, ["amd64", "arm64"]);
        assert_eq!(res.components, ["main", "contrib", "non-free"]);
    }
    macro_rules! sa {
        ($string:expr, $a:expr) => {
            $string.push_str($a.as_str());
            $string.push_str("\n");
        };
    }
    const PACKAGES_URL_PREFIX: &str = "pool";
    struct FakePackage {
        name: String,
        version: String,
        depends: Option<String>,
        provides: Option<String>,
        data: String,
    }
    impl FakePackage {
        pub fn new(
            name: &str,
            version: &str,
            depends: Option<&str>,
            provides: Option<&str>,
        ) -> Self {
            FakePackage {
                name: name.to_string(),
                version: version.to_string(),
                depends: depends.map(String::from),
                provides: provides.map(String::from),
                data: crate::lib::utils::test::get_text(),
            }
        }
        pub fn get_filename(&self) -> String {
            format!("{}/{}", PACKAGES_URL_PREFIX, self.name)
        }
        pub fn get_content_entry(&self) -> String {
            let mut res = String::from("");
            sa!(res, format!("Package: {}", self.name));
            sa!(res, format!("Size: {}", self.data.len()));
            sa!(res, format!("Version: {}", self.version));
            sa!(res, format!("Filename: {}", self.get_filename()));
            /* Don't care about that for now*/
            if self.provides.is_some() {
                sa!(
                    res,
                    format!("Provides: {}", self.provides.as_ref().unwrap())
                );
            }
            if self.depends.is_some() {
                sa!(res, format!("Depends: {}", self.depends.as_ref().unwrap()));
            }
            res
        }
    }
    struct FakeComponent {
        name: String,
        /* Only one architecture per repository */
        architecture: String,
        pub packages: std::vec::Vec<FakePackage>,
    }
    impl FakeComponent {
        pub fn new(name: &str, architecture: &str) -> Self {
            FakeComponent {
                name: name.to_string(),
                architecture: architecture.to_string(),
                packages: std::vec::Vec::new(),
            }
        }
        pub fn get_package(&self) -> String {
            let mut res = String::from("");
            let a = |s: String| res.push_str(s.as_str());
            for package in &self.packages {
                sa!(res, package.get_content_entry());
                res.push_str("\n");
            }
            res
        }
        pub fn get_package_path(&self) -> String {
            format!("{}/binary-{}/Packages", self.name, self.architecture)
        }
    }
    struct FakeRepository {
        /* Only one distribution per repository */
        distribution: String,
        components: std::vec::Vec<FakeComponent>,
    }
    use sha2::{Digest, Sha256};
    impl FakeRepository {
        pub fn new(distribution: &str) -> Self {
            FakeRepository {
                distribution: distribution.to_string(),
                components: std::vec::Vec::new(),
            }
        }
        pub fn add_components(&mut self, component: FakeComponent) {
            self.components.push(component);
        }
        pub fn get_release(&self) -> String {
            let mut res = String::from("");
            let components = self
                .components
                .iter()
                .map(|c| c.name.as_str())
                .collect::<Vec<&str>>()
                .join(" ");
            let archs = self
                .components
                .iter()
                .map(|c| c.architecture.as_str())
                .collect::<Vec<&str>>()
                .join(" ");
            sa!(res, format!("Components: {}", components));
            sa!(res, format!("Architectures: {}", archs));
            sa!(res, format!("Date: 11/11/1918"));
            res.push_str("SHA256:");
            for component in &self.components {
                let content = component.get_package();
                let sha = Sha256::digest(&content);
                sa!(
                    res,
                    format!(
                        " {} {} {}",
                        base16ct::lower::encode_string(&sha),
                        content.len(),
                        component.get_package_path()
                    )
                );
            }
            res
        }
    }
    struct FakeHTTP {
        repositories: std::collections::HashMap<String, FakeRepository>,
    }
    impl FakeHTTP {
        pub fn new() -> Self {
            FakeHTTP {
                repositories: std::collections::HashMap::new(),
            }
        }
        pub fn add_repository(&mut self, name: &str, repository: FakeRepository) {
            self.repositories.insert(name.to_string(), repository);
        }
        pub fn publish(&self) -> httpmock::MockServer {
            let server = httpmock::MockServer::start();
            for (name, repository) in &self.repositories {
                /* Only one compression (none) is supported, but other routes MUST exists*/
                let release_uri = format!("/{}/dists/{}/Release", name, repository.distribution);
                server.mock(|when, then| {
                    when.method("GET").path(release_uri);
                    then.body(repository.get_release());
                });
                for component in &repository.components {
                    let packages_uri = format!(
                        "/{}/dists/{}/{}",
                        name,
                        repository.distribution,
                        component.get_package_path()
                    );
                    server.mock(|when, then| {
                        when.method("GET").path(packages_uri);
                        then.body(component.get_package());
                    });
                    for package in &component.packages {
                        let package_uri = format!(
                            "/{}/dists/{}/{}",
                            name,
                            repository.distribution,
                            package.get_filename()
                        );
                        server.mock(|when, then| {
                            when.method("GET").path(package_uri);
                            then.body(crate::lib::utils::test::get_text());
                        });
                    }
                }
            }
            server
        }
    }
    #[test]
    fn test_update() {
        let d = crate::lib::utils::test::get_temp_dir();
        let mut fake = FakeHTTP::new();
        let mut db = crate::lib::database::test::get_db("update");
        let mut repo = FakeRepository::new("current");
        let mut comp1 = FakeComponent::new("main", "amd64");
        comp1
            .packages
            .push(FakePackage::new("p1", "1.0", None, None));
        repo.components.push(comp1);
        fake.add_repository("debian", repo);
        let server = fake.publish();
        let repo = crate::lib::debian::repository::Repository::new(
            server.url("/debian").as_str(),
            "current",
            &["main"],
            None,
        )
        .unwrap();
        let mut updater = super::Updater::new();
        updater.add_architecture("amd64");
        updater.add_repository(&repo);
        updater.run(&mut db, &d).unwrap();
    }
}

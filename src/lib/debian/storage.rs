/*
 * This is about what is stored in the database
 * Not every field we get from the repositories are useful
 * Each module must focus on what really matters for the fetch part
*/
pub mod index;
pub mod package;

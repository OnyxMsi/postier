pub fn check_pgp_signature_and_get_content(
    content: &str,
    public_key: &pgp::SignedPublicKey,
) -> Result<String, super::Error> {
    log::debug!("Verify signature");
    let (mes, _h) = pgp::composed::cleartext::CleartextSignedMessage::from_string(content)
        .map_err(|e| {
            log::error!("Can't read it as a PGP signed block: {}", e);
            super::Error::InvalidSignatureError
        })?;
    mes.verify(&public_key).map_err(|e| {
        log::error!("Invalid signature: {}", e);
        super::Error::InvalidSignatureError
    })?;
    log::debug!("PGP signature is valid");
    Ok(String::from(mes.text()))
}

#[cfg(test)]
mod test {
    #[test]
    fn check_pgp_signature_and_get_content() {
        /*
                 * No password for this key
        -----BEGIN PGP PRIVATE KEY BLOCK-----

        lQVYBGZy6e8BDADdyytVgFHu6qFuOxwr8kJO8RWTyqS8w8mS56yFnzGrFKOQzTJi
        uoY5OK4UtMEydfRpUDNxwxRS/0ZE+Z6al3f/X7WvjBzPoSl4SIOiOi9aLGW6+GAH
        4wiEI3/PJiLXYHBoSonJG3b8nPTo2KQ6JFIv3+r9Ahq4bOuNTBiSvA9LSzGX1nzU
        ttLKmU3s/oFXzp3BhmSO6tIQ7gpMZB+qSA3wx2Ub44JM3l9sKwwO2DJw6RFGMcxM
        +pNWKb75M1geqRDMMFBo5RE9cRDHwYzGHkuGMG+U6FU16uh7UNib/HU262w+VQi+
        S8kwY48yRDK33CpVeox0IVsyOm832wtPfLPvWP7dXqYcq357V3PlnA5UVjXrLQp4
        E3YRqPtiLmcpVhHezS8ps6h1uTv3UrP2Jymxqtfv4e73vlkLBzX7DyThwxeS4DPt
        jJmr4QY4OajM8Ezz4EjNqY5Ss+RfjjRimNFjbhPKd+D+gYHXWOlB6KjpVeLcfhlE
        BcG9Rksn8x9p/90AEQEAAQAL+wQKzfdLiigRoyUMPehyzHC+Cz+2Yle6tWuwaLrG
        YA7x5pmUP85AVNi/TB7o/MS+am4D7e1tbytPEeOzwpynKjhKMS2/SY7NgrZkPe/r
        jGLh6xhj//NiLiIjDSiEQFTOt2W93AfWsLhhukNHICvGGdsGWJ+eqvLVsK+n0CKB
        JmyKdN+DTiLzUmFVDMWcYtTr7qtGV4KqTBn5Dft+mfwjaYuhRVoOL1kBdtmUvu4Y
        qcdYhr3R2QXFzasfl3vZCsquQpfXGa6cWAWTaDgIqgnjXgZydYa5qU+ENDuEdDi0
        QHelA5M7HQjZq2j6U0rEB+9nexd9eQ6bDxF9rWaFfpfJjO7GvMYST20RusxcajPA
        HOJX/mLFcH9PKmtEISwg4vQqCbtcNr6gSngse9U3yIBTsY+FRsHBmQxmo8pqu/7Q
        KiTfFjRNZ8mRmUZh5DwKCRwwWFaqU5cFNid387nOyRQMGAnvq00la0ycxfK9kGF2
        j46EFgjPo29kicEWSzaOxxH6eQYA5/WpNEj8CjDI27SDldoJbACbWoDEjr32Y4Ht
        qO02QYeEMuSxP6xf5FHrSNOZ9EYNQv6Jox84Vtzs0OuWGSkf5T1hJD0rVpOY+g0t
        46M3OtGggDd1jhdIBfVpOtCQj450AwteqTWSF1J3+eqkSV8i9YmgbChIqcECEz9/
        y+J0UiumLAuOhbf/O9Y9e2K4KYFAxCJX0FECa3kvFvcbF9Up7aqKxN08qIn6TXPn
        +BLMH2EorxT1p49uRk22PE3tTqBVBgD0x8j4jk0xhBbROyevBxFK5ZZIZyoX7Zx5
        XDMgigCzKoah7g+qeyI3CCt02KEHlTisadqvHoiLy8UfmopjbiA/sJU+lk7hNWCi
        6YaA7lzefvrPkkVu8TfLZFFIBX+kn4Fo9X/OFycKYENG4Q60ZuMgwdV4HG0q/tpA
        RaZQSkI1aoZE46puFp1oGBVSdLV4hhB9HZplrZkrxkRiAPwSYFRGg09oE9Jkay77
        QUQnW8wE2TFeAkLhAAmjAXKkPU6qSWkGANX7Yw4dtFDv0NbqJM3zC8mCSePSO9ad
        xxHuRfky6DQkWUFXx3UzzhWtrCYtJT5ATnUjmh4b/QArfFG3+m3d788aZhP2UfkK
        pKdcYEOO2EcLpdqrGNBeQ5IG+8UVNffCvyfhBjLJBhE4RVzOpxfLunoG6jNYOVAM
        wlrH3RxUX4IEaysyiNMkwJH85Gjl0sTRFJCMwEPEn0aWaxKVAHeLGYlQMfbEat+5
        vx4IpKJ4kgXGppkTBmUhP6eJYngFVAtAx8aRtAtwYXVsYmlzbXV0aIkBzgQTAQoA
        OBYhBDJNEbgna8GFnCeiS/nq+VhSsV1nBQJmcunvAhsDBQsJCAcCBhUKCQgLAgQW
        AgMBAh4BAheAAAoJEPnq+VhSsV1ntSIL/2tLo5eYSqCLBglhlp0P+4JKy647EX2x
        snlqZyKrzxaI0dY0CQKPVmCSdobPer1U6iDnH/xfLJFRrWSsk+oLuq13sShNzmP7
        FrMeUEpJyWnJgt46dm1S3JBbZ9busBsq30kosKDqtf6Otxe5hVcgkASiyJEhjuZF
        w3MVjCwHWNJr3lJrrpxtm4iPNSWfLzUAyhMuBUUT4AhpUplxlXDBteCZ/CtIDJ8B
        QQ05+ZntftTw514FzxKHvTysLkL4TAo5MvO/rrVqyqWgxGrYEsX2qkP3JAy2REcM
        zRZA678TnZNZMI0+S6D9XTIJh1mFZjzoX1h6cVX6Yw4I8tu/3E7QCualPKhuk8xZ
        cyBYJMorXcDaupdRLUll71YH4NQP7opvTU3lPedciNO4lUwKj5bEE0pO4a56VDsY
        igddR58JjRV39Etx7OQcR0/j64r/it3yNUd7N8bbpkiYqhEMcYqtgoPuQ8EsnbXr
        +4S9g+Pcqj2bQhgsWRKRMQ+trIN/SOS3NZ0FWARmcunvAQwAwjuIsrQ1DbKAwZmI
        R9ds5l6Nd3zUXi74TNV4Gu1QIkLRI/qvpEgxmjqRlMAnRuHuqBi2sZitunw2bIdX
        DWaCQi2jyeGRphmxndoSLFZ2D1RdKnrolYH+YpY5IYkuZTaMKin5Z7bTmrZcjsGG
        yrFTBIUDjnhSmBsXpFZHWHH3NcHwKkMdT8sym6dRz8WjJ4u1977xpTgiesaVfgjH
        VNlXokpvJM5GyAhTahlFkUDwF1qh6cob5VqeKEX4YJpvMJ8uyzyYGXKYSdl+hX0H
        5vja2a4SqpM+Mp/skockfCXq1QiLzeloSuDfgcot0oxajRNKAaRGzaZu03xelIRQ
        TC+zh1EZM9HBdW0teoSt3GT0q68v/iORtzLPkEb5/1Iwdz9q815o/HPCfF3Jrr2d
        xvNdqOPHncKybxPbwbS4lstIFe1HlfQHUJGm7Mt3JswrXav46BdI2aUsDc0TY8Uz
        XSvpXbqT+hx3zUZ61y8D1I5J7RZLtVLCpTWsg7+p9eits/i1ABEBAAEAC/oC5zXC
        AFRo9m42iN9gvc5Jm8foGyn6BdciRJTY2jASTCtfz5WR7cLjd/l/p9TAvud8u3Cw
        xXHf49hCsQTlFRhywseXhVMt2a53PtUMtORrJM+HHOCehcAtf6Gb1bbwh31619vE
        6BaHCMHn+zMcJVc75MY4Z+8lQE783tb8KQR+xsbjxkDADLgwyWkGWiXBJ7VMcIvE
        9JOyyT1fEi2psksYmubI9MPHgVl++XxMSDjp50539fj4e+1CrtfpauDyTGNaJKlZ
        g/eIvCHG3PEgFwBiqtnOEGbZqSWtP4paDcAm/0B4kJqfDYTyIkxZ1hlg0CUMriAF
        eyFoa6lYg9l4hh+1nsclwu3italXD3dBOfix18880M1hjDjRI/xT4RbKymP30qWd
        WKxWq3KZxANrVdjsvg1da+JG+wJpqEA9A+Igr3LEUTWSJsPyYXujZoSQnU7YXMVY
        7/fXWtfVoaY+jOjRkGjGyJT5JHjCrDCDqVkY/SRh1osME9dBFa6V9yPADcEGAN4K
        nkjrTkxDzesHv5N9td+/q3xdfC2pNIanrZ8b0owQJ2oIGKQ4B1RXHxEoOm/+H5cy
        fY1otac3RZ7Lji9AraECj/N4UDjOr4LeOfUCKi4FDBtbb/Rf2AzBOH2hoYgoEsek
        r7+pjP4NwsieHjBcdjWl4d2hXSk5mxaFy1iXDOdCbsv6vJut75ddbSG4vyGI0uVc
        jdMOpITVbuIwa28Cd6RElSwzIMfIi9HxR8VpuY7XfM1YmpKs50Vd5eMQUqysZQYA
        3/AjlR8wRUdg5XYRz1wbiumc46MYNSAb4u9vk8zs0cTSzDWZqYjsk/FFAQpH5bqX
        eidh8DtTXj39jD05L5g8I2hIX4DY7u7lkUsgNN6mGezo69fyjc0oz+KO4Sf2yqSH
        ymvhL8wLxrjT62BDFyxSaFijUkP7tNi7LKJHcoPAuiV7s53tWgranRLdP9YXhs0o
        7yeZFzGix3j0sBXD9UCk72Lwyn69bSCMke3djKij6vg5doBA2XE860Fon14pIw4R
        BfwLIhcHoYGo/UAxCbPTW5fhuUmfGRLmLB8LZNaRk0vgaQzldzAHu2LXJFMFUIDE
        bLkLvPrC5P6mBwVHkevAkmNwJtbL5DyU2eie9ml8ex66V19pe3tSmdooZNQVQRl0
        QN05/8Ebf80B67Aq39YpQlxr2HevNLavgXuhQgUvbL91ac9x+SZQ22OdCsYqP9qe
        AhHjruyVLAq4lsKu30ir/re7O9Lak3acMMAhXavuj9ou4tKnizOh4x6ndt3aJGJS
        pQT0FokBtgQYAQoAIBYhBDJNEbgna8GFnCeiS/nq+VhSsV1nBQJmcunvAhsMAAoJ
        EPnq+VhSsV1ndA0L/jtWW2HZn7jDTBX/sGdbyn1FQnMp3EYi5ax3Wty5YJ6/T1Dn
        dmeHvk/Dww3r3u4Q/gjGWtZxnOPmrZ5Ghyw4woacmMEkTzbu6n2wrsGFpMrr14vT
        VkXC6H++1U7NUzJYeBr0SL4Er76iGWBn9pe/WQ3PWN4Mr0lCJZL1qXIK3K8OLPTo
        xDY21xMK5kYoBmlSqR6gWCJtbbTgojle0DFYdxtOzrbeUacIVKGgVXx5NouL4eoV
        tw1w2jeE+JiaC2eB803FndCVXGE3fzjQY2zIL2EhJ3K877pIcMKTh8uLckkjWLDl
        tnOPJMKH+pVL07QSWzc61/C3oiZ0skef/Pj8FHR8uVjLo4uEB7fNQ0dVgkfHmd7K
        +0+CbaViD7xBNmQLPUEs5g6LlxO2IgpEE745yyuUKOxQjPktEBbIMC/uQp/IhXcT
        +iemTWEz7hbau/zkU9UsuOx1HzB9zUGYtNfUbeWR1WCOmPlbif6eC+NEFADOHHJM
        kvrV/UmtzK765Bqpqg==
        =UTto
        -----END PGP PRIVATE KEY BLOCK-----
                */
        let public_key = r#"
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQGNBGZy6e8BDADdyytVgFHu6qFuOxwr8kJO8RWTyqS8w8mS56yFnzGrFKOQzTJi
uoY5OK4UtMEydfRpUDNxwxRS/0ZE+Z6al3f/X7WvjBzPoSl4SIOiOi9aLGW6+GAH
4wiEI3/PJiLXYHBoSonJG3b8nPTo2KQ6JFIv3+r9Ahq4bOuNTBiSvA9LSzGX1nzU
ttLKmU3s/oFXzp3BhmSO6tIQ7gpMZB+qSA3wx2Ub44JM3l9sKwwO2DJw6RFGMcxM
+pNWKb75M1geqRDMMFBo5RE9cRDHwYzGHkuGMG+U6FU16uh7UNib/HU262w+VQi+
S8kwY48yRDK33CpVeox0IVsyOm832wtPfLPvWP7dXqYcq357V3PlnA5UVjXrLQp4
E3YRqPtiLmcpVhHezS8ps6h1uTv3UrP2Jymxqtfv4e73vlkLBzX7DyThwxeS4DPt
jJmr4QY4OajM8Ezz4EjNqY5Ss+RfjjRimNFjbhPKd+D+gYHXWOlB6KjpVeLcfhlE
BcG9Rksn8x9p/90AEQEAAbQLcGF1bGJpc211dGiJAc4EEwEKADgWIQQyTRG4J2vB
hZwnokv56vlYUrFdZwUCZnLp7wIbAwULCQgHAgYVCgkICwIEFgIDAQIeAQIXgAAK
CRD56vlYUrFdZ7UiC/9rS6OXmEqgiwYJYZadD/uCSsuuOxF9sbJ5amciq88WiNHW
NAkCj1ZgknaGz3q9VOog5x/8XyyRUa1krJPqC7qtd7EoTc5j+xazHlBKSclpyYLe
OnZtUtyQW2fW7rAbKt9JKLCg6rX+jrcXuYVXIJAEosiRIY7mRcNzFYwsB1jSa95S
a66cbZuIjzUlny81AMoTLgVFE+AIaVKZcZVwwbXgmfwrSAyfAUENOfmZ7X7U8Ode
Bc8Sh708rC5C+EwKOTLzv661asqloMRq2BLF9qpD9yQMtkRHDM0WQOu/E52TWTCN
Pkug/V0yCYdZhWY86F9YenFV+mMOCPLbv9xO0ArmpTyobpPMWXMgWCTKK13A2rqX
US1JZe9WB+DUD+6Kb01N5T3nXIjTuJVMCo+WxBNKTuGuelQ7GIoHXUefCY0Vd/RL
cezkHEdP4+uK/4rd8jVHezfG26ZImKoRDHGKrYKD7kPBLJ216/uEvYPj3Ko9m0IY
LFkSkTEPrayDf0jktzW5AY0EZnLp7wEMAMI7iLK0NQ2ygMGZiEfXbOZejXd81F4u
+EzVeBrtUCJC0SP6r6RIMZo6kZTAJ0bh7qgYtrGYrbp8NmyHVw1mgkIto8nhkaYZ
sZ3aEixWdg9UXSp66JWB/mKWOSGJLmU2jCop+We205q2XI7BhsqxUwSFA454Upgb
F6RWR1hx9zXB8CpDHU/LMpunUc/FoyeLtfe+8aU4InrGlX4Ix1TZV6JKbyTORsgI
U2oZRZFA8BdaoenKG+VanihF+GCabzCfLss8mBlymEnZfoV9B+b42tmuEqqTPjKf
7JKHJHwl6tUIi83paErg34HKLdKMWo0TSgGkRs2mbtN8XpSEUEwvs4dRGTPRwXVt
LXqErdxk9KuvL/4jkbcyz5BG+f9SMHc/avNeaPxzwnxdya69ncbzXajjx53Csm8T
28G0uJbLSBXtR5X0B1CRpuzLdybMK12r+OgXSNmlLA3NE2PFM10r6V26k/ocd81G
etcvA9SOSe0WS7VSwqU1rIO/qfXorbP4tQARAQABiQG2BBgBCgAgFiEEMk0RuCdr
wYWcJ6JL+er5WFKxXWcFAmZy6e8CGwwACgkQ+er5WFKxXWd0DQv+O1ZbYdmfuMNM
Ff+wZ1vKfUVCcyncRiLlrHda3Llgnr9PUOd2Z4e+T8PDDeve7hD+CMZa1nGc4+at
nkaHLDjChpyYwSRPNu7qfbCuwYWkyuvXi9NWRcLof77VTs1TMlh4GvRIvgSvvqIZ
YGf2l79ZDc9Y3gyvSUIlkvWpcgrcrw4s9OjENjbXEwrmRigGaVKpHqBYIm1ttOCi
OV7QMVh3G07Ott5RpwhUoaBVfHk2i4vh6hW3DXDaN4T4mJoLZ4HzTcWd0JVcYTd/
ONBjbMgvYSEncrzvukhwwpOHy4tySSNYsOW2c48kwof6lUvTtBJbNzrX8LeiJnSy
R5/8+PwUdHy5WMuji4QHt81DR1WCR8eZ3sr7T4JtpWIPvEE2ZAs9QSzmDouXE7Yi
CkQTvjnLK5Qo7FCM+S0QFsgwL+5Cn8iFdxP6J6ZNYTPuFtq7/ORT1Sy47HUfMH3N
QZi019Rt5ZHVYI6Y+VuJ/p4L40QUAM4cckyS+tX9Sa3MrvrkGqmq
=gxs7
-----END PGP PUBLIC KEY BLOCK-----"#;
        use pgp::composed::Deserializable;
        let (pkey, _h) = pgp::SignedPublicKey::from_string(public_key).unwrap();
        let valid = r#"-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Ni dieu, ni César ni tribun
-----BEGIN PGP SIGNATURE-----

iQGzBAEBCgAdFiEEMk0RuCdrwYWcJ6JL+er5WFKxXWcFAmZy7BIACgkQ+er5WFKx
XWc12gv/UEDicNvZ9rH+6bTaoOoHpL3nKR1pFOWbaxVD2T/NJ03ebulUUoGdX3Lr
G8kG6c4tIL6ssklzWSl5TRGLRVukXLHrYKfLpCVmVigFMk4vTn/IgOxjja6wQKl7
gB4UR3KkfLxXLWwyd9vQZFI/yx0PBzgjs0ovpLeTeIOOQ0xpA5+GB3sDSrmjhX2k
uDkRAjF2DqF8iluUcjJRzNWEk3rqazZPM7RfOvU90ARUDXqQyAnbfBeg8JzuvLBN
zojmdEKnN3wEX4VvsozVHXktmWKy2bBHoSSC432Z5F/3KahgCrrzcJ/lkYB69FOZ
AAGclOMagXixHL0y2qiiu8NFfhJV93oCmX9HAMLCbu8yunlVx39o7q0T+YfyrgHT
YH8ScmVbk9seAW/zZEE21p/57se+eeTJrDDLg2LdgHhefLI8O19JTL319LVZZ8ct
rp4PwmDmDj7vx2Ttv6g2LTBCH1YjJWYjAdc54PzpjAIJ0oh6KizIgf7YEd8BZijf
+UmpyUpv
=Mq9d
-----END PGP SIGNATURE-----"#;
        let c = super::check_pgp_signature_and_get_content(valid, &pkey).unwrap();
        assert_eq!(c.as_str(), "Ni dieu, ni César ni tribun");
        let invalid = r#"-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Dieu et mon droit
-----BEGIN PGP SIGNATURE-----

iQGzBAEBCgAdFiEEMk0RuCdrwYWcJ6JL+er5WFKxXWcFAmZy7BIACgkQ+er5WFKx
XWc12gv/UEDicNvZ9rH+6bTaoOoHpL3nKR1pFOWbaxVD2T/NJ03ebulUUoGdX3Lr
G8kG6c4tIL6ssklzWSl5TRGLRVukXLHrYKfLpCVmVigFMk4vTn/IgOxjja6wQKl7
gB4UR3KkfLxXLWwyd9vQZFI/yx0PBzgjs0ovpLeTeIOOQ0xpA5+GB3sDSrmjhX2k
uDkRAjF2DqF8iluUcjJRzNWEk3rqazZPM7RfOvU90ARUDXqQyAnbfBeg8JzuvLBN
zojmdEKnN3wEX4VvsozVHXktmWKy2bBHoSSC432Z5F/3KahgCrrzcJ/lkYB69FOZ
AAGclOMagXixHL0y2qiiu8NFfhJV93oCmX9HAMLCbu8yunlVx39o7q0T+YfyrgHT
YH8ScmVbk9seAW/zZEE21p/57se+eeTJrDDLg2LdgHhefLI8O19JTL319LVZZ8ct
rp4PwmDmDj7vx2Ttv6g2LTBCH1YjJWYjAdc54PzpjAIJ0oh6KizIgf7YEd8BZijf
+UmpyUpv
=Mq9d
-----END PGP SIGNATURE-----"#;
        super::check_pgp_signature_and_get_content(invalid, &pkey).unwrap_err();
    }
}

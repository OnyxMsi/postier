//! See Debian
//! [documentation](https://wiki.debian.org/DebianRepository/Format#A.22Packages.22_Indices)
use crate::lib::debian::format::indice::{
    get_optional_field, get_required_field, DocumentDefinition as IDocumentDefinition,
};
pub struct DocumentDefinition {
    /* Mandatory */
    pub filename: String,
    pub size: usize,
    pub sha256: String,
    /* Optional */
    pub package: Option<String>,
    pub version: Option<String>,
    pub depends: Option<String>,
    pub provides: Option<String>,
    /* ... and many more */
}
impl DocumentDefinition {
    pub fn from_definition(
        definition: &IDocumentDefinition,
    ) -> Result<DocumentDefinition, crate::lib::debian::Error> {
        let size_str = get_required_field(definition, "Size")?;
        let size = size_str.parse::<usize>().map_err(|_e| {
            log::error!(
                "Invalid packages indice: Size is not an integer (got {})",
                size_str
            );
            crate::lib::debian::Error::InvalidContentError
        })?;
        Ok(DocumentDefinition {
            filename: get_required_field(definition, "Filename")?.to_string(),
            size,
            sha256: get_required_field(definition, "SHA256")?.to_string(),
            version: get_optional_field(definition, "Version").map(str::to_string),
            depends: get_optional_field(definition, "Depends").map(str::to_string),
            provides: get_optional_field(definition, "Provides").map(str::to_string),
            package: get_optional_field(definition, "Package").map(str::to_string),
        })
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn parse() {
        let t: &str = "
Name: section 1
FieldA: nothing


Name: section 2
FieldB:
 same
 as 1
 but
 on many lines
FieldC: nothing


";
        let mut d = crate::lib::debian::format::indice::IndiceDocumentParser::new(t).unwrap();
        let first = d.next().unwrap();
        assert!(first.contains_key("Name"));
        assert!(first.contains_key("FieldA"));
        assert_eq!(first.get("Name").unwrap().as_str(), "section 1");
        assert_eq!(first.get("FieldA").unwrap().as_str(), "nothing");
        let second = d.next().unwrap();
        assert!(second.contains_key("Name"));
        assert!(second.contains_key("FieldB"));
        assert!(second.contains_key("FieldC"));
        assert_eq!(second.get("Name").unwrap().as_str(), "section 2");
        assert_eq!(
            second.get("FieldB").unwrap().as_str(),
            "same\nas 1\nbut\non many lines"
        );
        assert_eq!(second.get("FieldC").unwrap().as_str(), "nothing");
        assert_eq!(d.next(), None);
    }

    #[test]
    fn parse_packages() {
        let t: &str = "
Architecture: all
Build-Depends: inkscape, librsvg2-bin
Maintainer: Bert van der Weerd <bert@stanzabird.nl>
Package: librewolf
Priority: optional
Version: 120.0.1-1
Filename: pool/bullseye/toujours_le_meme.deb
MD5Sum: b954665f9df1a3738df788351fd026e4
SHA1: 220194dec48d36a14fae02395ef6d70532344d93
SHA256: d996f332a82a2b65740423d74472d6383cc71e836c3849c70e968e56d6d40514
SHA512: 3a9defaaffaeac829d1e7831997d143561927ebe329a9cd838b6da3faeb245f1153bf0362adb308620136712525ddd4719689f30b78ea051bca294d8a0718c58
Size: 74026630

Architecture: all
Build-Depends: inkscape, librsvg2-bin
Depends: lsb-release, libasound2 (>= 1.0.16)
Recommends: libcanberra0, libdbusmenu-glib4, libdbusmenu-gtk3-4
Priority: optional
Size: 73940054
Provides: gnome-www-browser, www-browser, x-www-browser
Section: web
Version: 120.0-1
Filename: pool/bullseye/toujours_le_meme.deb
MD5Sum: a68f3498db3e2bbb4080c9391a542520
SHA1: 25bf931892d10cb470392014ea099f23389a7b04
SHA256: e7b69d676b3f9ccabd4ed98fdf933b6bf270b9bb7c69e59816f4abb5d47c100e
SHA512: 004ed032fd28028dfb5ff23178680a3f42d5b0cd2eb2d039bc048668cc9d5bf9e213dd255a5aca74ecde4685849ad618dc715fb2ec0048dacf79d1ef89e4d344
";
        let d = crate::lib::debian::format::indice::IndiceDocumentParser::new(t).unwrap();
        let mut cnt = 0;
        for definition in d {
            cnt += 1;
            match crate::lib::debian::format::packages::DocumentDefinition::from_definition(
                &definition,
            ) {
                Ok(p) => {
                    assert_eq!(p.filename.as_str(), "pool/bullseye/toujours_le_meme.deb");
                }
                _ => assert!(false),
            }
        }
        assert_eq!(cnt, 2);
    }
}

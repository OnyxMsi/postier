//! See Debian
//! [documentation](https://www.debian.org/doc//debian-policy/ch-relationships.html)

use super::architecture;
use super::policy;
use super::version;
use crate::lib::database;
use crate::lib::debian;
use crate::lib::debian::storage::package;

#[derive(PartialEq, Eq, std::hash::Hash)]
pub(super) enum VersionOperator {
    Equal,
    Differ,
    Greater,
    GreaterEqual,
    Lower,
    LowerEqual,
}

impl VersionOperator {
    pub(super) fn test(&self, v1: &version::Version, v2: &version::Version) -> bool {
        match self {
            VersionOperator::Equal => v1 == v2,
            VersionOperator::Differ => v1 != v2,
            VersionOperator::Greater => v1 > v2,
            VersionOperator::GreaterEqual => v1 >= v2,
            VersionOperator::Lower => v1 < v2,
            VersionOperator::LowerEqual => v1 <= v2,
        }
    }
    pub(super) fn parse(o: &str) -> Self {
        match o {
            "=" => VersionOperator::Equal,
            "!=" => VersionOperator::Differ,
            ">>" => VersionOperator::Greater,
            ">=" => VersionOperator::GreaterEqual,
            "<<" => VersionOperator::Lower,
            "<=" => VersionOperator::LowerEqual,
            _ => unreachable!("Unknown version operator {}", o),
        }
    }
}
impl std::fmt::Display for VersionOperator {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            VersionOperator::Equal => "=",
            VersionOperator::Differ => "!=",
            VersionOperator::Greater => ">>",
            VersionOperator::GreaterEqual => ">=",
            VersionOperator::Lower => "<<",
            VersionOperator::LowerEqual => "<=",
        };
        write!(f, "{}", s)
    }
}

#[derive(PartialEq, Eq, std::hash::Hash)]
pub(super) enum ArchitectureOperator {
    Equal,
    Differ,
}
impl std::fmt::Display for ArchitectureOperator {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ArchitectureOperator::Equal => Ok(()),
            ArchitectureOperator::Differ => write!(f, "!"),
        }
    }
}

impl ArchitectureOperator {
    fn test(&self, a1: &architecture::Architecture, a2: &architecture::Architecture) -> bool {
        match self {
            ArchitectureOperator::Equal => a1.test(a2),
            ArchitectureOperator::Differ => !a1.test(a2),
        }
    }
}

#[derive(PartialEq, Eq, std::hash::Hash)]
pub(super) struct ArchitectureCheck<'s> {
    pub(super) operator: ArchitectureOperator,
    pub(super) architecture: architecture::Architecture<'s>,
}

impl<'s> ArchitectureCheck<'s> {
    fn test(&self, a: &architecture::Architecture) -> bool {
        self.operator.test(a, &self.architecture)
    }
}
impl std::fmt::Display for ArchitectureCheck<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}{}", self.operator, self.architecture)
    }
}
#[derive(PartialEq, Eq, std::hash::Hash)]
pub(super) struct VersionCheck<'s> {
    pub(super) operator: VersionOperator,
    pub(super) version: version::Version<'s>,
}

impl<'s> VersionCheck<'s> {
    fn test(&self, v: &version::Version) -> bool {
        self.operator.test(v, &self.version)
    }
}
impl std::fmt::Display for VersionCheck<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}{}", self.operator, self.version)
    }
}

#[derive(PartialEq, Eq, Hash)]
pub(super) struct Alternative<'s> {
    pub(super) package: &'s str,
    pub(super) version: Option<VersionCheck<'s>>,
    pub(super) architectures: std::vec::Vec<ArchitectureCheck<'s>>,
}
impl<'s> Alternative<'s> {
    pub fn test(
        &self,
        package: &str,
        version: &version::Version,
        architecture: &architecture::Architecture,
    ) -> bool {
        package == self.package
            && (self.version.is_none() || self.version.as_ref().is_some_and(|v| v.test(version)))
            && (self.architectures.len() == 0
                || self.architectures.iter().any(|c| c.test(architecture)))
    }
}
impl std::fmt::Display for Alternative<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.package)?;
        if self.version.is_some() {
            write!(f, " ({})", self.version.as_ref().unwrap())?;
        }
        if self.architectures.len() > 0 {
            write!(f, "[")?;
            for a in &self.architectures {
                write!(f, "{}", a)?;
            }
            write!(f, "]")?;
        }
        Ok(())
    }
}
pub struct Relationship<'s> {
    pub(super) alternatives: std::vec::Vec<Alternative<'s>>,
}

impl<'s> Relationship<'s> {
    pub fn parse(s: &'s str) -> Result<std::vec::Vec<Self>, debian::Error> {
        log::debug!("Parse relationships \"{}\"", s);
        let relationships_p = policy::parse(policy::Rule::relationships, s)?;
        Ok(relationships_p
            .into_inner()
            .map(policy::parse_relationship)
            .collect())
    }
    pub(in crate::lib::debian) fn test(
        &self,
        package: &str,
        version: &version::Version,
        architecture: &architecture::Architecture,
    ) -> bool {
        self.alternatives
            .iter()
            .any(|a| a.test(package, version, architecture))
    }
    /* Use the database to solve this relationship using a specific architecture */
    pub(in crate::lib::debian) fn solve(
        &self,
        db: &mut database::Database,
        architecture: &architecture::Architecture,
    ) -> Option<package::Entry> {
        for alternative in &self.alternatives {
            /* First is to find packages with this name */
            let packages = package::Entry::load(db, alternative.package).ok()?;
            /* Now filter to those who pass the alternative */
            let filtered = packages.iter().filter(|p| {
                let v = version::Version::parse(p.version.as_str());
                let a = architecture::Architecture::parse(p.architecture.as_str());
                if v.is_err() || a.is_err() {
                    log::warn!(
                        "Invalid version {} {} [{}]",
                        alternative.package,
                        p.version,
                        p.architecture
                    );
                    false
                } else {
                    let b = a.unwrap();
                    let c = v.unwrap();
                    b.test(architecture) && alternative.test(alternative.package, &c, &b)
                }
            });
            /* Get the one with the highest version */
            let r = filtered.max_by_key(|p| version::Version::parse(p.version.as_str()).unwrap());
            if r.is_some() {
                let s = r.unwrap();
                /* Not efficient but its a quick way to get rid of borrow */
                return Some(s.clone());
            }
        }
        None
    }
}
impl<'s> PartialEq for Relationship<'s> {
    fn eq(&self, other: &Self) -> bool {
        let mut z = std::iter::zip(&self.alternatives, &other.alternatives);
        self.alternatives.len() == other.alternatives.len() && z.all(|(a1, a2)| a1 == a2)
    }
}
impl<'s> Eq for Relationship<'s> {}

impl<'s> std::hash::Hash for Relationship<'s> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        for a in &self.alternatives {
            a.hash(state);
        }
    }
}

impl std::fmt::Display for Relationship<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for (idx, a) in self.alternatives.iter().enumerate() {
            if idx == self.alternatives.len() - 1 {
                write!(f, "{}", a)?;
            } else {
                write!(f, "{} | ", a)?;
            }
        }
        Ok(())
    }
}

#[cfg(test)]
mod test {
    fn p(s: &str) -> std::vec::Vec<super::Relationship> {
        super::Relationship::parse(s).unwrap()
    }
    #[test]
    fn parse() {
        let r = p("p (>> 1) [a o-b c], r|z[!c]");
        assert_eq!(r.len(), 2);
        let a1 = &r[0];
        assert_eq!(a1.alternatives.len(), 1);
        let a11 = &a1.alternatives[0];
        assert_eq!(a11.package, "p");
        assert!(a11.version.is_some());
        assert!(matches!(
            a11.version.as_ref().unwrap().operator,
            super::VersionOperator::Greater
        ));
        assert_eq!(a11.architectures.len(), 3);
        assert_eq!(a11.architectures.len(), 3);
        assert!(matches!(
            a11.architectures[0].operator,
            super::ArchitectureOperator::Equal
        ));
        assert!(a11.architectures[0].architecture.os.is_none());
        assert_eq!(a11.architectures[0].architecture.cpu, "a");
        assert!(matches!(
            a11.architectures[1].operator,
            super::ArchitectureOperator::Equal
        ));
        assert_eq!(a11.architectures[1].architecture.os, Some("o"));
        assert_eq!(a11.architectures[1].architecture.cpu, "b");
        assert!(matches!(
            a11.architectures[2].operator,
            super::ArchitectureOperator::Equal
        ));
        assert!(a11.architectures[2].architecture.os.is_none());
        assert_eq!(a11.architectures[2].architecture.cpu, "c");
        assert!(matches!(
            a11.architectures[0].operator,
            super::ArchitectureOperator::Equal
        ));
    }
    #[test]
    fn test() {
        fn t(d: &str, c: &str, v: &str, a: &str) -> bool {
            /* Works only on first relationship */
            let dep = p(d);
            let version = crate::lib::debian::format::version::Version::parse(v).unwrap();
            let arch = crate::lib::debian::format::architecture::Architecture::parse(a).unwrap();
            dep[0].test(c, &version, &arch)
        }
        assert!(t("a (= 1) [spec]", "a", "1", "any"));
        assert!(!t("a (>> 1) [spec]", "a", "1", "any"));
        assert!(!t("a (<< 1) [spec]", "a", "1", "any"));
        assert!(!t("a (= 1) [spec]", "a", "1", "nspec"));
        assert!(t("a (=1) [spec] | b", "b", "22", "spec"));
        assert!(t("a", "a", "22", "spec"));
    }
    #[test]
    fn eq() {
        assert!(p("a (=1) [s]") == p("a  (=1)   [s]"));
        assert!(p("a (=1) [s]") != p("a (=1) [!s]"))
    }
    #[test]
    fn solve() {
        fn solve(
            db: &mut crate::lib::database::Database,
            relationship: &str,
            architecture: &str,
        ) -> Option<crate::lib::debian::storage::package::Entry> {
            let r = crate::lib::debian::format::relationship::Relationship::parse(relationship)
                .unwrap();
            let a = crate::lib::debian::format::architecture::Architecture::parse(architecture)
                .unwrap();
            r.iter().map(|s| s.solve(db, &a)).find(|o| o.is_some())?
        }
        fn eq(
            package: &Option<crate::lib::debian::storage::package::Entry>,
            v: Option<(&str, &str)>,
        ) -> bool {
            match package {
                None => v.is_none(),
                Some(p) => match v {
                    None => false,
                    Some((pn, ve)) => {
                        let a = crate::lib::debian::format::version::Version::parse(&p.version)
                            .unwrap();
                        let b = crate::lib::debian::format::version::Version::parse(ve).unwrap();
                        a == b
                    }
                },
            }
        }
        fn save(
            db: &mut crate::lib::database::Database,
            package_name: &str,
            version: &str,
            architecture: &str,
        ) -> () {
            crate::lib::debian::storage::package::Entry::save(
                db,
                package_name,
                version,
                architecture,
                "http://www.perdu.com",
                &None,
                "aabbccdd",
            )
            .unwrap()
        }
        let mut db = crate::lib::database::test::get_db("test_debian_format_relationship_solve");
        save(&mut db, "first", "1.0", "linux-aarch64");
        save(&mut db, "first", "1.1", "linux-i386");
        let res = solve(&mut db, "first (<<2)", "any");
        assert!(eq(&res, Some(("first", "1.1"))));
        let res = solve(&mut db, "first (>= 2)", "any");
        assert!(eq(&res, None));
        let res = solve(&mut db, "second", "any");
        assert!(eq(&res, None));
        let res = solve(&mut db, "first", "amd64");
        assert!(eq(&res, None));
        let res = solve(&mut db, "first (<<2)", "aarch64");
        assert!(eq(&res, Some(("first", "1.0"))));
    }
}

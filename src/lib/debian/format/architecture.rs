//! See Debian
//! [documentation](https://www.debian.org/doc//debian-policy/ch-customized-programs.html#architecture-specification-strings)

use super::policy;
use crate::lib::debian;
use crate::lib::debian::format::policy::Rule;

pub struct Architecture<'s> {
    pub(super) os: Option<&'s str>,
    pub(super) cpu: &'s str,
}

const ANY_ARCH: &str = "any";

impl<'s> Architecture<'s> {
    pub fn parse(s: &'s str) -> Result<Self, debian::Error> {
        let pair = policy::parse(Rule::architecture, s)?;
        Ok(policy::parse_architecture(pair))
    }
    /* https://www.debian.org/doc//debian-policy/ch-customized-programs.html#architecture-specification-strings */
    pub(in crate::lib::debian) fn test(&self, other: &Self) -> bool {
        let sos = self.os.unwrap_or(ANY_ARCH);
        let oos = other.os.unwrap_or(ANY_ARCH);
        (sos == ANY_ARCH || oos == ANY_ARCH || sos == oos)
            && (self.cpu == ANY_ARCH || other.cpu == ANY_ARCH || self.cpu == other.cpu)
    }
}
impl std::fmt::Display for Architecture<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.os {
            Some(c) => write!(f, "{}-{}", c, self.cpu),
            None => write!(f, "{}", self.cpu),
        }
    }
}

impl<'s> PartialEq for Architecture<'s> {
    fn eq(&self, other: &Self) -> bool {
        self.os == other.os && self.cpu == other.cpu
    }
}
impl<'s> Eq for Architecture<'s> {}

impl<'s> std::hash::Hash for Architecture<'s> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.os.hash(state);
        self.cpu.hash(state);
    }
}

#[cfg(test)]
mod test {
    fn a<'s>(s: &'s str) -> super::Architecture<'s> {
        super::Architecture::parse(s).unwrap()
    }
    #[test]
    fn parse() {
        assert!(a("any").os.is_none());
        assert_eq!(a("any").cpu, "any");
        assert!(a("any-any").os.is_some_and(|s| s == "any"), "any");
        assert_eq!(a("any-any").cpu, "any");
    }
    #[test]
    fn test() {
        assert!(a("any").test(&a("linux-amd64")));
        assert!(a("any").test(&a("linux-amd64")));
        assert!(a("any").test(&a("macos-i386")));
        assert!(a("any-amd64").test(&a("macos-amd64")));
        assert!(a("any-amd64").test(&a("linux-amd64")));
        assert!(!a("any-amd64").test(&a("linux-aarch64")));
        assert!(a("linux-any").test(&a("linux-amd64")));
        assert!(a("linux-any").test(&a("linux-i386")));
        assert!(!a("linux-any").test(&a("macos-i386")));
    }
    #[test]
    fn eq() {
        assert!(a("any") != a("linux-amd64"));
        assert!(a("amd64") == a("amd64"));
        assert!(a("darwin-amd64") == a("darwin-amd64"));
    }
}

use super::policy;
use crate::lib::debian;
use std::cmp::Ordering;

#[derive(PartialEq, Eq, std::hash::Hash)]
pub(super) struct UpstreamVersionSegment<'s> {
    pub(super) non_digit_segment: Option<&'s str>,
    pub(super) digit_segment: Option<usize>,
}
impl std::fmt::Display for UpstreamVersionSegment<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.non_digit_segment.is_some() {
            write!(f, "{}", self.non_digit_segment.unwrap())?;
        }
        if self.digit_segment.is_some() {
            write!(f, "{}", self.digit_segment.unwrap())?;
        }
        Ok(())
    }
}

/* See https://www.debian.org/doc/debian-policy/ch-controlfields.html#version */
/* From a character, return a indice that will be used in a comparaison */
fn debian_crazy_ascii_converter(co: Option<char>) -> i32 {
    match co {
        None => 1,
        Some(c) => {
            /* Tilde is lower than everything, even None */
            if c == '~' {
                0
            }
            /* Letter sort before than the rest */
            else if c.is_ascii_alphabetic() {
                c as i32
            } else {
                (c as i32) << 8
            }
        }
    }
}
fn debian_crazy_ascii_compare(s1: &str, s2: &str) -> std::cmp::Ordering {
    use itertools::Itertools;
    s1.chars()
        .zip_longest(s2.chars())
        .map(|pair| {
            let (c1, c2) = match pair {
                itertools::EitherOrBoth::Both(c1, c2) => (Some(c1), Some(c2)),
                itertools::EitherOrBoth::Left(c1) => (Some(c1), None),
                itertools::EitherOrBoth::Right(c2) => (None, Some(c2)),
            };
            debian_crazy_ascii_converter(c1).cmp(&debian_crazy_ascii_converter(c2))
        })
        .find(|c| c != &std::cmp::Ordering::Equal)
        .unwrap_or(std::cmp::Ordering::Equal)
}
impl<'s> Ord for UpstreamVersionSegment<'s> {
    fn cmp(&self, other: &Self) -> Ordering {
        let n1 = self.non_digit_segment.unwrap_or("");
        let n2 = self.non_digit_segment.unwrap_or("");
        let d1 = self.digit_segment.unwrap_or(0);
        let d2 = other.digit_segment.unwrap_or(0);
        debian_crazy_ascii_compare(n1, n2).then(d1.cmp(&d2))
    }
}
impl<'s> PartialOrd for UpstreamVersionSegment<'s> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, std::hash::Hash)]
pub struct Version<'s> {
    pub(super) epoch: usize,
    pub(super) upstream_version: std::vec::Vec<UpstreamVersionSegment<'s>>,
    pub(super) debian_revision: std::vec::Vec<UpstreamVersionSegment<'s>>,
}
impl<'s> Version<'s> {
    pub fn parse(s: &'s str) -> Result<Self, debian::Error> {
        log::debug!("Parse version {}", s);
        let pairs = policy::parse(policy::Rule::version, s)?;
        Ok(policy::parse_version(pairs))
    }
}
impl std::fmt::Display for Version<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.epoch != 0 {
            write!(f, "{}:", self.epoch)?;
        }
        for u in &self.upstream_version {
            write!(f, "{}", u)?;
        }
        if self.debian_revision.len() > 0 {
            write!(f, "-")?;
            for u in &self.debian_revision {
                write!(f, "{}", u)?;
            }
        }
        Ok(())
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn parse() {
        let v = super::Version::parse("1.2.3").unwrap();
        assert_eq!(v.epoch, 0);
        assert_eq!(v.debian_revision.len(), 0);
        assert_eq!(v.upstream_version.len(), 3);
        assert_eq!(v.upstream_version[0].non_digit_segment, None);
        assert_eq!(v.upstream_version[0].digit_segment, Some(1));
        assert_eq!(v.upstream_version[1].non_digit_segment, Some("."));
        assert_eq!(v.upstream_version[1].digit_segment, Some(2));
        assert_eq!(v.upstream_version[2].non_digit_segment, Some("."));
        assert_eq!(v.upstream_version[2].digit_segment, Some(3));
        assert_eq!(format!("{}", v), "1.2.3");
        let v = super::Version::parse("51:2.3+really2.2-a1").unwrap();
        assert_eq!(v.epoch, 51);
        assert_eq!(v.upstream_version.len(), 4);
        assert_eq!(v.upstream_version[0].non_digit_segment, None);
        assert_eq!(v.upstream_version[0].digit_segment, Some(2));
        assert_eq!(v.upstream_version[1].non_digit_segment, Some("."));
        assert_eq!(v.upstream_version[1].digit_segment, Some(3));
        assert_eq!(v.upstream_version[2].non_digit_segment, Some("+really"));
        assert_eq!(v.upstream_version[2].digit_segment, Some(2));
        assert_eq!(v.upstream_version[3].non_digit_segment, Some("."));
        assert_eq!(v.upstream_version[3].digit_segment, Some(2));
        assert_eq!(v.debian_revision.len(), 1);
        assert_eq!(v.debian_revision[0].digit_segment, Some(1));
        assert_eq!(v.debian_revision[0].non_digit_segment, Some("a"));
        assert_eq!(format!("{}", v), "51:2.3+really2.2-a1");
    }
    #[test]
    fn crazy_ascii_compare() {
        assert_eq!(
            super::debian_crazy_ascii_compare("~~", "~~"),
            std::cmp::Ordering::Equal
        );
        assert_eq!(
            super::debian_crazy_ascii_compare("~~", "~~a"),
            std::cmp::Ordering::Less
        );
        assert_eq!(
            super::debian_crazy_ascii_compare("~~a", "~"),
            std::cmp::Ordering::Less
        );
        assert_eq!(
            super::debian_crazy_ascii_compare("~", ""),
            std::cmp::Ordering::Less
        );
        assert_eq!(
            super::debian_crazy_ascii_compare("", "a"),
            std::cmp::Ordering::Less
        );
        assert_eq!(
            super::debian_crazy_ascii_compare("~~a", "~~"),
            std::cmp::Ordering::Greater
        );
        assert_eq!(
            super::debian_crazy_ascii_compare("~", "~~a"),
            std::cmp::Ordering::Greater
        );
        assert_eq!(
            super::debian_crazy_ascii_compare("", "~"),
            std::cmp::Ordering::Greater
        );
        assert_eq!(
            super::debian_crazy_ascii_compare("a", ""),
            std::cmp::Ordering::Greater
        );
    }
    #[test]
    fn compare() {
        let v = |s| super::Version::parse(s).unwrap();
        assert!(v("1.0.0") == v("1.0.0"));
        assert!(v("1.0.0") != v("1.0.1"));
        assert!(v("1.0.1") > v("1.0.0"));
        assert!(v("1.0.0") < v("1.0.1"));
        assert!(v("1:1") > v("1.0.1"));
        assert!(v("2:1") > v("1:1"));
        assert!(v("1.4-5") < v("1.4-5+deb10u1"));
        assert!(v("1.4-5+deb10u1") < v("1.4-5+deb10u2"));
    }
}

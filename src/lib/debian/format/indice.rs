//! See Debian
//! [documentation](https://www.debian.org/doc/debian-policy/ch-controlfields.html)
use crate::lib::debian;
use pest::iterators::Pairs;
use pest::Parser;
use pest_derive::Parser;

#[derive(Parser)]
#[grammar = "lib/debian/grammar/indice.pest"]
pub struct IParser;

/* Consider an Indice document section (ie definition) as a hash table
 * Values are to be String because it can be different from what is in the
 * document (multiline escaping)
 */
pub type DocumentDefinition<'a> = std::collections::HashMap<&'a str, String>;
/* A shortcut */
pub fn get_required_field<'a>(
    definition: &'a DocumentDefinition,
    name: &str,
) -> Result<&'a str, debian::Error> {
    match definition.get(name) {
        Some(s) => Ok(s),
        None => {
            log::error!("{}: Can't find required field", name);
            Err(debian::Error::IndiceParseError)
        }
    }
}
/* An even stupidier shortcut, I don't understand why its required */
pub fn get_optional_field<'a>(def: &'a DocumentDefinition, name: &str) -> Option<&'a str> {
    match def.get(name) {
        Some(s) => Some(s),
        None => None,
    }
}
/* IndiceDocumentParser is to be used to parse a string as an indice document
 * Its an iterator of DocumentDefinition(s)
 */
pub struct IndiceDocumentParser<'a> {
    definitions_pairs: Pairs<'a, crate::lib::debian::format::indice::Rule>,
}
impl<'a> IndiceDocumentParser<'a> {
    pub fn new(source: &'a str) -> Result<IndiceDocumentParser<'a>, debian::Error> {
        let definitions_pairs = match IParser::parse(Rule::definitions, source) {
            Err(e) => {
                log::error!(
                    "[Debian] Failure to parse {} character as a indice document: {}",
                    source.len(),
                    e
                );
                return Err(debian::Error::IndiceParseError);
            }
            Ok(mut r) => r.next().unwrap().into_inner(),
        };
        Ok(IndiceDocumentParser { definitions_pairs })
    }
    fn get_next_definition(&mut self) -> Option<DocumentDefinition<'a>> {
        /* Read pairs until a new definition starts, or the end of the input is reached
         * then return the last definition
         */
        loop {
            match self.definitions_pairs.next() {
                Some(next) => {
                    /* If this is a definition parse it, then return it */
                    match next.as_rule() {
                        Rule::definition => {
                            let mut new_def = DocumentDefinition::new();
                            for field in next.into_inner() {
                                match field.as_rule() {
                                    Rule::field => {
                                        let mut inner_field = field.into_inner();
                                        let name = inner_field.next().unwrap().as_str();
                                        let value_fields = inner_field.next().unwrap();
                                        let mut value = String::new();
                                        for value_field in value_fields.into_inner() {
                                            match value_field.as_rule() {
                                                Rule::value_segment => {
                                                    if !value.is_empty() {
                                                        value.push('\n');
                                                    }
                                                    value.push_str(value_field.as_str());
                                                }
                                                _ => {}
                                            }
                                        }
                                        new_def.insert(name, value);
                                    }
                                    _ => {}
                                }
                            }
                            return Some(new_def);
                        }
                        _ => {}
                    }
                }
                /* End of it */
                None => return None,
            }
        }
    }
}
impl<'a> Iterator for IndiceDocumentParser<'a> {
    type Item = DocumentDefinition<'a>;
    fn next(&mut self) -> Option<DocumentDefinition<'a>> {
        self.get_next_definition()
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn parse() {
        let t: &str = "
Name: section 1
FieldA: nothing


Name: section 2
FieldB:
 same
 as 1
 but
 on many lines
FieldC: nothing


";
        let mut d = crate::lib::debian::format::indice::IndiceDocumentParser::new(t).unwrap();
        let first = d.next().unwrap();
        assert!(first.contains_key("Name"));
        assert!(first.contains_key("FieldA"));
        assert_eq!(first.get("Name").unwrap().as_str(), "section 1");
        assert_eq!(first.get("FieldA").unwrap().as_str(), "nothing");
        let second = d.next().unwrap();
        assert!(second.contains_key("Name"));
        assert!(second.contains_key("FieldB"));
        assert!(second.contains_key("FieldC"));
        assert_eq!(second.get("Name").unwrap().as_str(), "section 2");
        assert_eq!(
            second.get("FieldB").unwrap().as_str(),
            "same\nas 1\nbut\non many lines"
        );
        assert_eq!(second.get("FieldC").unwrap().as_str(), "nothing");
        assert_eq!(d.next(), None);
    }
}

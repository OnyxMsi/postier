use super::architecture;
use super::relationship;
use super::version;
use crate::lib::debian::Error;
use pest::iterators::Pair;
use pest::Parser;
use pest_derive::Parser;

#[derive(Parser)]
#[grammar = "lib/debian/grammar/policy.pest"]
pub(super) struct PParser;

/* Rule architecture */
pub(super) fn parse_architecture<'s>(pair: Pair<'s, Rule>) -> architecture::Architecture<'s> {
    let mut os = None;
    let mut cpu = None;
    /* Get each field of an architecture */
    for architecture_pair in pair.into_inner() {
        match architecture_pair.as_rule() {
            Rule::os => {
                os = Some(architecture_pair.as_str());
            }
            Rule::cpu => {
                cpu = Some(architecture_pair.as_str());
            }
            _ => {}
        }
    }
    architecture::Architecture {
        os,
        cpu: cpu.unwrap(),
    }
}
/* Rule alternative_architecture */
pub(super) fn parse_alternative_architecture<'s>(
    pair: Pair<'s, Rule>,
) -> relationship::ArchitectureCheck<'s> {
    let mut operator = relationship::ArchitectureOperator::Equal;
    let mut architecture = None;
    /* Get each field of an architecture check */
    for alternative_architecture_p in pair.into_inner() {
        match alternative_architecture_p.as_rule() {
            Rule::not_operator => {
                operator = relationship::ArchitectureOperator::Differ;
            }
            Rule::architecture => {
                architecture = Some(parse_architecture(alternative_architecture_p));
            }
            _ => {}
        }
    }
    relationship::ArchitectureCheck {
        operator,
        architecture: architecture.unwrap(),
    }
}

/* Rule alternative_version */
pub(super) fn parse_alternative_version<'s>(
    pair: Pair<'s, Rule>,
) -> relationship::VersionCheck<'s> {
    let mut operator = None;
    let mut v = None;
    /* Parse a version check of an alternative */
    for alternative_version_p in pair.into_inner() {
        match alternative_version_p.as_rule() {
            Rule::operator => {
                operator = Some(relationship::VersionOperator::parse(
                    alternative_version_p.as_str(),
                ));
            }
            Rule::version => {
                v = Some(parse_version(alternative_version_p));
            }
            _ => {}
        }
    }
    relationship::VersionCheck {
        operator: operator.unwrap(),
        version: v.unwrap(),
    }
}

/* Rule alternative */
pub(super) fn parse_alternative<'s>(pair: Pair<'s, Rule>) -> relationship::Alternative<'s> {
    let mut package = None;
    let mut version = None;
    let mut architectures = std::vec::Vec::new();
    /* Get each field of the alternative */
    for alternative_p in pair.into_inner() {
        match alternative_p.as_rule() {
            Rule::package => {
                package = Some(alternative_p.as_str());
            }
            Rule::alternative_version => {
                version = Some(parse_alternative_version(alternative_p));
            }
            Rule::alternative_architectures => {
                for alternative_architecture_p in alternative_p.into_inner() {
                    architectures.push(parse_alternative_architecture(alternative_architecture_p));
                }
            }
            _ => {}
        }
    }
    relationship::Alternative {
        package: package.unwrap(),
        version,
        architectures,
    }
}

/* Rule relationship */
pub(super) fn parse_relationship<'s>(pair: Pair<'s, Rule>) -> relationship::Relationship<'s> {
    let alternatives = pair.into_inner().map(parse_alternative).collect();
    relationship::Relationship { alternatives }
}

/* Rule version */
pub(super) fn parse_version<'s>(pair: Pair<'s, Rule>) -> version::Version<'s> {
    let mut epoch = 0;
    let mut upstream_version = std::vec::Vec::new();
    let mut debian_revision = std::vec::Vec::new();
    for version_p in pair.into_inner() {
        match version_p.as_rule() {
            Rule::epoch => {
                epoch = version_p.as_str().parse::<usize>().unwrap();
            }
            Rule::upstream_version => {
                for upstream_version_segment_p in version_p.into_inner() {
                    upstream_version
                        .push(parse_upstream_version_segment(upstream_version_segment_p));
                }
            }
            Rule::debian_revision => {
                for debian_revision_segment_p in version_p.into_inner() {
                    debian_revision.push(parse_upstream_version_segment(debian_revision_segment_p));
                }
            }
            _ => {}
        }
    }
    version::Version {
        epoch,
        upstream_version,
        debian_revision,
    }
}

/* Rule upstream_version_segment */
pub(super) fn parse_upstream_version_segment(pairs: Pair<Rule>) -> version::UpstreamVersionSegment {
    let mut non_digit_segment = None;
    let mut digit_segment = None;
    for segment_pair in pairs.into_inner() {
        match segment_pair.as_rule() {
            Rule::non_digit_segment => {
                non_digit_segment = Some(segment_pair.as_str());
            }
            Rule::debian_revision_non_digit_segment => {
                non_digit_segment = Some(segment_pair.as_str());
            }
            Rule::digit_segment => {
                digit_segment = Some(segment_pair.as_str().parse::<usize>().unwrap());
            }
            _ => {}
        }
    }
    version::UpstreamVersionSegment {
        non_digit_segment,
        digit_segment,
    }
}

pub(super) fn parse(rule: Rule, s: &str) -> Result<Pair<Rule>, Error> {
    Ok(PParser::parse(rule, s.trim())
        .map_err(|e| {
            log::error!("Invalid debian policy string \"{}\": {}", s, e);
            Error::PolicyParseError
        })?
        .next()
        .unwrap())
}

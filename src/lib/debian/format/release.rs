//! See Debian
//! [documentation](https://wiki.debian.org/DebianRepository/Format#A.22Release.22_files)
use crate::lib::debian;
use crate::lib::debian::format::indice::{
    get_optional_field, get_required_field, DocumentDefinition as IDocumentDefinition,
};
use std::vec::Vec;
pub struct IndexElementDefinition {
    pub checksum: String,
    pub size: usize,
    pub filename: String,
}
impl IndexElementDefinition {
    fn from_string(line: &str) -> Result<IndexElementDefinition, debian::Error> {
        let items: Vec<&str> = line.split_ascii_whitespace().collect();
        if items.len() != 3 {
            log::error!(
                "Invalid index element definition \"{}\": not 3 items (found {})",
                line,
                items.len()
            );
            return Err(debian::Error::InvalidContentError);
        }
        let checksum = items[0].to_string();
        let size = items[1].parse::<usize>().map_err(|_e| {
            log::error!(
                "Invalid index element definition {}: invalid size {}",
                line,
                items[1]
            );
            debian::Error::InvalidContentError
        })?;
        let filename = items[2].to_string();
        Ok(IndexElementDefinition {
            checksum,
            size,
            filename,
        })
    }
}
pub struct DocumentDefinition {
    /* Mandatory */
    pub architectures: Vec<String>,
    pub components: Vec<String>,
    pub date: String,
    pub indexes: Vec<IndexElementDefinition>,
    /* Optionals */
    pub description: Option<String>,
    pub origin: Option<String>,
    pub label: Option<String>,
    pub suite: Option<String>, // Either suite or codename or both
    pub codename: Option<String>,
}
impl DocumentDefinition {
    pub fn from_definition(
        definition: &IDocumentDefinition,
    ) -> Result<DocumentDefinition, debian::Error> {
        let components = get_required_field(definition, "Components")?
            .split(' ')
            .map(str::to_string)
            .collect();
        let architectures = get_required_field(definition, "Architectures")?
            .split(' ')
            .map(str::to_string)
            .collect();
        let iter = get_required_field(definition, "SHA256")?
            .split('\n')
            .map(IndexElementDefinition::from_string);
        let indexes_r: Result<Vec<IndexElementDefinition>, debian::Error> = iter.collect();
        Ok(DocumentDefinition {
            components,
            architectures,
            indexes: indexes_r?,
            date: get_required_field(definition, "Date")?.to_string(),
            description: get_optional_field(definition, "Description").map(str::to_string),
            origin: get_optional_field(definition, "Origin").map(str::to_string),
            label: get_optional_field(definition, "Label").map(str::to_string),
            suite: get_optional_field(definition, "Suite").map(str::to_string),
            codename: get_optional_field(definition, "Codename").map(str::to_string),
        })
    }
    /* Retreive an index from an URL */
    pub fn get_index_by_url(&self, url: &url::Url) -> Option<&IndexElementDefinition> {
        /* Note that this is not the safest way of doing that
         * Because index filename is not a full URL, it is simpler just to compare the end
         * of the URL path
         */
        self.indexes
            .iter()
            .find(|i| url.path().ends_with(i.filename.as_str()))
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn parse_release() {
        let t: &str = "
Origin: Debian
Label: Debian-Security
Suite: oldstable-security
Version: 11
Codename: bullseye-security
Date: Mon, 04 Mar 2024 08:13:26 UTC
Valid-Until: Mon, 11 Mar 2024 08:13:26 UTC
Acquire-By-Hash: yes
Architectures: amd64 arm64
Components: updates/main updates/contrib updates/non-free
SHA256:
 e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855        0 contrib/Contents-amd64
 f61f27bd17de546264aa58f40f3aafaac7021e0ef69c17f6b1b4cd7664a037ec       20 contrib/Contents-amd64.gz
 e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855        0 contrib/Contents-arm64
 f61f27bd17de546264aa58f40f3aafaac7021e0ef69c17f6b1b4cd7664a037ec       20 contrib/Contents-arm64.gz
";
        let mut d = crate::lib::debian::format::indice::IndiceDocumentParser::new(t).unwrap();
        let definition = d.next().unwrap();
        let release =
            crate::lib::debian::format::release::DocumentDefinition::from_definition(&definition)
                .unwrap();
        assert_eq!(release.date, "Mon, 04 Mar 2024 08:13:26 UTC");
        assert_eq!(release.codename, Some("bullseye-security".to_string()));
        assert_eq!(release.description, None);
        assert_eq!(release.architectures.len(), 2);
        assert!(release.architectures.contains(&"amd64".to_string()));
        assert!(release.architectures.contains(&"arm64".to_string()));
        assert_eq!(release.indexes.len(), 4);
        let index1 = &release.indexes[1];
        assert_eq!(
            index1.checksum,
            "f61f27bd17de546264aa58f40f3aafaac7021e0ef69c17f6b1b4cd7664a037ec"
        );
        assert_eq!(index1.size, 20);
        assert_eq!(index1.filename, "contrib/Contents-amd64.gz");
        /* Test to retreive URL */
        assert!(release
            .get_index_by_url(
                &url::Url::parse("http://some.where/contrib/Contents-amd64.gz").unwrap()
            )
            .is_some());
        assert!(release
            .get_index_by_url(
                &url::Url::parse("http://some.where/contrib/Contents-i386.gz").unwrap()
            )
            .is_none());
        /* Same thing but with missing stuff */
        let t: &str = r#"
Origin: Debian
Label: Debian-Security
Suite: oldstable-security
Version: 11
Codename: bullseye-security
Date: Mon, 04 Mar 2024 08:13:26 UTC
Valid-Until: Mon, 11 Mar 2024 08:13:26 UTC
Acquire-By-Hash: yes
Components: updates/main updates/contrib updates/non-free
"#;
        let mut d = crate::lib::debian::format::indice::IndiceDocumentParser::new(t).unwrap();
        let definition = d.next().unwrap();
        match crate::lib::debian::format::release::DocumentDefinition::from_definition(&definition)
        {
            Err(_) => assert!(true),
            _ => assert!(false),
        };
    }
}

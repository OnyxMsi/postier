/// <https://github.com/npm/registry/blob/main/docs/REGISTRY-API.md>
use crate::lib::{
    npm,
    utils::{self, reqwest_send},
};
use serde::Deserialize;

/// <https://github.com/npm/registry/blob/main/docs/REGISTRY-API.md#package>
#[derive(Deserialize)]
struct Package {
    name: String,
    versions: std::vec::Vec<PackageVersion>,
}

#[derive(Deserialize)]
struct PackageVersion {
    name: String,
    version: String,
    dependencies: std::collections::HashMap<String, String>,
}

struct Registry<'r> {
    client: &'r reqwest::blocking::Client,
    url: &'r url::Url,
}

struct V1SearchParameters<'r> {
    text: Option<&'r str>,
    size: Option<usize>,
    from: Option<usize>,
    quality: Option<f64>,
    popularity: Option<f64>,
    maintenance: Option<f64>,
}
impl<'r> V1SearchParameters<'r> {
    fn new() -> Self {
        V1SearchParameters {
            text: None,
            size: None,
            from: None,
            quality: None,
            popularity: None,
            maintenance: None,
        }
    }
    fn text(mut self, text: &'r str) -> Self {
        self.text = Some(text);
        self
    }
    fn size(mut self, size: usize) -> Self {
        self.size = Some(size);
        self
    }
    fn from(mut self, from: usize) -> Self {
        self.from = Some(from);
        self
    }
    fn quality(mut self, quality: f64) -> Self {
        self.quality = Some(quality);
        self
    }
    fn popularity(mut self, popularity: f64) -> Self {
        self.popularity = Some(popularity);
        self
    }
    fn maintenance(mut self, maintenance: f64) -> Self {
        self.maintenance = Some(maintenance);
        self
    }
}
#[derive(Deserialize)]
struct V1SearchPackage {
    name: String,
    version: String,
}
#[derive(Deserialize)]
struct V1SearchObjects {
    packages: std::vec::Vec<V1SearchPackage>,
}
#[derive(Deserialize)]
struct V1SearchResult {
    objects: std::vec::Vec<V1SearchObjects>,
}

macro_rules! ujoin {
    ($u:expr, $s:expr) => {
        crate::lib::utils::ujoin($u, $s).map_err(npm::Error::LibError)
    };
}
macro_rules! reqwest_send {
    ($r:expr) => {
        crate::lib::utils::reqwest_send($r).map_err(npm::Error::LibError)
    };
}

impl<'r> Registry<'r> {
    pub fn new(client: &'r reqwest::blocking::Client, url: &'r url::Url) -> Self {
        Registry { client, url }
    }
    pub fn get_package(&self, package: &str) -> Result<Package, npm::Error> {
        log::debug!("Retreive package {}", package);
        let u = ujoin!(self.url, &format!("/{}", package))?;
        let p: Package = reqwest_send!(self.client.get(u))?.json().map_err(|e| {
            log::error!("Can't deserialize package {}: {}", package, e);
            npm::Error::RegistryError
        })?;
        Ok(p)
    }
    pub fn get_package_version(
        &self,
        package: &str,
        version: &str,
    ) -> Result<PackageVersion, npm::Error> {
        log::debug!("Retreive package {} version {}", package, version);
        let u = ujoin!(self.url, &format!("/{}/{}", package, version))?;
        let p: PackageVersion = reqwest_send!(self.client.get(u))?.json().map_err(|e| {
            log::error!(
                "Can't deserialize package {} version {}: {}",
                package,
                version,
                e
            );
            npm::Error::RegistryError
        })?;
        Ok(p)
    }
    pub fn v1_search(
        &self,
        parameters: V1SearchParameters,
    ) -> Result<std::vec::Vec<V1SearchObjects>, npm::Error> {
        let u = ujoin!(self.url, "/-/v1/search")?;
        let mut b = self.client.get(u);
        if let Some(text) = parameters.text {
            b = b.query(&["text", text]);
        }
        if let Some(size) = parameters.size {
            b = b.query(&["size", &format!("{}", size)]);
        }
        if let Some(from) = parameters.from {
            b = b.query(&["from", &format!("{}", from)]);
        }
        if let Some(quality) = parameters.quality {
            b = b.query(&["quality", &format!("{}", quality)]);
        }
        if let Some(popularity) = parameters.popularity {
            b = b.query(&["popularity", &format!("{}", popularity)]);
        }
        if let Some(maintenance) = parameters.maintenance {
            b = b.query(&["maintenance", &format!("{}", maintenance)]);
        }
        let r: V1SearchResult = reqwest_send!(b)?.json().map_err(|e| {
            log::error!("Can't deserialize search results: {}", e);
            npm::Error::RegistryError
        })?;
        Ok(r.objects)
    }
}

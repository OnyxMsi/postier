use crate::lib::npm;

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
pub(super) enum PartialVersionField {
    Any,
    Literal(usize),
}

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
pub(super) struct PartialVersion<'v> {
    pub(super) major: PartialVersionField,
    pub(super) minor: PartialVersionField,
    pub(super) patch: PartialVersionField,
    pub(super) pre: std::vec::Vec<super::version::PreReleaseSegment<'v>>,
}

impl<'v> PartialVersion<'v> {
    pub fn parse(text: &'v str) -> Result<Self, npm::Error> {
        super::packages::parse(
            super::packages::Rule::partial_semver,
            text,
            super::packages::parse_partial_semver,
        )
    }
}
/* https://github.com/npm/node-semver?tab=readme-ov-file#x-ranges-12x-1x-12- */
fn test_x_range(
    partial: &PartialVersion,
    version: &super::version::Version,
    options: &super::dependency::Options,
) -> bool {
    /* If major is "any", then every version pass (even if the other fields are not "any", see https://semver.npmjs.com/)*/
    match partial.major {
        PartialVersionField::Any => version.pre.len() == 0 || options.include_pre_release,
        PartialVersionField::Literal(major) => {
            major == version.major
                && match partial.minor {
                    PartialVersionField::Any => true,
                    PartialVersionField::Literal(minor) => {
                        minor == version.minor
                            && match partial.patch {
                                PartialVersionField::Any => true,
                                PartialVersionField::Literal(patch) => {
                                    /* In this case also compare the prerelease */
                                    patch == version.patch && partial.pre == version.pre
                                }
                            }
                    }
                }
        }
    }
}

/* Just a quick way to define a static version */
macro_rules! static_version {
    ($major:expr) => {
        super::version::Version {
            major: $major,
            minor: 0,
            patch: 0,
            pre: [].to_vec(),
        }
    };
    ($major:expr, $minor:expr) => {
        super::version::Version {
            major: $major,
            minor: $minor,
            patch: 0,
            pre: [].to_vec(),
        }
    };
    ($major:expr, $minor:expr, $patch:expr) => {
        super::version::Version {
            major: $major,
            minor: $minor,
            patch: $patch,
            pre: [].to_vec(),
        }
    };
    ($major:expr, $minor:expr, $patch:expr, $pre:expr) => {
        super::version::Version {
            major: $major,
            minor: $minor,
            patch: $patch,
            pre: $pre.to_vec(),
        }
    };
}

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
pub(super) enum RangePrimitiveOperator {
    LessEqual,
    Less,
    GreaterEqual,
    Greater,
    Equal,
}
#[derive(Debug, Hash, PartialEq, Eq, Clone)]
pub(super) struct RangePrimitive<'v> {
    pub(super) operator: RangePrimitiveOperator,
    pub(super) version: super::version::Version<'v>,
}
impl RangePrimitive<'_> {
    pub fn test(
        &self,
        version: &super::version::Version,
        options: &super::dependency::Options,
    ) -> bool {
        match self.operator {
            RangePrimitiveOperator::Equal => version == &self.version,
            RangePrimitiveOperator::Less => version < &self.version,
            RangePrimitiveOperator::LessEqual => version <= &self.version,
            RangePrimitiveOperator::Greater => version > &self.version,
            RangePrimitiveOperator::GreaterEqual => version >= &self.version,
        }
    }
}

/* https://github.com/npm/node-semver?tab=readme-ov-file#tilde-ranges-123-12-1 */
fn test_tilde_range(
    partial: &PartialVersion,
    version: &super::version::Version,
    options: &super::dependency::Options,
) -> bool {
    let (min_v, max_v) = match partial.major {
        PartialVersionField::Any => return true,
        PartialVersionField::Literal(major) => match partial.minor {
            PartialVersionField::Any => (static_version!(major), static_version!(major + 1)),
            PartialVersionField::Literal(minor) => match partial.patch {
                PartialVersionField::Any => (
                    static_version!(major, minor),
                    static_version!(major, minor + 1),
                ),
                PartialVersionField::Literal(patch) => (
                    static_version!(major, minor, patch, partial.pre),
                    static_version!(major, minor + 1),
                ),
            },
        },
    };
    /* If version is a pre-release then it must be the same major minor patch as min version */
    if version.pre.len() > 0 {
        version.major == min_v.major
            && version.minor == min_v.minor
            && version.patch == min_v.patch
            && version.pre >= min_v.pre
    } else {
        &min_v <= version && version < &max_v
    }
}
/* https://github.com/npm/node-semver#caret-ranges-123-025-004 */
fn test_caret_range(
    partial: &PartialVersion,
    version: &super::version::Version,
    options: &super::dependency::Options,
) -> bool {
    let (min_v, max_v) = match partial.major {
        PartialVersionField::Any => return true,
        PartialVersionField::Literal(major) => match partial.minor {
            PartialVersionField::Any => (static_version!(major), static_version!(major + 1)),
            PartialVersionField::Literal(minor) => match partial.patch {
                PartialVersionField::Any => {
                    let min_v = static_version!(major, minor);
                    if major > 0 {
                        (min_v, static_version!(major + 1))
                    } else {
                        (min_v, static_version!(major, minor + 1))
                    }
                }
                PartialVersionField::Literal(patch) => {
                    let min_v = static_version!(major, minor, patch, partial.pre);
                    if major > 0 {
                        (min_v, static_version!(major + 1))
                    } else if minor > 0 {
                        (min_v, static_version!(major, minor + 1))
                    } else {
                        (min_v, static_version!(major, minor, patch + 1))
                    }
                }
            },
        },
    };
    /* If version is a pre-release then it must be the same major minor patch as min version */
    if version.pre.len() > 0 {
        version.major == min_v.major
            && version.minor == min_v.minor
            && version.patch == min_v.patch
            && version.pre >= min_v.pre
    } else {
        &min_v <= version && version < &max_v
    }
}

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
pub(super) enum RangeSimple<'v> {
    Primitive(RangePrimitive<'v>),
    Partial(PartialVersion<'v>),
    Caret(PartialVersion<'v>),
    Tilde(PartialVersion<'v>),
}
impl RangeSimple<'_> {
    pub fn test(
        &self,
        version: &super::version::Version,
        options: &super::dependency::Options,
    ) -> bool {
        match self {
            RangeSimple::Primitive(p) => p.test(version, options),
            RangeSimple::Partial(p) => test_x_range(p, version, options),
            RangeSimple::Caret(p) => test_caret_range(p, version, options),
            RangeSimple::Tilde(p) => test_tilde_range(p, version, options),
        }
    }
}

/* https://github.com/npm/node-semver#hyphen-ranges-xyz---abc */
fn test_hyphen_range(
    min: &PartialVersion,
    max: &PartialVersion,
    version: &super::version::Version,
    options: &super::dependency::Options,
) -> bool {
    let min_v = static_version!(
        match min.major {
            PartialVersionField::Any => 0,
            PartialVersionField::Literal(n) => n,
        },
        match min.minor {
            PartialVersionField::Any => 0,
            PartialVersionField::Literal(n) => n,
        },
        match min.patch {
            PartialVersionField::Any => 0,
            PartialVersionField::Literal(n) => n,
        }
    );
    match max.major {
        PartialVersionField::Any => version >= &min_v,
        PartialVersionField::Literal(major) => match max.minor {
            PartialVersionField::Any => {
                let max_v = static_version!(major + 1);
                version >= &min_v && version < &max_v
            }
            PartialVersionField::Literal(minor) => match max.patch {
                PartialVersionField::Any => {
                    let max_v = static_version!(major, minor + 1);
                    version >= &min_v && version < &max_v
                }
                PartialVersionField::Literal(patch) => {
                    let max_v = static_version!(major, minor, patch);
                    version >= &min_v && version <= &max_v
                }
            },
        },
    }
}

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
pub(super) enum Range<'v> {
    Hyphen((PartialVersion<'v>, PartialVersion<'v>)),
    Simple(std::vec::Vec<RangeSimple<'v>>),
}
impl Range<'_> {
    pub fn test(
        &self,
        version: &super::version::Version,
        options: &super::dependency::Options,
    ) -> bool {
        match self {
            Range::Hyphen((min, max)) => test_hyphen_range(min, max, version, options),
            Range::Simple(simples) => simples.iter().all(|s| s.test(version, options)),
        }
    }
}
#[derive(Debug, Hash, PartialEq, Eq, Clone)]
pub struct RangeSet<'v> {
    pub(super) ranges: std::vec::Vec<Range<'v>>,
}
impl<'v> RangeSet<'v> {
    pub fn parse(text: &'v str) -> Result<Self, npm::Error> {
        super::packages::parse(
            super::packages::Rule::range_set,
            text,
            super::packages::parse_range_set,
        )
    }
    pub fn test(
        &self,
        version: &super::version::Version,
        options: &super::dependency::Options,
    ) -> bool {
        self.ranges.iter().any(|r| r.test(version, options))
    }
}

#[cfg(test)]
mod test {
    macro_rules! r {
        ($text:literal) => {
            super::RangeSet::parse($text).unwrap()
        };
    }
    macro_rules! v {
        ($text:literal) => {
            crate::lib::npm::syntax::version::Version::parse($text).unwrap()
        };
    }
    use crate::lib::npm::syntax::packages;
    fn p(text: &str) -> super::PartialVersion {
        packages::parse(
            packages::Rule::partial_semver,
            text,
            packages::parse_partial_semver,
        )
        .unwrap()
    }
    #[test]
    fn parse_partial_semver() {
        assert_eq!(
            p("1"),
            super::PartialVersion {
                major: super::PartialVersionField::Literal(1),
                minor: super::PartialVersionField::Any,
                patch: super::PartialVersionField::Any,
                pre: [].to_vec(),
            }
        );
        assert_eq!(
            p("1.0.x"),
            super::PartialVersion {
                major: super::PartialVersionField::Literal(1),
                minor: super::PartialVersionField::Literal(0),
                patch: super::PartialVersionField::Any,
                pre: [].to_vec(),
            }
        );
    }
    #[test]
    fn parse() {
        assert_eq!(
            r!(">1.2.3 ^0.x || 2.3.4 - 5.6.7"),
            super::RangeSet {
                ranges: [
                    super::Range::Simple(
                        [
                            super::RangeSimple::Primitive(super::RangePrimitive {
                                operator: super::RangePrimitiveOperator::Greater,
                                version: v!("1.2.3"),
                            }),
                            super::RangeSimple::Caret(p("0.x"))
                        ]
                        .to_vec()
                    ),
                    super::Range::Hyphen((p("2.3.4"), p("5.6.7")))
                ]
                .to_vec()
            }
        )
    }
    #[test]
    fn test_hyphen_range() {
        macro_rules! t {
            ($min:literal, $max:literal, $version:literal) => {
                super::test_hyphen_range(
                    &p($min),
                    &p($max),
                    &v!($version),
                    &crate::lib::npm::syntax::dependency::Options::new(),
                )
            };
        }
        assert!(t!("1.2.3", "2.3.4", "1.2.3"));
        assert!(t!("1.2.3", "2.3.4", "2.3.4"));
        assert!(t!("1.2.3", "2.3.4", "2.0.0"));
        assert!(!t!("1.2.3", "2.3.4", "1.2.2"));
        assert!(!t!("1.2.3", "2.3.4", "2.3.5"));
        /* Partial min */
        assert!(t!("1.2", "2.3.4", "1.2.0"));
        assert!(t!("1.2", "2.3.4", "2.3.4"));
        assert!(!t!("1.2", "2.3.4", "1.1.9999"));
        assert!(!t!("1.2.3", "2.3.4", "2.3.5"));
        /* Partial max */
        assert!(t!("1.2.3", "2.3", "1.2.3"));
        assert!(t!("1.2.3", "2.3", "2.3.99"));
        assert!(!t!("1.2.3", "2.3", "1.2.2"));
        assert!(!t!("1.2.3", "2.3", "2.4.0"));
        assert!(t!("1.2.3", "2", "1.2.3"));
        assert!(t!("1.2.3", "2", "2.9.99"));
        assert!(!t!("1.2.3", "2", "1.2.2"));
        assert!(!t!("1.2.3", "2", "3.0.0"));
    }
    #[test]
    fn test_x_range() {
        macro_rules! t {
            ($x:literal, $version:literal) => {
                super::test_x_range(
                    &p($x),
                    &v!($version),
                    &crate::lib::npm::syntax::dependency::Options::new(),
                )
            };
            ($x:literal, $version:literal, $i:literal) => {
                super::test_x_range(
                    &p($x),
                    &v!($version),
                    &crate::lib::npm::syntax::dependency::Options::new().include_pre_release($i),
                )
            };
        }
        /* Test with include_pre_release */
        assert!(t!("*", "1.2.3-alpha", true));
        assert!(!t!("*", "1.2.3-alpha", false));
        assert!(t!("*", "0.0.0"));
        assert!(t!("*", "99.999.999"));
        assert!(t!("1.x", "1.0.0"));
        assert!(t!("1.x", "1.99.999"));
        assert!(!t!("1.x", "0.99.999"));
        assert!(!t!("1.x", "2.0.0"));
        assert!(t!("1.2.x", "1.2.0"));
        assert!(t!("1.2.x", "1.2.999"));
        assert!(!t!("1.2.x", "1.1.999"));
        assert!(!t!("1.2.x", "1.3.0"));
    }
    #[test]
    fn test_tilde_range() {
        macro_rules! t {
            ($x:literal, $version:literal) => {
                super::test_tilde_range(
                    &p($x),
                    &v!($version),
                    &crate::lib::npm::syntax::dependency::Options::new(),
                )
            };
        }
        assert!(t!("1.2.3", "1.2.3"));
        assert!(t!("1.2.3", "1.2.99"));
        assert!(!t!("1.2.3", "1.3.0"));
        assert!(!t!("1.2.3", "1.3.1"));
        assert!(t!("1.2", "1.2.0"));
        assert!(t!("1.2", "1.2.99"));
        assert!(!t!("1.2", "1.1.99"));
        assert!(!t!("1.2", "1.3.0"));
        assert!(t!("1", "1.0.0"));
        assert!(t!("1", "1.99.999"));
        assert!(!t!("1", "0.99.999"));
        assert!(!t!("1", "2.0.0"));
        assert!(t!("1.2.3-beta.2", "1.2.3-beta.2"));
        assert!(t!("1.2.3-beta.2", "1.2.3-beta.4"));
        assert!(t!("1.2.3-beta.2", "1.2.99"));
        assert!(!t!("1.2.3-beta.2", "1.2.4-beta.4"));
        assert!(!t!("1.2.3-beta.2", "1.3.0-0"));
    }
    #[test]
    fn test_caret_range() {
        macro_rules! t {
            ($x:literal, $version:literal) => {
                super::test_caret_range(
                    &p($x),
                    &v!($version),
                    &crate::lib::npm::syntax::dependency::Options::new(),
                )
            };
        }
        assert!(t!("1.2.3", "1.2.3"));
        assert!(t!("1.2.3", "1.99.999"));
        assert!(!t!("1.2.3", "2.0.0"));
        assert!(!t!("1.2.3", "1.1.99"));
        assert!(t!("0.2.3", "0.2.3"));
        assert!(t!("0.2.3", "0.2.99"));
        assert!(!t!("0.2.3", "0.3.0"));
        assert!(!t!("0.2.3", "0.2.2"));
        assert!(t!("0.0.3", "0.0.3"));
        assert!(!t!("0.0.3", "0.0.4"));
        assert!(!t!("0.0.3", "0.0.2"));
        assert!(t!("1.2.3-beta.2", "1.2.3-beta.2"));
        assert!(t!("1.2.3-beta.2", "1.99.999"));
        assert!(!t!("1.2.3-beta.2", "2.0.0"));
        assert!(!t!("1.2.3-beta.2", "1.1.99"));
        assert!(t!("1.2.3-beta.2", "1.2.3-beta.4"));
        assert!(!t!("1.2.3-beta.2", "1.2.4-beta.2"));
        assert!(t!("0.0.3-beta", "0.0.3-pr.2"));
        assert!(t!("1.2.x", "1.2.0"));
        assert!(t!("1.2.x", "1.2.99"));
        assert!(!t!("1.2.x", "1.1.99"));
        assert!(!t!("1.2.x", "2.0.0"));
        assert!(t!("0.0.x", "0.0.0"));
        assert!(t!("0.0.x", "0.0.99"));
        assert!(!t!("0.0.x", "0.1.0"));
        assert!(t!("1.x", "1.0.0"));
        assert!(t!("1.x", "1.99.999"));
        assert!(!t!("1.x", "0.99.999"));
        assert!(!t!("1.x", "2.0.0"));
        assert!(t!("0.x", "0.0.0"));
        assert!(t!("0.x", "0.99.999"));
        assert!(!t!("0.x", "1.0.0"));
    }
    #[test]
    fn test() {
        macro_rules! t {
            ($x:literal, $version:literal) => {
                r!($x).test(
                    &v!($version),
                    &crate::lib::npm::syntax::dependency::Options::new(),
                )
            };
        }
        assert!(t!("=1.2.3 || =2.3.4", "1.2.3"));
        assert!(t!("=1.2.3 || =2.3.4", "2.3.4"));
        assert!(!t!("=1.2.3 || =2.3.4", "3.4.5"));
        assert!(!t!(">1.2.3 <2.3.4", "1.2.3"));
        assert!(!t!(">1.2.3 <2.3.4", "2.3.4"));
        assert!(t!(">1.2.3 <2.3.4", "2.0.0"));
    }
}

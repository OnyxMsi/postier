use pest::iterators::Pair;
use pest::Parser;
use pest_derive::Parser;

#[derive(Parser)]
#[grammar = "lib/npm/grammar/packages.pest"]
struct PackageParser;

pub(super) fn parse<'a, T, F>(
    rule: Rule,
    s: &'a str,
    parse_fct: F,
) -> Result<T, crate::lib::npm::Error>
where
    F: Fn(Pair<'a, Rule>) -> T,
    T: 'a,
{
    let p = PackageParser::parse(rule, s.trim())
        .map_err(|e| {
            log::warn!("Can't parse npm package \"{}\": {}", s, e);
            crate::lib::npm::Error::PackageError
        })?
        .next()
        .unwrap();
    Ok(parse_fct(p))
}

fn parse_usize(pair: Pair<Rule>) -> usize {
    pair.as_str().trim().parse::<usize>().unwrap()
}

fn parse_pre_release_segment(pair: Pair<Rule>) -> super::version::PreReleaseSegment {
    match pair.as_rule() {
        Rule::semver_alphanumeric_identifier => {
            super::version::PreReleaseSegment::NonNumeric(pair.as_str())
        }
        Rule::semver_numeric_identifier => {
            super::version::PreReleaseSegment::Numeric(parse_usize(pair))
        }
        _ => unreachable!(),
    }
}

fn parse_pre_release(pair: Pair<Rule>) -> std::vec::Vec<super::version::PreReleaseSegment> {
    pair.into_inner().map(parse_pre_release_segment).collect()
}

pub(super) fn parse_semver(pair: Pair<Rule>) -> super::version::Version {
    let mut major = None;
    let mut minor = None;
    let mut patch = None;
    let mut pre = None;
    for p in pair.into_inner() {
        match p.as_rule() {
            Rule::semver_core => {
                for r in p.into_inner() {
                    match r.as_rule() {
                        Rule::semver_major => major = Some(parse_usize(r)),
                        Rule::semver_minor => minor = Some(parse_usize(r)),
                        Rule::semver_patch => patch = Some(parse_usize(r)),
                        _ => unreachable!(),
                    }
                }
            }
            Rule::semver_pre_release => pre = Some(parse_pre_release(p)),
            _ => unreachable!(),
        }
    }
    super::version::Version {
        major: major.unwrap(),
        minor: minor.unwrap(),
        patch: patch.unwrap(),
        pre: pre.unwrap_or_else(std::vec::Vec::new),
    }
}

fn parse_partial_semver_pseudo_numeric_identifier(
    pair: Pair<Rule>,
) -> super::range::PartialVersionField {
    match pair.as_str() {
        "*" => super::range::PartialVersionField::Any,
        "x" => super::range::PartialVersionField::Any,
        _ => super::range::PartialVersionField::Literal(parse_usize(pair)),
    }
}

pub(super) fn parse_partial_semver(pair: Pair<Rule>) -> super::range::PartialVersion {
    let mut major = super::range::PartialVersionField::Any;
    let mut minor = super::range::PartialVersionField::Any;
    let mut patch = super::range::PartialVersionField::Any;
    let mut pre = None;
    for p in pair.into_inner() {
        match p.as_rule() {
            Rule::partial_semver_core => {
                for r in p.into_inner() {
                    match r.as_rule() {
                        Rule::partial_semver_major => {
                            major = parse_partial_semver_pseudo_numeric_identifier(r)
                        }
                        Rule::partial_semver_minor => {
                            minor = parse_partial_semver_pseudo_numeric_identifier(r)
                        }
                        Rule::partial_semver_patch => {
                            patch = parse_partial_semver_pseudo_numeric_identifier(r)
                        }
                        _ => unreachable!(),
                    }
                }
            }
            Rule::semver_pre_release => pre = Some(parse_pre_release(p)),
            _ => unreachable!(),
        }
    }
    super::range::PartialVersion {
        major,
        minor,
        patch,
        pre: pre.unwrap_or_else(std::vec::Vec::new),
    }
}
fn parse_range_primitive(pair: Pair<Rule>) -> super::range::RangePrimitive {
    let mut it = pair.into_inner();
    let operator = match it.next().unwrap().as_str() {
        "<=" => super::range::RangePrimitiveOperator::LessEqual,
        "<" => super::range::RangePrimitiveOperator::Less,
        ">=" => super::range::RangePrimitiveOperator::GreaterEqual,
        ">" => super::range::RangePrimitiveOperator::Greater,
        "=" => super::range::RangePrimitiveOperator::Equal,
        _ => unreachable!(),
    };
    let version = parse_semver(it.next().unwrap());
    super::range::RangePrimitive { operator, version }
}
fn parse_range_simple(pair: Pair<Rule>) -> super::range::RangeSimple {
    match pair.as_rule() {
        Rule::range_primitive => super::range::RangeSimple::Primitive(parse_range_primitive(pair)),
        Rule::partial_semver => super::range::RangeSimple::Partial(parse_partial_semver(pair)),
        Rule::range_tilde => super::range::RangeSimple::Tilde(parse_partial_semver(
            pair.into_inner().next().unwrap(),
        )),
        Rule::range_caret => super::range::RangeSimple::Caret(parse_partial_semver(
            pair.into_inner().next().unwrap(),
        )),
        _ => unreachable!(),
    }
}
fn parse_range_hyphen(
    pair: Pair<Rule>,
) -> (super::range::PartialVersion, super::range::PartialVersion) {
    let mut it = pair.into_inner();
    (
        parse_partial_semver(it.next().unwrap()),
        parse_partial_semver(it.next().unwrap()),
    )
}
fn parse_range_simples(pair: Pair<Rule>) -> std::vec::Vec<super::range::RangeSimple> {
    pair.into_inner().map(parse_range_simple).collect()
}
fn parse_range(pair: Pair<Rule>) -> super::range::Range {
    match pair.as_rule() {
        Rule::range_hyphen => super::range::Range::Hyphen(parse_range_hyphen(pair)),
        Rule::range_simples => super::range::Range::Simple(parse_range_simples(pair)),
        _ => unreachable!(),
    }
}
pub(super) fn parse_range_set(pair: Pair<Rule>) -> super::range::RangeSet {
    let ranges = pair.into_inner().map(parse_range).collect();
    super::range::RangeSet { ranges }
}
pub(super) fn parse_dependency(pair: Pair<Rule>) -> super::dependency::Dependency {
    match pair.as_rule() {
        Rule::url_dependency => super::dependency::Dependency::Url(pair.as_str()),
        Rule::local_path_dependency => super::dependency::Dependency::Url(pair.as_str()),
        Rule::range_set => super::dependency::Dependency::Range(parse_range_set(pair)),
        _ => unreachable!(),
    }
}

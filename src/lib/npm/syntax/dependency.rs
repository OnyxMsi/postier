/* https://github.com/npm/node-semver?tab=readme-ov-file#functions */
pub struct Options {
    pub(super) include_pre_release: bool,
}
impl Options {
    pub fn new() -> Self {
        Options {
            include_pre_release: true,
        }
    }
    pub fn include_pre_release(mut self, v: bool) -> Self {
        self.include_pre_release = v;
        self
    }
}
/* https://docs.npmjs.com/cli/v10/configuring-npm/package-json#dependencies */
pub enum Dependency<'d> {
    Range(super::range::RangeSet<'d>),
    Url(&'d str),
    /* Complete this */
}
impl<'d> Dependency<'d> {
    pub fn parse(text: &'d str) -> Result<Self, crate::lib::npm::Error> {
        super::packages::parse(
            super::packages::Rule::dependency,
            text,
            super::packages::parse_dependency,
        )
    }
    pub fn test(&self, version: &super::version::Version, options: &Options) -> bool {
        match self {
            Dependency::Range(r) => r.test(version, options),
            Dependency::Url(_) => true,
        }
    }
}

use crate::lib::npm;

#[derive(Debug, Clone, Hash, Eq, PartialEq)]
pub enum PreReleaseSegment<'v> {
    Numeric(usize),
    NonNumeric(&'v str),
}
impl PartialOrd for PreReleaseSegment<'_> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl Ord for PreReleaseSegment<'_> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self {
            PreReleaseSegment::Numeric(n) => match other {
                PreReleaseSegment::Numeric(m) => n.cmp(m),
                PreReleaseSegment::NonNumeric(m) => std::cmp::Ordering::Less,
            },
            PreReleaseSegment::NonNumeric(n) => match other {
                PreReleaseSegment::Numeric(m) => std::cmp::Ordering::Greater,
                PreReleaseSegment::NonNumeric(m) => n.cmp(m),
            },
        }
    }
}

#[derive(Debug, Clone, Hash, Eq, PartialEq)]
pub struct Version<'v> {
    pub major: usize,
    pub minor: usize,
    pub patch: usize,
    pub pre: std::vec::Vec<PreReleaseSegment<'v>>,
    /* Ignore build metadata */
}

impl<'v> Version<'v> {
    pub fn parse(text: &'v str) -> Result<Self, npm::Error> {
        super::packages::parse(
            super::packages::Rule::semver,
            text,
            super::packages::parse_semver,
        )
    }
}
impl PartialOrd for Version<'_> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl Ord for Version<'_> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.major.cmp(&other.major).then_with(|| {
            self.minor.cmp(&other.minor).then_with(|| {
                self.patch.cmp(&other.patch).then_with(|| {
                    /* No pre release is greater than a pre release */
                    if self.pre.len() == 0 {
                        if other.pre.len() == 0 {
                            std::cmp::Ordering::Equal
                        } else {
                            std::cmp::Ordering::Greater
                        }
                    } else if other.pre.len() == 0 {
                        std::cmp::Ordering::Less
                    } else {
                        self.pre.cmp(&other.pre)
                    }
                })
            })
        })
    }
}

#[cfg(test)]
mod test {
    macro_rules! v {
        ($t:literal) => {
            super::Version::parse($t).unwrap()
        };
    }
    #[test]
    fn parse() {
        assert_eq!(
            v!("1.2.3"),
            super::Version {
                major: 1,
                minor: 2,
                patch: 3,
                pre: [].to_vec()
            }
        );
        assert_eq!(
            v!("1.2.3-a9.4.ph.5"),
            super::Version {
                major: 1,
                minor: 2,
                patch: 3,
                pre: [
                    super::PreReleaseSegment::NonNumeric("a9"),
                    super::PreReleaseSegment::Numeric(4),
                    super::PreReleaseSegment::NonNumeric("ph"),
                    super::PreReleaseSegment::Numeric(5),
                ]
                .to_vec()
            }
        );
    }
    #[test]
    fn compare() {
        assert!(v!("1.0.0") < v!("2.0.0"));
        assert!(v!("2.0.0") < v!("2.1.0"));
        assert!(v!("2.1.0") < v!("2.1.1"));
        assert!(v!("1.0.0-alpha") < v!("1.0.0"));
        assert!(v!("1.0.0-alpha") < v!("1.0.0-alpha.1"));
        assert!(v!("1.0.0-alpha.1") < v!("1.0.0-alpha.beta"));
        assert!(v!("1.0.0-alpha.beta") < v!("1.0.0-beta"));
        assert!(v!("1.0.0-beta") < v!("1.0.0-beta.2"));
        assert!(v!("1.0.0-beta.2") < v!("1.0.0-beta.11"));
        assert!(v!("1.0.0-beta.11") < v!("1.0.0-rc.1"));
        assert!(v!("1.0.0-rc.1") < v!("1.0.0"));
    }
}

/// A cache built on top of a [reqwest::blocking::Client] to make sure a file is downloaded only
/// once.
/// This is to avoid fetching the same item twice.
pub struct FetchCache {
    pool: std::collections::HashMap<std::vec::Vec<u8>, std::vec::Vec<std::path::PathBuf>>,
    client: reqwest::blocking::Client,
}

pub trait FetchItem {
    /// Build a request from an item.
    /// Two item with different requests might have the same checsum.
    fn request_factory(
        &self,
        client: &reqwest::blocking::Client,
    ) -> reqwest::blocking::RequestBuilder;
    /// Build a path where to store  an item
    /// Two item with different paths might have the same checsum.
    fn path_factory(&self) -> std::path::PathBuf;
    /// Build a checksum for an item.
    /// This what determine that two items are equivalent.
    fn get_sha256(&self) -> std::vec::Vec<u8>;
    /// Fetch something from the internet and save it in the path
    fn fetch(
        &self,
        request: reqwest::blocking::RequestBuilder,
        path: &std::path::Path,
    ) -> Result<(), super::Error> {
        super::utils::blocking_fetch_url(request, path)
    }
}

impl FetchCache {
    pub fn new() -> Self {
        Self {
            pool: std::collections::HashMap::new(),
            client: reqwest::blocking::Client::new(),
        }
    }
    /// Get a path that contains the provided item content.
    /// If the item was not already downloaded then it will be done.
    /// If an item with the same checksum was already downloaded it will be copied.
    /// Else it already exists at this path. So nothing will be done.
    pub fn get(&mut self, item: &impl FetchItem) -> Result<std::path::PathBuf, super::Error> {
        let ck = item.get_sha256();
        let p = item.path_factory();
        let r = p.clone();
        /* If this item was not downloaded or if the cache data was deleted */
        if !self.pool.contains_key(&ck) || !self.pool.get(&ck).unwrap().iter().any(|p| p.exists()) {
            let r = item.request_factory(&self.client);
            if !super::utils::check_file_and_hash::<sha2::Sha256>(p.as_path(), &ck) {
                item.fetch(r, p.as_path())?;
            }
            self.pool.insert(ck, std::vec![p]);
        } else {
            let e = self.pool.get_mut(&ck).unwrap();
            if !e.contains(&p) || !p.exists() {
                let prev = e.iter().find(|p| p.exists()).unwrap();
                std::fs::copy(e.get(0).unwrap(), p.as_path()).map_err(|r| {
                    log::error!("Can't copy {} to {}: {}", prev.display(), p.display(), r);
                    super::Error::IOError(r)
                })?;
                e.push(p);
            }
        }
        Ok(r)
    }
    /// Check if such item already exists.
    /// [true] means that this item checksum AND path already exists in the database
    pub fn has(&self, item: &impl FetchItem) -> bool {
        let ck = item.get_sha256();
        let p = item.path_factory();
        self.pool
            .get(&ck)
            .map_or(false, |v| v.contains(&p) && p.as_path().exists())
    }
    pub fn flush(&mut self) {
        self.pool.clear();
    }
}

#[cfg(test)]
mod test {
    use std::io::Read;

    use crate::lib::utils;
    struct Test<'t> {
        base: &'t str,
        src_path: std::path::PathBuf,
        dest_path: Option<&'t std::path::Path>,
        u: std::vec::Vec<u8>,
    }
    impl<'t> Test<'t> {
        fn new(pool: &std::path::Path, base: &'t str) -> Self {
            let src_path = pool.join(utils::test::get_text());
            let mut fd = std::fs::File::create(src_path.as_path()).unwrap();
            use std::io::Write;
            fd.write(utils::test::get_text().as_bytes()).unwrap();
            let u = utils::stupid_path_hash::<sha2::Sha256>(src_path.as_path()).unwrap();
            Self {
                src_path,
                u,
                base,
                dest_path: None,
            }
        }
        fn dest_path(mut self, path: &'t std::path::Path) -> Self {
            self.dest_path = Some(path);
            self
        }
        fn prefix(&self) -> String {
            hex::encode(&self.u)
        }
        fn content(&self) -> std::vec::Vec<u8> {
            let mut c = std::vec::Vec::new();
            std::fs::File::open(self.src_path.as_path())
                .unwrap()
                .read_to_end(&mut c)
                .unwrap();
            c
        }
    }
    impl super::FetchItem for Test<'_> {
        fn request_factory(
            &self,
            client: &reqwest::blocking::Client,
        ) -> reqwest::blocking::RequestBuilder {
            client.get(format!("{}/{}", self.base, self.prefix()))
        }
        fn path_factory(&self) -> std::path::PathBuf {
            self.dest_path.unwrap().to_path_buf()
        }
        fn get_sha256(&self) -> std::vec::Vec<u8> {
            self.u.clone()
        }
    }
    #[test]
    fn fetch_get() {
        let mut c = super::FetchCache::new();
        let pool = utils::test::get_temp_dir();
        let res_dir = pool.join("res");
        std::fs::create_dir_all(res_dir.as_path()).unwrap();
        let server = httpmock::MockServer::start();
        let base_url = server.base_url();
        let mut t1 = Test::new(pool.as_path(), &base_url.as_str());
        let t2 = Test::new(pool.as_path(), &base_url.as_str());
        let mock_1 = server.mock(|when, then| {
            when.method("GET").path(format!("/{}", t1.prefix()));
            then.body(t1.content());
        });
        server.mock(|when, then| {
            when.method("GET").path(format!("/{}", t2.prefix()));
            then.body(t2.content());
        });
        let t1_dest = res_dir.join(utils::test::get_text());
        let t11_dest = res_dir.join(utils::test::get_text());
        t1 = t1.dest_path(t1_dest.as_path());
        let d = c.get(&t1).unwrap();
        mock_1.assert_hits(1);
        assert!(t1_dest.exists());
        assert_eq!(d, t1_dest);
        /* Call it again, no request should be made */
        c.get(&t1).unwrap();
        mock_1.assert_hits(1);
        /* Delete the destination to make sure it is downloaded again */
        std::fs::remove_file(t1_dest.as_path()).unwrap();
        let d = c.get(&t1).unwrap();
        mock_1.assert_hits(2);
        assert!(t1_dest.exists());
        assert_eq!(d, t1_dest);
        /* Now try to download the same thing to another destination, nothing is downloaded */
        t1 = t1.dest_path(t11_dest.as_path());
        c.get(&t1).unwrap();
        mock_1.assert_hits(2);
        assert!(t11_dest.exists());
    }
    #[test]
    fn fetch_has() {
        let mut c = super::FetchCache::new();
        let pool = utils::test::get_temp_dir();
        let res_dir = pool.join("res");
        std::fs::create_dir_all(res_dir.as_path()).unwrap();
        std::fs::create_dir_all(res_dir.as_path()).unwrap();
        let server = httpmock::MockServer::start();
        let base_url = server.base_url();
        let mut t1 = Test::new(pool.as_path(), &base_url.as_str());
        let mut t2 = Test::new(pool.as_path(), &base_url.as_str());
        let t1_dest = res_dir.join(utils::test::get_text());
        let t2_dest = res_dir.join(utils::test::get_text());
        let t11_dest = res_dir.join(utils::test::get_text());
        t1 = t1.dest_path(&t1_dest);
        t2 = t2.dest_path(&t2_dest);
        server.mock(|when, then| {
            when.method("GET").path(format!("/{}", t1.prefix()));
            then.body(t1.content());
        });
        assert!(!c.has(&t1));
        assert!(!c.has(&t2));
        let d = c.get(&t1).unwrap();
        assert!(c.has(&t1));
        assert!(!c.has(&t2));
        /* Delete the content */
        std::fs::remove_file(d).unwrap();
        assert!(!c.has(&t1));
        assert!(!c.has(&t2));
        c.get(&t1).unwrap();
        t1 = t1.dest_path(&t11_dest);
        assert!(!c.has(&t1));
        assert!(!c.has(&t2));
        c.get(&t1).unwrap();
        assert!(c.has(&t1));
        assert!(!c.has(&t2));
    }
}

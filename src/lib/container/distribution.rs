mod helpers;
/// See the [documentation](https://distribution.github.io/distribution/)
use crate::lib::{container, Error as LError};
use serde::Deserialize;

pub struct Distribution<'d> {
    parameters: &'d dyn container::Parameters,
    client: &'d reqwest::blocking::Client,
    base_url: &'d url::Url,
    token: Option<String>,
}

#[derive(Deserialize)]
struct ListRepositories {
    repositories: std::vec::Vec<String>,
}

#[derive(Deserialize)]
pub struct Platform {
    pub architecture: String,
    pub os: String,
    #[serde(rename(deserialize = "os.version"))]
    pub os_version: Option<String>,
}
#[derive(Deserialize)]
pub struct ManifestV2Layer {
    pub digest: String,
    pub size: usize,
}
impl ManifestV2Layer {
    pub fn digest(&self) -> Result<super::syntax::digest::Digest, container::Error> {
        super::syntax::digest::Digest::parse(self.digest.as_str())
    }
}
#[derive(Deserialize)]
pub struct ManifestV2 {
    #[serde(rename(deserialize = "mediaType"))]
    pub media_type: String,
    pub config: ManifestV2Layer,
    pub layers: std::vec::Vec<ManifestV2Layer>,
}
#[derive(Deserialize)]
pub struct ManifestListV2 {
    #[serde(rename(deserialize = "mediaType"))]
    pub media_type: String,
    pub manifests: std::vec::Vec<ManifestListManifest>,
}
#[derive(Deserialize)]
pub struct ManifestListManifest {
    #[serde(rename(deserialize = "mediaType"))]
    pub media_type: String,
    pub digest: String,
    pub platform: Platform,
}
impl ManifestListManifest {
    pub fn digest(&self) -> Result<super::syntax::digest::Digest, container::Error> {
        super::syntax::digest::Digest::parse(self.digest.as_str())
    }
}
#[derive(Deserialize)]
pub struct ListTags {
    tags: std::vec::Vec<String>,
}

macro_rules! ujoin {
    ($u:expr, $s:expr) => {
        crate::lib::utils::ujoin($u, $s).map_err(container::Error::LibError)
    };
}

impl<'d> Distribution<'d> {
    pub fn new(
        client: &'d reqwest::blocking::Client,
        base_url: &'d url::Url,
        parameters: &'d dyn container::Parameters,
    ) -> Self {
        Distribution {
            parameters,
            client,
            base_url,
            token: None,
        }
    }
    fn try_or_get_token<F>(
        &mut self,
        req_factory: F,
    ) -> Result<reqwest::blocking::Response, container::Error>
    where
        F: Fn(
            &reqwest::blocking::Client,
        ) -> Result<reqwest::blocking::RequestBuilder, container::Error>,
    {
        let (r, t) = helpers::try_or_get_token(&self.client, req_factory, self.token.as_deref())?;
        self.token = t;
        Ok(r)
    }
    fn paginator<R, F>(&mut self, req_factory: F) -> helpers::Paginator<R, F>
    where
        F: Fn(
            &reqwest::blocking::Client,
        ) -> Result<reqwest::blocking::RequestBuilder, container::Error>,
        R: serde::de::DeserializeOwned,
    {
        helpers::Paginator::new(&self.client, req_factory, self.token.as_deref())
    }
    /// See the [documentation](https://distribution.github.io/distribution/spec/api/#api-version-check)
    pub fn api_version_check(&mut self) -> Result<bool, container::Error> {
        log::debug!("Check API version > 2 for {}", self.base_url);
        let u = ujoin!(&self.base_url, "v2/")?;
        let r = self.try_or_get_token(|c| Ok(c.get(u.as_str())))?;
        Ok(r.status().as_u16() == 200)
    }
    /// See the [documentation](https://distribution.github.io/distribution/spec/api/#listing-repositories)
    pub fn list_repositories(&mut self) -> Result<std::vec::Vec<String>, container::Error> {
        log::debug!("List repositories");
        let mut r = std::vec::Vec::new();
        let u = ujoin!(&self.base_url, "v2/_catalog/")?;
        for mut e in self.paginator::<ListRepositories, _>(|c| Ok(c.get(u.as_str()))) {
            r.append(&mut e.repositories);
        }
        Ok(r)
    }
    /// See the [documentation](https://distribution.github.io/distribution/spec/api/#pulling-an-image-manifest)
    pub fn pull_image_manifest(
        &mut self,
        image_name: &str,
        reference: &str,
    ) -> Result<ManifestListV2, container::Error> {
        log::debug!("Pull image {} manifest {}", image_name, reference);
        let u = ujoin!(
            &self.base_url,
            &format!("v2/{}/manifests/{}", image_name, reference)
        )?;
        let m: ManifestListV2 = self
            .try_or_get_token(|c| Ok(c.get(u.as_str())))?
            .json()
            .map_err(|e| {
                log::error!(
                    "Can't convert {} to Manifest List V2 format: {}",
                    u.as_str(),
                    e
                );
                super::Error::DistributionError
            })?;
        Ok(m)
    }
    /// See the [documentation](https://distribution.github.io/distribution/spec/api/#pulling-a-layer)
    pub fn pull_image_layer(
        &mut self,
        image_name: &str,
        digest: &str,
        destination: &std::path::Path,
    ) -> Result<(), container::Error> {
        log::debug!(
            "Pull image {} digest {} to {}",
            image_name,
            digest,
            destination.display()
        );
        let u = ujoin!(
            &self.base_url,
            &format!("v2/{}/blobs/{}", image_name, digest)
        )?;
        let mut r = self.try_or_get_token(|c| Ok(c.get(u.as_str())))?;
        let mut buf = std::fs::File::create(destination).map_err(|e| {
            log::error!("Can't open {}: {}", destination.display(), e);
            container::Error::LibError(LError::IOError(e))
        })?;
        r.copy_to(&mut buf).map_err(|e| {
            log::error!(
                "Can't copy {} content into {}: {}",
                u,
                destination.display(),
                e
            );
            container::Error::LibError(LError::ReqwestError(e))
        })?;
        Ok(())
    }

    /// See the [documentation](https://distribution.github.io/distribution/spec/api/#existing-manifests)
    pub fn image_manifest_exists(
        &mut self,
        image_name: &str,
        reference: &str,
    ) -> Result<bool, container::Error> {
        let u = ujoin!(
            &self.base_url,
            &format!("v2/{}/blobs/{}", image_name, reference)
        )?;
        Ok(self
            .try_or_get_token(|c| Ok(c.head(u.as_str())))?
            .status()
            .is_success())
    }
    /// See the [documentation](https://distribution.github.io/distribution/spec/api/#listing-image-tags)
    pub fn list_image_tags(
        &mut self,
        image_name: &str,
    ) -> Result<std::vec::Vec<String>, container::Error> {
        let u = ujoin!(&self.base_url, &format!("v2/{}/tags/list", image_name))?;
        let mut r = std::vec::Vec::new();
        for mut e in self.paginator::<ListTags, _>(|c| Ok(c.get(u.as_str()))) {
            r.append(&mut e.tags);
        }
        Ok(r)
    }
}

use crate::lib;
use crate::lib::container::syntax::http_authenticate_challenge::{Challenge, ChallengeValue};
use crate::lib::{container, utils};
use reqwest::blocking::{Client, RequestBuilder};
use serde::Deserialize;

/// This module contains helpers functions and struct for the [`super`] module. They should remains
/// private to this module.

/* https://distribution.github.io/distribution/spec/auth/token/#token-response-fields */
struct TokenAuthentication<'t> {
    realm: url::Url,
    scope: &'t str,
    service: &'t str,
    client_id: Option<&'t str>,
}
#[derive(Deserialize)]
struct TokenResponse {
    token: String,
    /* Ignore the others */
}

impl<'t> TokenAuthentication<'t> {
    fn parse_header(header: &'t str) -> Result<Self, container::Error> {
        let challenge = Challenge::parse(header)?;
        if challenge.auth_scheme != HTTP_BEARER_AUTHENTICATION_SCHEME {
            log::error!(
                "Invalid HTTP authentication scheme \"{}\", only \"{}\" is supported",
                HTTP_BEARER_AUTHENTICATION_SCHEME,
                challenge.auth_scheme
            );
            return Err(container::Error::DistributionError);
        }
        match challenge.auth_value {
            ChallengeValue::Token(_) => {
                log::error!("Invalid HTTP authentication token, requires \"realm\" \"scope\" \"service\" parameters");
                Err(container::Error::DistributionError)
            }
            ChallengeValue::Parameters(t) => {
                macro_rules! requires {
                    ($name:literal) => {
                        t.get($name).ok_or_else(|| {
                            log::error!(concat!(
                                "Invalid HTTP authentication parameters, requires \"",
                                $name,
                                "\""
                            ));
                            container::Error::DistributionError
                        })
                    };
                }
                let realm_s = requires!("realm")?;
                let realm = url::Url::parse(realm_s).map_err(|e| {
                    log::error!("Can't parse realm URL {}: {}", realm_s, e);
                    container::Error::LibError(lib::Error::UrlError(e))
                })?;
                let scope = requires!("scope")?;
                let service = requires!("service")?;
                let client_id = t.get("client_id").copied();
                Ok(TokenAuthentication {
                    scope,
                    service,
                    client_id,
                    realm,
                })
            }
        }
    }
    fn fetch(self, client: &Client) -> Result<TokenResponse, container::Error> {
        log::debug!(
            "Fetch token from {}, scope={}, service={}",
            self.realm,
            self.scope,
            self.service
        );
        let mut req = client
            .get(self.realm)
            .query(&[("scope", self.scope), ("service", self.service)]);
        if let Some(client_id) = self.client_id {
            req = req.query(&["client_id", client_id]);
        }
        utils::reqwest_send(req)
            .map_err(container::Error::LibError)?
            .error_for_status()
            .map_err(|e| {
                log::error!(
                    "Can't get token (scope={} service={}): {}",
                    self.scope,
                    self.service,
                    e
                );
                container::Error::DistributionError
            })?
            .json::<TokenResponse>()
            .map_err(|e| {
                log::error!("Can't decode result into token response: {}", e);
                container::Error::DistributionError
            })
    }
}

const HTTP_BEARER_AUTHENTICATION_SCHEME: &str = "Bearer";
const HTTP_HEADER_AUTHORIZATION: &str = "Authorization";
const HTTP_HEADER_WWW_AUTHENTICATE: &str = "Www-Authenticate";

/// This how to handle authentication. Try to solve the request once, if it fails try to
/// authenticate then retry
pub(super) fn try_or_get_token<F>(
    client: &Client,
    req_factory: F,
    token: Option<&str>,
) -> Result<(reqwest::blocking::Response, Option<String>), container::Error>
where
    F: Fn(&Client) -> Result<RequestBuilder, container::Error>,
{
    let mut req = req_factory(client)?;
    if let Some(tok) = token {
        req = req.header(HTTP_HEADER_AUTHORIZATION, format!("Bearer {}", tok));
    }
    let res = utils::reqwest_send(req).map_err(container::Error::LibError)?;
    if res.status().as_u16() == 401 && res.headers().contains_key(HTTP_HEADER_WWW_AUTHENTICATE) {
        let h = res
            .headers()
            .get(HTTP_HEADER_WWW_AUTHENTICATE)
            .unwrap()
            .to_str()
            .map_err(|e| {
                log::error!(
                    "Invalid header {}, can't read it as string: {}",
                    HTTP_HEADER_WWW_AUTHENTICATE,
                    e
                );
                container::Error::DistributionError
            })?;
        log::debug!(
            "Request was rejected (401), try to authenticate using {}",
            h
        );
        let token_res = TokenAuthentication::parse_header(h)?.fetch(client)?;
        let token_str = format!("Bearer {}", token_res.token);
        log::debug!("Use token {} to authenticate", token_str);
        let req = req_factory(client)?.header(HTTP_HEADER_AUTHORIZATION, &token_str);
        /* Now retry it */
        let res = utils::reqwest_send(req).map_err(container::Error::LibError)?;
        Ok((res, Some(token_res.token)))
    } else {
        Ok((res, token.map(|s| s.to_string())))
    }
}
/// See the [documentation](https://distribution.github.io/distribution/spec/api/#pagination)
/// The page size is a constant, see [`PAGINATOR_LIMIT`]
pub(super) struct Paginator<'p, R, F>
where
    F: Fn(&Client) -> Result<RequestBuilder, container::Error>,
{
    first: bool,
    client: &'p Client,
    link: Option<String>,
    n: usize,
    req_factory: F,
    token: Option<String>,
    phantom: std::marker::PhantomData<R>,
}

impl<'p, R, F> Iterator for Paginator<'p, R, F>
where
    R: serde::de::DeserializeOwned,
    F: Fn(&Client) -> Result<RequestBuilder, container::Error>,
{
    type Item = R;

    fn next(&mut self) -> Option<R> {
        /* If first run was done and no link was saved then there is no more content to fetch */
        if !self.first && self.link.is_none() {
            return None;
        }
        self.first = false;
        let factory = |c: &Client| {
            Ok(match &self.link {
                Some(l) => {
                    log::debug!("Continue paginated request with {}", l);
                    c.get(l)
                }
                None => (self.req_factory)(c)?.query(&[("n", self.n)]),
            })
        };
        let (r, t) = try_or_get_token(self.client, factory, self.token.as_deref()).ok()?;
        self.token = t;
        self.link = r
            .headers()
            .get("Link")
            .map(|h| {
                h.to_str()
                    .inspect_err(|e| {
                        log::error!("Can't read header Link as string: {}", e);
                    })
                    .ok()
                    .map(|s| s.to_string())
            })
            .flatten();
        r.json::<R>()
            .inspect_err(|e| {
                log::error!("Can't convert pagination result: {}", e);
            })
            .ok()
    }
}

const PAGINATOR_LIMIT: usize = 30;

impl<'p, R, F> Paginator<'p, R, F>
where
    F: Fn(&Client) -> Result<RequestBuilder, container::Error>,
{
    pub(super) fn new(client: &'p Client, req_factory: F, token: Option<&str>) -> Self {
        Paginator::<R, F> {
            first: true,
            client,
            link: None,
            req_factory,
            token: token.map(|s| s.to_string()),
            n: PAGINATOR_LIMIT,
            phantom: std::marker::PhantomData::<R>,
        }
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn try_or_get_token() {
        let server = httpmock::MockServer::start();
        let auth_token = "aabbccddeeff11";
        let service = "service";
        let scope = "scope";
        let body = crate::lib::utils::test::get_text();
        let no_auth_url = server.url("/no_auth");
        let mock_no_auth = server.mock(|when, then| {
            when.method("GET").path("/no_auth");
            then.status(200).body(body.as_bytes());
        });
        let auth_url = server.url("/auth");
        let www_authenticate = format!(
            "Bearer realm=\"{}\",scope={},service={}",
            auth_url, scope, service
        );
        let mock_auth = server.mock(|when, then| {
            when.method("GET")
                .path("/auth")
                .query_param("scope", scope)
                .query_param("service", service);
            let r = format!("{{\"token\":\"{}\"}}", auth_token);
            then.status(200).body(r.as_bytes());
        });
        let with_auth_url = server.url("/with_auth");
        let mock_with_auth_with_token = server.mock(|when, then| {
            when.method("GET").path("/with_auth").header(
                super::HTTP_HEADER_AUTHORIZATION,
                format!("Bearer {}", auth_token),
            );
            then.status(200).body(body.as_bytes());
        });
        let mock_with_auth_without_token = server.mock(|when, then| {
            when.method("GET").path("/with_auth");
            then.status(401).header(
                super::HTTP_HEADER_WWW_AUTHENTICATE,
                www_authenticate.as_str(),
            );
        });
        /* The URL does not matter */
        let c = reqwest::blocking::Client::new();
        /* This will always work */
        let (r, t) = super::try_or_get_token(&c, |d| Ok(d.get(&no_auth_url)), None).unwrap();
        mock_no_auth.assert();
        assert_eq!(r.status().as_u16(), 200);
        assert_eq!(r.text().unwrap(), body);
        assert_eq!(mock_auth.hits(), 0);
        assert!(t.is_none());
        /* Even with an invalid token */
        let (r, t) =
            super::try_or_get_token(&c, |d| Ok(d.get(&no_auth_url)), Some("fake")).unwrap();
        assert_eq!(r.status().as_u16(), 200);
        assert_eq!(r.text().unwrap(), body);
        assert_eq!(t.unwrap(), "fake");
        /* This will request for a token */
        let (r, t) = super::try_or_get_token(&c, |d| Ok(d.get(&with_auth_url)), None).unwrap();
        mock_with_auth_without_token.assert();
        mock_auth.assert();
        mock_with_auth_with_token.assert();
        assert_eq!(r.status().as_u16(), 200);
        assert_eq!(r.text().unwrap(), body);
        assert_eq!(t.unwrap(), auth_token);
        /* Using the same token will work without using auth again */
        let (r, t) =
            super::try_or_get_token(&c, |d| Ok(d.get(&with_auth_url)), Some(auth_token)).unwrap();
        assert_eq!(r.status().as_u16(), 200);
        assert_eq!(r.text().unwrap(), body);
        assert_eq!(t.unwrap(), auth_token);
        mock_with_auth_without_token.assert_hits(1);
        mock_auth.assert_hits(1);
        mock_with_auth_with_token.assert_hits(2);
        /* Using an invalid token will make the auth link to be called*/
        let (r, t) =
            super::try_or_get_token(&c, |d| Ok(d.get(&with_auth_url)), Some("fake")).unwrap();
        assert_eq!(r.status().as_u16(), 200);
        assert_eq!(r.text().unwrap(), body);
        assert_eq!(t.unwrap(), auth_token);
        mock_with_auth_without_token.assert_hits(2);
        mock_auth.assert_hits(2);
        mock_with_auth_with_token.assert_hits(3);
    }
    #[test]
    fn paginator() {
        let server = httpmock::MockServer::start();
        let count = (super::PAGINATOR_LIMIT as f32 * 2.5).round() as usize;
        let data = crate::lib::utils::test::unique(count, || {
            /* Ensure ascii content */
            crate::lib::utils::test::get_words(2, 10, "_")
                .escape_default()
                .to_string()
        });
        let resources_url = server.url("/resources");
        let p2_link = format!(
            "{}?last={}",
            resources_url,
            data[super::PAGINATOR_LIMIT - 1]
        );
        let p3_link = format!(
            "{}?last={}",
            resources_url,
            data[super::PAGINATOR_LIMIT * 2 - 1]
        );
        let mock_resources_p1 = server.mock(|when, then| {
            when.query_param("n", format!("{}", super::PAGINATOR_LIMIT));
            let res: std::vec::Vec<String> = data[0..super::PAGINATOR_LIMIT]
                .iter()
                .map(|s| s.clone())
                .collect();
            then.header("Link", &p2_link).json_body(res);
        });
        let mock_resources_p2 = server.mock(|when, then| {
            when.query_param("last", &data[super::PAGINATOR_LIMIT - 1]);
            let res: std::vec::Vec<String> = data
                [super::PAGINATOR_LIMIT..super::PAGINATOR_LIMIT * 2]
                .iter()
                .map(|s| s.clone())
                .collect();
            then.header("Link", &p3_link).json_body(res);
        });
        let mock_resources_p3 = server.mock(|when, then| {
            when.query_param("last", &data[super::PAGINATOR_LIMIT * 2 - 1]);
            let res: std::vec::Vec<String> = data[super::PAGINATOR_LIMIT * 2..]
                .iter()
                .map(|s| s.clone())
                .collect();
            then.json_body(res);
        });
        /*
        let mock_resources = server.mock(|when, then| {
            let res: std::vec::Vec<String> = data.iter().map(|s| s.clone()).collect();
            then.json_body(res);
        });
        */
        let mut r: std::vec::Vec<String> = std::vec::Vec::new();
        let c = reqwest::blocking::Client::new();
        for mut e in super::Paginator::<std::vec::Vec<String>, _>::new(
            &c,
            |l| Ok(l.get(&resources_url)),
            None,
        ) {
            r.append(&mut e);
        }
        mock_resources_p1.assert();
        mock_resources_p2.assert();
        mock_resources_p3.assert();
        itertools::assert_equal(data, r);
    }
}

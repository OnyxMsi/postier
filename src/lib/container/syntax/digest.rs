/// See the [documentation](https://distribution.github.io/distribution/spec/api/#content-digests)
use crate::lib::container;

enum Algorithm {
    Sha256,
}

pub struct Digest<'d> {
    algorithm: Algorithm,
    pub hex: &'d str,
}

impl<'d> Digest<'d> {
    pub fn parse(txt: &'d str) -> Result<Self, container::Error> {
        log::debug!("Parse digest \"{}\"", txt);
        let colon = txt.find(":").ok_or_else(|| {
            log::error!("Invalid digest \"{}\": can't find a colon", txt);
            container::Error::DistributionError
        })?;
        if colon == txt.len() - 1 {
            log::error!("Invalid digest \"{}\": no hex", txt);
            return Err(container::Error::DistributionError);
        }
        if colon == 0 {
            log::error!("Invalid digest \"{}\": no algorithm", txt);
            return Err(container::Error::DistributionError);
        }
        let algorithm = match &txt[..colon] {
            "sha256" => Algorithm::Sha256,
            v => {
                log::error!(
                    "Invalid digest \"{}\": unsupported algorithm algorithm {}",
                    txt,
                    v
                );
                return Err(container::Error::DistributionError);
            }
        };
        let hex = &txt[colon + 1..];
        Ok(Self { hex, algorithm })
    }
    /// Test a path hash match this digest
    /// Any error will make the function return false
    pub fn check_path(&self, path: &std::path::Path) -> bool {
        let v = match hex::decode(self.hex) {
            Ok(h) => h,
            Err(_) => return false,
        };
        match self.algorithm {
            Algorithm::Sha256 => {
                crate::lib::utils::check_file_and_hash::<sha2::Sha256>(path, v.as_slice())
            }
        }
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn parse() {
        assert!(super::Digest::parse(":aabbcc").is_err());
        assert!(super::Digest::parse("sha256:").is_err());
        assert!(super::Digest::parse("sha256").is_err());
        let l = super::Digest::parse("sha256:aabbccddeeff").unwrap();
        match l.algorithm {
            super::Algorithm::Sha256 => assert!(true),
            _ => unreachable!(),
        };
        assert_eq!(l.hex, "aabbccddeeff");
    }
}

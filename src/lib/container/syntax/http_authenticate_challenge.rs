/// See [the RFC](https://datatracker.ietf.org/doc/html/rfc9110#name-challenge-and-response) for
/// explanations
/// The implementation of this syntax is minimal for the usage on the API
use pest::iterators::Pair;
use pest::Parser;
use pest_derive::Parser;

#[derive(Parser)]
#[grammar = "lib/container/grammar/http_authenticate_challenge.pest"]
struct ChallengeParser;

pub enum ChallengeValue<'c> {
    Token(&'c str),
    Parameters(std::collections::HashMap<&'c str, &'c str>),
}

pub struct Challenge<'c> {
    pub auth_scheme: &'c str,
    pub auth_value: ChallengeValue<'c>,
}

impl<'c> Challenge<'c> {
    pub fn parse(txt: &'c str) -> Result<Self, crate::lib::container::Error> {
        log::debug!("Parse HTTP authentication challenge \"{}\"", txt);
        let pair = ChallengeParser::parse(Rule::challenge, txt)
            .map_err(|e| {
                log::error!(
                    "Can't parse HTTP authentication challenge \"{}\": {}",
                    txt,
                    e
                );
                crate::lib::container::Error::DistributionError
            })?
            .next()
            .unwrap();
        Ok(parse_challenge(pair))
    }
}

fn parse_challenge(pair: Pair<Rule>) -> Challenge {
    let mut scheme = None;
    let mut value = None;
    for p in pair.into_inner() {
        match p.as_rule() {
            Rule::auth_scheme => {
                scheme = Some(p.as_str());
            }
            Rule::token68 => {
                value = Some(ChallengeValue::Token(p.as_str()));
            }
            Rule::auth_parameters => {
                value = Some(ChallengeValue::Parameters(parse_http_parameters(p)));
            }
            Rule::EOI => {}
            _ => unreachable!(),
        }
    }
    Challenge {
        auth_scheme: scheme.unwrap(),
        auth_value: value.unwrap(),
    }
}
fn parse_http_parameters(pair: Pair<Rule>) -> std::collections::HashMap<&str, &str> {
    std::collections::HashMap::from_iter(pair.into_inner().map(parse_http_parameter))
}
fn parse_http_parameter(pair: Pair<Rule>) -> (&str, &str) {
    let mut it = pair.into_inner();
    (it.next().unwrap().as_str(), it.next().unwrap().as_str())
}

#[cfg(test)]
mod test {
    fn c(s: &str) -> super::Challenge {
        super::Challenge::parse(s).unwrap()
    }
    #[test]
    fn parse() {
        let a = c("tttttt 00aaff");
        assert_eq!(a.auth_scheme, "tttttt");
        match a.auth_value {
            super::ChallengeValue::Token(s) => assert_eq!(s, "00aaff"),
            _ => unreachable!(),
        };
        let a = c("ooo aa=bb,cc=\"dd\"");
        assert_eq!(a.auth_scheme, "ooo");
        let p = std::collections::HashMap::from([("aa", "bb"), ("cc", "dd")]);
        match a.auth_value {
            super::ChallengeValue::Parameters(t) => assert_eq!(t, p),
            _ => unreachable!(),
        };
    }
}

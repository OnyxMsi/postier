mod registry;
mod syntax;

#[derive(Debug)]
pub enum Error {
    RegistryError,
    PackageError,
    LibError(super::Error),
}

trait Parameters {
    fn get_directory(&self) -> &std::path::Path;
}

pub struct Source<'s> {
    registries: std::vec::Vec<url::Url>,
    dependencies: std::vec::Vec<Dependency>,
    parameters: &'s dyn Parameters,
}

/// Something to look for. Will be solved using [TSource::fetch]
pub struct Dependency {
    text: String,
    include_pre_release: bool,
}
impl Dependency {
    /// Create a new dependency. In theory the text can be anything defined in
    /// [npm package.json](https://docs.npmjs.com/cli/v10/configuring-npm/package-json#dependencies)
    /// however some of these are meaningless in our case
    pub fn new(text: String) -> Self {
        Self {
            text,
            include_pre_release: false,
        }
    }
    /// See the [documentation](https://github.com/npm/node-semver#functions)
    pub fn include_pre_release(mut self, v: bool) -> Self {
        self.include_pre_release = v;
        self
    }
}

impl<'s> Source<'s> {
    fn new(parameters: &'s dyn Parameters) -> Result<Self, Error> {
        Ok(Source {
            registries: std::vec::Vec::new(),
            dependencies: std::vec::Vec::new(),
            parameters,
        })
    }
    /// Add a new registry URL (compatible with [this](https://github.com/npm/registry/blob/main/docs/REGISTRY-API.md))
    pub fn add_registry(&mut self, index: url::Url) {
        log::debug!("Add index {}", index);
        self.registries.push(index);
    }
    /// Add a new [Dependency] to solve
    pub fn add_dependency(&mut self, dependency: Dependency) {
        log::debug!("Add dependency {}", dependency.text);
        self.dependencies.push(dependency);
    }
}

impl super::Source<Error> for Source<'_> {
    fn init(&self) -> Result<(), Error> {
        crate::lib::utils::create_directory(self.parameters.get_directory())
            .map_err(Error::LibError)?;
        Ok(())
    }
    fn fetch(&self) -> Result<(), Error> {
        let client = reqwest::blocking::Client::new();
        Ok(())
    }
}

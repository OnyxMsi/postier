pub mod dependency;
pub mod distribution;
pub(super) mod metadata;
pub mod simple_repository;
mod specification;
mod specifier;
pub mod version;

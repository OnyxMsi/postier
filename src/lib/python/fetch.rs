use super::distribution as pdistribution;
use super::syntax::{dependency, distribution, simple_repository};
use crate::lib::{python, utils};

#[derive(Hash, PartialEq, Eq, Clone)]
struct Entry {
    dependency: String,
    environment_marker: python::EnvironmentMarker,
}

struct FetchEntry<'f> {
    package: &'f simple_repository::Package,
    repository: &'f std::path::Path,
}
impl crate::lib::cache::FetchItem for FetchEntry<'_> {
    fn request_factory(
        &self,
        client: &reqwest::blocking::Client,
    ) -> reqwest::blocking::RequestBuilder {
        client.get(self.package.url.as_str())
    }
    fn get_sha256(&self) -> std::vec::Vec<u8> {
        self.package.sha256.as_ref().unwrap().as_bytes().into()
    }
    fn path_factory(&self) -> std::path::PathBuf {
        self.get_path()
    }
}
impl FetchEntry<'_> {
    fn get_path(&self) -> std::path::PathBuf {
        self.repository
            .join(utils::url_last_segment!(self.package.url).unwrap())
    }
}
/* Simple wrapper to download a package using a cache */
fn fetch(
    cache: &mut crate::lib::cache::FetchCache,
    package: &simple_repository::Package,
    repository: &std::path::Path,
) -> Result<std::path::PathBuf, crate::lib::Error> {
    let f = FetchEntry {
        package,
        repository,
    };
    /* If sha256 is not set, then just fetch it */
    match package.sha256 {
        None => {
            let p = f.get_path();
            /* Really not efficient */
            let c = reqwest::blocking::Client::new();
            utils::blocking_fetch_url(c.get(f.package.url.as_str()), p.as_path())?;
            Ok(p)
        }
        Some(_) => cache.get(&f),
    }
}

pub struct Fetcher<'a> {
    /* Parameters */
    repository: &'a std::path::Path,
    working_repository: &'a std::path::Path,
    interpreter: Option<&'a std::path::Path>,
    requirements: &'a [python::Requirement],
    environment_markers: &'a [python::EnvironmentMarker],
    /* Index parser */
    repository_reader: simple_repository::SimpleRepository<'a>,
    /* Web client */
    fetch_cache: crate::lib::cache::FetchCache,
}

macro_rules! distribution_version {
    ( $dist:expr ) => {
        match $dist {
            distribution::Distribution::Source(s) => &s.version,
            distribution::Distribution::Binary(b) => &b.version,
        }
    };
}

/* Compare a list of dependencies extracted from a distribution file with current environment
 * The goal is to filter only those whose extra is expected
 * Those without extra are always kept
 * */
fn filter_dependency<'a>(
    dependency: &'a pdistribution::Dependency,
    markers: &python::EnvironmentMarker,
) -> Option<&'a str> {
    let (dep, extra) = dependency;
    if extra.is_none() || extra.as_ref() == markers.extra.as_ref() {
        Some(dep.as_str())
    } else {
        None
    }
}
macro_rules! filter_dependencies {
    ($dependencies:expr, $markers:expr) => {
        $dependencies
            .iter()
            .filter_map(|d| filter_dependency(d, $markers))
    };
}

impl<'a> Fetcher<'a> {
    pub fn new(
        repository: &'a std::path::Path,
        working_repository: &'a std::path::Path,
        interpreter: Option<&'a std::path::Path>,
        indexes: &'a [python::Index],
        requirements: &'a [python::Requirement],
        environment_markers: &'a [python::EnvironmentMarker],
    ) -> Self {
        Fetcher {
            repository,
            working_repository,
            interpreter,
            requirements,
            environment_markers,
            repository_reader: simple_repository::SimpleRepository::new(indexes),
            fetch_cache: crate::lib::cache::FetchCache::new(),
        }
    }
    /* Helper function to compare distribution */
    fn compare_distribution(
        &self,
        dist1: &distribution::Distribution,
        dist2: &distribution::Distribution,
    ) -> std::cmp::Ordering {
        let v1 = distribution_version!(dist1);
        let v2 = distribution_version!(dist2);
        match v1.cmp(v2) {
            std::cmp::Ordering::Equal => {
                /* If both version are equal, prefer binary over source */
                match dist1 {
                    distribution::Distribution::Binary(_) => match dist2 {
                        distribution::Distribution::Source(_) => std::cmp::Ordering::Greater,
                        _ => std::cmp::Ordering::Equal,
                    },
                    _ => match dist2 {
                        distribution::Distribution::Binary(_) => std::cmp::Ordering::Less,
                        _ => std::cmp::Ordering::Equal,
                    },
                }
            }
            r => r,
        }
    }
    fn solve_candidates(
        &self,
        candidates: &[&'a simple_repository::Package],
        dep: &dependency::Dependency,
        markers: &python::EnvironmentMarker,
    ) -> Option<(
        &'a simple_repository::Package,
        distribution::Distribution<'a>,
    )> {
        /* 1. Parse filename as a distribution, ignore unknowns formats.
         * 2. Keep those which match dependency version and environment markers
         * 2. Return the candidate with the highiest version.
         *    If two distribution have the same version, preffer a binary distribution
         */
        candidates
            .iter()
            .filter_map(|package| {
                log::debug!("Check distribution {}", package.filename);
                let dist = distribution::Distribution::parse(&package.filename).ok()?;
                match dist {
                    distribution::Distribution::Binary(ref d) => {
                        if !d.match_environment(markers)
                            || !dep.match_version(d.distribution, &d.version)
                        {
                            return None;
                        }
                    }
                    distribution::Distribution::Source(ref d) => {
                        if !dep.match_version(d.distribution, &d.version) {
                            return None;
                        }
                    }
                };
                Some((*package, dist))
            })
            .max_by(|e1, e2| {
                let (_, dist1) = e1;
                let (_, dist2) = e2;
                self.compare_distribution(dist1, dist2)
            })
    }
    /* How to fetch a package
     * The network part is made using a simple_repository fetcher
     * It returns every candidates this dependency has
     * Once a single candidate is found it is downloaded and returned
     * */
    fn fetch_entry(
        &mut self,
        dependency: &dependency::Dependency,
        entry: &Entry,
    ) -> Result<Option<std::path::PathBuf>, python::Error> {
        log::debug!("Solve and fetch {}", entry.dependency);
        self.repository_reader.fetch(&dependency.package)?;
        let candidates = self.repository_reader.get(&dependency.package).unwrap();
        match self.solve_candidates(
            candidates.as_slice(),
            &dependency,
            &entry.environment_marker,
        ) {
            Some(e) => {
                let (package, _) = e;
                fetch(&mut self.fetch_cache, package, self.repository)
                    .map_err(python::Error::LibError)
                    .map(|p| Some(p))
            }
            None => {
                log::info!("Can't find a candidate that solve {}", entry.dependency);
                Ok(None)
            }
        }
    }
    /* Extract dependencies from a distribution file
     * This is mostly a wrapper
     */
    fn fetch_dependencies(
        &self,
        path: &std::path::Path,
    ) -> Result<pdistribution::Dependencies, python::Error> {
        log::debug!("Extract dependencies from {}", path.display());
        let p = path.file_name().unwrap().to_str().unwrap();
        match distribution::Distribution::parse(p)? {
            distribution::Distribution::Source(s) => pdistribution::source::find_dependencies(
                &s,
                path,
                self.working_repository,
                self.interpreter,
            ),
            distribution::Distribution::Binary(b) => {
                pdistribution::binary::find_dependencies(&b, path)
            }
        }
    }
    pub fn run(&mut self, parameters: &dyn python::Parameters) -> Result<(), python::Error> {
        /* These are cache in order to avoid doing the same thing 100 times */
        let mut entries_pool: std::collections::HashSet<Entry> = std::collections::HashSet::new();
        /* The stack of entries to solve to work with */
        let mut stack: std::collections::VecDeque<Entry> = std::collections::VecDeque::new();
        /* This will push a new entry on the stack if it was not already processed */
        macro_rules! push_entry {
            ($dep:expr, $environment:expr) => {{
                let dependency = format!("{}", $dep);
                let entry = Entry {
                    dependency,
                    environment_marker: $environment,
                };
                if (entries_pool.insert(entry.clone())) {
                    parameters
                        .push_dependency(entry.dependency.as_str(), &entry.environment_marker);
                    stack.push_front(entry);
                } else {
                    log::debug!("Skip dependency \"{}\" since it was already solved", $dep);
                }
            }};
        }
        /* Push as many dependencies as required given a dependency::Dependency and an environment */
        macro_rules! push_dependencies {
            ($dep:expr, $environment:expr) => {{
                /* Add one without extra */
                let mut e = $environment.clone();
                e.extra = None;
                push_entry!($dep, e);
                /* Then one for each extra the dependency requires */
                for extra in &$dep.extras {
                    let mut e = $environment.clone();
                    e.extra = Some(extra.to_string());
                    push_entry!($dep, e);
                }
            }};
        }
        /* First thing is to populate the stack with parameters */
        for requirement in self.requirements {
            match dependency::Dependency::parse(&requirement.text) {
                Ok(d) => {
                    for env in self.environment_markers {
                        push_dependencies!(d, env);
                    }
                }
                Err(_e) => {
                    log::warn!("Invalid dependency '{}'", requirement.text);
                }
            }
        }
        let mut ret = Ok(());
        while stack.len() > 0 {
            let top = stack.pop_front().unwrap();
            let d = super::syntax::dependency::Dependency::parse(&top.dependency)?;
            parameters.solve_dependency_start(&d, &top.environment_marker);
            match self.fetch_entry(&d, &top) {
                Ok(package) => {
                    if let Some(p) = package {
                        /* First fetch dependencies, then keep only what is required */
                        let pdependencies = self.fetch_dependencies(&p)?;
                        for dep in filter_dependencies!(pdependencies, &top.environment_marker) {
                            log::debug!("Push dependency '{}'", &dep);
                            match dependency::Dependency::parse(&dep) {
                                Ok(depp) => push_dependencies!(depp, top.environment_marker),
                                Err(_e) => log::debug!("Skip unsupported dependency {}", dep),
                            }
                        }
                        parameters.dependency_solved(&d, &top.environment_marker);
                    } else {
                        log::warn!("Can't find a candidate for {}", top.dependency);
                        let e = python::Error::PythonScriptError;
                        parameters.solve_dependency_failed(&d, &top.environment_marker, &e);
                        ret = Err(e);
                    }
                }
                Err(e) => {
                    log::warn!("Can't solve {}: {:?}", top.dependency, e);
                    parameters.solve_dependency_failed(&d, &top.environment_marker, &e);
                }
            }
        }
        if ret.is_err() {
            log::error!("Could not solve every requirement");
        }
        ret
    }
}

#[cfg(test)]
mod test {
    use crate::lib::python;
    use crate::lib::python::syntax;
    use crate::lib::utils;
    fn get_index(u: &str) -> python::Index {
        python::Index {
            url: url::Url::parse(u).unwrap(),
        }
    }
    fn get_requirement(r: &str) -> python::Requirement {
        python::Requirement {
            text: r.to_string(),
        }
    }
    macro_rules! get_version {
        ($r:literal) => {
            syntax::version::Version::parse($r).unwrap()
        };
    }
    macro_rules! get_dependency {
        ($d:literal) => {
            syntax::dependency::Dependency::parse($d).unwrap()
        };
    }
    macro_rules! get_markers {
        () => {
        python::EnvironmentMarker {
            python_version: get_version!("1.2"),
            python_full_version: get_version!("1.2.3"),
            os_name: utils::test::get_text(),
            sys_platform: utils::test::get_text(),
            platform_release: utils::test::get_text(),
            platform_system: utils::test::get_text(),
            platform_version: utils::test::get_text(),
            platform_machine: utils::test::get_text(),
            platform_python_implementation: utils::test::get_text(),
            implementation_name: utils::test::get_text(),
            implementation_version: get_version!("2.3"),
            extra: None,
            /* This is for https://packaging.python.org/en/latest/specifications/platform-compatibility-tags/#abi-tag
             * I can't find a way to guess it from other field
             */
            abi_tags: std::vec::Vec::new(),
            /* This is for https://packaging.python.org/en/latest/specifications/platform-compatibility-tags/#abi-tag
             * If none then it will be build using platform_system + platform_machine
             */
            platform_tags: std::vec::Vec::new(),
        }
        }
    }
    #[test]
    fn compare_distribution() {
        let d = utils::test::get_temp_dir();
        let i: [python::Index; 1] = [get_index("http://www.perdu.com")];
        let r: [python::Requirement; 1] = [get_requirement("test")];
        let m: [python::EnvironmentMarker; 1] = [get_markers!()];
        let f = super::Fetcher::new(&d, &d, None, &i, &r, &m);
        macro_rules! get_distribution {
            ($d:literal) => {
                syntax::distribution::Distribution::parse($d).unwrap()
            };
        }
        macro_rules! c {
            ($d1:literal, $d2:literal) => {
                f.compare_distribution(&get_distribution!($d1), &get_distribution!($d2))
            };
        }
        /* Only the version and distribution type matters */
        assert_eq!(
            c!("pkg-1.0-py3-none-any.whl", "other-1.1-py2-none-any.whl"),
            std::cmp::Ordering::Less
        );
        assert_eq!(
            c!("pkg-1.0-py3-none-any.whl", "other-1.0-py2-none-any.whl"),
            std::cmp::Ordering::Equal
        );
        assert_eq!(
            c!("pkg-1.0-py3-none-any.whl", "other-1.0.tar.gz"),
            std::cmp::Ordering::Greater
        );
        assert_eq!(
            c!("pkg-1.0.tar.gz", "other-1.0-py3-none-any.whl"),
            std::cmp::Ordering::Less
        );
    }
    #[test]
    fn solve_candidates() {
        let d = utils::test::get_temp_dir();
        let i: [python::Index; 1] = [get_index("http://www.perdu.com")];
        let r: [python::Requirement; 1] = [get_requirement("test")];
        let m: [python::EnvironmentMarker; 1] = [get_markers!()];
        macro_rules! get_binary_package {
            ($name:literal, $version:literal, $python:literal) => {
                syntax::simple_repository::Package {
                    filename: format!("{}-{}-{}-none-any.whl", $name, $version, $python),
                    url: url::Url::parse("http://www.perdu.com").unwrap(),
                    sha256: None,
                }
            };
        }
        macro_rules! get_source_package {
            ($name:literal, $version:literal) => {
                syntax::simple_repository::Package {
                    filename: format!("{}-{}.tar.gz", $name, $version),
                    url: url::Url::parse("http://www.perdu.com").unwrap(),
                    sha256: None,
                }
            };
        }
        let f = super::Fetcher::new(&d, &d, None, &i, &r, &m);
        let dep = get_dependency!("pkg");
        let mut m = get_markers!();
        m.implementation_version = get_version!("3.2");
        let candidates: [&syntax::simple_repository::Package; 6] = [
            &get_binary_package!("pkg", "1.1", "py3"), /* Expected*/
            &get_binary_package!("pkg", "1.2", "py2"), /* Invalid environment */
            &get_binary_package!("pkg", "1.1", "py2"), /* Invalid environment */
            &get_binary_package!("pkg", "1.0", "py3"), /* Lower version */
            &get_source_package!("pkg", "1.1"),        /* Prefer binary version if any */
            &get_source_package!("pkg", "1.0"),        /* Lower version */
        ];
        match f.solve_candidates(&candidates, &dep, &m) {
            Some(e) => {
                let (p, d) = e;
                assert_eq!(p.filename, "pkg-1.1-py3-none-any.whl");
                match d {
                    syntax::distribution::Distribution::Binary(b) => {
                        assert_eq!(b.version, get_version!("1.1"));
                    }
                    _ => assert!(false),
                }
            }
            None => assert!(false),
        }
        m.implementation_version = get_version!("2.7");
        match f.solve_candidates(&candidates, &dep, &m) {
            Some(e) => {
                let (p, d) = e;
                assert_eq!(p.filename, "pkg-1.2-py2-none-any.whl");
                match d {
                    syntax::distribution::Distribution::Binary(b) => {
                        assert_eq!(b.version, get_version!("1.2"));
                    }
                    _ => assert!(false),
                }
            }
            None => assert!(false),
        }
        let dep = get_dependency!("pkg > 2.0");
        assert!(f.solve_candidates(&candidates, &dep, &m).is_none());
    }
    #[test]
    fn filter_dependency() {
        macro_rules! test {
            ($extra:expr, $env:expr) => {
                super::filter_dependency(&("pkg".to_string(), $extra.map(|s| s.to_string())), &$env)
            };
        }
        let none = get_markers!();
        let mut first = get_markers!();
        first.extra = Some("first".to_string());

        assert_eq!(test!(None::<String>, none), Some("pkg"));
        assert_eq!(test!(Some("first"), none), None);
        assert_eq!(test!(Some("second"), none), None);
        assert_eq!(test!(None::<String>, first), Some("pkg"));
        assert_eq!(test!(Some("first"), first), Some("pkg"));
        assert_eq!(test!(Some("second"), none), None);
    }
}

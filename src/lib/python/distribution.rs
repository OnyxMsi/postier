/* Type aliases:
 * In this module a dependency is a two item tuple:
 * - a dependency as defined in https://packaging.python.org/en/latest/specifications/dependency-specifiers/
 *   but not parsed yet
 * - the "extra" tag for this dependency to apply
 * */
pub type Dependency = (String, Option<String>);
pub type Dependencies = std::vec::Vec<Dependency>;
pub(super) mod binary;
pub(super) mod source;

#[cfg(test)]
pub(super) mod test {
    /* A conversion function so you can use assert_* with dependencies
     * The returned vector is sorted by extra then by dependencies
     */
    pub fn dependencies<'a>(
        deps: &'a super::Dependencies,
    ) -> std::vec::Vec<(&'a str, Option<&'a str>)> {
        let mut r = deps
            .iter()
            .map(|e| {
                let (d, ex) = e;
                (d.as_str(), ex.as_ref().map(|s| s.as_str()))
            })
            .collect::<std::vec::Vec<(&str, Option<&str>)>>();
        r.sort_by(|e1, e2| {
            let (dep1, extra1) = e1;
            let (dep2, extra2) = e2;
            /* No extra is lower than an extra */
            match extra1 {
                Some(ex1) => match extra2 {
                    Some(ex2) => match ex1.cmp(ex2) {
                        std::cmp::Ordering::Equal => dep1.cmp(dep2),
                        e => e,
                    },
                    None => std::cmp::Ordering::Greater,
                },
                None => {
                    if extra2.is_some() {
                        std::cmp::Ordering::Less
                    } else {
                        dep1.cmp(dep2)
                    }
                }
            }
        });
        r
    }
}

use crate::lib::python;
use std::io::Write;

const CURRENT_ENVIRONMENT_MARKER: &[u8] = include_bytes!("current_environment_markers.py");
const EXTRACT_DEPENDENCIES_FROM_SETUP: &[u8] = include_bytes!("extract_dependencies_from_setup.py");

pub fn execute_python_code(
    interpreter: &std::path::Path,
    code: &[u8],
    args: &[&str],
) -> Result<String, python::Error> {
    let mut cmd = std::process::Command::new(interpreter);
    let cmd2 = cmd.arg("-").args(args);
    let mut proc = cmd2
        .stdin(std::process::Stdio::piped())
        .stdout(std::process::Stdio::piped())
        .spawn()
        .map_err(|e| {
            log::error!("Python process failed: {}", e);
            python::Error::PythonScriptError
        })?;
    if let Some(mut stdin) = proc.stdin.take() {
        stdin.write_all(code).map_err(|e| {
            log::error!("Can't set python process input: {}", e);
            python::Error::PythonScriptError
        })?;
    }
    let output = proc.wait_with_output().map_err(|e| {
        log::error!("Can't get python process output: {}", e);
        python::Error::PythonScriptError
    })?;
    String::from_utf8(output.stdout).map_err(|e| {
        log::error!("Can't convert python process output to string: {}", e);
        python::Error::PythonScriptError
    })
}

pub fn current_environment_markers(interpreter: &std::path::Path) -> Result<String, python::Error> {
    log::debug!("Execute python code to get current environment_markers");
    execute_python_code(interpreter, CURRENT_ENVIRONMENT_MARKER, &[])
}
pub fn extract_dependencies_from_setup_py(
    interpreter: &std::path::Path,
    setup_py_path: &std::path::Path,
) -> Result<String, python::Error> {
    log::debug!(
        "Execute python code to extract dependencies from setup.py {}",
        setup_py_path.display()
    );
    let s = setup_py_path.as_os_str().to_str().ok_or_else(|| {
        log::error!("Can't convert path {} to string", setup_py_path.display());
        python::Error::InvalidDistributionError
    })?;
    execute_python_code(interpreter, EXTRACT_DEPENDENCIES_FROM_SETUP, &[s])
}

#[cfg(test)]
pub mod test {
    pub fn get_interpreter() -> std::path::PathBuf {
        std::path::PathBuf::new().join("python3")
    }
    pub fn check_interpreter() -> bool {
        std::process::Command::new(&get_interpreter())
            .arg("--version")
            .status()
            .is_ok()
    }
    #[test]
    /* This test requires python3 executable in the current user path */
    fn execute_python_code() {
        if check_interpreter() {
            let r = super::execute_python_code(
                &get_interpreter(),
                "import sys ; sys.stdout.write('ALLO')".as_bytes(),
                &[],
            )
            .unwrap();
            assert_eq!(r, "ALLO");
        }
    }
}

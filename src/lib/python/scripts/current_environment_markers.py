# This script is to be run by calling it as an argument of a python interpreter
# It will print the interpreter current environment markers, as defined in https://packaging.python.org/en/latest/specifications/dependency-specifiers/#environment-markers
import os
import sys
import platform
import packaging.tags
import json


def p(k: str, v: str):
    print(f"{k},{v}")


def format_full_version(info):
    version = "{0.major}.{0.minor}.{0.micro}".format(info)
    kind = info.releaselevel
    if kind != "final":
        version += kind[0] + str(info.serial)
    return version


def abi_tags():
    a = (t.abi for t in packaging.tags.sys_tags())
    a = set(filter(lambda t: t != "none", a))
    return list(a)


def platform_tags():
    p = (t.platform for t in packaging.tags.sys_tags())
    p = set(filter(lambda t: t != "any", p))
    return list(p)


if hasattr(sys, "implementation"):
    implementation_version = format_full_version(sys.implementation.version)
else:
    implementation_version = "0"

print(
    json.dumps(
        {
            "os_name": os.name,
            "sys_platform": sys.platform,
            "platform_machine": platform.machine(),
            "platform_python_implementation": platform.python_implementation(),
            "platform_release": platform.release(),
            "platform_system": platform.system(),
            "platform_version": platform.version(),
            "python_version": ".".join(platform.python_version_tuple()[:2]),
            "python_full_version": platform.python_version(),
            "implementation_name": sys.implementation.name,
            "implementation_version": implementation_version,
            "abi_tags": abi_tags(),
            "platform_tags": platform_tags(),
        }
    )
)

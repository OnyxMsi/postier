import unittest.mock
import sys
import os

SEPARATOR = "###"


# Output needs to be a valid METADATA file as for binary distributions
REQUIRES_DIST = "Requires-Dist"
PROVIDES_EXTRA = "Provides-Extra"


def p_dep(dep):
    print(f"{REQUIRES_DIST}: {dep}")


def p_extra(extra):
    print(f"{PROVIDES_EXTRA}: {extra}")


def fake_setup(*a, **kw):
    for dep in kw.get("install_requires", []):
        p_dep(dep)
    for extra, deps in kw.get("extras_require", {}).items():
        p_extra(extra)
        for dep in deps:
            p_dep(dep)


PATCH = unittest.mock.patch("setuptools.setup", fake_setup)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit(1)
    else:
        f = open(sys.argv[1])
        # Move into the script directory
        # And set variables to match him
        rundir = os.path.dirname(sys.argv[1])
        os.chdir(rundir)
        __file__ = sys.argv[0]
        with f as fd, PATCH as p:
            exec(fd.read())

use super::{specification, version};
use crate::lib::python;

pub struct ComparedVersion {
    pub(super) epoch: usize,
    pub(super) prefix_matching: bool,
    pub(super) release_segments: std::vec::Vec<usize>,
    pub(super) suffix: version::VersionSuffix,
}
impl ComparedVersion {
    pub fn parse(s: &str) -> Result<Self, python::Error> {
        super::specification::parse(
            super::specification::Rule::compared_version,
            s,
            super::specification::parse_compare_version,
        )
    }
    /* Extract the version (ignore the prefix)*/
    pub fn to_version(&self) -> version::Version {
        version::Version {
            epoch: self.epoch,
            release_segments: self.release_segments.to_vec(),
            suffix: self.suffix,
        }
    }
    /* Build a compared version from a version */
    pub fn from_version(v: &version::Version, prefix_matching: bool) -> Self {
        ComparedVersion {
            epoch: v.epoch,
            release_segments: v.release_segments.to_vec(),
            suffix: v.suffix,
            prefix_matching,
        }
    }
}
impl std::fmt::Display for ComparedVersion {
    /* This must be the same as version display */
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        super::version::version_display(f, self.epoch, &self.release_segments, &self.suffix)?;
        if self.prefix_matching {
            write!(f, ".*");
        }
        Ok(())
    }
}

/* version::Version ordering is too complex to use Ord and PartialOrd traits */
/* https://packaging.python.org/en/latest/specifications/version-specifiers/#compatible-release
 * ~= V.N is equivalent to >= V.N and == V.*
 * */
fn compatible_release(v: &version::Version, c: &ComparedVersion) -> bool {
    if c.release_segments.len() < 2 {
        log::error!("Compatible release specifier must have more than one version number segment");
        return false;
    }
    let mut cv = c.to_version();
    cv.suffix = version::VersionSuffix::Empty;
    let mut c2 = ComparedVersion::from_version(&cv, true);
    c2.release_segments.pop();
    v >= &cv && version_matching(v, &c2)
}
/* https://packaging.python.org/en/latest/specifications/version-specifiers/#version-matching */
pub(super) fn version_matching(v: &version::Version, c: &ComparedVersion) -> bool {
    /* If no prefix then its simple */
    if !c.prefix_matching {
        v == &c.to_version()
    } else if v.epoch != c.epoch {
        false
    } else {
        /* If version is shorter then it cannot match */
        if v.release_segments.len() < c.release_segments.len() {
            false
        } else {
            for (vs, cs) in std::iter::zip(&v.release_segments, &c.release_segments) {
                if vs != cs {
                    return false;
                }
            }
            true
        }
    }
}
/* https://packaging.python.org/en/latest/specifications/version-specifiers/#version-matching */
pub(super) fn version_exclusion(v: &version::Version, c: &ComparedVersion) -> bool {
    !version_matching(v, c)
}
/* https://packaging.python.org/en/latest/specifications/version-specifiers/#compatible-release */
pub(super) fn inclusive_ordered_comparaison_lesser(
    v: &version::Version,
    c: &ComparedVersion,
) -> bool {
    v <= &c.to_version()
}
pub(super) fn inclusive_ordered_comparaison_greater(
    v: &version::Version,
    c: &ComparedVersion,
) -> bool {
    v >= &c.to_version()
}
/* https://packaging.python.org/en/latest/specifications/version-specifiers/#exclusive-ordered-comparison */
pub(super) fn exclusive_convert_version(v: &version::Version) -> version::Version {
    let suffix = match v.suffix {
        version::VersionSuffix::Pre(_) => version::VersionSuffix::Empty,
        version::VersionSuffix::Post(_) => version::VersionSuffix::Empty,
        _ => v.suffix,
    };
    version::Version {
        epoch: v.epoch,
        release_segments: v.release_segments.to_vec(),
        suffix,
    }
}
pub(super) fn exclusive_ordered_comparaison_lesser(
    v: &version::Version,
    c: &ComparedVersion,
) -> bool {
    exclusive_convert_version(v) < c.to_version()
}
pub(super) fn exclusive_ordered_comparaison_greater(
    v: &version::Version,
    c: &ComparedVersion,
) -> bool {
    match c.suffix {
        /* Keep v post release because c is itself a post release */
        version::VersionSuffix::Post(_) => {
            let suffix = match v.suffix {
                version::VersionSuffix::Pre(vs) => match vs.suffix {
                    version::PreReleaseSuffix::Post(p) => version::VersionSuffix::Post(p),
                    version::PreReleaseSuffix::Development(d) => {
                        version::VersionSuffix::Development(d)
                    }
                    version::PreReleaseSuffix::Empty => version::VersionSuffix::Empty,
                },
                e => e,
            };
            let v2 = version::Version {
                epoch: v.epoch,
                release_segments: v.release_segments.to_vec(),
                suffix,
            };
            v2 > c.to_version()
        }
        _ => exclusive_convert_version(v) > c.to_version(),
    }
}
/* https://packaging.python.org/en/latest/specifications/version-specifiers/#arbitrary-equality */
pub(super) fn arbitrary_equality(v: &version::Version, c: &ComparedVersion) -> bool {
    /* Not supported */
    log::warn!("Python arbitrary equality is not supported, use version matching instead");
    version_matching(v, c)
}
pub enum Operator {
    CompatibleRelease,
    VersionMatching,
    VersionExclusion,
    InclusiveOrderedComparisonLesser,
    InclusiveOrderedComparisonGreater,
    ExclusiveOrderedComparisonLesser,
    ExclusiveOrderedComparisonGreater,
    ArbitraryEquality,
}
impl std::fmt::Display for Operator {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Operator::CompatibleRelease => specification::COMPATIBILITY_RELEASE,
            Operator::VersionMatching => specification::VERSION_MATCHING,
            Operator::VersionExclusion => specification::VERSION_EXCLUSION,
            Operator::InclusiveOrderedComparisonLesser => {
                specification::INCLUSIVE_ORDERED_COMPARISON_LESSER
            }
            Operator::InclusiveOrderedComparisonGreater => {
                specification::INCLUSIVE_ORDERED_COMPARISON_GREATER
            }
            Operator::ExclusiveOrderedComparisonLesser => {
                specification::EXCLUSIVE_ORDERED_COMPARISON_LESSER
            }
            Operator::ExclusiveOrderedComparisonGreater => {
                specification::EXCLUSIVE_ORDERED_COMPARISON_GREATER
            }
            Operator::ArbitraryEquality => specification::ARBITRARY_EQUALITY,
        }
        .fmt(f)
    }
}
pub struct Specifier {
    pub(super) operator: Operator,
    pub(super) compared_version: ComparedVersion,
}

impl Specifier {
    pub(super) fn parse(s: &str) -> Result<std::vec::Vec<Self>, python::Error> {
        super::specification::parse(
            super::specification::Rule::version_specifiers,
            s,
            super::specification::parse_version_specifiers,
        )
    }
    pub fn match_(&self, version: &version::Version) -> bool {
        let fct = match self.operator {
            Operator::CompatibleRelease => compatible_release,
            Operator::VersionMatching => version_matching,
            Operator::VersionExclusion => version_exclusion,
            Operator::InclusiveOrderedComparisonLesser => inclusive_ordered_comparaison_lesser,
            Operator::InclusiveOrderedComparisonGreater => inclusive_ordered_comparaison_greater,
            Operator::ExclusiveOrderedComparisonLesser => exclusive_ordered_comparaison_lesser,
            Operator::ExclusiveOrderedComparisonGreater => exclusive_ordered_comparaison_greater,
            Operator::ArbitraryEquality => arbitrary_equality,
        };
        fct(version, &self.compared_version)
    }
    pub fn match_one(specifiers: &std::vec::Vec<Self>, version: &version::Version) -> bool {
        specifiers.iter().any(|s| s.match_(version))
    }
}
impl std::fmt::Display for Specifier {
    /* This must be the same as version display */
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {}", self.operator, self.compared_version)
    }
}
#[cfg(test)]
mod test {
    use crate::lib::python::syntax::version;
    fn v(s: &str) -> version::Version {
        match version::Version::parse(s) {
            Ok(v) => v,
            Err(_) => panic!("Invalid version {}", s),
        }
    }
    fn c(s: &str) -> super::ComparedVersion {
        match super::ComparedVersion::parse(s) {
            Ok(v) => v,
            Err(_) => panic!("Invalid version {}", s),
        }
    }
    #[test]
    fn compare_version_parse() {
        fn check(
            s: &str,
            epoch: usize,
            release_segments: &[usize],
            prefix_matching: bool,
            suffix: version::VersionSuffix,
        ) {
            let v = c(s);
            assert_eq!(v.epoch, epoch);
            assert_eq!(v.release_segments, release_segments);
            assert_eq!(v.prefix_matching, prefix_matching);
            assert_eq!(v.suffix, suffix);
        }
        check(
            "1.4.5.*",
            0,
            &[1, 4, 5],
            true,
            version::VersionSuffix::Empty,
        );
        check(
            "1.dev0",
            0,
            &[1],
            false,
            version::VersionSuffix::Development(version::DevelopmentRelease { segment: 0 }),
        );
    }
    macro_rules! check_helper {
        ( $vs:literal, $cs:literal, $fct:path, $res:literal ) => {{
            let r = $fct(&v($vs), &c($cs));
            assert!(r == $res)
        }};
    }
    #[test]
    fn version_matching() {
        macro_rules! check {
            ( $vs:literal, $cs:literal, $res:literal ) => {{
                check_helper!($vs, $cs, super::version_matching, $res)
            }};
        }
        check!("1.1.post1", "1.1", false);
        check!("1.1.post1", "1.1.post1", true);
        check!("1.1.post1", "1.1.*", true);
        check!("1.1a1", "1.1", false);
        check!("1.1a1", "1.1a1", true);
        check!("1.1a1", "1.1.*", true);
        check!("1.1", "1.1", true);
        check!("1.1", "1.1.0", true);
        check!("1.1", "1.1.dev1", false);
        check!("1.1", "1.1a1", false);
        check!("1.1", "1.1.post1", false);
        check!("1.1", "1.1.*", true);
    }
    #[test]
    fn version_exclusion() {
        macro_rules! check {
            ( $vs:literal, $cs:literal, $res:literal ) => {{
                check_helper!($vs, $cs, super::version_exclusion, $res)
            }};
        }
        check!("1.1.post1", "1.1", true);
        check!("1.1.post1", "1.1.post1", false);
        check!("1.1.post1", "1.1.*", false);
    }
    #[test]
    fn exclusive_ordered_comparaison() {
        macro_rules! check_greater {
            ( $vs:literal, $cs:literal, $res:literal ) => {{
                check_helper!($vs, $cs, super::exclusive_ordered_comparaison_greater, $res)
            }};
        }
        check_greater!("1.7.1", "1.7", true);
        check_greater!("1.7.0.post1", "1.7", false);
        check_greater!("1.7.1", "1.7.post2", true);
        check_greater!("1.7.0.post3", "1.7.post2", true);
        check_greater!("1.7.0", "1.7.post2", false);
    }
    fn s(s: &str) -> std::vec::Vec<super::Specifier> {
        match super::Specifier::parse(s) {
            Ok(v) => v,
            Err(_) => panic!("Invalid specifiers {}", s),
        }
    }
    #[test]
    fn specifiers_parse() {
        let sp = s("~= 0.9, >= 1.0, != 1.3.4.*, < 2.0");
        assert_eq!(sp.len(), 4);
        matches!(sp[0].operator, super::Operator::CompatibleRelease);
        matches!(
            sp[1].operator,
            super::Operator::InclusiveOrderedComparisonGreater
        );
        matches!(sp[2].operator, super::Operator::VersionExclusion);
        matches!(
            sp[3].operator,
            super::Operator::ExclusiveOrderedComparisonLesser
        );
    }
    #[test]
    fn specifiers_match() {
        macro_rules! check {
            ( $ss:literal, $vs:literal, $res:literal ) => {{
                let sp = s($ss);
                let ve = v($vs);
                assert!(super::Specifier::match_one(&sp, &ve) == $res);
            }};
        }
        check!("< 1, < 0.1", "v1.2", false);
        check!("< 1, < 0.1", "v0.2", true);
    }
    #[test]
    fn display() {
        assert_eq!(s("< 1")[0].to_string(), "< 1");
    }
}

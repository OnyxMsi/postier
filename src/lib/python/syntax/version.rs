/* https://peps.python.org/pep-0440/ */
#[derive(Debug, Clone, Copy, Hash)]
pub(super) enum PreReleaseSegment {
    A(usize),
    B(usize),
    Rc(usize),
}
impl PreReleaseSegment {
    /* For comparaison */
    fn to_integers(&self) -> (usize, &usize) {
        match self {
            PreReleaseSegment::A(s) => (0, s),
            PreReleaseSegment::B(s) => (1, s),
            PreReleaseSegment::Rc(s) => (2, s),
        }
    }
}
impl PartialEq for PreReleaseSegment {
    fn eq(&self, other: &Self) -> bool {
        let (sa, ss) = self.to_integers();
        let (oa, os) = other.to_integers();
        sa == oa && ss == os
    }
}
impl Eq for PreReleaseSegment {}
impl Ord for PreReleaseSegment {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let (sa, ss) = self.to_integers();
        let (oa, os) = other.to_integers();
        sa.cmp(&oa).then(ss.cmp(&os))
    }
}
impl PartialOrd for PreReleaseSegment {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl std::fmt::Display for PreReleaseSegment {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            PreReleaseSegment::A(s) => write!(f, "a{}", s),
            PreReleaseSegment::B(s) => write!(f, "b{}", s),
            PreReleaseSegment::Rc(s) => write!(f, "rc{}", s),
        }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
pub(super) struct DevelopmentRelease {
    pub segment: usize,
}
impl std::fmt::Display for DevelopmentRelease {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, ".dev{}", self.segment)
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub(super) enum PostReleaseSuffix {
    Empty,
    Development(DevelopmentRelease),
}
impl Ord for PostReleaseSuffix {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self {
            PostReleaseSuffix::Empty => match other {
                PostReleaseSuffix::Empty => std::cmp::Ordering::Equal,
                PostReleaseSuffix::Development(_) => std::cmp::Ordering::Greater,
            },
            PostReleaseSuffix::Development(sd) => match other {
                PostReleaseSuffix::Empty => std::cmp::Ordering::Less,
                PostReleaseSuffix::Development(so) => sd.cmp(so),
            },
        }
    }
}
impl PartialOrd for PostReleaseSuffix {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl std::fmt::Display for PostReleaseSuffix {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            PostReleaseSuffix::Development(d) => d.fmt(f),
            _ => Ok(()),
        }
    }
}
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
pub(super) struct PostRelease {
    pub segment: usize,
    pub suffix: PostReleaseSuffix,
}
impl std::fmt::Display for PostRelease {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, ".post{}{}", self.segment, self.suffix)
    }
}
#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub(super) enum PreReleaseSuffix {
    Empty,
    Post(PostRelease),
    Development(DevelopmentRelease),
}
impl Ord for PreReleaseSuffix {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self {
            PreReleaseSuffix::Empty => match other {
                PreReleaseSuffix::Empty => std::cmp::Ordering::Equal,
                PreReleaseSuffix::Post(_) => std::cmp::Ordering::Less,
                PreReleaseSuffix::Development(_) => std::cmp::Ordering::Greater,
            },
            PreReleaseSuffix::Development(sd) => match other {
                PreReleaseSuffix::Empty => std::cmp::Ordering::Less,
                PreReleaseSuffix::Post(_) => std::cmp::Ordering::Less,
                PreReleaseSuffix::Development(so) => sd.cmp(so),
            },
            PreReleaseSuffix::Post(sd) => match other {
                PreReleaseSuffix::Empty => std::cmp::Ordering::Greater,
                PreReleaseSuffix::Post(so) => sd.cmp(so),
                PreReleaseSuffix::Development(_) => std::cmp::Ordering::Greater,
            },
        }
    }
}
impl PartialOrd for PreReleaseSuffix {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl std::fmt::Display for PreReleaseSuffix {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            PreReleaseSuffix::Development(sd) => sd.fmt(f),
            PreReleaseSuffix::Post(sd) => sd.fmt(f),
            _ => Ok(()),
        }
    }
}
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
pub(super) struct PreRelease {
    pub segment: PreReleaseSegment,
    pub suffix: PreReleaseSuffix,
}
impl std::fmt::Display for PreRelease {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{}{}", self.segment, self.suffix)
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub(super) enum VersionSuffix {
    Empty,
    Pre(PreRelease),
    Post(PostRelease),
    Development(DevelopmentRelease),
}
impl Ord for VersionSuffix {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self {
            VersionSuffix::Empty => match other {
                VersionSuffix::Empty => std::cmp::Ordering::Equal,
                VersionSuffix::Pre(_) => std::cmp::Ordering::Greater,
                VersionSuffix::Post(_) => std::cmp::Ordering::Less,
                VersionSuffix::Development(_) => std::cmp::Ordering::Greater,
            },
            VersionSuffix::Pre(sd) => match other {
                VersionSuffix::Empty => std::cmp::Ordering::Less,
                VersionSuffix::Pre(so) => sd.cmp(so),
                VersionSuffix::Post(_) => std::cmp::Ordering::Less,
                VersionSuffix::Development(_) => std::cmp::Ordering::Greater,
            },
            VersionSuffix::Development(sd) => match other {
                VersionSuffix::Empty => std::cmp::Ordering::Less,
                VersionSuffix::Pre(_) => std::cmp::Ordering::Less,
                VersionSuffix::Post(_) => std::cmp::Ordering::Less,
                VersionSuffix::Development(so) => sd.cmp(so),
            },
            VersionSuffix::Post(sd) => match other {
                VersionSuffix::Empty => std::cmp::Ordering::Greater,
                VersionSuffix::Pre(_) => std::cmp::Ordering::Greater,
                VersionSuffix::Post(so) => sd.cmp(so),
                VersionSuffix::Development(_) => std::cmp::Ordering::Greater,
            },
        }
    }
}
impl PartialOrd for VersionSuffix {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl std::fmt::Display for VersionSuffix {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            VersionSuffix::Pre(sd) => sd.fmt(f),
            VersionSuffix::Development(sd) => sd.fmt(f),
            VersionSuffix::Post(sd) => sd.fmt(f),
            _ => Ok(()),
        }
    }
}
#[derive(Eq, Debug, Clone, Hash)]
pub struct Version {
    pub(super) epoch: usize,
    pub(super) release_segments: std::vec::Vec<usize>,
    pub(super) suffix: VersionSuffix,
}

impl Version {
    pub fn parse(s: &str) -> Result<Self, crate::lib::python::Error> {
        super::specification::parse(
            super::specification::Rule::version,
            s,
            super::specification::parse_version,
        )
    }
}
fn compare_release_segments(
    s: &std::vec::Vec<usize>,
    o: &std::vec::Vec<usize>,
) -> std::cmp::Ordering {
    use itertools::Itertools;
    s.iter()
        .zip_longest(o)
        .map(|pair| {
            let (si, oi) = match pair {
                itertools::EitherOrBoth::Right(r) => (0usize, *r),
                itertools::EitherOrBoth::Left(l) => (*l, 0usize),
                itertools::EitherOrBoth::Both(l, r) => (*l, *r),
            };
            si.cmp(&oi)
        })
        .find(|c| *c != std::cmp::Ordering::Equal)
        .unwrap_or(std::cmp::Ordering::Equal)
}
impl PartialEq for Version {
    fn eq(&self, other: &Self) -> bool {
        self.epoch == other.epoch
            && compare_release_segments(&self.release_segments, &other.release_segments)
                == std::cmp::Ordering::Equal
            && self.suffix == other.suffix
    }
}
impl Ord for Version {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.epoch
            .cmp(&other.epoch)
            .then(compare_release_segments(
                &self.release_segments,
                &other.release_segments,
            ))
            .then(self.suffix.cmp(&other.suffix))
    }
}
impl PartialOrd for Version {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
pub(super) fn version_display(
    f: &mut std::fmt::Formatter<'_>,
    epoch: usize,
    segments: &std::vec::Vec<usize>,
    suffix: &VersionSuffix,
) -> std::fmt::Result {
    use std::fmt::Display;
    if epoch > 0 {
        write!(f, "{}!", epoch)?;
    }
    /* Can't find a */
    for (idx, segment) in segments.iter().enumerate() {
        if idx > 0 {
            write!(f, ".{}", segment)?;
        } else {
            segment.fmt(f)?
        }
    }
    suffix.fmt(f)
}
impl std::fmt::Display for Version {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        version_display(f, self.epoch, &self.release_segments, &self.suffix)
    }
}

#[cfg(test)]
mod test {
    fn v(s: &str) -> super::Version {
        match super::Version::parse(s) {
            Ok(v) => v,
            Err(_) => panic!("Invalid version {}", s),
        }
    }
    #[test]
    fn version_parse() {
        fn check(s: &str, epoch: usize, release_segments: &[usize], suffix: super::VersionSuffix) {
            let v = v(s);
            assert_eq!(v.epoch, epoch);
            assert_eq!(v.release_segments, release_segments);
            assert_eq!(v.suffix, suffix);
        }
        check("0.9", 0, &[0, 9], super::VersionSuffix::Empty);
        check("0.9.1", 0, &[0, 9, 1], super::VersionSuffix::Empty);
        check("0.9.10", 0, &[0, 9, 10], super::VersionSuffix::Empty);
        check(
            "1.dev0",
            0,
            &[1],
            super::VersionSuffix::Development(super::DevelopmentRelease { segment: 0 }),
        );
        check(
            "1.dev456",
            0,
            &[1],
            super::VersionSuffix::Development(super::DevelopmentRelease { segment: 456 }),
        );
        check(
            "1.0a1",
            0,
            &[1, 0],
            super::VersionSuffix::Pre(super::PreRelease {
                segment: super::PreReleaseSegment::A(1),
                suffix: super::PreReleaseSuffix::Empty,
            }),
        );
        check(
            "1.0beta2.dev456",
            0,
            &[1, 0],
            super::VersionSuffix::Pre(super::PreRelease {
                segment: super::PreReleaseSegment::B(2),
                suffix: super::PreReleaseSuffix::Development(super::DevelopmentRelease {
                    segment: 456,
                }),
            }),
        );
        check(
            "1.0rc3.post345.dev456",
            0,
            &[1, 0],
            super::VersionSuffix::Pre(super::PreRelease {
                segment: super::PreReleaseSegment::Rc(3),
                suffix: super::PreReleaseSuffix::Post(super::PostRelease {
                    segment: 345,
                    suffix: super::PostReleaseSuffix::Development(super::DevelopmentRelease {
                        segment: 456,
                    }),
                }),
            }),
        );
        check("1.0+abc.5", 0, &[1, 0], super::VersionSuffix::Empty);
        check("1!2.3", 1, &[2, 3], super::VersionSuffix::Empty);
        check("v1!2.3", 1, &[2, 3], super::VersionSuffix::Empty);
        check(
            "1.0.a2",
            0,
            &[1, 0],
            super::VersionSuffix::Pre(super::PreRelease {
                segment: super::PreReleaseSegment::A(2),
                suffix: super::PreReleaseSuffix::Empty,
            }),
        );
        /* Implicit release */
        check(
            "1.0-rc",
            0,
            &[1, 0],
            super::VersionSuffix::Pre(super::PreRelease {
                segment: super::PreReleaseSegment::Rc(0),
                suffix: super::PreReleaseSuffix::Empty,
            }),
        );
        check(
            "1.0-rc.2",
            0,
            &[1, 0],
            super::VersionSuffix::Pre(super::PreRelease {
                segment: super::PreReleaseSegment::Rc(2),
                suffix: super::PreReleaseSuffix::Empty,
            }),
        );
        check(
            "1.0-post",
            0,
            &[1, 0],
            super::VersionSuffix::Post(super::PostRelease {
                segment: 0,
                suffix: super::PostReleaseSuffix::Empty,
            }),
        );
        check(
            "1.0-post_1",
            0,
            &[1, 0],
            super::VersionSuffix::Post(super::PostRelease {
                segment: 1,
                suffix: super::PostReleaseSuffix::Empty,
            }),
        );
        check(
            "1.0-3",
            0,
            &[1, 0],
            super::VersionSuffix::Post(super::PostRelease {
                segment: 3,
                suffix: super::PostReleaseSuffix::Empty,
            }),
        );
        check(
            "1.0-dev",
            0,
            &[1, 0],
            super::VersionSuffix::Development(super::DevelopmentRelease { segment: 0 }),
        );
        check(
            "0.100.0b2",
            0,
            &[0, 100, 0],
            super::VersionSuffix::Pre(super::PreRelease {
                segment: super::PreReleaseSegment::B(2),
                suffix: super::PreReleaseSuffix::Empty,
            }),
        );
    }
    #[test]
    fn version_cmp() {
        assert!(v("v1.0") == v("1.0"));
        assert!(v("1.1") == v("1.1.0"));
        assert!(v("1.1") <= v("1.1.0"));
        /* Version release */
        assert!(v("0.9") < v("0.9.1"));
        assert!(v("0.9.1") < v("0.9.2"));
        assert!(v("0.9.10") < v("0.9.11"));
        assert!(v("0.9.11") < v("1.0"));
        assert!(v("1.0") < v("1.0.1"));
        assert!(v("1.0.1") < v("1.1"));
        assert!(v("1.1") < v("2.0"));
        assert!(v("2.0") < v("2.0.1"));
        /* Epoch */
        assert!(v("2014.04") < v("1!1.0"));
        /* pre-release */
        assert!(v("1.dev0") < v("1.0.dev456"));
        assert!(v("1.0.dev456") < v("1.0a1"));
        assert!(v("1.0a1") < v("1.0a2.dev456"));
        assert!(v("1.0a2.dev456") < v("1.0a12.dev456"));
        assert!(v("1.0a12.dev456") < v("1.0a12"));
        assert!(v("1.0a12") < v("1.0b1.dev456"));
        assert!(v("1.0b1.dev456") < v("1.0b2"));
        assert!(v("1.0b2") < v("1.0b2.post345.dev456"));
        assert!(v("1.0b2.post345.dev456") < v("1.0b2.post345"));
        assert!(v("1.0b2.post345") < v("1.0rc1.dev456"));
        assert!(v("1.0rc1.dev456") < v("1.0rc1"));
        assert!(v("1.0rc1") < v("1.0"));
        //assert!(v("1.0") < v("1.0+abc.5"));
        //assert!(v("1.0+abc.5") < v("1.0+abc.7"));
        //assert!(v("1.0+abc.7") < v("1.0+5"));
        //assert!(v("1.0+5") < v("1.0.post456.dev34"));
        assert!(v("1.0rc1") < v("1.0.post456.dev34"));
        assert!(v("1.0.post456.dev34") < v("1.0.post456"));
        assert!(v("1.0.post456") < v("1.0.15"));
        assert!(v("1.0.15") < v("1.1.dev1"));
    }

    #[test]
    fn display() {
        fn f(s: &str) -> String {
            format!("{}", v(s))
        }
        /* Epoch */
        assert_eq!(f("0!1.5"), "1.5");
        assert_eq!(f("2020!1.5"), "2020!1.5");
        /* Integer normalization */
        assert_eq!(f("1.0000"), "1.0");
        assert_eq!(f("1.0011"), "1.11");
        /* Pre release */
        assert_eq!(f("1.1.a1"), "1.1a1");
        assert_eq!(f("1.1-a1"), "1.1a1");
        assert_eq!(f("1.1alpha1"), "1.1a1");
        assert_eq!(f("1.1BETA1"), "1.1b1");
        /* Post release */
        assert_eq!(f("1.2-post3"), "1.2.post3");
        assert_eq!(f("1.2.post3"), "1.2.post3");
        assert_eq!(f("1.2.post-3"), "1.2.post3");
        assert_eq!(f("1.2.POST-3"), "1.2.post3");
        assert_eq!(f("1.2-3"), "1.2.post3");
        /* Development release */
        assert_eq!(f("1.3-dev"), "1.3.dev0");
        assert_eq!(f("1.3_dev4"), "1.3.dev4");
        assert_eq!(f("1.3_DEV4"), "1.3.dev4");
    }
}

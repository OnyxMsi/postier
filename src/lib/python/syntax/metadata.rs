use pest::iterators::Pair;
use pest::Parser;
use pest_derive::Parser;

#[derive(Parser)]
#[grammar = "lib/python/grammar/metadata.pest"]
struct MetadataParser;

pub type MetadataContent<'m> = std::vec::Vec<(&'m str, &'m str)>;

fn parse_field(pair: Pair<Rule>) -> (&str, &str) {
    let mut it = pair.into_inner();
    (it.next().unwrap().as_str(), it.next().unwrap().as_str())
}

fn parse_fields<'m>(pair: Pair<'m, Rule>) -> MetadataContent<'m> {
    pair.into_inner()
        .filter(|p| p.as_rule() == Rule::field)
        .map(parse_field)
        .collect()
}

pub fn parse<'m>(s: &'m str) -> Result<MetadataContent<'m>, crate::lib::python::Error> {
    let p = MetadataParser::parse(Rule::fields, s.trim())
        .map_err(|e| {
            log::warn!("Can't parse metadata content \"{}\": {}", s, e);
            crate::lib::python::Error::MetadataParseError
        })?
        .next()
        .unwrap();
    Ok(parse_fields(p))
}

#[cfg(test)]
mod test {
    #[test]
    fn parse() {
        let s: &str = r#"
A: premier
A: second
un nom long: un long message
C:
A: troisième
 sur plusieures lignes

Ceci doit être ignoré
"#;
        let m = super::parse(s).unwrap();
        assert_eq!(
            m,
            [
                ("A", "premier"),
                ("A", "second"),
                ("un nom long", "un long message"),
                ("C", ""),
                ("A", "troisième\n sur plusieures lignes")
            ]
        );
    }
}

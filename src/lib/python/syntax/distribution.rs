use crate::lib::python;

/* https://packaging.python.org/en/latest/specifications/platform-compatibility-tags/ */
#[derive(PartialEq, Eq)]
pub struct ManyLinuxPlatformTag<'a> {
    pub(super) glibc_major: usize,
    pub(super) glibc_minor: usize,
    pub(super) arch: &'a str,
}
macro_rules! mlpt_match {
    ($glibc_major:literal, $glibc_minor:literal, $arch:literal, $env:ident) => {
        ManyLinuxPlatformTag {
            glibc_major: $glibc_major,
            glibc_minor: $glibc_minor,
            arch: $arch,
        }
        .match_environment($env)
    };
}
impl<'a> ManyLinuxPlatformTag<'_> {
    pub fn match_environment(&self, environment_marker: &python::EnvironmentMarker) -> bool {
        if environment_marker.platform_tags.len() == 0 {
            environment_marker
                .sys_platform
                .eq_ignore_ascii_case("linux")
                && environment_marker
                    .platform_machine
                    .eq_ignore_ascii_case(self.arch)
        } else {
            /* Check every platform tag */
            environment_marker
                .platform_tags
                .iter()
                .filter_map(|p| match BinaryDistributionPlatformTag::parse(p).ok()? {
                    BinaryDistributionPlatformTag::ManyLinux(m) => Some(m),
                    _ => None,
                })
                .any(|m| {
                    m.glibc_major >= self.glibc_major
                        && m.glibc_minor >= self.glibc_minor
                        && (self.arch == "any" || self.arch == m.arch)
                })
        }
    }
}
#[derive(PartialEq, Eq)]
pub struct MuslLinuxPlatformTag<'a> {
    pub(super) major: usize,
    pub(super) minor: usize,
    pub(super) arch: &'a str,
}
impl<'a> MuslLinuxPlatformTag<'_> {
    pub fn match_environment(&self, environment_marker: &python::EnvironmentMarker) -> bool {
        /* If no platform tag build one with other parameters */
        if environment_marker.platform_tags.len() == 0 {
            environment_marker
                .sys_platform
                .eq_ignore_ascii_case("linux")
                && environment_marker
                    .platform_machine
                    .eq_ignore_ascii_case(self.arch)
        } else {
            /* Check every platform tag */
            environment_marker
                .platform_tags
                .iter()
                .filter_map(|p| match BinaryDistributionPlatformTag::parse(p).ok()? {
                    BinaryDistributionPlatformTag::MuslLinux(m) => Some(m),
                    _ => None,
                })
                .any(|m| {
                    m.major >= self.major
                        && m.minor >= self.minor
                        && (self.arch == "any" || self.arch == m.arch)
                })
        }
    }
}

pub enum BinaryDistributionPlatformTag<'a> {
    Any,
    ManyLinux1,
    ManyLinux2010,
    ManyLinux2014,
    ManyLinux(ManyLinuxPlatformTag<'a>),
    MuslLinux(MuslLinuxPlatformTag<'a>),
    Basic(&'a str),
}
impl<'a> BinaryDistributionPlatformTag<'a> {
    pub fn parse(s: &'a str) -> Result<Self, python::Error> {
        super::specification::parse(
            super::specification::Rule::platform_tag,
            s,
            super::specification::parse_binary_distribution_platform_tag,
        )
    }
    pub fn match_environment(&self, environment_marker: &python::EnvironmentMarker) -> bool {
        match self {
            BinaryDistributionPlatformTag::Any => true,
            BinaryDistributionPlatformTag::ManyLinux1 => {
                mlpt_match!(2, 5, "i386", environment_marker)
                    || mlpt_match!(2, 5, "x86_64", environment_marker)
            }
            BinaryDistributionPlatformTag::ManyLinux2010 => {
                mlpt_match!(2, 12, "i386", environment_marker)
                    || mlpt_match!(2, 12, "x86_64", environment_marker)
            }
            BinaryDistributionPlatformTag::ManyLinux2014 => {
                mlpt_match!(2, 17, "i386", environment_marker)
                    || mlpt_match!(2, 17, "x86_64", environment_marker)
                    || mlpt_match!(2, 17, "aarch64", environment_marker)
                    || mlpt_match!(2, 17, "armv71", environment_marker)
                    || mlpt_match!(2, 17, "ppc64", environment_marker)
                    || mlpt_match!(2, 17, "ppc641e", environment_marker)
                    || mlpt_match!(2, 17, "s390x", environment_marker)
            }
            BinaryDistributionPlatformTag::ManyLinux(pt) => {
                pt.match_environment(environment_marker)
            }
            BinaryDistributionPlatformTag::MuslLinux(pt) => {
                pt.match_environment(environment_marker)
            }
            BinaryDistributionPlatformTag::Basic(pt) => {
                /* If no platform tag is set compute it from another source*/
                if environment_marker.platform_tags.len() == 0 {
                    pt.eq_ignore_ascii_case(&format!(
                        "{}_{}",
                        environment_marker.sys_platform, environment_marker.platform_machine
                    ))
                } else {
                    environment_marker.platform_tags.iter().any(|p| p == pt)
                }
            }
        }
    }
}

/* https://packaging.python.org/en/latest/specifications/binary-distribution-format/#file-format */
pub struct BinaryDistributionBuildTag<'a> {
    pub(super) digits: usize,
    pub(super) rest: Option<&'a str>,
}
pub struct BinaryDistributionPythonTag<'a> {
    pub(super) implementation: &'a str,
    pub(super) version: usize,
}
impl<'a> BinaryDistributionPythonTag<'a> {
    pub fn parse(s: &'a str) -> Result<Self, python::Error> {
        super::specification::parse(
            super::specification::Rule::python_tag,
            s,
            super::specification::parse_binary_distribution_python_tag,
        )
    }
    pub fn match_environment(&self, environment_marker: &python::EnvironmentMarker) -> bool {
        /* Major implementation have abbreviated codes */
        let implementation = match self.implementation {
            "py" => "python",
            "cp" => "cpython",
            "ip" => "ironpython",
            "pp" => "pypy",
            "jy" => "jython",
            _ => self.implementation,
        };
        /* "python" is compatible with every implementation */
        if implementation != "python" && implementation != environment_marker.implementation_name {
            return false;
        }
        /* Python version have no epoch, so compare only release_segments
         * if version is only one character, then compare only the major version
         * else major and minor
         */
        assert!(
            environment_marker
                .implementation_version
                .release_segments
                .len()
                >= 2
        );
        if self.version < 10 {
            let major = environment_marker.implementation_version.release_segments[0];
            self.version == major
        } else {
            let major = environment_marker.implementation_version.release_segments[0];
            let minor = environment_marker.implementation_version.release_segments[1];
            self.version == format!("{}{}", major, minor).parse::<usize>().unwrap()
        }
    }
}
pub struct BinaryDistribution<'a> {
    pub distribution: &'a str,
    pub version: super::version::Version,
    pub(super) build_tag: Option<BinaryDistributionBuildTag<'a>>,
    pub(super) python_tags: std::vec::Vec<BinaryDistributionPythonTag<'a>>,
    pub(super) abi_tags: std::vec::Vec<&'a str>,
    pub(super) platform_tags: std::vec::Vec<BinaryDistributionPlatformTag<'a>>,
}
impl<'a> BinaryDistribution<'a> {
    pub fn parse(s: &'a str) -> Result<Self, python::Error> {
        /* Drop suffix
         * Can't do it in grammar because there is a collision with platform_tags which is
         * .-separated */
        if !s.ends_with(".whl") {
            log::error!(
                "{}: invalid binary distribution filename, must end with \".whl\"",
                s
            );
            return Err(python::Error::SpecificationParseError);
        }
        super::specification::parse(
            super::specification::Rule::binary_distribution_filename,
            &s[0..s.len() - 4],
            super::specification::parse_binary_distribution_filename,
        )
    }
    pub fn match_environment(&self, environment_marker: &python::EnvironmentMarker) -> bool {
        self.python_tags
            .iter()
            .any(|p| p.match_environment(environment_marker))
            && self
                .abi_tags
                .iter()
                .any(|a| BinaryDistribution::match_abi_tag_environment(a, environment_marker))
            && self
                .platform_tags
                .iter()
                .any(|p| p.match_environment(environment_marker))
    }
    fn match_abi_tag_environment(
        abi_tag: &str,
        environment_marker: &python::EnvironmentMarker,
    ) -> bool {
        abi_tag == "none" || environment_marker.abi_tags.iter().any(|a| a == abi_tag)
    }
}

pub struct SourceDistribution<'a> {
    pub distribution: &'a str,
    pub version: super::version::Version,
}
impl<'a> SourceDistribution<'a> {
    pub fn parse(s: &'a str) -> Result<Self, python::Error> {
        super::specification::parse(
            super::specification::Rule::source_distribution_filename,
            s,
            super::specification::parse_source_distribution_filename,
        )
    }
}

pub enum Distribution<'a> {
    Binary(BinaryDistribution<'a>),
    Source(SourceDistribution<'a>),
}

impl<'a> Distribution<'a> {
    pub fn parse(s: &'a str) -> Result<Self, python::Error> {
        if s.ends_with(".tar.gz") {
            Ok(Distribution::Source(SourceDistribution::parse(s)?))
        } else if s.ends_with(".whl") {
            Ok(Distribution::Binary(BinaryDistribution::parse(s)?))
        } else {
            log::error!("Unsupported distribution format \"{}\"", s);
            Err(python::Error::SpecificationParseError)
        }
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn binary_distribution_filename_parse() {
        fn check(s: &str, v: super::BinaryDistribution) {
            let p = super::BinaryDistribution::parse(s).unwrap();
            assert_eq!(p.distribution, v.distribution);
            assert!(p.version == v.version);
            assert_eq!(p.build_tag.is_some(), v.build_tag.is_some());
            assert_eq!(p.python_tags.len(), v.python_tags.len());
            for (pp, pv) in std::iter::zip(p.python_tags, v.python_tags) {
                assert_eq!(pp.implementation, pv.implementation);
                assert_eq!(pp.version, pv.version);
            }
            assert_eq!(p.abi_tags, v.abi_tags);
            assert_eq!(p.platform_tags.len(), v.platform_tags.len());
        }
        check(
            "distribution-1.0-py32.py2-cp32d.ir27m-manylinux1.nt_amd64.whl",
            super::BinaryDistribution {
                distribution: "distribution",
                version: crate::lib::python::syntax::version::Version {
                    epoch: 0,
                    release_segments: std::vec![1usize, 0usize],
                    suffix: crate::lib::python::syntax::version::VersionSuffix::Empty,
                },
                build_tag: None,
                python_tags: std::vec![
                    super::BinaryDistributionPythonTag {
                        implementation: "py",
                        version: 32usize
                    },
                    super::BinaryDistributionPythonTag {
                        implementation: "py",
                        version: 2usize
                    },
                ],
                abi_tags: std::vec!["cp32d", "ir27m"],
                platform_tags: std::vec![
                    super::BinaryDistributionPlatformTag::ManyLinux1,
                    super::BinaryDistributionPlatformTag::Basic("nt_amd64"),
                ],
            },
        )
    }
    #[test]
    fn source_distribution_filename_parse() {
        fn check(s: &str, v: super::SourceDistribution) {
            let p = super::SourceDistribution::parse(s).unwrap();
            assert_eq!(p.distribution, v.distribution);
            assert!(p.version == v.version);
        }
        check(
            "foo-1.0.tar.gz",
            super::SourceDistribution {
                distribution: "foo",
                version: crate::lib::python::syntax::version::Version {
                    epoch: 0,
                    release_segments: std::vec![1usize, 0usize],
                    suffix: crate::lib::python::syntax::version::VersionSuffix::Empty,
                },
            },
        )
    }
    use crate::lib::python::syntax::version::Version;
    use crate::lib::utils::test::get_text;
    #[test]
    fn binary_python_tag_match_environment() {
        fn g(s: &str, implementation: &str, version: &str) -> bool {
            let v = Version::parse(version).unwrap();
            let d = super::BinaryDistributionPythonTag::parse(s).unwrap();
            let e = crate::lib::python::EnvironmentMarker {
                python_version: Version::parse("1.0").unwrap(),
                python_full_version: Version::parse("1.0").unwrap(),
                os_name: get_text(),
                sys_platform: get_text(),
                platform_release: get_text(),
                platform_system: get_text(),
                platform_version: get_text(),
                platform_machine: get_text(),
                platform_python_implementation: get_text(),
                implementation_name: implementation.to_string(),
                implementation_version: v,
                extra: None,
                abi_tags: std::vec::Vec::new(),
                platform_tags: std::vec::Vec::new(),
            };
            d.match_environment(&e)
        }
        assert!(g("py3", "python", "3.2.11"));
        assert!(g("py3", "cython", "3.1"));
        assert!(g("py3", "pp", "3.0"));
        assert!(g("py3", "jy", "3.0"));
        assert!(!g("py3", "python", "2.7"));
        assert!(g("pp34", "pypy", "3.4.25"));
        assert!(!g("pp34", "pypy", "3.5"));
        assert!(!g("pp34", "cp", "3.4.25"));
    }
    #[test]
    fn binary_abi_tag_match_environment() {
        fn g(s: &str, abi: Option<&str>) -> bool {
            let mut a = std::vec::Vec::new();
            if let Some(b) = abi {
                a.push(b.to_string());
            }
            let e = crate::lib::python::EnvironmentMarker {
                python_version: Version::parse("1.0").unwrap(),
                python_full_version: Version::parse("1.0").unwrap(),
                os_name: get_text(),
                sys_platform: get_text(),
                platform_release: get_text(),
                platform_system: get_text(),
                platform_version: get_text(),
                platform_machine: get_text(),
                platform_python_implementation: get_text(),
                implementation_name: get_text(),
                implementation_version: Version::parse("1.0").unwrap(),
                extra: None,
                abi_tags: a,
                platform_tags: std::vec::Vec::new(),
            };
            super::BinaryDistribution::match_abi_tag_environment(s, &e)
        }
        assert!(g("none", Some("none")));
        assert!(g("none", None));
        assert!(g("none", Some("cp32d")));
        assert!(!g("abi3", None));
        assert!(!g("abi3", Some("cp32d")));
        assert!(g("abi3", Some("abi3")));
    }
    #[test]
    fn binary_platform_tag_match_environment() {
        fn g(
            s: &str,
            sys_platform: Option<&str>,
            platform_machine: Option<&str>,
            platform_tag: Option<&str>,
        ) -> bool {
            let mut p = std::vec::Vec::new();
            if let Some(b) = platform_tag {
                p.push(b.to_string());
            }
            let e = crate::lib::python::EnvironmentMarker {
                python_version: Version::parse("1.0").unwrap(),
                python_full_version: Version::parse("1.0").unwrap(),
                os_name: get_text(),
                sys_platform: sys_platform.map_or(get_text(), |p| p.to_string()),
                platform_release: get_text(),
                platform_system: get_text(),
                platform_version: get_text(),
                platform_machine: platform_machine.map_or(get_text(), |p| p.to_string()),
                platform_python_implementation: get_text(),
                implementation_name: get_text(),
                implementation_version: Version::parse("1.0").unwrap(),
                extra: None,
                abi_tags: std::vec::Vec::new(),
                platform_tags: p,
            };
            super::BinaryDistributionPlatformTag::parse(s)
                .unwrap()
                .match_environment(&e)
        }
        assert!(g("linux_x86_64", Some("Linux"), Some("x86_64"), None));
        assert!(!g(
            "linux_x86_64",
            Some("Linux"),
            Some("x86_64"),
            Some("Darwin_amd64")
        ));
        assert!(!g("linux_x86_64", Some("Linux"), Some("amd64"), None));
        assert!(g("manylinux1", Some("Linux"), Some("i386"), None));
        assert!(g("manylinux1", Some("Linux"), Some("x86_64"), None));
        assert!(g("manylinux2010", Some("Linux"), Some("x86_64"), None));
        assert!(!g("manylinux2010", Some("Linux"), Some("aarch64"), None));
        assert!(g("manylinux2014", Some("Linux"), Some("aarch64"), None));
        assert!(g("manylinux2014", Some("Linux"), Some("s390x"), None));
        assert!(!g("manylinux2014", Some("NT"), Some("x86_64"), None));
        assert!(g(
            "manylinux_3_33_amd64",
            Some("Linux"),
            Some("amd64"),
            None
        ));
        assert!(!g(
            "manylinux_3_33_amd64",
            Some("Linux"),
            Some("x86_64"),
            None
        ));
        assert!(!g(
            "manylinux_3_33_amd64",
            Some("Darwin"),
            Some("ppc64"),
            Some("LINUX_amd64")
        ));
        assert!(g(
            "manylinux_2_5_x86_64",
            Some("Darwin"),
            Some("ppc64"),
            Some("manylinux_2_11_x86_64")
        ));
        assert!(!g(
            "manylinux_2_5_x86_64",
            Some("Darwin"),
            Some("ppc64"),
            Some("manylinux_2_4_x86_64")
        ));
        assert!(!g(
            "manylinux_2_5_x86_64",
            Some("Darwin"),
            Some("ppc64"),
            Some("manylinux_1_42_x86_64")
        ));
        assert!(!g(
            "manylinux_2_5_i386",
            Some("Darwin"),
            Some("ppc64"),
            Some("manylinux_2_11_x86_64")
        ));
        assert!(!g(
            "manylinux_2_5_i386",
            Some("Darwin"),
            Some("ppc64"),
            Some("musllinux_2_11_x86_64")
        ));
    }
    #[test]
    fn binary_distribution_match_environment() {
        fn g(
            s: &str,
            implementation_name: &str,
            implementation_version: &str,
            abi_tag: &str,
            sys_platform: &str,
            platform_machine: &str,
        ) -> bool {
            let mut a = std::vec::Vec::new();
            a.push(abi_tag.to_string());
            let e = crate::lib::python::EnvironmentMarker {
                python_version: Version::parse("1.0").unwrap(),
                python_full_version: Version::parse("1.0").unwrap(),
                os_name: get_text(),
                sys_platform: sys_platform.to_string(),
                platform_release: get_text(),
                platform_system: get_text(),
                platform_version: get_text(),
                platform_machine: platform_machine.to_string(),
                platform_python_implementation: get_text(),
                implementation_name: implementation_name.to_string(),
                implementation_version: Version::parse(implementation_version).unwrap(),
                extra: None,
                abi_tags: a,
                platform_tags: std::vec::Vec::new(),
            };
            super::BinaryDistribution::parse(s)
                .unwrap()
                .match_environment(&e)
        }
        assert!(g(
            "pkg-1.0-py3.py2-cp22.jp33-manylinux1.win32_aarch64.whl",
            "cpython",
            "3.12",
            "cp22",
            "Linux",
            "x86_64"
        ));
        assert!(g(
            "pkg-1.0-py3.py2-cp22.jp33-manylinux1.win32_aarch64.whl",
            "jython",
            "3.12",
            "cp22",
            "win32",
            "aarch64"
        ));
    }
    #[test]
    fn parse() {
        fn p(t: &str) -> super::Distribution {
            super::Distribution::parse(t).unwrap()
        }
        let s = p("pkg-1.0.tar.gz");
        let b = p("pkg-1.0-py3-any-none.whl");
        match s {
            super::Distribution::Source(_) => assert!(true),
            super::Distribution::Binary(_) => assert!(false),
        }
        match b {
            super::Distribution::Source(_) => assert!(false),
            super::Distribution::Binary(_) => assert!(true),
        }
    }
}

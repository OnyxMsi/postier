use super::specification;
use crate::lib::python;

/* https://packaging.python.org/en/latest/specifications/dependency-specifiers/#dependency-specifiers */
pub(super) enum MarkerEnv {
    OsName,
    SysPlatform,
    PlatformMachine,
    PlatformPythonImplementation,
    PlatformRelease,
    PlatformSystem,
    PlatformVersion,
    PythonVersion,
    PythonFullVersion,
    ImplementationName,
    ImplementationVersion,
    Extra,
}

impl<'d> MarkerEnv {
    /* Because some markers are complex struct, there is no way to use a &str
     * Build a String
     */
    fn to_string(&self, environment_marker: &python::EnvironmentMarker) -> String {
        match self {
            MarkerEnv::OsName => environment_marker.os_name.to_string(),
            MarkerEnv::SysPlatform => environment_marker.sys_platform.to_string(),
            MarkerEnv::PlatformMachine => environment_marker.platform_machine.to_string(),
            MarkerEnv::PlatformPythonImplementation => environment_marker
                .platform_python_implementation
                .to_string(),
            MarkerEnv::PlatformRelease => environment_marker.platform_release.to_string(),
            MarkerEnv::PlatformSystem => environment_marker.platform_system.to_string(),
            MarkerEnv::PlatformVersion => environment_marker.platform_version.to_string(),
            MarkerEnv::PythonVersion => environment_marker.python_version.to_string(),
            MarkerEnv::PythonFullVersion => environment_marker.python_full_version.to_string(),
            MarkerEnv::ImplementationName => environment_marker.implementation_name.to_string(),
            MarkerEnv::ImplementationVersion => {
                environment_marker.implementation_version.to_string()
            }
            MarkerEnv::Extra => environment_marker
                .extra
                .as_ref()
                .map(|s| s.to_string())
                .unwrap_or("".to_string()),
        }
    }
    fn to_version_ref(
        &self,
        environment_marker: &'d python::EnvironmentMarker,
    ) -> Option<&'d super::version::Version> {
        match self {
            MarkerEnv::PythonVersion => Some(&environment_marker.python_version),
            MarkerEnv::PythonFullVersion => Some(&environment_marker.python_full_version),
            MarkerEnv::ImplementationVersion => Some(&environment_marker.implementation_version),
            _ => None,
        }
    }
    fn to_string_ref(&self, environment_marker: &'d python::EnvironmentMarker) -> Option<&'d str> {
        match self {
            MarkerEnv::OsName => Some(environment_marker.os_name.as_str()),
            MarkerEnv::SysPlatform => Some(environment_marker.sys_platform.as_str()),
            MarkerEnv::PlatformMachine => Some(environment_marker.platform_machine.as_str()),
            MarkerEnv::PlatformPythonImplementation => {
                Some(environment_marker.platform_python_implementation.as_str())
            }
            MarkerEnv::PlatformRelease => Some(environment_marker.platform_release.as_str()),
            MarkerEnv::PlatformSystem => Some(environment_marker.platform_system.as_str()),
            MarkerEnv::PlatformVersion => Some(environment_marker.platform_version.as_str()),
            MarkerEnv::ImplementationName => Some(environment_marker.implementation_name.as_str()),
            MarkerEnv::Extra => environment_marker.extra.as_ref().map(|s| s.as_str()),
            _ => None,
        }
    }
}

impl std::fmt::Display for MarkerEnv {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            MarkerEnv::OsName => specification::OS_NAME,
            MarkerEnv::SysPlatform => specification::SYS_PLATFORM,
            MarkerEnv::PlatformMachine => specification::PLATFORM_MACHINE,
            MarkerEnv::PlatformPythonImplementation => {
                specification::PLATFORM_PYTHON_IMPLEMENTATION
            }
            MarkerEnv::PlatformRelease => specification::PLATFORM_RELEASE,
            MarkerEnv::PlatformSystem => specification::PLATFORM_SYSTEM,
            MarkerEnv::PlatformVersion => specification::PLATFORM_VERSION,
            MarkerEnv::PythonVersion => specification::PYTHON_VERSION,
            MarkerEnv::PythonFullVersion => specification::PYTHON_FULL_VERSION,
            MarkerEnv::ImplementationName => specification::IMPLEMENTATION_NAME,
            MarkerEnv::ImplementationVersion => specification::IMPLEMENTATION_VERSION,
            MarkerEnv::Extra => specification::EXTRA,
        }
        .fmt(f)
    }
}

pub(super) enum MarkerVarOperator {
    Less,
    LessEqual,
    Differ,
    Equal,
    GreaterEqual,
    Greater,
    In,
    NotIn,
}

/* Helpers to interpret variables
 * tmp_var is required to take ownership of the created version if need be
 */
macro_rules! compared_version_or_false {
    ( $var:expr, $env:expr, $tmp_var:expr ) => {
        match $var.to_version_ref($env) {
            Some(v) => {
                /* Transform the version into a ComparedVersion and store it in the temporary
                 * variable */
                $tmp_var = Some(super::specifier::ComparedVersion::from_version(v, false));
                $tmp_var.as_ref().unwrap()
            }
            None => match super::specifier::ComparedVersion::parse(&$var.to_string($env)) {
                Ok(w) => {
                    $tmp_var = Some(w);
                    $tmp_var.as_ref().unwrap()
                }
                Err(_) => return false,
            },
        }
    };
}
macro_rules! version_or_false {
    ( $var:expr, $env:expr, $tmp_var:expr ) => {
        match $var.to_version_ref($env) {
            Some(v) => v,
            None => match super::version::Version::parse(&$var.to_string($env)) {
                Ok(w) => {
                    $tmp_var = Some(w);
                    $tmp_var.as_ref().unwrap()
                }
                Err(_) => return false,
            },
        }
    };
}
macro_rules! version_or_none {
    ( $var:expr, $env:expr ) => {
        $var.to_version_ref($env)
            .or(super::version::Version::parse(&$var.to_string($env))
                .ok()
                .as_ref())
    };
}
/* This is to avoid calling to_string() when you can get the raw string */
macro_rules! to_str {
    ( $var:expr, $env:expr ) => {
        $var.to_string_ref($env).unwrap_or(&$var.to_string($env))
    };
}
impl MarkerVarOperator {
    /* According to documentation, solve a var expression
     * Some tests are only for version,
     * others are only for string
     * Use comparaison function fron the specifier module
     */
    fn match_environment(
        &self,
        left: &MarkerVar,
        right: &MarkerVar,
        environment_marker: &python::EnvironmentMarker,
    ) -> bool {
        match self {
            MarkerVarOperator::Less => self.match_version_environment(
                left,
                right,
                environment_marker,
                super::specifier::exclusive_ordered_comparaison_lesser,
            ),
            MarkerVarOperator::LessEqual => self.match_version_environment(
                left,
                right,
                environment_marker,
                super::specifier::inclusive_ordered_comparaison_lesser,
            ),
            MarkerVarOperator::Differ => self.match_version_or_str_environment(
                left,
                right,
                environment_marker,
                super::specifier::version_matching,
                |l, r| l != r,
            ),
            MarkerVarOperator::Equal => self.match_version_or_str_environment(
                left,
                right,
                environment_marker,
                super::specifier::version_exclusion,
                |l, r| l == r,
            ),
            MarkerVarOperator::GreaterEqual => self.match_version_environment(
                left,
                right,
                environment_marker,
                super::specifier::inclusive_ordered_comparaison_greater,
            ),
            MarkerVarOperator::Greater => self.match_version_environment(
                left,
                right,
                environment_marker,
                super::specifier::exclusive_ordered_comparaison_greater,
            ),
            MarkerVarOperator::In => {
                self.match_str_environment(left, right, environment_marker, |l, r| l.contains(r))
            }
            MarkerVarOperator::NotIn => {
                self.match_str_environment(left, right, environment_marker, |l, r| !l.contains(r))
            }
        }
    }
    /* Will try to compare versions
     * If one of the operand cannot be one then return false
     */
    fn match_version_environment<V>(
        &self,
        left: &MarkerVar,
        right: &MarkerVar,
        environment_marker: &python::EnvironmentMarker,
        cmp: V,
    ) -> bool
    where
        V: Fn(&super::version::Version, &super::specifier::ComparedVersion) -> bool,
    {
        let mut _left_v = None;
        let mut _right_v = None;
        /* First try to get versions, try to parse them if not possible */
        cmp(
            version_or_false!(left, environment_marker, _left_v),
            compared_version_or_false!(right, environment_marker, _right_v),
        )
    }
    /* Will try to compare versions
     * If the first is a version and not the second then return false
     * Else compare as strings
     */
    fn match_version_or_str_environment<V, S>(
        &self,
        left: &MarkerVar,
        right: &MarkerVar,
        environment_marker: &python::EnvironmentMarker,
        cmp_v: V,
        cmp_s: S,
    ) -> bool
    where
        V: Fn(&super::version::Version, &super::specifier::ComparedVersion) -> bool,
        S: Fn(&str, &str) -> bool,
    {
        let mut _right_v = None;
        match version_or_none!(left, environment_marker) {
            Some(left_v) => cmp_v(
                left_v,
                /* If right cannot be parsed then return false */
                compared_version_or_false!(right, environment_marker, _right_v),
            ),
            None => cmp_s(
                to_str!(left, environment_marker),
                to_str!(right, environment_marker),
            ),
        }
    }
    /* Compare as strings
     */
    fn match_str_environment<S>(
        &self,
        left: &MarkerVar,
        right: &MarkerVar,
        environment_marker: &python::EnvironmentMarker,
        cmp: S,
    ) -> bool
    where
        S: Fn(&str, &str) -> bool,
    {
        cmp(
            to_str!(left, environment_marker),
            to_str!(right, environment_marker),
        )
    }
}

impl std::fmt::Display for MarkerVarOperator {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            MarkerVarOperator::Less => specification::LESS,
            MarkerVarOperator::LessEqual => specification::LESS_EQUAL,
            MarkerVarOperator::Differ => specification::DIFFER,
            MarkerVarOperator::Equal => specification::EQUAL,
            MarkerVarOperator::GreaterEqual => specification::GREATER_EQUAL,
            MarkerVarOperator::Greater => specification::GREATER,
            MarkerVarOperator::In => specification::IN,
            MarkerVarOperator::NotIn => specification::NOT_IN,
        }
        .fmt(f)
    }
}

pub(super) enum MarkerVar<'d> {
    String(&'d str),
    Env(MarkerEnv),
}
impl<'d> MarkerVar<'d> {
    fn to_version_ref(
        &self,
        environment_marker: &'d python::EnvironmentMarker,
    ) -> Option<&'d super::version::Version> {
        match self {
            MarkerVar::String(_) => None,
            MarkerVar::Env(e) => e.to_version_ref(environment_marker),
        }
    }
    fn to_string_ref(&self, environment_marker: &'d python::EnvironmentMarker) -> Option<&'d str> {
        match self {
            MarkerVar::String(s) => Some(s),
            MarkerVar::Env(e) => e.to_string_ref(environment_marker),
        }
    }
    fn to_string(&self, environment_marker: &'d python::EnvironmentMarker) -> String {
        match self {
            MarkerVar::String(s) => s.to_string(),
            MarkerVar::Env(e) => e.to_string(environment_marker),
        }
    }
}
impl<'d> std::fmt::Display for MarkerVar<'d> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            MarkerVar::String(s) => write!(f, "'{}'", s),
            MarkerVar::Env(e) => e.fmt(f),
        }
    }
}

pub(super) struct MarkerVarExpr<'d> {
    pub(super) left: MarkerVar<'d>,
    pub(super) operator: MarkerVarOperator,
    pub(super) right: MarkerVar<'d>,
}
impl MarkerVarExpr<'_> {
    fn match_environment(&self, environment_marker: &python::EnvironmentMarker) -> bool {
        self.operator
            .match_environment(&self.left, &self.right, environment_marker)
    }
}
impl<'d> std::fmt::Display for MarkerVarExpr<'d> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{} {} {}", self.left, self.operator, self.right)
    }
}

pub(super) enum Marker<'d> {
    Var(MarkerVarExpr<'d>),
    Or(Box<(Marker<'d>, Marker<'d>)>),
    And(Box<(Marker<'d>, Marker<'d>)>),
}
impl Marker<'_> {
    fn match_environment(&self, environment_marker: &python::EnvironmentMarker) -> bool {
        match self {
            Marker::Var(e) => e.match_environment(environment_marker),
            Marker::Or(b) => {
                b.0.match_environment(environment_marker)
                    || b.1.match_environment(environment_marker)
            }
            Marker::And(b) => {
                b.0.match_environment(environment_marker)
                    && b.1.match_environment(environment_marker)
            }
        }
    }
}

impl<'d> std::fmt::Display for Marker<'d> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            /* Note that this one is not very efficient
             * It will add brackets even if not required */
            Marker::Var(v) => write!(f, "{}", v),
            Marker::Or(v) => write!(f, "({} or {})", v.0, v.1),
            Marker::And(v) => write!(f, "({} and {})", v.0, v.1),
        }
    }
}

pub enum DependencySpecification<'d> {
    Name(std::vec::Vec<super::specifier::Specifier>),
    Url(&'d str),
}
impl<'d> std::fmt::Display for DependencySpecification<'d> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            DependencySpecification::Name(s) => {
                let mut next = false;
                for spec in s {
                    if next {
                        write!(f, ", {}", spec)?;
                    } else {
                        spec.fmt(f)?;
                        next = true;
                    }
                }
                Ok(())
            }
            DependencySpecification::Url(u) => u.fmt(f),
        }
    }
}
pub struct Dependency<'d> {
    pub package: &'d str,
    pub extras: std::vec::Vec<&'d str>,
    pub(super) specification: Option<DependencySpecification<'d>>,
    pub(super) marker_specifier: Option<Marker<'d>>,
}
impl<'d> Dependency<'d> {
    pub fn parse(s: &'d str) -> Result<Self, python::Error> {
        super::specification::parse(
            super::specification::Rule::dependency,
            s,
            super::specification::parse_dependency,
        )
    }
    /* Match this dependency with an environment
     * Return true if the dependency is to be evaluated for this environment
     * */
    pub fn match_environment(&self, environment_marker: &python::EnvironmentMarker) -> bool {
        match &self.marker_specifier {
            Some(m) => m.match_environment(environment_marker),
            None => true,
        }
    }
    /* Match this dependency with a distribution package name and version
     */
    pub fn match_version(&self, package: &str, version: &super::version::Version) -> bool {
        /* This means two reallocation for each call, this might take time ... */
        super::specification::name_normalization(package)
            == super::specification::name_normalization(self.package)
            && match &self.specification {
                Some(specification) => match specification {
                    DependencySpecification::Name(specifiers) => {
                        super::specifier::Specifier::match_one(specifiers, version)
                    }
                    DependencySpecification::Url(_) => true,
                },
                None => true,
            }
    }
}
impl<'d> std::fmt::Display for Dependency<'d> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        super::specification::name_normalization(self.package).fmt(f)?;
        if self.extras.len() > 0 {
            let mut next = false;
            write!(f, " [")?;
            for extra in &self.extras {
                let n = super::specification::name_normalization(extra);
                if next {
                    write!(f, ",{}", n)?;
                } else {
                    n.fmt(f)?;
                    next = true;
                }
            }
            write!(f, "]")?;
        }
        if let Some(ref spec) = self.specification {
            write!(f, " ")?;
            spec.fmt(f)?;
        }
        if let Some(ref mark) = self.marker_specifier {
            write!(f, " ; {}", mark)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod test {
    fn d(s: &str) -> super::Dependency {
        super::Dependency::parse(s).unwrap()
    }
    fn v(s: &str) -> crate::lib::python::syntax::version::Version {
        crate::lib::python::syntax::version::Version::parse(s).unwrap()
    }
    #[test]
    fn parse() {
        let e = d("A");
        assert_eq!(e.package, "A");
        assert_eq!(e.extras.len(), 0);
        assert!(e.specification.is_none());
        assert!(e.marker_specifier.is_none());
        let e = d("A.B-C_D");
        assert_eq!(e.package, "A.B-C_D");
        assert_eq!(e.extras.len(), 0);
        assert!(e.specification.is_none());
        assert!(e.marker_specifier.is_none());
        let e = d("name");
        assert_eq!(e.package, "name");
        assert_eq!(e.extras.len(), 0);
        assert!(e.specification.is_none());
        assert!(e.marker_specifier.is_none());
        let e = d("name<=1");
        assert_eq!(e.package, "name");
        assert_eq!(e.extras.len(), 0);
        assert!(e.specification.is_some());
        if let Some(d) = e.specification {
            match d {
                super::DependencySpecification::Name(specifiers) => {
                    assert_eq!(specifiers.len(), 1);
                }
                _ => {
                    panic!("Invalid specification");
                }
            }
        } else {
            panic!("Invalid specification");
        }

        assert!(e.marker_specifier.is_none());
        d("name>=3");
        d("name>=3,");
        d("name>=3,<2");
        let e = d("name@http://foo.com");
        if let Some(d) = e.specification {
            match d {
                super::DependencySpecification::Url(u) => {
                    assert_eq!(u, "http://foo.com");
                }
                _ => {
                    panic!("Invalid specification");
                }
            }
        } else {
            panic!("Invalid specification");
        }
        let e = d("name [fred,bar] @ http://foo.com ; python_version=='2.7'");
        assert_eq!(e.extras, &["fred", "bar"]);
        assert!(e.marker_specifier.is_some());
        d("name[quux, strange];python_version<'2.7' and platform_version=='2'");
        d("name; os_name=='a' or os_name=='b'");
        d("name; os_name=='a' and os_name=='b' or os_name=='c'");
        d("name; os_name=='a' and (os_name=='b' or os_name=='c')");
        d("name; os_name=='a' or os_name=='b' and os_name=='c'");
        d("name; (os_name=='a' or os_name=='b') and os_name=='c'");
        d("requests [security,tests] >= 2.8.1, == 2.8.* ; python_version < '2.7'");
        d("uvloop!=0.15.0,!=0.15.1,>=0.14.0; (sys_platform != 'win32' and (sys_platform != 'cygwin' and platform_python_implementation != 'PyPy')) and extra == 'standard'");
    }
    #[test]
    fn match_version() {
        /* Make sure name are normalized on match */
        assert!(d("name_normalized").match_version("name_normalized", &v("1.0")));
        assert!(d("name-not-normalized").match_version("name_not_normalized", &v("1.0")));
    }
    #[test]
    fn display() {
        macro_rules! f {
            ($s:literal) => {
                format!("{}", d($s))
            };
        }
        /* Normalization */
        assert_eq!(
            f!("requests [security,tests] >= 2.8.1, == 2.8.* ; python_version < \"2.7\""),
            "requests [security,tests] >= 2.8.1, == 2.8.* ; python_version < '2.7'"
        );
        assert_eq!(
            f!("requests [security,tests] >= 2.8.1-A1"),
            "requests [security,tests] >= 2.8.1a1"
        );
    }
}

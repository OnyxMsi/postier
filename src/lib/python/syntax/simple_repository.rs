use crate::lib;
use crate::lib::python;
use serde::Deserialize;

/*
 * https://packaging.python.org/en/latest/specifications/simple-repository-api/
 * https://peps.python.org/pep-0691/
 */
pub struct SimpleRepository<'a> {
    urls: &'a [python::Index],
    cache: std::collections::HashMap<String, std::vec::Vec<Package>>,
    client: reqwest::blocking::Client,
}

#[derive(Deserialize)]
struct APIProjectFilename<'a> {
    filename: &'a str,
    url: &'a str,
    hashes: std::collections::HashMap<&'a str, &'a str>,
    /* Ignore the rest */
}

#[derive(Deserialize)]
struct APIProjectDetail<'a> {
    name: &'a str,
    files: std::vec::Vec<APIProjectFilename<'a>>,
    /* Ignore the rest */
}

pub struct Package {
    pub filename: String,
    pub url: url::Url,
    pub sha256: Option<String>,
}
impl Package {
    /* Build a list of OWNED stuff that represent a package */
    fn from_detail(detail: &APIProjectDetail) -> std::vec::Vec<Self> {
        let mut r: std::vec::Vec<Package> = std::vec::Vec::new();
        for file in &detail.files {
            match url::Url::parse(&file.url) {
                Ok(u) => r.push(Package {
                    filename: file.filename.to_string(),
                    url: u,
                    sha256: file.hashes.get("sha256").map(|s| s.to_string()),
                }),
                Err(e) => log::error!(
                    "Invalid URL {} for package {}: {}",
                    file.url,
                    file.filename,
                    e
                ),
            }
        }
        r
    }
}

static HTTP_SIMPLE_JSON_CONTENT_TYPE: &str = "application/vnd.pypi.simple.v1+json";

/* Fetch a single package detail using the JSON Simple API
 * Return None if this package is not present in the repository
 * Return an APIProjectDetail if it is set
 */
fn get_json_package_entries(
    client: &reqwest::blocking::Client,
    url: &url::Url,
) -> Result<std::vec::Vec<Package>, python::Error> {
    let mut req = client.get(url.as_str());
    req = req.header("Accept", HTTP_SIMPLE_JSON_CONTENT_TYPE);
    let res = req.send().map_err(|e| {
        log::error!("Can't fetch {}: {}", url, e);
        python::Error::SimpleRepositoryError
    })?;
    if res.status().is_success() {
        /* Not the most efficient way ... */
        let txt = res.text().map_err(|e| {
            log::error!("Can't decode {} content: {}", url.as_str(), e);
            python::Error::SimpleRepositoryError
        })?;
        let detail: APIProjectDetail = serde_json::from_str(txt.as_str()).map_err(|e| {
            log::error!("Invalid API content for {}: {}", url.as_str(), e);
            python::Error::SimpleRepositoryError
        })?;
        Ok(Package::from_detail(&detail))
    } else {
        if res.status().as_u16() == 404 {
            Ok(std::vec::Vec::new())
        } else {
            log::warn!("{} fails with code {}", url, res.status().as_u16());
            Err(python::Error::SimpleRepositoryError)
        }
    }
}

impl<'a> SimpleRepository<'a> {
    pub fn new(urls: &'a [python::Index]) -> Self {
        SimpleRepository {
            urls,
            cache: std::collections::HashMap::new(),
            client: reqwest::blocking::Client::new(),
        }
    }
    fn load_package_json(&mut self, package_name: &str) -> Result<(), python::Error> {
        log::debug!("Find candidates for package {}", package_name);
        let package_name_normalized = super::specification::name_normalization(package_name);
        for repo in self.urls {
            let repo_url = &repo.url;
            let packages_url = repo_url.join(&package_name_normalized).map_err(|e| {
                log::error!(
                    "Invalid URL {}/{}: {}",
                    repo_url.as_str(),
                    package_name_normalized,
                    e
                );
                python::Error::LibError(lib::Error::UrlError(e))
            })?;
            let mut packages = get_json_package_entries(&self.client, &packages_url)?;
            if packages.len() == 0 {
                log::debug!(
                    "Can't find package {} in repository {}",
                    package_name,
                    repo_url.as_str()
                );
            } else {
                self.cache
                    .get_mut(package_name)
                    .unwrap()
                    .append(packages.as_mut());
            }
        }
        Ok(())
    }
    pub fn fetch(&mut self, package_name: &str) -> Result<(), python::Error> {
        if !self.cache.contains_key(package_name) {
            self.cache
                .insert(package_name.to_string(), std::vec::Vec::new());
            self.load_package_json(package_name)?;
        }
        Ok(())
    }
    pub fn get(&self, package_name: &str) -> Option<std::vec::Vec<&Package>> {
        self.cache.get(package_name).map(|v| v.iter().collect())
    }
    pub fn clear(&mut self) -> () {
        self.cache.clear()
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn get_json_package_entries() {
        fn get_url() -> String {
            let s = crate::lib::utils::test::get_words(3, 5, "_");
            let u = percent_encoding::utf8_percent_encode(&s, percent_encoding::NON_ALPHANUMERIC);
            format!("/{}", u.to_string())
        }
        let server = httpmock::MockServer::start();
        let package_exists_url_s = get_url();
        let package_does_not_exists_url_s = get_url();
        let package_invalid_content_url_s = get_url();
        let package_exists_url = server.url(&package_exists_url_s);
        let package_does_not_exists_url = server.url(&package_does_not_exists_url_s);
        let package_invalid_url = server.url(&package_invalid_content_url_s);
        let package_exists_content = r#"
{
    "name": "package",
    "files": [
        {
            "url": "http://some.where/else",
            "hashes": {"md5sum": "aabb"},
            "filename": "package1"
        },
        {
            "url": "http://some.where/else2",
            "hashes": {"md5sum": "bbaa"},
            "filename": "package2"
        }
    ]
}"#;
        server.mock(|when, then| {
            when.method("GET").path(&package_exists_url_s);
            then.status(200).body(&package_exists_content);
        });
        server.mock(|when, then| {
            when.method("GET").path(&package_does_not_exists_url_s);
            then.status(404);
        });
        server.mock(|when, then| {
            when.method("GET").path(&package_invalid_content_url_s);
            then.status(200).body(crate::lib::utils::test::get_text());
        });
        let client = reqwest::blocking::Client::new();
        let detail = super::get_json_package_entries(
            &client,
            &url::Url::parse(&package_exists_url).unwrap(),
        )
        .unwrap();
        assert_eq!(detail.len(), 2);
        let detail = super::get_json_package_entries(
            &client,
            &url::Url::parse(&package_does_not_exists_url).unwrap(),
        )
        .unwrap();
        assert_eq!(detail.len(), 0);
        let detail = super::get_json_package_entries(
            &client,
            &url::Url::parse(&package_invalid_url).unwrap(),
        );
        assert!(detail.is_err());
    }
}

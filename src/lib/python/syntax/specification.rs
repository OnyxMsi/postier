use super::{dependency, distribution, specifier, version};
use pest::iterators::Pair;
use pest::Parser;
use pest_derive::Parser;

#[derive(Parser)]
#[grammar = "lib/python/grammar/specification.pest"]
struct SpecificationParser;

pub(super) fn parse<'a, T, F>(
    rule: Rule,
    s: &'a str,
    parse_fct: F,
) -> Result<T, crate::lib::python::Error>
where
    F: Fn(Pair<'a, Rule>) -> T,
    T: 'a,
{
    let p = SpecificationParser::parse(rule, s.trim())
        .map_err(|e| {
            log::warn!("Can't parse python specification \"{}\": {}", s, e);
            crate::lib::python::Error::SpecificationParseError
        })?
        .next()
        .unwrap();
    Ok(parse_fct(p))
}

fn parse_usize(pair: Pair<Rule>) -> usize {
    pair.as_str().trim().parse::<usize>().unwrap()
}

fn parse_pre_release_segment(pair: Pair<Rule>) -> version::PreReleaseSegment {
    let mut i = None;
    let mut v = 0;
    for p in pair.into_inner() {
        match p.as_rule() {
            Rule::pre_release_identifier => {
                i = Some(p.as_str());
            }
            Rule::normalized_pre_release_identifier => {
                i = Some(p.as_str());
            }
            Rule::integer => {
                v = parse_usize(p);
            }
            _ => unreachable!(),
        }
    }
    /* This is not very efficient */
    match i.unwrap().to_lowercase().as_str() {
        "alpha" => version::PreReleaseSegment::A(v),
        "a" => version::PreReleaseSegment::A(v),
        "beta" => version::PreReleaseSegment::B(v),
        "b" => version::PreReleaseSegment::B(v),
        "rc" => version::PreReleaseSegment::Rc(v),
        "pre" => version::PreReleaseSegment::Rc(v),
        "c" => version::PreReleaseSegment::Rc(v),
        _ => unreachable!(),
    }
}

fn parse_post_release_segment(pair: Pair<Rule>) -> usize {
    let mut v = None;
    for p in pair.into_inner() {
        match p.as_rule() {
            Rule::post_release_segment_long => {
                v = p.into_inner().find(|q| q.as_rule() == Rule::integer);
            }
            Rule::post_release_segment_short => {
                v = p.into_inner().find(|q| q.as_rule() == Rule::integer);
            }
            _ => {}
        }
    }
    v.map_or(0, |s| parse_usize(s))
}

fn parse_normalized_post_release_segment(pair: Pair<Rule>) -> usize {
    let p = pair.into_inner().find(|q| q.as_rule() == Rule::integer);
    parse_usize(p.unwrap())
}

fn parse_development_release(pair: Pair<Rule>) -> version::DevelopmentRelease {
    /* One inner rule: development_release_segment*/
    let dev_re_seg_p = pair.into_inner().next().unwrap();
    let segment = dev_re_seg_p
        .into_inner()
        .find(|p| p.as_rule() == Rule::integer)
        .map_or(0, parse_usize);
    version::DevelopmentRelease { segment }
}

fn parse_post_release(pair: Pair<Rule>) -> version::PostRelease {
    let mut segment = None;
    let mut suffix = version::PostReleaseSuffix::Empty;
    for p in pair.into_inner() {
        match p.as_rule() {
            Rule::post_release_segment => segment = Some(parse_post_release_segment(p)),
            Rule::normalized_post_release_segment => {
                segment = Some(parse_normalized_post_release_segment(p))
            }
            Rule::development_release => {
                suffix = version::PostReleaseSuffix::Development(parse_development_release(p));
            }
            Rule::normalized_development_release => {
                suffix = version::PostReleaseSuffix::Development(parse_development_release(p));
            }
            _ => {}
        }
    }
    version::PostRelease {
        segment: segment.unwrap(),
        suffix,
    }
}

fn parse_pre_release(pair: Pair<Rule>) -> version::PreRelease {
    let mut segment = None;
    let mut suffix = version::PreReleaseSuffix::Empty;
    for p in pair.into_inner() {
        match p.as_rule() {
            Rule::pre_release_segment => segment = Some(parse_pre_release_segment(p)),
            Rule::normalized_pre_release_segment => segment = Some(parse_pre_release_segment(p)),
            Rule::post_release => {
                suffix = version::PreReleaseSuffix::Post(parse_post_release(p));
            }
            Rule::normalized_post_release => {
                suffix = version::PreReleaseSuffix::Post(parse_post_release(p));
            }
            Rule::development_release => {
                suffix = version::PreReleaseSuffix::Development(parse_development_release(p));
            }
            Rule::normalized_development_release => {
                suffix = version::PreReleaseSuffix::Development(parse_development_release(p));
            }
            _ => {}
        }
    }
    version::PreRelease {
        segment: segment.unwrap(),
        suffix,
    }
}
fn parse_release_segments(pair: Pair<Rule>) -> std::vec::Vec<usize> {
    /* Build an iterator of integer elements */
    pair.into_inner().map(parse_usize).collect()
}
fn parse_version_suffix(pair: Pair<Rule>) -> version::VersionSuffix {
    for p in pair.into_inner() {
        match p.as_rule() {
            Rule::pre_release => {
                return version::VersionSuffix::Pre(parse_pre_release(p));
            }
            Rule::normalized_pre_release => {
                return version::VersionSuffix::Pre(parse_pre_release(p));
            }
            Rule::post_release => {
                return version::VersionSuffix::Post(parse_post_release(p));
            }
            Rule::normalized_post_release => {
                return version::VersionSuffix::Post(parse_post_release(p));
            }
            Rule::development_release => {
                return version::VersionSuffix::Development(parse_development_release(p));
            }
            Rule::normalized_development_release => {
                return version::VersionSuffix::Development(parse_development_release(p));
            }
            _ => {}
        }
    }
    unreachable!()
}

/* This supports both normalized and open grammar */
pub(super) fn parse_version(pair: Pair<Rule>) -> version::Version {
    let mut epoch = 0;
    let mut release_segments = None;
    let mut suffix = version::VersionSuffix::Empty;
    for p in pair.into_inner() {
        match p.as_rule() {
            Rule::epoch => {
                epoch = parse_usize(p.into_inner().next().unwrap());
            }
            Rule::release_segments => {
                release_segments = Some(parse_release_segments(p));
            }
            Rule::version_suffix => {
                suffix = parse_version_suffix(p);
            }
            Rule::normalized_version_suffix => {
                suffix = parse_version_suffix(p);
            }
            _ => {}
        }
    }
    version::Version {
        epoch,
        release_segments: release_segments.unwrap(),
        suffix,
    }
}
pub(super) fn parse_compare_version(pair: Pair<Rule>) -> specifier::ComparedVersion {
    let mut epoch = 0;
    let mut release_segments = None;
    let mut prefix_matching = false;
    let mut suffix = version::VersionSuffix::Empty;
    for p in pair.into_inner() {
        match p.as_rule() {
            Rule::epoch => {
                epoch = parse_usize(p.into_inner().next().unwrap());
            }
            Rule::prefix_matching => {
                prefix_matching = true;
            }
            Rule::release_segments => {
                release_segments = Some(parse_release_segments(p));
            }
            Rule::version_suffix => {
                suffix = parse_version_suffix(p);
            }
            _ => {}
        }
    }
    specifier::ComparedVersion {
        epoch,
        release_segments: release_segments.unwrap(),
        suffix,
        prefix_matching,
    }
}
pub(super) const ARBITRARY_EQUALITY: &str = "===";
pub(super) const COMPATIBILITY_RELEASE: &str = "~=";
pub(super) const VERSION_MATCHING: &str = "==";
pub(super) const VERSION_EXCLUSION: &str = "!=";
pub(super) const INCLUSIVE_ORDERED_COMPARISON_LESSER: &str = "<=";
pub(super) const INCLUSIVE_ORDERED_COMPARISON_GREATER: &str = ">=";
pub(super) const EXCLUSIVE_ORDERED_COMPARISON_LESSER: &str = "<";
pub(super) const EXCLUSIVE_ORDERED_COMPARISON_GREATER: &str = ">";
fn parse_version_specifier_operator(pair: Pair<Rule>) -> specifier::Operator {
    match pair.as_str() {
        ARBITRARY_EQUALITY => specifier::Operator::ArbitraryEquality,
        COMPATIBILITY_RELEASE => specifier::Operator::CompatibleRelease,
        VERSION_MATCHING => specifier::Operator::VersionMatching,
        VERSION_EXCLUSION => specifier::Operator::VersionExclusion,
        INCLUSIVE_ORDERED_COMPARISON_LESSER => {
            specifier::Operator::InclusiveOrderedComparisonLesser
        }
        INCLUSIVE_ORDERED_COMPARISON_GREATER => {
            specifier::Operator::InclusiveOrderedComparisonGreater
        }
        EXCLUSIVE_ORDERED_COMPARISON_LESSER => {
            specifier::Operator::ExclusiveOrderedComparisonLesser
        }
        EXCLUSIVE_ORDERED_COMPARISON_GREATER => {
            specifier::Operator::ExclusiveOrderedComparisonGreater
        }
        _ => unreachable!(),
    }
}
pub(super) fn parse_version_specifier(pair: Pair<Rule>) -> specifier::Specifier {
    let mut p = pair.into_inner();
    let operator = parse_version_specifier_operator(p.next().unwrap());
    let compared_version = parse_compare_version(p.next().unwrap());
    specifier::Specifier {
        operator,
        compared_version,
    }
}
pub(super) fn parse_version_specifiers(pair: Pair<Rule>) -> std::vec::Vec<specifier::Specifier> {
    pair.into_inner().map(parse_version_specifier).collect()
}

fn parse_binary_distribution_build_tag(
    pair: Pair<Rule>,
) -> distribution::BinaryDistributionBuildTag {
    let mut it = pair.into_inner();
    let digits = parse_usize(it.next().unwrap());
    let rest = it.next().map(|p| p.as_str());
    distribution::BinaryDistributionBuildTag { digits, rest }
}

pub(super) fn parse_binary_distribution_python_tag(
    pair: Pair<Rule>,
) -> distribution::BinaryDistributionPythonTag {
    let mut it = pair.into_inner();
    let implementation = it.next().unwrap().as_str();
    let version = parse_usize(it.next().unwrap());
    distribution::BinaryDistributionPythonTag {
        implementation,
        version,
    }
}

fn parse_binary_distribution_musllinux_platform_tag(
    pair: Pair<Rule>,
) -> distribution::BinaryDistributionPlatformTag {
    let mut it = pair.into_inner();
    let major = parse_usize(it.next().unwrap());
    let minor = parse_usize(it.next().unwrap());
    let arch = it.next().unwrap().as_str();
    distribution::BinaryDistributionPlatformTag::MuslLinux(distribution::MuslLinuxPlatformTag {
        major,
        minor,
        arch,
    })
}
fn parse_binary_distribution_manylinux_platform_tag(
    pair: Pair<Rule>,
) -> distribution::BinaryDistributionPlatformTag {
    match pair.as_str() {
        "any" => distribution::BinaryDistributionPlatformTag::Any,
        "manylinux1" => distribution::BinaryDistributionPlatformTag::ManyLinux1,
        "manylinux2010" => distribution::BinaryDistributionPlatformTag::ManyLinux2010,
        "manylinux2014" => distribution::BinaryDistributionPlatformTag::ManyLinux2014,
        _ => {
            let mut it = pair.into_inner().next().unwrap().into_inner();
            let glibc_major = parse_usize(it.next().unwrap());
            let glibc_minor = parse_usize(it.next().unwrap());
            let arch = it.next().unwrap().as_str();
            distribution::BinaryDistributionPlatformTag::ManyLinux(
                distribution::ManyLinuxPlatformTag {
                    glibc_major,
                    glibc_minor,
                    arch,
                },
            )
        }
    }
}

pub(super) fn parse_binary_distribution_platform_tag(
    pair: Pair<Rule>,
) -> distribution::BinaryDistributionPlatformTag {
    for p in pair.into_inner() {
        match p.as_rule() {
            Rule::manylinux_platform_tag => {
                return parse_binary_distribution_manylinux_platform_tag(p);
            }
            Rule::musllinux_platform_tag => {
                return parse_binary_distribution_musllinux_platform_tag(p)
            }
            Rule::basic_platform_tag => {
                return distribution::BinaryDistributionPlatformTag::Basic(p.as_str());
            }
            _ => {}
        }
    }
    unreachable!();
}

pub(super) fn parse_binary_distribution_filename(
    pair: Pair<Rule>,
) -> distribution::BinaryDistribution {
    let mut distribution = None;
    let mut version = None;
    let mut build_tag = None;
    let mut python_tags = None;
    let mut abi_tags = None;
    let mut platform_tags = None;
    for p in pair.into_inner() {
        match p.as_rule() {
            Rule::distribution => {
                distribution = Some(p.as_str());
            }
            Rule::normalized_version => {
                version = Some(parse_version(p));
            }
            Rule::build_tag => {
                build_tag = Some(parse_binary_distribution_build_tag(p));
            }
            Rule::python_tags => {
                python_tags = Some(
                    p.into_inner()
                        .map(parse_binary_distribution_python_tag)
                        .collect(),
                );
            }
            Rule::abi_tags => {
                abi_tags = Some(p.into_inner().map(|r| r.as_str()).collect());
            }
            Rule::platform_tags => {
                platform_tags = Some(
                    p.into_inner()
                        .map(parse_binary_distribution_platform_tag)
                        .collect(),
                );
            }
            _ => {}
        }
    }
    distribution::BinaryDistribution {
        distribution: distribution.unwrap(),
        version: version.unwrap(),
        build_tag,
        python_tags: python_tags.unwrap(),
        abi_tags: abi_tags.unwrap(),
        platform_tags: platform_tags.unwrap(),
    }
}

pub(super) fn parse_source_distribution_filename(
    pair: Pair<Rule>,
) -> distribution::SourceDistribution {
    let mut distribution = None;
    let mut version = None;
    for p in pair.into_inner() {
        match p.as_rule() {
            Rule::distribution => {
                distribution = Some(p.as_str());
            }
            Rule::normalized_version => {
                version = Some(parse_version(p));
            }
            _ => {}
        }
    }
    distribution::SourceDistribution {
        distribution: distribution.unwrap(),
        version: version.unwrap(),
    }
}
pub(super) const PYTHON_VERSION: &str = "python_version";
pub(super) const PYTHON_FULL_VERSION: &str = "python_full_version";
pub(super) const OS_NAME: &str = "os_name";
pub(super) const SYS_PLATFORM: &str = "sys_platform";
pub(super) const PLATFORM_RELEASE: &str = "platform_release";
pub(super) const PLATFORM_SYSTEM: &str = "platform_system";
pub(super) const PLATFORM_VERSION: &str = "platform_version";
pub(super) const PLATFORM_MACHINE: &str = "platform_machine";
pub(super) const PLATFORM_PYTHON_IMPLEMENTATION: &str = "platform_python_implementation";
pub(super) const IMPLEMENTATION_NAME: &str = "implementation_name";
pub(super) const IMPLEMENTATION_VERSION: &str = "implementation_version";
pub(super) const EXTRA: &str = "extra";
fn parse_quoted_marker_env_var(p: Pair<Rule>) -> dependency::MarkerEnv {
    match p.as_str() {
        PYTHON_VERSION => dependency::MarkerEnv::PythonVersion,
        PYTHON_FULL_VERSION => dependency::MarkerEnv::PythonFullVersion,
        OS_NAME => dependency::MarkerEnv::OsName,
        SYS_PLATFORM => dependency::MarkerEnv::SysPlatform,
        PLATFORM_RELEASE => dependency::MarkerEnv::PlatformRelease,
        PLATFORM_SYSTEM => dependency::MarkerEnv::PlatformSystem,
        PLATFORM_VERSION => dependency::MarkerEnv::PlatformVersion,
        PLATFORM_MACHINE => dependency::MarkerEnv::PlatformMachine,
        PLATFORM_PYTHON_IMPLEMENTATION => dependency::MarkerEnv::PlatformPythonImplementation,
        IMPLEMENTATION_NAME => dependency::MarkerEnv::ImplementationName,
        IMPLEMENTATION_VERSION => dependency::MarkerEnv::ImplementationVersion,
        EXTRA => dependency::MarkerEnv::Extra,
        _ => unreachable!(),
    }
}
pub(super) const LESS: &str = "<";
pub(super) const LESS_EQUAL: &str = "<=";
pub(super) const DIFFER: &str = "!=";
pub(super) const EQUAL: &str = "==";
pub(super) const GREATER_EQUAL: &str = ">=";
pub(super) const GREATER: &str = ">";
pub(super) const IN: &str = "in";
pub(super) const NOT_IN: &str = "not in";
fn parse_quoted_marker_operator(p: Pair<Rule>) -> dependency::MarkerVarOperator {
    match p.as_str() {
        LESS => dependency::MarkerVarOperator::Less,
        LESS_EQUAL => dependency::MarkerVarOperator::LessEqual,
        DIFFER => dependency::MarkerVarOperator::Differ,
        EQUAL => dependency::MarkerVarOperator::Equal,
        GREATER_EQUAL => dependency::MarkerVarOperator::GreaterEqual,
        GREATER => dependency::MarkerVarOperator::Greater,
        IN => dependency::MarkerVarOperator::In,
        NOT_IN => dependency::MarkerVarOperator::NotIn,
        _ => unreachable!(),
    }
}
fn parse_quoted_marker_var(p: Pair<Rule>) -> dependency::MarkerVar {
    let r = p.into_inner().next().unwrap();
    match r.as_rule() {
        Rule::quoted_marker_env_var => dependency::MarkerVar::Env(parse_quoted_marker_env_var(r)),
        Rule::quoted_marker_str => {
            dependency::MarkerVar::String(r.into_inner().next().unwrap().as_str())
        }
        _ => unreachable!(),
    }
}
fn parse_quoted_marker_expr(pair: Pair<Rule>) -> dependency::Marker {
    let mut left = None;
    let mut right = None;
    let mut operator = None;
    /* quoted_marker_expr can be either:
     * - a single Marker
     * - a MarkerVarExpr
     * */
    for p in pair.into_inner() {
        match p.as_rule() {
            Rule::quoted_marker => {
                return parse_quoted_marker(p);
            }
            Rule::quoted_marker_var => {
                let p = Some(parse_quoted_marker_var(p));
                /* Left element will come first */
                match left {
                    Some(_) => {
                        right = p;
                    }
                    None => {
                        left = p;
                    }
                };
            }
            Rule::quoted_marker_operator => operator = Some(parse_quoted_marker_operator(p)),
            _ => {}
        }
    }
    dependency::Marker::Var(dependency::MarkerVarExpr {
        left: left.unwrap(),
        right: right.unwrap(),
        operator: operator.unwrap(),
    })
}
fn parse_and_quoted_marker(pair: Pair<Rule>) -> dependency::Marker {
    let mut it = pair.into_inner();
    let left = parse_quoted_marker_expr(it.next().unwrap());
    /* If there is more than one marker, then return a And marker, else the first one */
    match it.next() {
        Some(right_pair) => {
            let right = parse_quoted_marker_expr(right_pair);
            dependency::Marker::And(Box::new((left, right)))
        }
        None => left,
    }
}
fn parse_or_quoted_marker(pair: Pair<Rule>) -> dependency::Marker {
    let mut it = pair.into_inner();
    let left = parse_and_quoted_marker(it.next().unwrap());
    /* If there is more than one marker, then return a Or marker, else the first one */
    match it.next() {
        Some(right_pair) => {
            let right = parse_and_quoted_marker(right_pair);
            dependency::Marker::Or(Box::new((left, right)))
        }
        None => left,
    }
}
pub(super) fn parse_quoted_marker(pair: Pair<Rule>) -> dependency::Marker {
    /* Just read the quoted_marker_or */
    parse_or_quoted_marker(pair.into_inner().next().unwrap())
}
pub(super) fn parse_dependency(pair: Pair<Rule>) -> dependency::Dependency {
    let mut package = None;
    let mut package_extras = None;
    let mut specification = None;
    let mut marker_specifier = None;
    for p in pair.into_inner() {
        match p.as_rule() {
            Rule::package => {
                package = Some(p.as_str());
            }
            Rule::package_extras => {
                package_extras = Some(p.into_inner().map(|r| r.as_str()).collect());
            }
            Rule::version_specifiers => {
                specification = Some(dependency::DependencySpecification::Name(
                    parse_version_specifiers(p),
                ));
            }
            Rule::uri => {
                specification = Some(dependency::DependencySpecification::Url(p.as_str()));
            }
            Rule::quoted_marker => marker_specifier = Some(parse_quoted_marker(p)),
            _ => {}
        }
    }
    dependency::Dependency {
        package: package.unwrap(),
        extras: package_extras.or(Some(std::vec::Vec::new())).unwrap(),
        specification,
        marker_specifier,
    }
}

/* https://packaging.python.org/en/latest/specifications/name-normalization/#name-normalization */
static PYTHON_NAME_NORMALIZATION_RE_INIT: std::sync::Once = std::sync::Once::new();
static mut PYTHON_NAME_NORMALIZATION_RE: Option<regex::Regex> = None;

pub(super) fn name_normalization(name: &str) -> String {
    unsafe {
        PYTHON_NAME_NORMALIZATION_RE_INIT.call_once(|| {
            PYTHON_NAME_NORMALIZATION_RE = Some(regex::Regex::new(r"[-_.]+").unwrap());
        })
    }
    let res = name.to_lowercase();
    unsafe {
        PYTHON_NAME_NORMALIZATION_RE
            .as_ref()
            .unwrap()
            .replace_all(&res, "-")
            .to_string()
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn name_normalization() {
        assert_eq!(super::name_normalization("friendly-bard"), "friendly-bard");
        assert_eq!(super::name_normalization("Friendly-Bard"), "friendly-bard");
        assert_eq!(super::name_normalization("FRIENDLY-BARD"), "friendly-bard");
        assert_eq!(super::name_normalization("friendly.bard"), "friendly-bard");
        assert_eq!(super::name_normalization("friendly_bard"), "friendly-bard");
        assert_eq!(super::name_normalization("friendly--bard"), "friendly-bard");
        assert_eq!(
            super::name_normalization("friendly-._.-bard"),
            "friendly-bard"
        );
    }
}

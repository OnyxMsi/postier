use crate::lib::python;
use crate::lib::python::syntax::distribution;
use serde::Deserialize;

/* All of the process here is "lazy"
 * The function will try to extract things from a source distribution archive but will
 * ignore any errors or missing values
 * Sources distributions are a real pain to work with, even with a proper
 * python environment.
 *
 * The introduction of pyproject.toml https://packaging.python.org/en/latest/specifications/pyproject-toml/
 * has made things easier, since you don't rely on a python interpreter anymore
 */

fn load(
    path: &std::path::Path,
) -> Result<tar::Archive<flate2::read::GzDecoder<std::fs::File>>, python::Error> {
    log::debug!(
        "Open source distribution file {} as a tar.gz file",
        path.display()
    );
    let fd = std::fs::File::open(path).map_err(|e| {
        log::error!(
            "Can't open source distribution file {}: {}",
            path.display(),
            e
        );
        python::Error::InvalidDistributionError
    })?;
    let gz_reader = flate2::read::GzDecoder::new(fd);
    Ok(tar::Archive::new(gz_reader))
}

/* Find and extract a single entry as a text file
 * Maybe this is not the most efficient way, but it is simple
 */
fn extract_file_from_archive<E>(
    archive: &mut tar::Archive<E>,
    distribution_name: &str,
    filename: &str,
) -> Option<String>
where
    E: std::io::Read,
{
    let mut path = std::path::PathBuf::new();
    path.push(distribution_name);
    path.push(filename);
    log::debug!("Look for a file {} in source distribution", path.display());
    for r_entry in archive.entries().ok()? {
        if let Ok(mut entry) = r_entry {
            if entry.path().map(|p| p == path).unwrap_or(false) {
                let mut d = String::new();
                use std::io::Read;
                entry
                    .read_to_string(&mut d)
                    .inspect_err(|e| {
                        log::warn!(
                            "Can't read distribution {} file {} as text: {}",
                            distribution_name,
                            path.display(),
                            e
                        );
                    })
                    .ok()?;
                return Some(d);
            }
        }
    }
    None
}

fn unpack_distribution<E>(
    archive: &mut tar::Archive<E>,
    distribution: &distribution::SourceDistribution,
    working_directory: &std::path::Path,
) -> Result<std::path::PathBuf, python::Error>
where
    E: std::io::Read,
{
    let final_path = working_directory.join(distribution.distribution);
    if final_path.exists() {
        log::debug!("Remove {}", final_path.display());
        std::fs::remove_dir_all(&final_path).map_err(|e| {
            log::error!("Can't remove {}: {}", final_path.display(), e);
            python::Error::InvalidDistributionError
        })?;
    }
    archive.unpack(&final_path).map_err(|e| {
        log::error!(
            "Can't extract source distribution {} into {}: {}",
            distribution.distribution,
            final_path.display(),
            e
        );
        python::Error::InvalidDistributionError
    })?;
    Ok(final_path.join(format!(
        "{}-{}",
        distribution.distribution, distribution.version
    )))
}

/* TODO work with borrowed string */
#[derive(Deserialize)]
struct Pyproject {
    project: Project,
}

#[derive(Deserialize)]
struct Project {
    dependencies: std::vec::Vec<String>,
    #[serde(rename(deserialize = "optional-dependencies"))]
    optional_dependencies: Option<std::collections::HashMap<String, std::vec::Vec<String>>>,
}

fn dependencies_from_pyproject_toml(data: &str) -> super::Dependencies {
    let mut res = std::vec::Vec::new();
    log::debug!("Extract dependencies from pyproject.toml");
    match toml::from_str::<Pyproject>(&data) {
        Ok(py) => {
            /* Now extract dependencies from the structure
             * Take ownership of everything*/
            for dep in py.project.dependencies {
                res.push((dep, None))
            }
            if let Some(mut op) = py.project.optional_dependencies {
                for (extra, dependencies) in op.drain() {
                    for dep in dependencies {
                        /* Extra value needs to be copied because it will
                         * be used many times */
                        res.push((dep, Some(extra.as_str().to_string())));
                    }
                }
            }
        }
        Err(e) => {
            log::error!("Can't extract data from pyproject.toml: {}", e);
        }
    };
    res
}

fn dependencies_from_setup_py(
    interpreter: &std::path::Path,
    setup_py: &std::path::Path,
) -> super::Dependencies {
    let data = match crate::lib::python::scripts::extract_dependencies_from_setup_py(
        interpreter,
        setup_py,
    ) {
        Ok(s) => s,
        Err(_e) => return std::vec::Vec::new(),
    };
    let metadata = match crate::lib::python::syntax::metadata::parse(&data) {
        Ok(s) => s,
        Err(_e) => return std::vec::Vec::new(),
    };
    super::binary::extract_dependencies(&metadata)
}

pub fn find_dependencies(
    distribution: &distribution::SourceDistribution,
    path: &std::path::Path,
    working_directory: &std::path::Path,
    interpreter: Option<&std::path::Path>,
) -> Result<super::Dependencies, python::Error> {
    /* First thing is to look for a pyproject.toml, if it is found then use it */
    let mut archive = load(path)?;
    let look_for_pyproject =
        extract_file_from_archive(&mut archive, distribution.distribution, "pyproject.tomd");
    if let Some(pyproject) = look_for_pyproject {
        return Ok(dependencies_from_pyproject_toml(&pyproject));
    }
    /* If not found then try with setup.py */
    // TODO use seek ?
    match interpreter {
        Some(int) => {
            let mut archive = load(path)?;
            let setup = unpack_distribution(&mut archive, distribution, working_directory)?
                .join("setup.py");
            if !setup.exists() {
                log::debug!("No setup.py in {}, no dependencies", path.display());
                return Ok(std::vec::Vec::new());
            }
            Ok(dependencies_from_setup_py(int, &setup))
        }
        None => {
            log::debug!("No python interpreter, can't get dependencies from setup.py");
            Ok(std::vec::Vec::new())
        }
    }
}

#[cfg(test)]
mod test {
    const SDIST: &'static str = "pkg-0.0.1.tar.gz";
    fn source_filepath() -> std::path::PathBuf {
        std::path::PathBuf::from(env!("CARGO_MANIFEST_DIR"))
            .join("python_test_pkg")
            .join("dist")
            .join(SDIST)
    }
    #[test]
    fn dependencies_from_pyproject_toml() {
        let p = std::path::PathBuf::from(env!("CARGO_MANIFEST_DIR"))
            .join("python_test_pkg")
            .join("pyproject.toml");
        let mut s = String::new();
        use std::io::Read;
        std::fs::File::open(p)
            .unwrap()
            .read_to_string(&mut s)
            .unwrap();
        let res = super::dependencies_from_pyproject_toml(&s);
        assert_eq!(
            crate::lib::python::distribution::test::dependencies(&res),
            [
                ("dep1>24", None),
                ("dep2 [test]", None),
                ("extra1-dep1 [doc]", Some("extra1")),
                ("extra1-dep2==1.0", Some("extra1")),
                ("extra2-dep1", Some("extra2")),
            ]
        );
    }
    fn test_setup_py_filepath() -> std::path::PathBuf {
        std::path::PathBuf::from(env!("CARGO_MANIFEST_DIR"))
            .join("src")
            .join("lib")
            .join("python")
            .join("distribution")
            .join("test_setup.py")
    }
    #[test]
    fn dependencies_from_setup_py() {
        if !crate::lib::python::scripts::test::check_interpreter() {
            return ();
        }
        let p = test_setup_py_filepath();
        let interpreter = crate::lib::python::scripts::test::get_interpreter();
        let res = super::dependencies_from_setup_py(&interpreter, &p);
        assert_eq!(
            crate::lib::python::distribution::test::dependencies(&res),
            [
                ("peppercorn", None),
                ("check-manifest", Some("dev")),
                ("coverage", Some("test")),
            ]
        );
    }
    #[test]
    fn unpack_distribution() {
        let wdir = crate::lib::utils::test::get_temp_dir();
        let dist =
            crate::lib::python::syntax::distribution::SourceDistribution::parse(SDIST).unwrap();
        let p =
            super::unpack_distribution(&mut super::load(&source_filepath()).unwrap(), &dist, &wdir)
                .unwrap();
        assert!(p.exists());
        let pyproject_toml = p.join("pyproject.toml");
        assert!(pyproject_toml.exists());
    }
}

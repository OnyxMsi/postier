use crate::lib::python;
use crate::lib::python::syntax::{distribution, metadata};

fn get_metadata(
    distribution: &distribution::BinaryDistribution,
    path: &std::path::Path,
) -> Result<String, python::Error> {
    log::debug!(
        "Open binary distribution file {} as a zip file",
        path.display()
    );
    let fd = std::fs::File::open(path).map_err(|e| {
        log::error!(
            "Can't open binary distribution file {}: {}",
            path.display(),
            e
        );
        python::Error::InvalidDistributionError
    })?;
    let mut archive = zip::ZipArchive::new(fd).map_err(|e| {
        log::error!("Invalid zip archive {}: {}", path.display(), e);
        python::Error::InvalidDistributionError
    })?;
    let metadata_path = format!(
        "{}-{}.dist-info/METADATA",
        distribution.distribution, distribution.version
    );
    let mut s = String::new();
    log::debug!(
        "Extract {} from binary distribution file {}",
        metadata_path,
        path.display()
    );
    use std::io::Read;
    archive
        .by_name(&metadata_path)
        .map_err(|e| {
            log::error!(
                "Can't find {} in binary distribution file {}: {}",
                metadata_path,
                path.display(),
                e
            );
            python::Error::InvalidDistributionError
        })?
        .read_to_string(&mut s)
        .map_err(|e| {
            log::error!(
                "Can't read {} from binary distribution file {} as a string: {}",
                metadata_path,
                path.display(),
                e
            );
            python::Error::InvalidDistributionError
        })?;
    Ok(s)
}

pub(super) fn extract_dependencies<'a>(
    metadata: &'a metadata::MetadataContent,
) -> super::Dependencies {
    let mut extra = None;
    let mut v = std::vec::Vec::new();
    for (key, value) in metadata {
        match *key {
            /* Note that the same extra will be copied many times
             * But I don't see a more efficient way
             */
            "Requires-Dist" => v.push((value.to_string(), extra.map(|s: &str| s.to_string()))),
            "Provides-Extra" => extra = Some(*value),
            _ => {}
        }
    }
    v
}

/* Just a wrapper to call from outside this module */
pub fn find_dependencies(
    distribution: &distribution::BinaryDistribution,
    path: &std::path::Path,
) -> Result<super::Dependencies, python::Error> {
    let content = get_metadata(distribution, path)?;
    let metadata = metadata::parse(&content)?;
    Ok(extract_dependencies(&metadata))
}

#[cfg(test)]
mod test {
    const BDIST: &'static str = "pkg-0.0.1-py3-none-any.whl";
    const METADATA: &'static str = r#"Metadata-Version: 2.1
Name: pkg
Version: 0.0.1
Requires-Python: >=3.11
Description-Content-Type: text/raw
Requires-Dist: dep1 >24
Requires-Dist: dep2[test]
Provides-Extra: extra1
Requires-Dist: extra1-dep1[doc] ; extra == 'extra1'
Requires-Dist: extra1-dep2 ==1.0 ; extra == 'extra1'
Provides-Extra: extra2
Requires-Dist: extra2-dep1 ; extra == 'extra2'

Some useless text"#;

    fn binary_filepath() -> std::path::PathBuf {
        std::path::PathBuf::from(env!("CARGO_MANIFEST_DIR"))
            .join("python_test_pkg")
            .join("dist")
            .join(BDIST)
    }
    #[test]
    fn get_metadata() {
        let distribution =
            crate::lib::python::syntax::distribution::BinaryDistribution::parse(BDIST).unwrap();
        let metadata = super::get_metadata(&distribution, &binary_filepath()).unwrap();
        assert_eq!(metadata.trim(), METADATA);
    }
    #[test]
    fn extract_dependencies() {
        let m = crate::lib::python::syntax::metadata::parse(METADATA).unwrap();
        let deps = super::extract_dependencies(&m);
        assert_eq!(
            crate::lib::python::distribution::test::dependencies(&deps),
            [
                ("dep1 >24", None),
                ("dep2[test]", None),
                ("extra1-dep1[doc] ; extra == 'extra1'", Some("extra1")),
                ("extra1-dep2 ==1.0 ; extra == 'extra1'", Some("extra1")),
                ("extra2-dep1 ; extra == 'extra2'", Some("extra2")),
            ]
        );
    }
}

use std::vec::Vec;
mod distribution;
mod fetch;
mod scripts;
pub mod syntax;
use super::utils;

#[derive(Debug)]
pub enum Error {
    LibError(super::Error),
    InvalidParameter,
    SimpleRepositoryError,
    PythonScriptError,
    SpecificationParseError,
    InvalidDistributionError,
    MetadataParseError,
}

pub trait Parameters {
    fn get_directory(&self) -> &std::path::Path;
    /// A new dependency is to be solved in the provided environment
    fn push_dependency(
        &self,
        dependency: &str,
        environment_marker: &EnvironmentMarker,
    ) -> super::Control {
        super::Control::Continue
    }
    /// Start solving this dependency using the provided environment
    fn solve_dependency_start(
        &self,
        dependency: &syntax::dependency::Dependency,
        environment_marker: &EnvironmentMarker,
    ) -> super::Control {
        super::Control::Continue
    }
    /// Can't solve this dependency
    fn solve_dependency_failed(
        &self,
        dependency: &syntax::dependency::Dependency,
        environment_marker: &EnvironmentMarker,
        error: &Error,
    ) -> super::Control {
        super::Control::Continue
    }
    /// Dependency was solved
    fn dependency_solved(
        &self,
        dependency: &syntax::dependency::Dependency,
        environment_marker: &EnvironmentMarker,
    ) -> super::Control {
        super::Control::Continue
    }
}

/// <https://packaging.python.org/en/latest/specifications/dependency-specifiers/#environment-markers>
/// This represents a environment python is running on.
/// These fields will be needed to choose one package among many
#[derive(PartialEq, Eq, Clone, Hash)]
pub struct EnvironmentMarker {
    pub python_version: syntax::version::Version,
    pub python_full_version: syntax::version::Version,
    pub os_name: String,
    pub sys_platform: String,
    pub platform_release: String,
    pub platform_system: String,
    pub platform_version: String,
    pub platform_machine: String,
    pub platform_python_implementation: String,
    pub implementation_name: String,
    pub implementation_version: syntax::version::Version,
    pub extra: Option<String>,
    /* This is for https://packaging.python.org/en/latest/specifications/platform-compatibility-tags/#abi-tag
     * I can't find a way to guess it from other field
     */
    pub abi_tags: std::vec::Vec<String>,
    /* This is for https://packaging.python.org/en/latest/specifications/platform-compatibility-tags/#abi-tag
     * If none then it will be build using platform_system + platform_machine
     */
    pub platform_tags: std::vec::Vec<String>,
}
struct Index {
    url: url::Url,
}
#[derive(PartialEq, Eq, Clone, Hash)]
struct Requirement {
    text: String,
}

pub struct Source<'s> {
    environment_markers: Vec<EnvironmentMarker>,
    indexes: Vec<Index>,
    requirements: Vec<Requirement>,
    local_interpreter_path: Option<std::path::PathBuf>,
    parameters: &'s dyn Parameters,
}

impl<'s> Source<'s> {
    pub fn new(parameters: &'s dyn Parameters) -> Result<Self, Error> {
        Ok(Source {
            indexes: Vec::new(),
            requirements: Vec::new(),
            environment_markers: Vec::new(),
            local_interpreter_path: None,
            parameters,
        })
    }
    /// Add a python environment to work on.
    /// See the [documentation](https://packaging.python.org/en/latest/specifications/dependency-specifiers/#environment-markers) for explanations
    pub fn add_environment(&mut self, markers: EnvironmentMarker) {
        log::debug!("Set environment marker");
        self.environment_markers.push(markers);
    }
    /// Add an index to look package from
    /// The URL be a [simple](https://packaging.python.org/en/latest/specifications/simple-repository-api/#base-html-api) API base URL
    pub fn add_index(&mut self, url: &str) -> Result<(), Error> {
        log::debug!("Add index {}", url);
        let url = utils::url_with_trailling_slash(url).map_err(Error::LibError)?;
        self.indexes.push(Index { url });
        Ok(())
    }
    /// Add a requirement to solve
    pub fn add_requirement(&mut self, req: &str) {
        log::debug!("Add requirement {}", req);
        self.requirements.push(Requirement {
            text: String::from(req),
        });
    }
    /// Set where a local python interpreter can be found so the module can run Python scripts.
    /// **This is an optional feature:** if this function is not called then the [super::Source]
    /// methods can still be called.
    ///
    /// Defining this will add the following features:
    /// * Extract dependencies from [source distribution](https://packaging.python.org/en/latest/specifications/source-distribution-format/) `setup.py`
    ///
    /// See [here](https://www.python.org/) to install it on your system
    ///
    /// *The interpreter must run python3*
    pub fn set_local_interpreter_path(&mut self, path: &std::path::Path) -> Result<(), Error> {
        log::debug!("Set {} as local python interpreter", path.display());
        if !path.is_file() {
            log::error!("{}: no such file", path.display());
            return Err(Error::InvalidParameter);
        }
        self.local_interpreter_path = Some(path.to_path_buf());
        Ok(())
    }
    fn get_working_directory(&self) -> std::path::PathBuf {
        self.parameters.get_directory().join("work")
    }
    fn get_distribution_directory(&self) -> std::path::PathBuf {
        self.parameters.get_directory().join("dists")
    }
}
impl super::Source<Error> for Source<'_> {
    fn init(&self) -> Result<(), Error> {
        log::debug!("Initialize source");
        utils::create_directory(self.parameters.get_directory()).map_err(Error::LibError)?;
        utils::create_directory(self.get_working_directory().as_path()).map_err(Error::LibError)?;
        utils::create_directory(self.get_distribution_directory().as_path())
            .map_err(Error::LibError)?;
        log::info!("Initialized");
        Ok(())
    }
    fn fetch(&self) -> Result<(), Error> {
        log::debug!("Fetch requirements");
        if self.local_interpreter_path.is_none() {
            log::warn!("No local interpreter was set, some features will be disabled");
        }
        let dist_dir = self.get_distribution_directory();
        let w_dir = self.get_working_directory();
        /* In order to avoid creating a new environment for each requirement, use a pool */
        let mut fetcher = fetch::Fetcher::new(
            &dist_dir,
            &w_dir,
            self.local_interpreter_path.as_deref(),
            self.indexes.as_slice(),
            self.requirements.as_slice(),
            self.environment_markers.as_slice(),
        );
        fetcher.run(self.parameters)?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    impl super::Parameters for std::path::PathBuf {
        fn get_directory(&self) -> &std::path::Path {
            self
        }
    }
    #[test]
    fn init() {
        let d = crate::lib::utils::test::get_temp_dir();
        let mut s = super::Source::new(&d).unwrap();
        use crate::lib::Source;
        /* No check for doubles */
        let r = "mypackage < 1.9.9";
        s.add_requirement(r);
        assert_eq!(s.requirements.len(), 1);
        assert_eq!(s.requirements[0].text, r);
        s.add_requirement(r);
        assert_eq!(s.requirements.len(), 2);
        assert_eq!(s.requirements[0].text, r);
        assert_eq!(s.requirements[1].text, r);
        s.init().unwrap();
        /* Check directories */
        assert!(s.get_working_directory().exists());
        assert!(s.get_distribution_directory().exists());
    }
}

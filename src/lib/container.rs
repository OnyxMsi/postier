pub mod distribution;
mod syntax;
use super::utils;

#[derive(Debug)]
pub enum Error {
    DistributionError,
    PullError,
    ArchiveError,
    LibError(super::Error),
}

pub struct Platform {
    pub architecture: String,
    pub os: String,
}

pub trait Parameters {
    fn get_directory(&self) -> &std::path::Path;
}
pub struct Source<'s> {
    registries: std::vec::Vec<url::Url>,
    images: std::vec::Vec<(String, String)>,
    platforms: std::vec::Vec<Platform>,
    parameters: &'s dyn Parameters,
}

/// Stupid wrapper around [std::fs::File::open] to handle errors
macro_rules! pull_open_wrapper {
    ($path:expr, $error:expr) => {
        std::fs::File::open($path).map_err(|e| {
            log::error!("Failed to open {}: {}", $path.display(), e);
            $error
        })
    };
}

impl<'s> Source<'s> {
    pub fn new(parameters: &'s dyn Parameters) -> Result<Self, Error> {
        Ok(Source {
            registries: std::vec::Vec::new(),
            images: std::vec::Vec::new(),
            platforms: std::vec::Vec::new(),
            parameters,
        })
    }
    fn get_working_directory(&self) -> std::path::PathBuf {
        self.parameters.get_directory().join("work")
    }
    fn get_images_directory(&self) -> std::path::PathBuf {
        self.parameters.get_directory().join("images")
    }
    pub fn add_registry(&mut self, index: url::Url) {
        log::debug!("Add index {}", index);
        self.registries.push(index);
    }
    pub fn add_image(&mut self, image: String, reference: String) {
        log::debug!("Add image {}", image);
        self.images.push((image, reference));
    }
    pub fn add_platform(&mut self, platform: Platform) {
        log::debug!(
            "Add platform architecture {} os {}",
            platform.architecture,
            platform.os
        );
        self.platforms.push(platform)
    }
    /// Wrapper to pull a layer only if not already in cache
    fn pull_layer(
        &self,
        registry_api: &mut distribution::Distribution,
        image: &str,
        digest: &syntax::digest::Digest,
    ) -> Result<std::path::PathBuf, Error> {
        let path = self.get_working_directory().join(digest.hex);
        if path.exists() && digest.check_path(&path) {
            log::debug!("Layer {} is already in cache", digest.hex);
        } else {
            log::debug!(
                "Pull image {} layer {} in {}",
                image,
                digest.hex,
                path.display()
            );
            registry_api.pull_image_layer(image, digest.hex, &path)?;
        }
        Ok(path)
    }
    /// Pull an image from a registry according to the requested platform
    /// Return the manifest used to pull stuff
    fn pull_image(
        &self,
        registry_api: &mut distribution::Distribution,
        image: &str,
        reference: &str,
        platform: &Platform,
    ) -> Result<distribution::ManifestV2, Error> {
        log::debug!("Pull image {}:{}", image, reference);
        let image_path = self
            .get_images_directory()
            .join(format!("{}_{}", image, reference));
        utils::create_directory(&image_path).map_err(Error::LibError)?;
        /* Fetch manifest list */
        /* Save manifests */
        let manifest_list = registry_api.pull_image_manifest(image, reference)?;
        /* Find the correct manifest according to parameters */
        let manifest = manifest_list
            .manifests
            .iter()
            .find(|m| {
                m.platform.os == platform.os && m.platform.architecture == platform.architecture
            })
            .ok_or_else(|| {
                log::debug!(
                    "Can't find a manifest for {}:{} on platform {} {}",
                    image,
                    reference,
                    platform.os,
                    platform.architecture
                );
                Error::PullError
            })?;
        /* Fetch the manifest */
        let digest = manifest.digest()?;
        log::debug!(
            "Found manifest {} for image {}:{} on platform {} {}",
            digest.hex,
            image,
            reference,
            platform.os,
            platform.architecture
        );
        /* Save it to a file first, then parse it */
        let manifest_path = self.pull_layer(registry_api, image, &digest)?;
        let manifest_fd = pull_open_wrapper!(&manifest_path, Error::PullError)?;
        let manifest: distribution::ManifestV2 =
            serde_json::from_reader(manifest_fd).map_err(|e| {
                log::error!("Can't build a Manifest V2 from layer {}: {}", digest.hex, e);
                Error::DistributionError
            })?;
        /* Then every layer */
        for layer in &manifest.layers {
            self.pull_layer(registry_api, image, &layer.digest()?)?;
        }
        Ok(manifest)
    }
    fn save_image_archive(
        &self,
        image: &str,
        reference: &str,
        manifest: &distribution::ManifestV2,
    ) -> Result<std::path::PathBuf, Error> {
        let working_directory = self.get_working_directory();
        let archive_path = self
            .get_images_directory()
            .join(format!("{}_{}.tar.gz", image, reference));
        log::debug!(
            "Save image {}:{} into tar.gz archive {}",
            image,
            reference,
            archive_path.display()
        );
        let out_fd = std::fs::File::create(&archive_path).map_err(|e| {
            log::error!("Failed to create archive {}: {}", archive_path.display(), e);
            Error::ArchiveError
        })?;
        let gz_writer = flate2::write::GzEncoder::new(out_fd, flate2::Compression::best());
        let mut tar_writer = tar::Builder::new(gz_writer);
        macro_rules! tar_append {
            ($digest:expr, $dest:expr) => {{
                let l_path = working_directory.join($digest.hex);
                let mut fd = pull_open_wrapper!(l_path.as_path(), Error::ArchiveError)?;
                log::debug!(
                    "Add {} to archive {}",
                    l_path.display(),
                    archive_path.display()
                );
                tar_writer.append_file($dest, &mut fd).map_err(|e| {
                    log::error!(
                        "Can't add {} to archive {}: {}",
                        l_path.display(),
                        archive_path.display(),
                        e
                    );
                    Error::ArchiveError
                })
            }};
        }
        /* First the manifest */
        tar_append!(&manifest.config.digest()?, "manifest.json")?;
        /* Now the layers */
        for layer in &manifest.layers {
            let d = layer.digest()?;
            tar_append!(&d, &d.hex)?;
        }
        tar_writer.finish().map_err(|e| {
            log::error!(
                "Can't complete tar archive {}: {}",
                archive_path.display(),
                e
            );
            Error::ArchiveError
        })?;
        Ok(archive_path)
    }
}

impl super::Source<Error> for Source<'_> {
    fn init(&self) -> Result<(), Error> {
        utils::create_directory(self.parameters.get_directory()).map_err(Error::LibError)?;
        utils::create_directory(self.get_working_directory().as_path()).map_err(Error::LibError)?;
        utils::create_directory(self.get_images_directory().as_path()).map_err(Error::LibError)?;
        Ok(())
    }
    fn fetch(&self) -> Result<(), Error> {
        let client = reqwest::blocking::Client::new();
        let mut registries: std::vec::Vec<distribution::Distribution> = self
            .registries
            .iter()
            .map(|r| distribution::Distribution::new(&client, &r, self.parameters))
            .collect();
        for ((image, reference), platform) in itertools::iproduct!(&self.images, &self.platforms) {
            log::debug!("Fetch image {}:{}", image, reference);
            /* For every index, search one that have this image */
            for registry_api in registries.iter_mut() {
                /* First thing is to check for this image */
                match registry_api.image_manifest_exists(image, reference) {
                    Ok(b) => {
                        if b {
                            match self.pull_image(registry_api, image, reference, platform) {
                                Ok(manifest) => {
                                    log::debug!(
                                        "Image {} {} was pulled, save it in archive",
                                        image,
                                        reference
                                    );
                                    self.save_image_archive(image, reference, &manifest)?;
                                }
                                Err(e) => {
                                    log::info!("Can't pull image {} {}: {:?}", image, reference, e);
                                }
                            }
                        }
                    }
                    Err(e) => {
                        log::debug!("Can't check for image {}:{} {:?}", image, reference, e);
                    }
                }
            }
        }
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use std::io::Write;

    use sha2::Digest;

    use crate::lib::{utils, Source};

    use super::syntax;

    impl super::Parameters for std::path::PathBuf {
        fn get_directory(&self) -> &std::path::Path {
            self.as_path()
        }
    }

    #[test]
    fn save_image_archive() {
        let d = utils::test::get_temp_dir();
        let s = super::Source::new(&d).unwrap();
        s.init().unwrap();
        // Build a fake manifest
        // digest must be parsable
        let manifest = super::distribution::ManifestV2 {
            media_type: utils::test::get_text(),
            config: super::distribution::ManifestV2Layer {
                digest: "sha256:aabbcc".to_string(),
                size: 51,
            },
            layers: std::vec![
                super::distribution::ManifestV2Layer {
                    digest: "sha256:ddeeff".to_string(),
                    size: 27,
                },
                super::distribution::ManifestV2Layer {
                    digest: "sha256:gghhii".to_string(),
                    size: 1664,
                }
            ],
        };
        // Create fake files in working directory
        let m_content = utils::test::get_text();
        let l1_content = utils::test::get_text();
        let l2_content = utils::test::get_text();
        std::fs::File::create(
            s.get_working_directory()
                .join(manifest.config.digest().unwrap().hex),
        )
        .unwrap()
        .write(m_content.as_bytes())
        .unwrap();
        std::fs::File::create(
            s.get_working_directory()
                .join(manifest.layers[0].digest().unwrap().hex),
        )
        .unwrap()
        .write(l1_content.as_bytes())
        .unwrap();
        std::fs::File::create(
            s.get_working_directory()
                .join(manifest.layers[1].digest().unwrap().hex),
        )
        .unwrap()
        .write(l2_content.as_bytes())
        .unwrap();
        // Create also other blobs that does not exists
        std::fs::File::create(s.get_working_directory().join(utils::test::get_text()))
            .unwrap()
            .write(utils::test::get_text().as_bytes())
            .unwrap();
        std::fs::File::create(s.get_working_directory().join(utils::test::get_text()))
            .unwrap()
            .write(utils::test::get_text().as_bytes())
            .unwrap();
        /* Now save it */
        let a = s.save_image_archive("test", "1.0", &manifest).unwrap();
        assert!(a.exists());
        /* Check the content */
        let dec = flate2::read::GzDecoder::new(std::fs::File::open(a).unwrap());
        let mut r = tar::Archive::new(dec);
        let e = utils::test::get_temp_dir();
        r.unpack(e.as_path()).unwrap();
        /* Make sure each layer from the manifest exists, and nothing else */
        for e in std::fs::read_dir(e.as_path()).unwrap() {
            let f = e.unwrap();
            match f.file_name().as_os_str().to_str().unwrap() {
                "manifest.json" => {}
                "ddeeff" => {}
                "gghhii" => {}
                _ => {
                    panic!("Invalid element {}", f.file_name().to_str().unwrap());
                }
            }
        }
    }
    #[test]
    fn pull_layer() {
        let d = utils::test::get_temp_dir();
        let s = super::Source::new(&d).unwrap();
        let server = httpmock::MockServer::start();
        let server_url = url::Url::parse(&server.base_url()).unwrap();
        let client = reqwest::blocking::Client::new();
        s.init().unwrap();
        let mut registry = super::distribution::Distribution::new(&client, &server_url, &d);
        let image = "test";
        let layer_content = crate::lib::utils::test::get_text();
        let layer_sha256_b = sha2::Sha256::new_with_prefix(layer_content.as_bytes())
            .finalize()
            .into_iter()
            .collect::<std::vec::Vec<u8>>();
        let layer_sha256 = hex::encode(layer_sha256_b);
        let layer_digest_s = format!("sha256:{}", layer_sha256);
        let layer_digest = syntax::digest::Digest::parse(&layer_digest_s).unwrap();
        let mock_layer = server.mock(|when, then| {
            when.method("GET")
                .path(format!("/v2/{}/blobs/{}", image, &layer_sha256));
            then.status(200).body(&layer_content);
        });
        s.pull_layer(&mut registry, image, &layer_digest).unwrap();
        let layer_path = s.get_working_directory().join(layer_digest.hex);
        /* A file must have been created */
        assert!(layer_path.exists());
        assert_eq!(
            std::fs::read_to_string(layer_path.as_path()).unwrap(),
            layer_content
        );
        mock_layer.assert_hits(1);
        /* Do it again, nothing should be downloaded */
        s.pull_layer(&mut registry, image, &layer_digest).unwrap();
        mock_layer.assert_hits(1);
        /* Now modify the file, this should be downloaded again */
        let mut fd = std::fs::File::create(layer_path.as_path()).unwrap();
        fd.write_all(crate::lib::utils::test::get_text().as_bytes())
            .unwrap();
        s.pull_layer(&mut registry, image, &layer_digest).unwrap();
        mock_layer.assert_hits(2);
    }
}

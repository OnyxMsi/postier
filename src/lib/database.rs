/* This is about saving content during an update */
#[derive(Debug)]
pub enum Error {
    InvalidParameter,
    Persy,
    Corrupted,
}

pub fn init(path: &std::path::Path) -> Result<persy::Persy, Error> {
    if !path.exists() {
        log::debug!("Create database at {}", path.display());
        persy::Persy::create(path).map_err(|e| {
            log::error!("Can't create database at {}: {}", path.display(), e);
            Error::InvalidParameter
        })?;
    }
    log::debug!("Open database from {}", path.display());
    persy::Persy::open(path, persy::Config::new()).map_err(|e| {
        log::error!("Can't open database {}: {}", path.display(), e);
        Error::InvalidParameter
    })
}

/* Just a shortcut to delete */
fn internal_delete<D: DeserializeOwned + Serialize>(
    tx: &mut persy::Transaction,
    interface: &impl Interface<D>,
) -> Result<(), Error> {
    let (segment, key) = interface.get_segment_and_key();
    let hkey = crate::lib::utils::calculate_hash(&key);
    let values: std::vec::Vec<persy::PersyId> = tx
        .get::<u64, persy::PersyId>(segment, &hkey)
        .map_err(|e| {
            log::error!("Can't get index {} {} values: {}", segment, key, e);
            Error::Persy
        })?
        .collect();
    for value in values {
        /* Remove entry */
        tx.delete(segment, &value).map_err(|e| {
            log::error!("Can't remove value {}: {}", value, e);
            Error::Persy
        })?;
        /* Remove entry id from index */
        tx.remove::<u64, persy::PersyId>(segment, hkey, Some(value))
            .map_err(|e| {
                log::error!("Can't remove index {} key {}: {}", segment, key, e);
                Error::Persy
            })?;
    }
    Ok(())
}

fn get_segment_key_hkey<D: DeserializeOwned + Serialize>(
    interface: &impl Interface<D>,
) -> (&str, &str, u64) {
    let (s, k) = interface.get_segment_and_key();
    (s, k, crate::lib::utils::calculate_hash(&k))
}

use serde::de::DeserializeOwned;
use serde::Serialize;

pub fn has<D: DeserializeOwned + Serialize>(
    tx: &mut persy::Transaction,
    interface: &impl Interface<D>,
) -> bool {
    let (segment, key, hkey) = get_segment_key_hkey(interface);
    tx.exists_segment(segment).unwrap_or(false)
        && tx.exists_index(segment).unwrap_or(false)
        && tx
            .one::<u64, persy::PersyId>(segment, &hkey)
            .map(|r| r.is_some())
            .unwrap_or(false)
}
pub fn get<D: DeserializeOwned + Serialize>(
    tx: &mut persy::Transaction,
    interface: &impl Interface<D>,
) -> Result<Option<D>, Error> {
    let (segment, key, hkey) = get_segment_key_hkey(interface);
    log::debug!("Load {} from segment {}", key, segment);
    if !tx.exists_segment(segment).unwrap_or(false) {
        return Ok(None);
    }
    if !tx.exists_index(segment).unwrap_or(false) {
        return Ok(None);
    }
    /* Only the first entry matters */
    let d_id = match tx
        .get::<u64, persy::PersyId>(segment, &hkey)
        .map_err(|e| {
            log::error!("Can't get index {} {} values: {}", segment, key, e);
            Error::Persy
        })?
        .next()
    {
        Some(i) => i,
        None => return Ok(None),
    };
    let d = tx
        .read(segment, &d_id)
        .map_err(|e| {
            log::error!("Can't read {} {}: {}", segment, d_id, e);
            Error::Persy
        })?
        .ok_or_else(|| {
            log::error!("Database is corrupted, {} {} is not found", segment, d_id);
            Error::Corrupted
        })?;
    if !d.is_empty() {
        let s = String::from_utf8(d).map_err(|e| {
            log::error!(
                "Can't read {} {}: database is corrupted: {}",
                segment,
                key,
                e
            );
            Error::Corrupted
        })?;
        ron::from_str(&s).map(|f| Some(f)).map_err(|e| {
            log::error!(
                "Can't deserialize {} {}: database is corrupted: {}",
                segment,
                key,
                e
            );
            Error::Corrupted
        })
    } else {
        Ok(None)
    }
}
pub fn save<D: DeserializeOwned + Serialize, I: Interface<D>>(
    tx: &mut persy::Transaction,
    interface: &I,
    to_serialize: &D,
) -> Result<(), Error> {
    let (segment, key, hkey) = get_segment_key_hkey(interface);
    log::debug!("Save {} in segment {}", key, segment);
    /* If needed create segment and index */
    if !tx.exists_segment(segment).unwrap_or(false) {
        tx.create_segment(segment).map_err(|e| {
            log::error!("Can't create segment {} in database: {}", segment, e);
            Error::Persy
        })?;
    }
    if !tx.exists_index(segment).unwrap_or(false) {
        tx.create_index::<u64, persy::PersyId>(segment, persy::ValueMode::Replace)
            .map_err(|e| {
                log::error!("Can't create index {} in database: {}", segment, e);
                Error::Persy
            })?;
    }
    /* Generate bytes */
    let b = ron::to_string(to_serialize)
        .map_err(|e| {
            log::error!("Can't generate data for {} {}: {}", segment, key, e);
            Error::InvalidParameter
        })?
        .into_bytes();
    /* If current object exists in index, remove it */
    internal_delete::<D>(tx, interface)?;
    /* Insert a single entry */
    let nid = tx.insert(segment, &b).map_err(|e| {
        log::error!("Can't insert value in {}: {}", segment, e);
        Error::Persy
    })?;
    tx.put::<u64, persy::PersyId>(segment, hkey, nid)
        .map_err(|e| {
            log::error!("Can't set key {} in index {}: {}", key, segment, e);
            Error::Persy
        })
}
pub fn delete<D: DeserializeOwned + Serialize, I: Interface<D>>(
    tx: &mut persy::Transaction,
    interface: &I,
) -> Result<(), Error> {
    let (segment, key) = interface.get_segment_and_key();
    log::debug!("Delete {} from segment {}", key, segment);
    internal_delete::<D>(tx, interface)
}

pub trait Interface<D: Serialize + DeserializeOwned> {
    fn get_segment_and_key(&self) -> (&str, &str);
}

/* Just a wrapper */
pub struct Database {
    cache: usize,
    index: usize,
    p: persy::Persy,
    tx: Option<persy::Transaction>,
}

impl Database {
    pub fn init(path: &std::path::Path, cache: usize) -> Result<Self, Error> {
        Ok(Database {
            p: init(path)?,
            tx: None,
            cache,
            index: 0,
        })
    }
    fn begin(&mut self) -> Result<&mut persy::Transaction, Error> {
        if self.tx.is_none() {
            self.tx = Some(self.p.begin().map_err(|e| {
                log::error!("Can't create transaction: {}", e);
                Error::Persy
            })?);
            self.index = 0;
        }
        Ok(self.tx.as_mut().unwrap())
    }
    pub fn commit(&mut self) -> Result<(), Error> {
        if let Some(tx) = self.tx.take() {
            self.index = 0;
            tx.commit().map_err(|e| {
                log::error!("Can't commit transaction: {}", e);
                Error::Persy
            })?;
        };
        Ok(())
    }
    pub fn save<D: DeserializeOwned + Serialize>(
        &mut self,
        interface: &impl Interface<D>,
        to_serialize: &D,
        commit: bool,
    ) -> Result<(), Error> {
        self.begin()?;
        save(self.tx.as_mut().unwrap(), interface, to_serialize)?;
        self.index += 1;
        if commit || self.index > self.cache {
            self.commit()
        } else {
            Ok(())
        }
    }
    pub fn delete<D: DeserializeOwned + Serialize>(
        &mut self,
        interface: &impl Interface<D>,
        commit: bool,
    ) -> Result<(), Error> {
        self.begin()?;
        delete(self.tx.as_mut().unwrap(), interface)?;
        self.index += 1;
        if commit || self.index > self.cache {
            self.commit()
        } else {
            Ok(())
        }
    }
    pub fn has<D: DeserializeOwned + Serialize>(&mut self, interface: &impl Interface<D>) -> bool {
        if let Err(_e) = self.begin() {
            return false;
        };
        has(self.tx.as_mut().unwrap(), interface)
    }
    pub fn get<D: DeserializeOwned + Serialize>(
        &mut self,
        interface: &impl Interface<D>,
    ) -> Result<Option<D>, Error> {
        self.begin()?;
        get(self.tx.as_mut().unwrap(), interface)
    }
    pub fn close(&mut self) -> () {
        if self.tx.is_some() {
            self.commit();
        }
    }
}
impl Drop for Database {
    fn drop(&mut self) {
        self.close()
    }
}

#[cfg(test)]
pub mod test {
    use crate::lib::utils::calculate_hash;
    use crate::lib::utils::test::get_temp_dir;
    use serde::{Deserialize, Serialize};
    #[derive(Debug, Deserialize, Serialize)]
    struct Obj {
        i1: u8,
        i2: usize,
    }
    struct If {
        name: String,
    }
    impl If {
        fn new(name: &str) -> Self {
            If {
                name: String::from(name),
            }
        }
    }
    const IF_SEGMENT: &str = "if";
    const DEFAULT: u8 = 0;
    impl super::Interface<Obj> for If {
        fn get_segment_and_key(&self) -> (&str, &str) {
            (IF_SEGMENT, &self.name)
        }
    }
    #[test]
    fn init() {
        let impossible = std::env::temp_dir()
            .join("not")
            .join("existing")
            .join("yet");
        match super::init(&impossible) {
            Ok(_) => assert!(false),
            Err(_) => assert!(true),
        }
        let possible = get_temp_dir();
        super::init(&possible).unwrap();
        assert!(possible.exists());
        /* Same database can be opened twice*/
        super::init(&possible).unwrap();
    }
    #[test]
    fn has() {
        let dbp = get_temp_dir();
        let db = super::init(&dbp).unwrap();
        let name: &str = "Oo";
        let i = If::new(name);
        /* No segment yet */
        let mut tx = db.begin().unwrap();
        assert!(!super::has(&mut tx, &i));
        let mut tx = db.begin().unwrap();
        tx.create_segment(IF_SEGMENT).unwrap();
        tx.commit().unwrap();
        /* No index yet */
        let mut tx = db.begin().unwrap();
        assert!(!super::has(&mut tx, &i));
        let mut tx = db.begin().unwrap();
        tx.create_index::<u64, persy::PersyId>(IF_SEGMENT, persy::ValueMode::Replace)
            .unwrap();
        tx.commit().unwrap();
        /* No such item in database */
        let mut tx = db.begin().unwrap();
        assert!(!super::has(&mut tx, &i));
        let mut tx = db.begin().unwrap();
        let v: std::vec::Vec<u8> = std::vec!(51u8);
        let id = tx.insert(IF_SEGMENT, &v).unwrap();
        tx.commit().unwrap();
        /* No key in index*/
        let mut tx = db.begin().unwrap();
        assert!(!super::has(&mut tx, &i));
        let mut tx = db.begin().unwrap();
        tx.put::<u64, persy::PersyId>(IF_SEGMENT, calculate_hash(&i.name), id)
            .unwrap();
        tx.commit().unwrap();
        /* Found it */
        let mut tx = db.begin().unwrap();
        assert!(super::has(&mut tx, &i));
    }
    #[test]
    fn get() {
        let dbp = get_temp_dir();
        let name1: &str = "aA";
        let name2: &str = "iI";
        let i1 = If::new(name1);
        let i2 = If::new(name2);
        let db = super::init(&dbp).unwrap();
        let mut tx = db.begin().unwrap();
        assert!(super::get(&mut tx, &i1).unwrap().is_none());
        /* Initialize database */
        let mut tx = db.begin().unwrap();
        tx.create_segment(IF_SEGMENT).unwrap();
        tx.create_index::<u64, persy::PersyId>(IF_SEGMENT, persy::ValueMode::Replace)
            .unwrap();
        let b = ron::to_string(&Obj { i1: 51u8, i2: 12 })
            .unwrap()
            .into_bytes();
        let id = tx.insert(IF_SEGMENT, &b).unwrap();
        tx.put::<u64, persy::PersyId>(IF_SEGMENT, calculate_hash(&i1.name), id)
            .unwrap();
        tx.commit().unwrap();
        let mut tx = db.begin().unwrap();
        let o = super::get(&mut tx, &i1).unwrap().unwrap();
        assert_eq!(o.i1, 51u8);
        assert_eq!(o.i2, 12);
        let mut tx = db.begin().unwrap();
        assert!(super::get(&mut tx, &i2).unwrap().is_none());
    }
    #[test]
    fn delete() {
        let dbp = get_temp_dir();
        let name: &str = "yY";
        let i = If::new(name);
        let db = super::init(&dbp).unwrap();
        let mut tx = db.begin().unwrap();
        /* If database is not initialized, return error */
        super::delete(&mut tx, &i).unwrap_err();
        tx.commit().unwrap();
        /* Initialize database */
        let mut tx = db.begin().unwrap();
        tx.create_segment(IF_SEGMENT).unwrap();
        tx.create_index::<u64, persy::PersyId>(IF_SEGMENT, persy::ValueMode::Replace)
            .unwrap();
        let v: std::vec::Vec<u8> = std::vec!(51u8);
        let id = tx.insert(IF_SEGMENT, &v).unwrap();
        tx.put::<u64, persy::PersyId>(IF_SEGMENT, calculate_hash(&i.name), id)
            .unwrap();
        tx.commit().unwrap();
        /* Once deletion is done, make sure resources were deleted */
        let mut tx = db.begin().unwrap();
        super::delete(&mut tx, &i).unwrap();
        tx.commit().unwrap();
        let mut tx = db.begin().unwrap();
        assert!(tx.exists_index(IF_SEGMENT).unwrap());
        assert!(tx.exists_segment(IF_SEGMENT).unwrap());
        assert!(tx
            .one::<u64, persy::PersyId>(IF_SEGMENT, &calculate_hash(&i.name))
            .unwrap()
            .is_none());
        assert!(tx.read(IF_SEGMENT.to_string(), &id).unwrap().is_none());
        tx.commit().unwrap();
        /* Does not fail if nothing exists */
        let mut tx = db.begin().unwrap();
        super::delete(&mut tx, &i).unwrap();
        tx.commit().unwrap();
    }
    #[test]
    fn save() {
        let dbp = get_temp_dir();
        let name1: &str = "mM";
        let name2: &str = "nN";
        let i1 = If::new(name1);
        let i2 = If::new(name2);
        let i1k = calculate_hash(&name1);
        let i2k = calculate_hash(&name2);
        let o1 = Obj { i1: 102u8, i2: 0 };
        let o2 = Obj { i1: 51u8, i2: 0 };
        let db = super::init(&dbp).unwrap();
        /* First save will create segment and index */
        let mut tx = db.begin().unwrap();
        super::save(&mut tx, &i1, &o1).unwrap();
        tx.commit().unwrap();
        assert!(db.exists_index(IF_SEGMENT).unwrap());
        assert!(db.exists_segment(IF_SEGMENT).unwrap());
        let mut tx = db.begin().unwrap();
        let id = tx
            .one::<u64, persy::PersyId>(IF_SEGMENT, &i1k)
            .unwrap()
            .unwrap();
        let b = tx.read(IF_SEGMENT, &id).unwrap().unwrap();
        let value = ron::from_str::<Obj>(&String::from_utf8(b).unwrap()).unwrap();
        assert_eq!(value.i1, 102u8);
        let mut tx = db.begin().unwrap();
        super::save(&mut tx, &i1, &o2).unwrap();
        tx.commit().unwrap();
        let mut tx = db.begin().unwrap();
        let id = tx
            .one::<u64, persy::PersyId>(IF_SEGMENT, &i1k)
            .unwrap()
            .unwrap();
        let b = tx.read(IF_SEGMENT, &id).unwrap().unwrap();
        let value = ron::from_str::<Obj>(&String::from_utf8(b).unwrap()).unwrap();
        assert_eq!(value.i1, 51u8);
        let mut tx = db.begin().unwrap();
        super::save(&mut tx, &i2, &o2).unwrap();
        tx.commit().unwrap();
        let mut tx = db.begin().unwrap();
        let id = tx
            .one::<u64, persy::PersyId>(IF_SEGMENT, &i2k)
            .unwrap()
            .unwrap();
        let b = tx.read(IF_SEGMENT, &id).unwrap().unwrap();
        let value = ron::from_str::<Obj>(&String::from_utf8(b).unwrap()).unwrap();
        assert_eq!(value.i1, 51u8);
    }
    pub fn get_db(name: &str) -> super::Database {
        let dbp = get_temp_dir();
        super::Database::init(&dbp, 0).unwrap()
    }
}

pub mod fetcher;
pub mod format;
pub mod repository;
mod sig;
mod storage;
mod updater;
use self::format::{architecture, relationship};

use super::utils;
use crate::lib::Source as TSource;

#[derive(Debug)]
pub enum Error {
    LibError(super::Error),
    IndiceParseError,
    PolicyParseError,
    HttpError,
    InvalidContentError,
    InvalidSignatureError,
    DatabaseError(super::database::Error),
}

pub trait Parameters {
    fn get_directory(&self) -> &std::path::Path;
    /// A new dependency is to be solved
    fn solve_dependency_start(
        &self,
        relationship: &relationship::Relationship,
        architecture: &architecture::Architecture,
        dependencies_stack_size: usize,
    ) -> super::Control {
        super::Control::Continue
    }
    /// A dependency can not be solved
    /// Note that it does not mean an [`Error`] will be raised, just that this dependency has no
    /// solution in the current database.
    fn solve_dependency_failed(
        &self,
        relationship: &relationship::Relationship,
        architecture: &architecture::Architecture,
        dependencies_stack_size: usize,
    ) -> super::Control {
        super::Control::Continue
    }
    /// A dependency was successfuly solved and when the result was not already downloaded
    fn dependency_result_found(
        &self,
        relationship: &relationship::Relationship,
        architecture: &architecture::Architecture,
        url: &str,
        destination: &std::path::Path,
        dependencies_stack_size: usize,
    ) -> super::Control {
        super::Control::Continue
    }
    /// A dependency was already solved
    fn dependency_result_already_found(
        &self,
        relationship: &relationship::Relationship,
        architecture: &architecture::Architecture,
        url: &str,
        destination: &std::path::Path,
        dependencies_stack_size: usize,
    ) -> super::Control {
        super::Control::Continue
    }
    /// A new dependency was added to the current stack of dependencies to solve.
    fn push_dependency(&self, relationship: &str, architecture: &str) -> super::Control {
        super::Control::Continue
    }
    /// Start updating a repository on architectures
    fn update_repository_start(
        &self,
        repository: &repository::Repository,
        architectures: &[&str],
    ) -> super::Control {
        super::Control::Continue
    }
    /// A repository update failed
    fn update_repository_failed(
        &self,
        repository: &repository::Repository,
        error: Error,
    ) -> super::Control {
        super::Control::Continue
    }
    /// A repository was updated
    fn update_repository_done(
        &self,
        repository: &repository::Repository,
        architectures: &[&str],
    ) -> super::Control {
        super::Control::Continue
    }
    /// Start updating a repository release component on a specific architecture
    fn update_repository_release_start(
        &self,
        repository: &repository::Repository,
        architecture: &str,
        component: &str,
    ) -> super::Control {
        super::Control::Continue
    }
    /// A repository release component update failed
    fn update_repository_release_failed(
        &self,
        repository: &repository::Repository,
        error: &Error,
    ) -> super::Control {
        super::Control::Continue
    }
    /// A repository release component was updated
    fn update_repository_release_done(
        &self,
        repository: &repository::Repository,
        architecture: &str,
        component: &str,
    ) -> super::Control {
        super::Control::Continue
    }
    /// A package was found during the update of a repository
    fn update_repository_package_found(
        &self,
        package: &str,
        version: &str,
        architecture: &str,
        repository: &repository::Repository,
        component: &str,
    ) -> super::Control {
        super::Control::Continue
    }
}

pub struct Source<'s> {
    deb_repositories: Vec<repository::Repository>,
    packages: Vec<String>,
    architectures: Vec<String>,
    parameters: &'s dyn Parameters,
}

impl<'s> Source<'s> {
    pub fn new(parameters: &'s dyn Parameters) -> Result<Self, Error> {
        Ok(Source {
            architectures: Vec::new(),
            deb_repositories: Vec::new(),
            packages: Vec::new(),
            parameters,
        })
    }
    /// Add a new architecture to work on.
    /// It must be parsable as a [`format::architecture::Architecture`].
    /// The [`Source`] will now look for elements supporting this architecture.
    pub fn add_architecture(&mut self, architecture: &str) -> () {
        self.architectures.push(architecture.to_string())
    }
    /// Add a debian repository to look for elements
    pub fn add_debian_repository(
        &mut self,
        url: &str,
        distribution: &str,
        components: &std::vec::Vec<&str>,
        signed_by: Option<&str>,
    ) -> Result<(), Error> {
        log::debug!("Add new debian repository {} {}", url, distribution);
        let r = repository::Repository::new(url, distribution, components, signed_by)?;
        self.deb_repositories.push(r);
        Ok(())
    }
    /// Add a debian package to look for
    /// It must be parsable as a [`format::relationship::Relationship`].
    pub fn add_debian_package(&mut self, package: &str) -> Result<(), Error> {
        self.packages.push(String::from(package));
        Ok(())
    }
    fn get_database(&self) -> Result<super::database::Database, Error> {
        let p = self.parameters.get_directory().join("db.persy");
        super::database::Database::init(&p, 1000).map_err(Error::DatabaseError)
    }
}

impl TSource<Error> for Source<'_> {
    fn init(&self) -> Result<(), Error> {
        log::debug!("Initialize source");
        utils::create_directory(self.parameters.get_directory()).map_err(Error::LibError)?;
        log::info!("Initialized");
        Ok(())
    }
    fn update(&self) -> Result<(), Error> {
        log::debug!("Update source");
        let mut db = self.get_database()?;
        let mut updater = updater::Updater::new();
        for architecture in &self.architectures {
            updater.add_architecture(architecture);
        }
        for repository in &self.deb_repositories {
            updater.add_repository(&repository);
        }
        updater.run(&mut db, self.parameters)?;
        log::debug!("Source updated");
        Ok(())
    }
    fn fetch(&self) -> Result<(), Error> {
        log::debug!("Fetch source");
        let mut db = self.get_database()?;
        let fetcher_pool_dir = self.parameters.get_directory().join("pool");
        utils::create_directory(fetcher_pool_dir.as_path()).map_err(Error::LibError)?;
        let mut fetcher = fetcher::Fetcher::new(fetcher_pool_dir);
        /* Load configuration packages */
        let it = itertools::iproduct!(self.architectures.iter(), self.packages.iter());
        for (architecture, package) in it {
            fetcher.add_item(self.parameters, package, architecture);
        }
        /* Then solve this */
        fetcher.run(&mut db, self.parameters)?;
        log::debug!("Source fetched");
        Ok(())
    }
}

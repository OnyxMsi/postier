pub fn create_directory(path: &std::path::Path) -> Result<(), super::Error> {
    if !path.exists() {
        std::fs::create_dir(path).map_err(|e| {
            log::error!(
                "Can't create directory {}: {}",
                path.to_str().unwrap_or(""),
                e
            );
            super::Error::IOError(e)
        })?
    }
    Ok(())
}

pub fn blocking_fetch_url(
    request: reqwest::blocking::RequestBuilder,
    dest_path: &std::path::Path,
) -> Result<(), super::Error> {
    let mut res = reqwest_send(request)?.error_for_status().map_err(|e| {
        log::error!("Can't fetch {}: {}", e.url().unwrap(), e);
        super::Error::ReqwestError(e)
    })?;
    let mut buf = std::fs::File::create(dest_path).map_err(|e| {
        log::error!("Can't open {}: {}", dest_path.display(), e);
        super::Error::IOError(e)
    })?;
    /* Not efficient AT ALL */
    let url = res.url().clone();
    res.copy_to(&mut buf).map_err(|e| {
        log::error!(
            "Can't copy {} content into {}: {}",
            url.as_str(),
            dest_path.display(),
            e
        );
        super::Error::ReqwestError(e)
    })?;
    Ok(())
}

/* https://doc.rust-lang.org/std/hash/index.html */
pub fn calculate_hash<T: std::hash::Hash>(t: &T) -> u64 {
    let mut s = std::hash::DefaultHasher::new();
    t.hash(&mut s);
    use std::hash::Hasher;
    s.finish()
}

/* A very simple way to do a for loop on a file like content */
const CHUNCKSIZE: usize = 4096;
pub struct ChunckReader<T: std::io::Read> {
    fd: T,
}
impl<T: std::io::Read> ChunckReader<T> {
    fn new(fd: T) -> Self {
        ChunckReader { fd }
    }
}
impl<T: std::io::Read> Iterator for ChunckReader<T> {
    type Item = std::vec::Vec<u8>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut buf = [0; CHUNCKSIZE];
        let s = self.fd.read(&mut buf).ok()?;
        if s == 0 {
            None
        } else {
            Some(buf[0..s].to_vec())
        }
    }
}
fn stupid_hash<H, T>(reader: ChunckReader<T>) -> std::vec::Vec<u8>
where
    T: std::io::Read,
    H: digest::Digest,
    H::OutputSize: std::ops::Add,
{
    let mut h = H::new();
    for c in reader {
        h.update(&c);
    }
    h.finalize().into_iter().collect()
}
pub fn stupid_path_hash<H>(path: &std::path::Path) -> Result<std::vec::Vec<u8>, std::io::Error>
where
    H: digest::Digest,
    H::OutputSize: std::ops::Add,
{
    let f = std::fs::File::open(path)?;
    Ok(stupid_hash::<H, std::fs::File>(ChunckReader::new(f)))
}
pub(crate) fn check_file_and_hash<H>(path: &std::path::Path, hash: &[u8]) -> bool
where
    H: digest::Digest,
    H::OutputSize: std::ops::Add,
{
    match stupid_path_hash::<H>(path) {
        Ok(r) => r == hash,
        Err(_) => false,
    }
}
/// Check that the file hash matches the argument
/// Any error (like `path` does not exists or `hash64` is not a valid base64 string) will make the
/// function return false
pub(crate) fn check_file_and_hash64<H>(path: &std::path::Path, hash64: &str) -> bool
where
    H: digest::Digest,
    H::OutputSize: std::ops::Add,
{
    match base16ct::mixed::decode_vec(hash64) {
        Ok(i) => check_file_and_hash::<H>(path, &i),
        Err(_) => false,
    }
}

/* Make sure there is a trailling space at the end of the URL */
pub(crate) fn url_with_trailling_slash(u: &str) -> Result<url::Url, super::Error> {
    if u.ends_with("/") {
        url::Url::parse(u)
    } else {
        url::Url::parse(&format!("{}/", u))
    }
    .map_err(|e| {
        log::error!("Can't parse url {}: {}", u, e);
        super::Error::UrlError(e)
    })
}

/* Extract the last segment from an URL */
macro_rules! url_last_segment {
    ($url:expr) => {
        $url.path_segments().map(|p| p.last().unwrap())
    };
}
pub(crate) use url_last_segment;

pub(crate) fn reqwest_send(
    req: reqwest::blocking::RequestBuilder,
) -> Result<reqwest::blocking::Response, super::Error> {
    req.send().map_err(|e| {
        log::error!(
            "Request on {} failed: {}",
            e.url().map(|u| u.as_str()).unwrap_or(""),
            e
        );
        super::Error::ReqwestError(e)
    })
}

/// Just a stupid wrapper around [url::Url::join] to handle its errors
pub(crate) fn ujoin(u: &url::Url, suffix: &str) -> Result<url::Url, super::Error> {
    u.join(suffix).map_err(|e| {
        log::error!("Can't join {} to Url {}: {}", suffix, u, e);
        super::Error::UrlError(e)
    })
}

#[cfg(test)]
pub mod test {
    const TEXT: &str = r#"
Écoutez, comme ça à priori, ça m'évoque rien, mais honnêtement c'est possible.
Ce serait assez le genre de la maison, en tout cas ! De l'autre côté, voyez
un peu l'ironie, si j'avais pas fomenté une attaque par l'ouest, vous seriez
allés vous écraser contre les Romains à l'est ! Oui, alors, pourquoi ?
Pourquoi trahir sans arrêt les gens avec qui je collabore ?
Je dirais que c'est probablement une réponse compulsive à une crainte de
m'attacher.
Briser une relation plutôt que la cultiver pour ne pas se retrouver démuni
face au bonheur. Oui, pour répondre à votre question : j'ai peur d'aimer !
"#;
    static RAND_WORDS_INIT: std::sync::Once = std::sync::Once::new();
    static mut WORDS: std::vec::Vec<String> = std::vec::Vec::new();
    use rand::seq::SliceRandom;
    use rand::{thread_rng, Rng};
    fn rand_words_init() {
        unsafe {
            RAND_WORDS_INIT.call_once(|| {
                let re = regex::Regex::new(r"[.?,':!]+").unwrap();
                let text = re.replace_all(TEXT, "");
                /* TODO: Skip punctuation */
                WORDS = text.split_whitespace().map(|s| s.to_string()).collect();
            })
        }
    }
    pub fn get_words(min: usize, max: usize, separator: &str) -> String {
        rand_words_init();
        assert!(min < max);
        let mut rng = thread_rng();
        let count = rng.gen_range(min..max);
        unsafe {
            let w = WORDS
                .choose_multiple(&mut rng, count)
                .map(|s| s.to_string());
            w.collect::<std::vec::Vec<String>>().join(separator)
        }
    }
    pub fn get_text() -> String {
        get_words(3, 10, " ")
    }
    pub fn unique<V>(count: usize, f: fn() -> V) -> std::vec::Vec<V>
    where
        V: std::hash::Hash + std::cmp::Eq,
    {
        let mut set = std::collections::HashSet::new();
        while set.len() <= count {
            set.insert(f());
        }
        set.drain().collect()
    }
    pub fn get_temp_dir() -> std::path::PathBuf {
        loop {
            let n = format!("postier_tmp_{}", get_words(4, 8, "_"));
            let p = std::env::temp_dir().join(n);
            if !p.exists() {
                return p;
            }
        }
    }
    static LOGS_INIT: std::sync::Once = std::sync::Once::new();
    pub fn log_init() {
        LOGS_INIT.call_once(|| {
            stderrlog::new().quiet(false).verbosity(3).init().unwrap();
        })
    }
    #[test]
    fn test_create_directory() {
        let possible = get_temp_dir();
        /* Make sure this does not exists */
        if possible.exists() {
            std::fs::remove_dir_all(&possible).unwrap();
        }
        super::create_directory(&possible).unwrap();
        assert!(possible.exists());
        /* Again, this will do nothing */
        super::create_directory(&possible).unwrap();
    }
    #[test]
    fn test_blocking_client_fetch_url() {
        let client = reqwest::blocking::Client::new();
        let fetch_path = get_temp_dir();
        let not_found_path = get_temp_dir();
        let server = httpmock::MockServer::start();
        let fetch_uri_s = "/fetch";
        let not_found_uri_s = "/notfound";
        let fetch_url = server.url(fetch_uri_s);
        let not_found_url = server.url(not_found_uri_s);
        let content = get_text();
        let mock_fetch = server.mock(|when, then| {
            when.method("GET").path(fetch_uri_s);
            then.status(200).body(&content);
        });
        let mock_not_found = server.mock(|when, then| {
            when.method("GET").path(not_found_uri_s);
            then.status(404);
        });
        /* Try something that works */
        match super::blocking_fetch_url(client.get(fetch_url), &fetch_path) {
            Ok(_) => assert!(true),
            Err(_) => assert!(false),
        };
        mock_fetch.assert();
        let dest_content = std::fs::read_to_string(&fetch_path).unwrap();
        assert!(fetch_path.exists());
        assert_eq!(dest_content.as_str(), content);
        /* And now something that fail */
        match super::blocking_fetch_url(client.get(not_found_url), &not_found_path) {
            Ok(_) => assert!(false),
            Err(_) => assert!(true),
        };
        mock_not_found.assert();
        assert!(!not_found_path.exists());
    }

    #[test]
    fn chunck_reader() {
        let mut data: std::vec::Vec<u8> = std::vec::Vec::new();
        let mut res: std::vec::Vec<u8> = std::vec::Vec::new();
        let mut rng = thread_rng();
        let size = rng.gen_range(super::CHUNCKSIZE..(super::CHUNCKSIZE * 4));
        for _ in 1..size {
            data.push(rand::random::<u8>());
        }
        let ch = super::ChunckReader::new(std::io::Cursor::new(&data));
        for chunck in ch {
            res.extend(chunck);
        }
        assert_eq!(data.len(), res.len());
        assert_eq!(data, res);
    }
    #[test]
    fn stupid_path_hash() {
        let dir = get_temp_dir();
        std::fs::create_dir(dir.as_path()).unwrap();
        let p = dir.join("text");
        let mut fs = std::fs::File::create(p.as_path()).unwrap();
        use std::io::Write;
        fs.write_all(
            "\
Oui. Alors moi je pourrais vous dire que, si on cueille pas les cerises quand \
elles sont sur l'arbre, on fera tintin pour le clafouti. Mais on sera pas plus \
avancé."
                .as_bytes(),
        )
        .unwrap();
        let h = super::stupid_path_hash::<sha2::Sha256>(p.as_path()).unwrap();
        assert_eq!(
            base16ct::lower::encode_string(&h),
            "f97a4d981a687373d744c7bd18be70eaf2c3cfe31aa4ad30d585bd669dd79a83"
        );
    }
    #[test]
    fn url_with_trailling_slash() {
        assert_eq!(
            super::url_with_trailling_slash("http://www.perdu.com")
                .unwrap()
                .as_str(),
            "http://www.perdu.com/"
        );
        assert_eq!(
            super::url_with_trailling_slash("http://www.perdu.com/")
                .unwrap()
                .as_str(),
            "http://www.perdu.com/"
        );
    }
    #[test]
    fn check_file_and_hash64() {
        let dir = get_temp_dir();
        std::fs::create_dir(dir.as_path()).unwrap();
        let p = dir.join("text");
        let h = "a5202d87198c350909ffa36ec7635ef106f478e110cebec09e406fe923c742f8";
        let mut fs = std::fs::File::create(p.as_path()).unwrap();
        /* File do not exists */
        assert!(!super::check_file_and_hash64::<sha2::Sha256>(&p, h));
        use std::io::Write;
        /* Invalid content */
        fs.write_all(h.as_bytes()).unwrap();
        assert!(!super::check_file_and_hash64::<sha2::Sha256>(&p, h));
        let mut fs = std::fs::File::create(p.as_path()).unwrap();
        fs.write_all("Bon. Ca c'est bien gentil mais… À quel moment on trahit ?".as_bytes())
            .unwrap();
        assert!(super::check_file_and_hash64::<sha2::Sha256>(&p, h));
    }
    #[test]
    fn ujoin() {
        let u = url::Url::parse("http://www.perdu.com").unwrap();
        assert_eq!(
            super::ujoin(&u, "first").unwrap().as_str(),
            "http://www.perdu.com/first"
        );
    }
}

//! Core module of the postier project
pub mod lib {
    //! Provides various implementation of the [`Source`] trait, one for each interface to fetch results.
    pub mod cache;
    pub mod container;
    mod database;
    pub mod debian;
    pub mod npm;
    pub mod python;
    mod utils;

    #[derive(Debug)]
    pub enum Error {
        InvalidParametersError,
        ReqwestError(reqwest::Error),
        IOError(std::io::Error),
        UrlError(url::ParseError),
    }

    /// Main trait of the project
    /// A source is an interface between this module and a "pool" of object that may have
    /// relationship from one to another.
    ///
    /// One example can be the [Simple repository API](https://packaging.python.org/en/latest/specifications/simple-repository-api/) of Python
    ///
    /// Each source is an independant way of getting informations or data from one interface at
    /// a time
    ///
    /// Because sources will need to store data, they all requires a `repository` as a parameter.
    /// Note that each source has its own way of storing things
    pub trait Source<E> {
        /// Initialize things for this source
        fn init(&self) -> Result<(), E>;
        /// Update information for this source
        /// This is typically for `apt-get update` for Debian-like distribution
        /// This is to be run prior to [Source::fetch].
        /// Default implementation does nothing.
        fn update(&self) -> Result<(), E> {
            Ok(())
        }
        /// Fetch stuff from source
        fn fetch(&self) -> Result<(), E>;
    }

    pub enum Control {
        Continue,
    }
}

# Test package

This is a useless package to use in postier::python tests.

In order to create distribution files run:

```bash
# Best practice is to do this in a virtualenv
pip install build
python -m build
# You can find distributions in dist/
ls -l dist/
```
